<?php
define('OPEN_DEBUG_DEMO_IMPORT', 'OFF'); //ON: open debug mode
class bk_install_demo {
	
	var $menu_ids = array();// save ids of created menus
	var $menu_position = 1;// index of menu item in database, can not duplicate
	var $post_ids = array();
	var $menu_item_ids = array();

	
	// run when create / construct the class
	function __construct() {
	}
	
    function report($text, $br = '<br/>') {
		/*
		echo '<script type="text/javascript">jQuery("#bk-demo-sample-process-log").append("'.$text.$br.'");</script>';
		flush();
		 */
		$log = get_option('bk_demo_log', '');
		$log .= $text.$br;
		update_option('bk_demo_log', $log);
	}
    
	function log($text, $br = '<br/>') {
        if(OPEN_DEBUG_DEMO_IMPORT == 'ON') {
    		/*
    		echo '<script type="text/javascript">jQuery("#bk-demo-sample-process-log").append("'.$text.$br.'");</script>';
    		flush();
    		 */
    		$log = get_option('bk_demo_log', '');
    		$log .= $text.$br;
    		update_option('bk_demo_log', $log);
        }
	}
    function current_demo_activated($bk_current_demo){
        update_option('bk_current_demo', $bk_current_demo);
    }
	function update_progress_bar($percent) {
		/*
		echo '<script type="text/javascript">bk_install_demo_progressbar_step('.$percent.');</script>';
		flush();
 		*/
		update_option('bk_demo_percent', $percent);
	}
    
	function create_menu($menu_name){
		$current_menu = wp_get_nav_menu_object($menu_name);
		
		// delete old menu with the same name
        if($current_menu){
			wp_delete_nav_menu($menu_name);
		}
		// and create a new one
		$id = wp_create_nav_menu($menu_name);
		wp_parse_args(array(
			$menu_name => $id
		), $menu_ids);
		$this->log("Create a menu with NAME: ($menu_name) and ID: ($id)");
	}
	
	// assign created menu into a 
	function assign_menu($menu_name, $location) {
		$menu = wp_get_nav_menu_object($menu_name);
		if ($menu) {
			$menu_locations = get_theme_mod('nav_menu_locations');
			$menu_locations[$location] = $menu->term_id;
			set_theme_mod('nav_menu_locations', $menu_locations);
			$this->log("Assign menu: ($menu_name) to location: ($location)");
		}
	}
	
	// add item to menu
	function add_item_to_menu($menu_name = '', $item_args = null) {
		$item_args = wp_parse_args(array(
			'type'				=> '',
			'title'				=> '',
			'link'				=> '#',
			'item_slug'			=>	'',
			'post_slug'			=>	'',
			'parent_slug'		=>	'',
			'item_meta'			=>	array()
		), $item_args);
		
		// not serve if not provide title and slug for item
		if (empty($item_args['title']) || empty($item_args['item_slug'])) {
			return;
		}
		
		// get latest menu if not provide the name
		if (empty($menu_name)) {
			if (empty($this->menu_ids)) {
				return;
			}
			$menu_name = $this->menu_ids[count($this->menu_ids) - 1];
		}
		
		// not serve if provided menu name is not exist
		if (!isset($this->menu_ids[$menu_name])) {
			return;
		}
		
		$item_data =  array(
			'menu-item-position'	=> $this->menu_position,
			'menu-item-object'		=> '',
			'menu-item-type'		=> 'custom',
			'menu-item-title'		=> $item_args['title'],
			'menu-item-url'			=> $item_args['link'],
			'menu-item-status'		=> 'publish'
		);

		// update parent id
		if (!empty($item_args['parent_slug'])) {
			// can not add submenu if not valid parent
			if (!isset($this->menu_item_ids[$item_args['parent_slug']])) {
				return;
			} else {
				// already exist parent, update item data
				$item_data = wp_parse_args(array(
					'menu-item-parent-id' => (int)($this->menu_item_ids[$item_args['parent_slug']]),
				),$item_data);
			}
		}

		// update type for page
		if ($item_args['type'] === 'page') {
			if (!empty($item_args['post_slug']) && !isset($this->post_ids[$item_args['post_slug']])) {
				$item_data = wp_parse_args(array(
					'menu-item-object-id' => (int)($this->post_ids[$item_args['post_slug']]),
					'menu-item-object' => 'page',
					'menu-item-type'	  => 'post_type',
				),$item_data);
			}
		}
		
		$item_id = wp_update_nav_menu_item($this->menu_ids[$menu_name], 0, $item_data);
		$item_slug = $item_args['item_slug'];
		
		// update object if create successful
		if(!is_wp_error( $item_id )) {
			$this->log("Add Item ($title) to Menu ($menu_name)");
			$this->menu_item_ids = wp_parse_args(array(
				$item_slug => $item_id
			),$this->menu_item_ids);
			
			// update menu meta
			if (!empty($item_args['item_meta'])) {
				foreach ($item_args['item_meta'] as $key => $value) {
					if (!empty($key) && !empty($value)) {
						update_post_meta( $item_id, $key, $value );
					}
				}
			}
			
			$this->menu_position++;
		}
	}
	
	function remove_duplicate_menu_item($menu_name) {
		$items = wp_get_nav_menu_items( $menu_name );
		$prev_title = '';
		foreach ($items as $item) {
			if ($prev_title === '') {
				$prev_title = $item->title;
				continue;
			}
			if ($prev_title === $item->title) {
				wp_delete_post($item->ID, true);
			} else {
				$prev_title = $item->title;
			}
		}
	}
	function remove_menu_item($menu_name, $menu_item_title) {
		$items = wp_get_nav_menu_items( $menu_name );
		foreach ($items as $item) {
			
			if ( $item->title === $menu_item_title ) {
				wp_delete_post($item->ID, true);
				return;
			}
		}
	}
	function update_menu_item_meta($menu_name, $menu_item_title, $meta_key, $meta_value) {
		$items = wp_get_nav_menu_items( $menu_name );
		foreach ( $items as $item ) {
			if ( $item->title === $menu_item_title ) {
				update_post_meta( $item->ID, $meta_key, sanitize_key($meta_value) );
			}
		}
	}
	
	// search all $key in a file and replace with a text
	function replace_text_in_file($file_url, $search, $replace, $file_path) {
		// modify xml file
        global $wp_filesystem;        
		$file = file_get_contents($file_path);
		if ($file === false) {
			$this->log("Can not load ($file_url)");
			return false;
		}
		$file = str_replace($search, $replace, $file);
		
		if (file_put_contents($file_path, $file) === false) {
			$this->log("Can not write file ($file_url). Please make sure your hosting files are wriable.");
			return false;
		}
		
		return true;
	}
	
	// remove all old widgets from target sidebar 
	function remove_widgets_from_sidebar($sidebar_id) {
		$sidebars_widgets = get_option( 'sidebars_widgets' );
		
		if (isset($sidebars_widgets[$sidebar_id])) {
			//empty the default sidebar
			$sidebars_widgets[$sidebar_id] = array();
		} else {
			$sidebars_widgets = wp_parse_args($sidebars_widgets, array(
				$sidebar_id => array()
			));
		}
		update_option('sidebars_widgets', $sidebars_widgets);
	}

    /**
     * Available widgets
     *
     * Gather site's widgets into array with ID base, name, etc.
     * Used by export and import functions.
     *
     * @since 0.4
     * @global array $wp_registered_widget_updates
     * @return array Widget information
     */
    function wie_available_widgets() {
    
    	global $wp_registered_widget_controls;
    
    	$widget_controls = $wp_registered_widget_controls;
    
    	$available_widgets = array();
    
    	foreach ( $widget_controls as $widget ) {
    
    		if ( ! empty( $widget['id_base'] ) && ! isset( $available_widgets[$widget['id_base']] ) ) { // no dupes
    
    			$available_widgets[$widget['id_base']]['id_base'] = $widget['id_base'];
    			$available_widgets[$widget['id_base']]['name'] = $widget['name'];
    
    		}
    
    	}
    
    	return( $available_widgets );
    
    }    
	/**
     * Import widget JSON data
     *
     * @since 0.4
     * @global array $wp_registered_sidebars
     * @param object $data JSON widget data from .wie file
     * @return array Results array
     */
    function wie_import_data( $data ) {
        $demo = new bk_install_demo();
    	global $wp_registered_sidebars;  
    	// Get all available widgets site supports
    	$available_widgets = $this -> wie_available_widgets();

    	// Get all existing widget instances
    	$widget_instances = array();
    	foreach ( $available_widgets as $widget_data ) {
    		$widget_instances[$widget_data['id_base']] = get_option( 'widget_' . $widget_data['id_base'] );
    	}
    	// Loop import data's sidebars
    	foreach ( $data as $sidebar_id => $widgets ) {
    		// Skip inactive widgets
    		// (should not be in export file)
    		if ( 'wp_inactive_widgets' == $sidebar_id ) {
    			continue;
    		}
    
    		// Check if sidebar is available on this site
    		// Otherwise add widgets to inactive, and say so
    		if ( isset( $wp_registered_sidebars[$sidebar_id] ) ) {
    			$sidebar_available = true;
    			$use_sidebar_id = $sidebar_id;
    			$sidebar_message_type = 'success';
    			$sidebar_message = '';
    		} else {
    			$sidebar_available = false;
    			$use_sidebar_id = 'wp_inactive_widgets'; // add to inactive if sidebar does not exist in theme
    			$sidebar_message_type = 'error';
    			$sidebar_message = esc_html__( 'Sidebar does not exist in theme (using Inactive)', 'widget-importer-exporter' );
    		}
    
    		// Loop widgets
    		foreach ( $widgets as $widget_instance_id => $widget ) {
    
    			$fail = false;
    
    			// Get id_base (remove -# from end) and instance ID number
    			$id_base = preg_replace( '/-[0-9]+$/', '', $widget_instance_id );
    			$instance_id_number = str_replace( $id_base . '-', '', $widget_instance_id );
    
    			// Does site support this widget?
    			if ( ! $fail && ! isset( $available_widgets[$id_base] ) ) {
    				$fail = true;
    				$widget_message_type = 'error';
    				$widget_message = esc_html__( 'Site does not support widget', 'widget-importer-exporter' ); // explain why widget not imported
    			}
    
    			// Filter to modify settings object before conversion to array and import
    			// Leave this filter here for backwards compatibility with manipulating objects (before conversion to array below)
    			// Ideally the newer wie_widget_settings_array below will be used instead of this
    			$widget = apply_filters( 'wie_widget_settings', $widget ); // object
    
    			// Convert multidimensional objects to multidimensional arrays
    			// Some plugins like Jetpack Widget Visibility store settings as multidimensional arrays
    			// Without this, they are imported as objects and cause fatal error on Widgets page
    			// If this creates problems for plugins that do actually intend settings in objects then may need to consider other approach: https://wordpress.org/support/topic/problem-with-array-of-arrays
    			// It is probably much more likely that arrays are used than objects, however
    			$widget = json_decode( json_encode( $widget ), true );
    
    			// Filter to modify settings array
    			// This is preferred over the older wie_widget_settings filter above
    			// Do before identical check because changes may make it identical to end result (such as URL replacements)
    			$widget = apply_filters( 'wie_widget_settings_array', $widget );
    
    			// Does widget with identical settings already exist in same sidebar?
    			if ( ! $fail && isset( $widget_instances[$id_base] ) ) {
    
    				// Get existing widgets in this sidebar
    				$sidebars_widgets = get_option( 'sidebars_widgets' );
    				$sidebar_widgets = isset( $sidebars_widgets[$use_sidebar_id] ) ? $sidebars_widgets[$use_sidebar_id] : array(); // check Inactive if that's where will go
    
    				// Loop widgets with ID base
    				$single_widget_instances = ! empty( $widget_instances[$id_base] ) ? $widget_instances[$id_base] : array();
    				foreach ( $single_widget_instances as $check_id => $check_widget ) {
    
    					// Is widget in same sidebar and has identical settings?
    					if ( in_array( "$id_base-$check_id", $sidebar_widgets ) && (array) $widget == $check_widget ) {
    
    						$fail = true;
    						$widget_message_type = 'warning';
    						$widget_message = esc_html__( 'Widget already exists', 'widget-importer-exporter' ); // explain why widget not imported
    
    						break;
    
    					}
    
    				}
    
    			}
    
    			// No failure
    			if ( ! $fail ) {
    
    				// Add widget instance
    				$single_widget_instances = get_option( 'widget_' . $id_base ); // all instances for that widget ID base, get fresh every time
    				$single_widget_instances = ! empty( $single_widget_instances ) ? $single_widget_instances : array( '_multiwidget' => 1 ); // start fresh if have to
    				$single_widget_instances[] = $widget; // add it
    
    					// Get the key it was given
    					end( $single_widget_instances );
    					$new_instance_id_number = key( $single_widget_instances );
    
    					// If key is 0, make it 1
    					// When 0, an issue can occur where adding a widget causes data from other widget to load, and the widget doesn't stick (reload wipes it)
    					if ( '0' === strval( $new_instance_id_number ) ) {
    						$new_instance_id_number = 1;
    						$single_widget_instances[$new_instance_id_number] = $single_widget_instances[0];
    						unset( $single_widget_instances[0] );
    					}
    
    					// Move _multiwidget to end of array for uniformity
    					if ( isset( $single_widget_instances['_multiwidget'] ) ) {
    						$multiwidget = $single_widget_instances['_multiwidget'];
    						unset( $single_widget_instances['_multiwidget'] );
    						$single_widget_instances['_multiwidget'] = $multiwidget;
    					}
    
    					// Update option with new widget
    					update_option( 'widget_' . $id_base, $single_widget_instances );
    
    				// Assign widget instance to sidebar
    				$sidebars_widgets = get_option( 'sidebars_widgets' ); // which sidebars have which widgets, get fresh every time
    				$new_instance_id = $id_base . '-' . $new_instance_id_number; // use ID number from new widget instance
    				$sidebars_widgets[$use_sidebar_id][] = $new_instance_id; // add new instance to sidebar
    				update_option( 'sidebars_widgets', $sidebars_widgets ); // save the amended data
    
    				// Success message
    				if ( $sidebar_available ) {
    					$widget_message_type = 'success';
    					$widget_message = esc_html__( 'Imported', 'widget-importer-exporter' );
    				} else {
    					$widget_message_type = 'warning';
    					$widget_message = esc_html__( 'Imported to Inactive', 'widget-importer-exporter' );
    				}
    
    			}
    
    		}
    
    	}
    	return '';
    
    }

}