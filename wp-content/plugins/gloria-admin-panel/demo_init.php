<?php
require_once (GLORIA_AD_PLUGIN_DIR.'demo_class.php');

function bk_install_demo_data() {
    if (!isset($_GET['frame'])) : ?>
    <script type="text/javascript">
    var current_percent = 0.1;
    var duration_counter = 0;
    var target_percent = 0;
    var current_demo = "<?php echo $_GET['act']; ?>";
    // make progress bar
    var timer = setInterval(function(){
    	jQuery('#bk-demo-sample-process-bar').css('width', target_percent + '%');
    	
    	if (target_percent >= 100) {
    		jQuery('#bk-demo-sample-process-message').html('<h3>CONGRATULATION! THE DEMO IS LIVE NOW.</h3>');
            jQuery('#bk-demo-notice').remove();
            jQuery('.bk-'+current_demo).find('.install-status').html('Installed');
    		clearInterval(timer);
    	}
    	if (target_percent == -1) {
    		clearInterval(timer);
    	}
        
        before = new Date(); 
        
    }, 1);
    
    function bk_install_demo_progressbar_step() {
    	// ajax loop query
    	jQuery.post(ajaxurl, { 
    		'action': 'bk_install_demo_ajax'
    	}).done(function( data ) {
    		if (data) {
    			var data_arr = data.split('#####');
    			if (data_arr.length < 2) {
    				alert('Wrong polling data when installing demo');
    				target_percent = -1;
    				return;
    			}
    			var percent = data_arr[0];
    			var log = data_arr[1].split('+++++')[0];
    			if (!isNaN(percent)) {
    				target_percent = Number(percent);
    				if (target_percent != -1 && target_percent != 100) {
    					bk_install_demo_progressbar_step();
    				}
    				jQuery("#bk-demo-sample-process-log").html(log);
    			} else {
    				alert('Unexpected polling data when installing demo');
    				target_percent = -1;
    				return;
    			}
    		}
    	});
    	//target_percent = step_to_percent;
    }
    // reset progess before load
    jQuery.post(ajaxurl, { 
    	'action': 'bk_install_demo_ajax', 
    	'reset': 'true'
    }).done(function( data ) {
    	bk_install_demo_progressbar_step();
    	// run script by iframe, the iframe will update option for us quering in ajax
    	jQuery('<iframe style="opacity:0;width:0px;height:0px;" src="<?php echo admin_url('admin.php?page=bk-theme-demos&act='.$_GET['act'].'&task=import&frame=true');?>"/>').appendTo('body');
    
    }).fail(function() {
    	alert('Can not connect to server to start installing demo');
    });
    
    </script>
    
    <?php
    	else:
    		// loading importers
    		if ( !defined('WP_LOAD_IMPORTERS') ) {
    			define('WP_LOAD_IMPORTERS', true); 
    		}
    
    		// if main importer class doesn't exist, include if from core
    		if ( ! class_exists( 'WP_Importer' ) ) { 
    			$wp_importer = ABSPATH . 'wp-admin/includes/class-wp-importer.php';
    			include $wp_importer;
    		}
    
    		// if WP importer doesn't exist, include it from theme
    		if ( ! class_exists('WP_Import') ) { 
    			require_once (GLORIA_AD_PLUGIN_DIR.'plugins/wordpress-importer/wordpress-importer.php');
    		}
    		// check for main import class and wp import class to start
    		if ( class_exists( 'WP_Importer' ) && class_exists( 'WP_Import' ) ) { 
		    	$demo = new bk_install_demo();
    			$demo_dir = GLORIA_AD_PLUGIN_DIR;
    			$demo_data_dir = $demo_dir.'data/';
    			$demo_up_url = GLORIA_AD_PLUGIN_URL.'uploads/';
                $demo_data_url = GLORIA_AD_PLUGIN_URL.'data/';
      
    			// extract
    			require_once(ABSPATH .'/wp-admin/includes/file.php');
    
    			global $wp_filesystem;
    			if ( ! $filesystem ) {
    				WP_Filesystem();
    				if ( ! $wp_filesystem ) {
    					$demo->log('Can not init file system');
    					$demo->update_progress_bar(-1);
    					return;
    				}
    			}
    
    			if (is_wp_error(unzip_file($demo_data_dir.'data.zip', $demo_data_dir))) {
    				$demo->log('Can not extract data file at: '.$demo_data_dir.'data.zip');
    				$demo->update_progress_bar(-1);
    				return;
    			}
    			// modify media and domain in xml files
    			//$demo->report('Loading and Modifying XML files ...');
    			$modify_texts = array(
                    '<guid isPermaLink="false">http://localhost/gloria/wp-content/uploads/' => '<guid isPermaLink="false">'.$demo_up_url,
                    '<wp:attachment_url>http://localhost/gloria/wp-content/uploads/' => '<wp:attachment_url>'.$demo_up_url,
    				'http://localhost/gloria/' => home_url('/')
    			);
                
                // Delete Hello World Post
                $hello_world = get_page_by_title("Hello world!", OBJECT, 'post');
                if ( ! empty ( $hello_world ) ) {
                    wp_delete_post( $hello_world->ID );
                }
    
    			$modify_files = array(
                    'main.xml',
                    'sport.xml',
                    'sport2.xml',
                    'art.xml',
                    'tech.xml',
                    'tech2.xml',
                    'others.xml',
                    'navigation_1.xml',
                    'navigation_2.xml',
                    'navigation_3.xml',
                    'design_1.xml',
                    'design_2.xml',
                    'design_3.xml',
                    'design_4.xml',
                    'design_5.xml',
                    'design_6.xml',
                    'design_7.xml',
                    'design_8.xml',
                    'fashion_1.xml',
                    'fashion_2.xml',
                    'fashion_3.xml',
                    'fashion_4.xml',
                    'fashion_5.xml',
                    'fashion_6.xml',
                    'fashion_7.xml',
                    'fashion_8.xml',
                    'fashion_9.xml',
                    'fashion_10.xml',
                    'fashion_11.xml',
                    'food_1.xml',
                    'food_2.xml',
                    'food_3.xml',
                    'food_4.xml',
                    'food_5.xml',
                    'food_6.xml',
                    'food_7.xml',
                    'food_8.xml',
    			);
    
    			foreach ($modify_texts as $key => $value) {
    				foreach ($modify_files as $file_name) {
    					if ($demo->replace_text_in_file($demo_data_url.$file_name,$key,$value, $demo_data_dir.$file_name) == FALSE) {
    						$demo->update_progress_bar(-1);
    						return;
    					}
    				}
    			}
    // Declare importer
    			$importer = new WP_Import();
    			$importer->fetch_attachments = true;
                //$options_link = 'http://sport-gloria.bk-ninja.com/wp-admin/admin-ajax.php?action=redux_download_options-bk_option&secret=1bac7e42bbf845960ff75e4eebeb7e2f';
                //$import_data = wp_remote_retrieve_body( wp_remote_get( $options_link ) );
                if($_GET['act'] == 'main-demo') {
                    $import_data = file_get_contents( $demo_data_dir.'main_options.json' );
                }elseif($_GET['act'] == 'sport-demo') {
                    $import_data = file_get_contents( $demo_data_dir.'sport_options.json' );
                }elseif($_GET['act'] == 'sport2-demo') {
                    $import_data = file_get_contents( $demo_data_dir.'sport2_options.json' );
                }elseif($_GET['act'] == 'art-demo') {
                    $import_data = file_get_contents( $demo_data_dir.'art_options.json' );
                }elseif($_GET['act'] == 'tech-demo') {
                    $import_data = file_get_contents( $demo_data_dir.'tech_options.json' );
                }elseif($_GET['act'] == 'tech2-demo') {
                    $import_data = file_get_contents( $demo_data_dir.'tech2_options.json' );
                }
                if ( ! empty ( $import_data ) ) {
                    $imported_options = json_decode( $import_data, true );
                }
                ob_start();            
    			//$demo->report('Importing posts/pages/comments/media ... This will take about 3-5 minutes. If any issue occurs, please reload and click import button to continue the process');
                
                $gloria_option_original = get_option( 'gloria_option' );
                
                $redux_options = wp_parse_args( $imported_options, $gloria_option );
                
    /**Import and Validate Options **/
                $retry = 5;
                $retry_cnt = 0;
                for ($retry_cnt = 0; $retry_cnt < $retry; $retry_cnt ++) {
                    $gloria_option = get_option( 'gloria_option' );
                    if ($redux_options != $gloria_option) {
                        update_option( 'gloria_option', $redux_options );
                    }else {
                        $retry_cnt = 10;
                    }
                }
                if($retry_cnt != 11 ) {
                    $demo->report("Import Theme Options Failed. Please follow the document to manual import Theme Options.");
                }    
            //$demo->report('Importing Navigation');
    DEL_TOP_MENU:
    
    			// delete menus before import
                $retry = 5;
                $retry_cnt = 0;
                for ($retry_cnt = 0; $retry_cnt < $retry; $retry_cnt ++) {
                    wp_delete_nav_menu('Top Menu');
                }
                $demo->update_progress_bar(3);
    DEL_MAIN_MENU:
                for ($retry_cnt = 0; $retry_cnt < $retry; $retry_cnt ++) {
                    wp_delete_nav_menu('Main Menu');
                }
                $demo->update_progress_bar(5);
    DEL_FOOTER_MENU:
                for ($retry_cnt = 0; $retry_cnt < $retry; $retry_cnt ++) {
                    wp_delete_nav_menu('Footer Menu');
                }
                $demo->update_progress_bar(7);
                
    NAV:
    			// import xml file
                $importer->import($demo_data_dir.'navigation_1.xml');
                $demo->update_progress_bar(10);
                $importer->import($demo_data_dir.'navigation_2.xml');
                $demo->update_progress_bar(13);
                $importer->import($demo_data_dir.'navigation_3.xml');
                $demo->update_progress_bar(15);                
                //$demo->report('Importing Pages');  
                            
                if($_GET['act'] == 'main-demo') {
                    
                    $importer->import($demo_data_dir.'main.xml');
                    
                }elseif($_GET['act'] == 'sport-demo') {
                    
                    $importer->import($demo_data_dir.'sport.xml');
                    
                }elseif($_GET['act'] == 'sport2-demo') {
                    
                    $importer->import($demo_data_dir.'sport2.xml');
                    
                }elseif($_GET['act'] == 'art-demo') {
                    
                    $importer->import($demo_data_dir.'art.xml');
                    
                }elseif($_GET['act'] == 'tech-demo') {
                    
                    $importer->import($demo_data_dir.'tech.xml');
                    
                }elseif($_GET['act'] == 'tech2-demo') {
                    
                    $importer->import($demo_data_dir.'tech2.xml');
                    
                }
    /*
    IMPORT_HOME2:
                $demo->update_progress_bar(10);
    
                $importer->import($demo_data_dir.'home2.xml');
                
    IMPORT_HOME3:
                $demo->update_progress_bar(20);
                
                $importer->import($demo_data_dir.'home3.xml');
                
    IMPORT_HOME4:
                $demo->update_progress_bar(30);
                
                $importer->import($demo_data_dir.'home4.xml');
                
    IMPORT_HOME5:
                $demo->update_progress_bar(40);
                
                $importer->import($demo_data_dir.'home5.xml');
    */
                
    OTHER_PAGES:
                $demo->update_progress_bar(20);
                
                $importer->import($demo_data_dir.'others.xml');
                
                //$demo->report('Importing Posts and Media Files'); 
    CAT1_POSTS:
                
    			$importer->import($demo_data_dir.'design_1.xml');
                $demo->update_progress_bar(23);
                $importer->import($demo_data_dir.'design_2.xml');
                $demo->update_progress_bar(26);
                $importer->import($demo_data_dir.'design_3.xml');
                $demo->update_progress_bar(30);
                $importer->import($demo_data_dir.'design_4.xml');
                $demo->update_progress_bar(33);
                $importer->import($demo_data_dir.'design_5.xml');
                $demo->update_progress_bar(36);
                $importer->import($demo_data_dir.'design_6.xml');
                $demo->update_progress_bar(40);
                $importer->import($demo_data_dir.'design_7.xml');
                $demo->update_progress_bar(43);
                $importer->import($demo_data_dir.'design_8.xml');
                
    CAT2_POSTS:
                $demo->update_progress_bar(46);
    
                $importer->import($demo_data_dir.'fashion_1.xml');
                $demo->update_progress_bar(48);
                $importer->import($demo_data_dir.'fashion_2.xml');
                $demo->update_progress_bar(50);
                $importer->import($demo_data_dir.'fashion_3.xml');
                $demo->update_progress_bar(52);
                $importer->import($demo_data_dir.'fashion_4.xml');
                $demo->update_progress_bar(54);
                $importer->import($demo_data_dir.'fashion_5.xml');
                $demo->update_progress_bar(56);
                $importer->import($demo_data_dir.'fashion_6.xml');
                $demo->update_progress_bar(58);
                $importer->import($demo_data_dir.'fashion_7.xml');
                $demo->update_progress_bar(60);
                $importer->import($demo_data_dir.'fashion_8.xml');
                $demo->update_progress_bar(62);
                $importer->import($demo_data_dir.'fashion_9.xml');
                $demo->update_progress_bar(64);
                $importer->import($demo_data_dir.'fashion_10.xml');
                $demo->update_progress_bar(66);
                $importer->import($demo_data_dir.'fashion_11.xml');
                
    CAT3_POSTS:
                $demo->update_progress_bar(70);
                $importer->import($demo_data_dir.'food_1.xml');
                $demo->update_progress_bar(73);
                $importer->import($demo_data_dir.'food_2.xml');
                $demo->update_progress_bar(76);
                $importer->import($demo_data_dir.'food_3.xml');
                $demo->update_progress_bar(80);
                $importer->import($demo_data_dir.'food_4.xml');
                $demo->update_progress_bar(83);
                $importer->import($demo_data_dir.'food_5.xml');
                $demo->update_progress_bar(86);
                $importer->import($demo_data_dir.'food_6.xml');
                $demo->update_progress_bar(90);
                $importer->import($demo_data_dir.'food_7.xml');
                $demo->update_progress_bar(93);
                $importer->import($demo_data_dir.'food_8.xml');
                
                $demo->update_progress_bar(96);
                
                ob_end_clean();
    
    			// modify menu and set up menu location
    			$demo->log('Configuring Theme Customizations!! ...');
    			$demo->remove_duplicate_menu_item('menu-top');
    			$demo->remove_duplicate_menu_item('main-menu');
    			$demo->remove_duplicate_menu_item('menu-footer');
    			
    			$demo->update_menu_item_meta('main-menu', 'Fashion', '_menu_item_bkmegamenu', 1);
                $demo->update_menu_item_meta('main-menu', 'Design', '_menu_item_bkmegamenu', 1);
                $demo->update_menu_item_meta('main-menu', 'Food', '_menu_item_bkmegamenu', 1);
    
    			$demo->assign_menu('Top Menu', 'menu-top');
    			$demo->assign_menu('Main Menu', 'main-menu');
    			$demo->assign_menu('Footer Menu', 'menu-footer');
    
    			// set theme logo
    			set_theme_mod('blog_logo', $demo_up_url.'2014/05/logo-bold.png');
    			set_theme_mod('enable_box_layout', 'on');
    			set_theme_mod('body_background_image', $demo_up_url.'2014/05/pattern.png');
    			set_theme_mod('enable_lightbox', 'on');
    			set_theme_mod('float_main_menu', 'on');
    			set_theme_mod('body_font_family', 'font-61');
    
    			// sidebars
    			$demo->remove_widgets_from_sidebar('wp_inactive_widgets');
                $demo->remove_widgets_from_sidebar('home_sidebar1');
                $demo->remove_widgets_from_sidebar('home_sidebar2');
                $demo->remove_widgets_from_sidebar('page_sidebar');
                $demo->remove_widgets_from_sidebar('single_sidebar');
                $demo->remove_widgets_from_sidebar('default_template_sidebar');
                $demo->remove_widgets_from_sidebar('home_sidebar');
                $demo->remove_widgets_from_sidebar('footer_sidebar_1');
                $demo->remove_widgets_from_sidebar('footer_sidebar_2');
                $demo->remove_widgets_from_sidebar('footer_sidebar_3');
    
    			// get widget define, exported by "http://wordpress.org/plugins/widget-settings-importexport/" 
    			//$widget_json = wp_remote_get(GLORIA_AD_PLUGIN_URL.'data/widget.wie');
                //$demo->log(GLORIA_AD_PLUGIN_DIR.'data/widget.wie');
                $widget_json = file_get_contents( $demo_data_dir.'widget.wie' );
                $widget_json = json_decode( $widget_json );
    			// add widgets to sidebars
    			$demo->wie_import_data($widget_json);
                
                // Setup Static Front Page
                if($_GET['act'] == 'main-demo') {
                    $page = get_page_by_title( 'Home Page' );
                }elseif($_GET['act'] == 'sport-demo') {
                    $page = get_page_by_title( 'Home Sport' );
                }elseif($_GET['act'] == 'sport2-demo') {
                    $page = get_page_by_title( 'Home Sport2' );
                }elseif($_GET['act'] == 'art-demo') {
                    $page = get_page_by_title( 'Home Art' );
                }elseif($_GET['act'] == 'tech-demo') {
                    $page = get_page_by_title( 'Home Tech' );
                }elseif($_GET['act'] == 'tech2-demo') {
                    $page = get_page_by_title( 'Home Tech2' );
                }
                
                update_option( 'show_on_front', 'page');
                update_option( 'page_on_front', $page->ID );
                
                /** Delete Import Files **/
                $demo->log("Delete Import Files");
                
                unlink($demo_data_dir.'main.xml');
                
                unlink($demo_data_dir.'sport.xml');
                
                unlink($demo_data_dir.'sport2.xml');
                
                unlink($demo_data_dir.'art.xml');
                
                unlink($demo_data_dir.'tech.xml');
                
                unlink($demo_data_dir.'tech2.xml');
                
                unlink($demo_data_dir.'others.xml');
                
    			unlink($demo_data_dir.'design_1.xml');
                
                unlink($demo_data_dir.'design_2.xml');
                
                unlink($demo_data_dir.'design_3.xml');
                
                unlink($demo_data_dir.'design_4.xml');
                
                unlink($demo_data_dir.'design_5.xml');
                
                unlink($demo_data_dir.'design_6.xml');
                
                unlink($demo_data_dir.'design_7.xml');
                
                unlink($demo_data_dir.'design_8.xml');
    
                unlink($demo_data_dir.'fashion_1.xml');
                
                unlink($demo_data_dir.'fashion_2.xml');
                
                unlink($demo_data_dir.'fashion_3.xml');
                
                unlink($demo_data_dir.'fashion_4.xml');
                
                unlink($demo_data_dir.'fashion_5.xml');
                
                unlink($demo_data_dir.'fashion_6.xml');
                
                unlink($demo_data_dir.'fashion_7.xml');
                
                unlink($demo_data_dir.'fashion_8.xml');
                
                unlink($demo_data_dir.'fashion_9.xml');
                
                unlink($demo_data_dir.'fashion_10.xml');
                
                unlink($demo_data_dir.'fashion_11.xml');
                
                unlink($demo_data_dir.'food_1.xml');
                
                unlink($demo_data_dir.'food_2.xml');
                
                unlink($demo_data_dir.'food_3.xml');
                
                unlink($demo_data_dir.'food_4.xml');
                
                unlink($demo_data_dir.'food_5.xml');
                
                unlink($demo_data_dir.'food_6.xml');
                
                unlink($demo_data_dir.'food_7.xml');
                
                unlink($demo_data_dir.'food_8.xml');
                
                unlink($demo_data_dir.'navigation_1.xml');
                
                unlink($demo_data_dir.'navigation_2.xml');
    
                unlink($demo_data_dir.'navigation_3.xml');
                
                unlink($demo_data_dir.'widget.wie');
                
                unlink($demo_data_dir.'main_options.json');
                
                unlink($demo_data_dir.'sport_options.json');
                
                unlink($demo_data_dir.'sport2_options.json');
                
                unlink($demo_data_dir.'art_options.json');
                
                unlink($demo_data_dir.'tech_options.json');
                
                unlink($demo_data_dir.'tech2_options.json');
    
                //$demo->report('Done - Thank you for your patience');
                
                $demo->update_progress_bar(100);
                
                $demo->current_demo_activated($_GET['act']);
    		}
    endif;
}