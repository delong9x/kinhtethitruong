<?php
    /*
    Plugin Name: Gloria Admin Panel
    Plugin URI: http://bk-ninja.com
    Description: Plugin used Gloria Admin Panel
    Author: BKNinja
    Version: 1.0
    Author URI: http://bk-ninja.com
    */
?>
<?php
define( 'GLORIA_AD_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'GLORIA_AD_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

require_once (GLORIA_AD_PLUGIN_DIR.'templates/welcome_template.php');
require_once (GLORIA_AD_PLUGIN_DIR.'templates/plugins_template.php');
require_once (GLORIA_AD_PLUGIN_DIR.'templates/status_report.php');
require_once (GLORIA_AD_PLUGIN_DIR.'demo_init.php');

function bk_theme_demo_html() {
	global $theme_options_tabs;
	$act = '';
	if (isset($_GET['act'])) {
		$act = $_GET['act'];
	}
    $bk_current_demo = get_option('bk_current_demo', '');
    $bk_theme_demos = array (
        array (
             'class' => 'bk-main-demo',
             'title' => 'Default Demo',
             'img'   => GLORIA_AD_PLUGIN_URL . 'demo_type_imgs/default-demo.jpg',
             'act'  => 'main-demo',
        ),
        array (
             'class' => 'bk-sport-demo',
             'title' => 'Sport Demo',
             'img' => GLORIA_AD_PLUGIN_URL . 'demo_type_imgs/sport-demo.jpg',
             'act'  => 'sport-demo',
        ),
        array (
             'class' => 'bk-sport2-demo',
             'title' => 'Sport 2 Demo',
             'img' => GLORIA_AD_PLUGIN_URL . 'demo_type_imgs/sport2-demo.jpg',
             'act'  => 'sport2-demo',
        ),
        array (
             'class' => 'bk-art-demo',
             'title' => 'Art Demo',
             'img' => GLORIA_AD_PLUGIN_URL . 'demo_type_imgs/art-demo.jpg',
             'act'  => 'art-demo',
        ),
        array (
             'class' => 'bk-tech-demo',
             'title' => 'Techology Demo',
             'img' => GLORIA_AD_PLUGIN_URL . 'demo_type_imgs/tech-demo.jpg',
             'act'  => 'tech-demo',
        ),
        array (
             'class' => 'bk-tech2-demo',
             'title' => 'Techology 2 Demo',
             'img' => GLORIA_AD_PLUGIN_URL . 'demo_type_imgs/tech2-demo.jpg',
             'act'  => 'tech2-demo',
        ),
    );
?>
<br />
<div class="page-wrap" style="margin: 20px 30px 0 2px;">
    <div class="nav-tab-wrapper">
        <a href="admin.php?page=bk-theme-welcome" class="nav-tab">Welcome</a>
        <a href="admin.php?page=bk-theme-plugins" class="nav-tab">Plugins</a>
        <a href="admin.php?page=bk-theme-demos" class="nav-tab nav-tab-active">Install demos</a>
        <a href="admin.php?page=bk-system-status" class="nav-tab">System status</a>
        <?php if ( class_exists( 'ReduxFrameworkPlugin' ) ) {?>
        <a href="admin.php?page=_options" class="nav-tab">Theme Options</a>
        <?php }?>
    </div>  
    <div class="postbox bkpostbox">
        <?php 
            if ( ! class_exists( 'ReduxFrameworkPlugin' ) ) {
        ?>
            <div class="hndle" style="padding: 15px 30px;">
                <p class="bk-admin-notice"><?php esc_html_e('Installation of ReduxFramework is required before using Gloria. Click below button to go to the plugin tab', 'gloria'); ?></p>
        		<div class="gloria-admin-button bk-button"><a href="admin.php?page=bk-theme-plugins" class="">Plugin Tab</a></div>
                <br />
            </div>
        <?php
    		}else {
        ?>   
    	<div class="hndle" style="padding: 15px 30px;">
            <h1><?php esc_html_e('Install demo with 1 CLICK', 'gloria'); ?></h1>
            <p class="bk-admin-notice">
    			Make sure you install demo / sample data with test website only.
    		</p>
        </div>
    	<div class="inside" style="margin: 30px -15px 30px -15px;">
    		<div class="main">
                <?php
        			if (isset($_GET['task'])) :
        		?>
                    <div class="bk-progress-wrapper">
            			<p class="warning" id="bk-demo-sample-process-message">
            				Installing demo / sample data, please wait when the demo is loading. It will take few minutes, so do not close or reload this page.
            			</p>
                        <p class="warning" id="bk-demo-notice">This will take about 3-5 minutes. If any issue occurs, please <strong>click re-import button to continue the process</strong></p>
            			<div class="bk-import-process-bar-wrapper" style="display:block;width:100%;background:white;box-shadow: none;padding:0;" id="bk-demo-sample-process-bar-wrapper">
            				<div class="bk-import-process-bar" style="display: block; width: 0%;padding:0;border:none;color:white;text-align: center;" id="bk-demo-sample-process-bar"></div>
            			</div>
                        <p class="warning" id="bk-demo-sample-process-log">
        			 </div>
                <?php 
                    bk_install_demo_data();
                    endif; 
                ?>
                <div class="bk-demo-wrapper clearfix">
                    <?php foreach ($bk_theme_demos as $theme_demo) {?>
                        <div class="bk-demo-item <?php echo $theme_demo['class'];?>">
                            <div class="bk-demo-item-inner">
                                <img src="<?php echo $theme_demo['img'];?>" alt="Default" style=" width: 100%; ">
                    			<div class="bk-demo-header"><?php echo $theme_demo['title'];?></div>
                                <?php
                        			if (isset($_GET['act']) && ($_GET['act'] == $theme_demo['act'])) :
                        		?>
                                    <div class="install-status">Installing</div>
                                    <a href="#import" onclick="javascript: if (confirm('The demo content cannot be uninstalled. You will have to manually delete this demo content. Click OK to continue or CANCEL to abort')) { window.location.href = '<?php echo admin_url('admin.php?page=bk-theme-demos&act='.$theme_demo['act'].'&task=re-import'); ?>';}">Re-import</a> 
                                <?php elseif($bk_current_demo == $theme_demo['act']):?>
                                    <div class="install-status">Installed</div>
                                    <a href="#import" onclick="javascript: if (confirm('The demo content cannot be uninstalled. You will have to manually delete this demo content. Click OK to continue or CANCEL to abort')) { window.location.href = '<?php echo admin_url('admin.php?page=bk-theme-demos&act='.$theme_demo['act'].'&task=re-import'); ?>';}">Re-import</a> 
                                <?php else:?>
                                    <a href="#import" onclick="javascript: if (confirm('The demo content cannot be uninstalled. You will have to manually delete this demo content. Click OK to continue or CANCEL to abort')) { window.location.href = '<?php echo admin_url('admin.php?page=bk-theme-demos&act='.$theme_demo['act'].'&task=import'); ?>';}">Install Demo</a> 
                                <?php 
                                    endif; 
                                ?>
                            </div>
                        </div>
                    <?php }?>
                </div>
    		</div>
    	</div>
        <?php }?>
    </div>
	
	<br class="clear"/>

</div>

<?php
}

function bk_theme_welcome() {
	// Check that the user is allowed to update options  
	if (current_user_can('manage_options')) {  
	   //add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );
        add_menu_page(esc_html__('Gloria', 'gloria'), esc_html__('Gloria', 'gloria'), 'edit_theme_options', 'bk-theme-welcome', 'bk_welcome_template', 'dashicons-admin-site', 4 );
    }
}
add_action('admin_menu', 'bk_theme_welcome');

function bk_theme_plugins() {
	// Check that the user is allowed to update options  
	if (current_user_can('manage_options')) {  
        add_submenu_page( 'bk-theme-welcome', 'Install Plugins', 'Install Plugins', 'edit_theme_options', 'bk-theme-plugins',  'bk_plugins_template' );
    }
}
add_action('admin_menu', 'bk_theme_plugins');

function bk_theme_demos() {
	// Check that the user is allowed to update options  
	if (current_user_can('manage_options')) { 
        if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
            add_submenu_page( 'bk-theme-welcome', 'Install Demos', 'Install Demos', 'edit_theme_options', 'bk-theme-demos',  'bk_theme_demo_html' );
    		//add_menu_page(esc_html__('Install Demos', 'gloria'), esc_html__('Install Demos', 'gloria'), 'edit_theme_options', 'bk-theme-demos', 'bk_theme_demo_html', 'dashicons-download', 4 );
	   }
    }
}
add_action('admin_menu', 'bk_theme_demos');

function bk_system_status() {
	// Check that the user is allowed to update options  
	if (current_user_can('manage_options')) {  
        if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
            add_submenu_page( 'bk-theme-welcome', 'Theme Options', 'Theme Options', 'edit_theme_options', ' ?page=_options',  '');
            add_submenu_page( 'bk-theme-welcome', 'System Status', 'System Status', 'edit_theme_options', 'bk-system-status',  'bk_system_status_template' );
        }
    }
    global $submenu; // this is a global from WordPress
    $submenu['bk-theme-welcome'][0][0] = 'Welcome';
}
add_action('admin_menu', 'bk_system_status');
//add_action('after_switch_theme', 'bk_theme_options_menu');
//add_action( 'redux/loaded', array( $this, 'init' ) );