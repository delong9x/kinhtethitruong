<?php
function bk_system_status_template() {
?>
<br />
<div class="page-wrap" style="margin: 20px 30px 0 2px;">
    <div class="nav-tab-wrapper">
        <a href="admin.php?page=bk-theme-welcome" class="nav-tab">Welcome</a>
        <a href="admin.php?page=bk-theme-plugins" class="nav-tab">Plugins</a>
        <?php if ( class_exists( 'ReduxFrameworkPlugin' ) ) {?>
            <a href="admin.php?page=bk-theme-demos" class="nav-tab">Install demos</a>
            <a href="admin.php?page=bk-system-status" class="nav-tab nav-tab-active">System status</a>
            <a href="admin.php?page=_options" class="nav-tab">Theme Options</a>
        <?php }?>
    </div>    
    <div class="postbox bkpostbox">
        <?php 
            if ( ! class_exists( 'ReduxFrameworkPlugin' ) ) {
        ?>
            <div class="hndle" style="padding: 15px 30px;">
                <p class="bk-admin-notice"><?php esc_html_e('Installation of ReduxFramework is required before using Gloria. Click below button to go to the plugin tab', 'gloria'); ?></p>
        		<div class="gloria-admin-button bk-button"><a href="admin.php?page=bk-theme-plugins" class="">Plugin Tab</a></div>
                <br />
            </div>
        <?php
    		}else {
    		  $sysinfo = Redux_Helpers::compileSystemStatus( false, true );
        ?>   
    	<div class="hndle" style="padding: 15px 30px;">
            <h1><?php esc_html_e('Gloria - System Status', 'gloria'); ?></h1>
            <p class="bk-admin-notice">
    			Here you can check the System Status. The yellow tick means that the current option is set TRUE. The yellow minus means that the current options is set FALSE. 
    		</p>
        </div>
    	<div class="inside" style="margin: 30px -15px 30px -15px;">
    		<div class="main">
                <div class="wrap about-wrap redux-status">
                    <table class="redux_status_table widefat" cellspacing="0" id="status">
                        <thead>
                        <tr>
                            <th colspan="3"
                                data-export-label="WordPress Environment"><?php esc_html_e( 'WordPress Environment', 'redux-framework' ); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td data-export-label="Home URL"><?php esc_html_e( 'Home URL', 'redux-framework' ); ?>:</td>
                            <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'The URL of your site\'s homepage.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                            <td><?php echo $sysinfo['home_url']; ?></td>
                        </tr>
                        <tr>
                            <td data-export-label="Site URL"><?php esc_html_e( 'Site URL', 'redux-framework' ); ?>:</td>
                            <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'The root URL of your site.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                            <td><?php echo $sysinfo['site_url']; ?></td>
                        </tr>
                        <tr>
                            <td data-export-label="WP Content URL"><?php esc_html_e( 'WP Content URL', 'redux-framework' ); ?>
                                :
                            </td>
                            <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'The location of Wordpress\'s content URL.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                            <td><?php
                                echo '<code>' . $sysinfo['wp_content_url'] . '</code> ';
                                ?></td>
                        </tr>        
                        <tr>
                            <td data-export-label="WP Version"><?php esc_html_e( 'WP Version', 'redux-framework' ); ?>:</td>
                            <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'The version of WordPress installed on your site.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                            <td><?php bloginfo( 'version' ); ?></td>
                        </tr>
                        <tr>
                            <td data-export-label="WP Multisite"><?php esc_html_e( 'WP Multisite', 'redux-framework' ); ?>:</td>
                            <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'Whether or not you have WordPress Multisite enabled.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                            <td><?php if ( $sysinfo['wp_multisite'] == true ) {
                                    echo '&#10004;';
                                } else {
                                    echo '&ndash;';
                                } ?></td>
                        </tr>
                        <tr>
                            <td data-export-label="Permalink Structure"><?php esc_html_e( 'Permalink Structure', 'redux-framework' ); ?>:</td>
                            <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'The current permalink structure as defined in Wordpress Settings->Permalinks.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                            <td><?php echo $sysinfo['permalink_structure']; ?></td>
                        </tr>
                        <?php $sof = $sysinfo['front_page_display']; ?>
                        <tr>
                            <td data-export-label="Front Page Display"><?php esc_html_e( 'Front Page Display', 'redux-framework' ); ?>:</td>
                            <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'The current Reading mode of Wordpress.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                            <td><?php echo $sof; ?></td>
                        </tr>
                
                        <?php
                            if ( $sof == 'page' ) {
                                ?>
                                <tr>
                                    <td data-export-label="Front Page"><?php esc_html_e( 'Front Page', 'redux-framework' ); ?>:</td>
                                    <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'The currently selected page which acts as the site\'s Front Page.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                                    <td><?php echo $sysinfo['front_page']; ?></td>
                                </tr>
                                <tr>
                                    <td data-export-label="Posts Page"><?php esc_html_e( 'Posts Page', 'redux-framework' ); ?>:</td>
                                    <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'The currently selected page in where blog posts are displayed.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                                    <td><?php echo $sysinfo['posts_page']; ?></td>
                                </tr>
                            <?php
                            }
                        ?>
                        <tr>
                            <td data-export-label="WP Memory Limit"><?php esc_html_e( 'WP Memory Limit', 'redux-framework' ); ?>:</td>
                            <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'The maximum amount of memory (RAM) that your site can use at one time.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                            <td><?php
                                    $memory = $sysinfo['wp_mem_limit']['raw'];
                
                                    if ( $memory < 40000000 ) {
                                        echo '<mark class="error">' . sprintf( esc_html__( '%s - We recommend setting memory to at least 40MB. See: <a href="%s" target="_blank">Increasing memory allocated to PHP</a>', 'redux-framework' ), $sysinfo['wp_mem_limit']['size'], 'http://codex.wordpress.org/Editing_wp-config.php#Increasing_memory_allocated_to_PHP' ) . '</mark>';
                                    } else {
                                        echo '<mark class="yes">' . $sysinfo['wp_mem_limit']['size'] . '</mark>';
                                    }
                                ?></td>
                        </tr>
                        <tr>
                            <td data-export-label="Database Table Prefix"><?php esc_html_e( 'Database Table Prefix', 'redux-framework' ); ?>:
                            </td>
                            <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'The prefix structure of the current Wordpress database.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                            <td><?php echo $sysinfo['db_table_prefix']; ?></td>
                        </tr>
                        <tr>
                            <td data-export-label="WP Debug Mode"><?php esc_html_e( 'WP Debug Mode', 'redux-framework' ); ?>:</td>
                            <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'Displays whether or not WordPress is in Debug Mode.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                            <td><?php if ( $sysinfo['wp_debug'] == true ) {
                                    echo '<mark class="yes">' . '&#10004;' . '</mark>';
                                } else {
                                    echo '<mark class="no">' . '&ndash;' . '</mark>';
                                } ?></td>
                        </tr>
                        <tr>
                            <td data-export-label="Language"><?php esc_html_e( 'Language', 'redux-framework' ); ?>:</td>
                            <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'The current language used by WordPress. Default = English', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                            <td><?php echo $sysinfo['wp_lang'] ?></td>
                        </tr>
                        </tbody>
                    </table>
                    <table class="redux_status_table widefat" cellspacing="0" id="status">
                        <thead>
                        <tr>
                            <th colspan="3" data-export-label="Browser"><?php esc_html_e( 'Browser', 'redux-framework' ); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td data-export-label="Browser Info"><?php esc_html_e( 'Browser Info', 'redux-framework' ); ?>:</td>
                            <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'Information about web browser current in use.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                            <td><?php
                                    foreach ( $sysinfo['browser'] as $key => $value ) {
                                        echo '<strong>' . ucfirst( $key ) . '</strong>: ' . $value . '<br/>';
                                    }
                                ?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                
                    <table class="redux_status_table widefat" cellspacing="0" id="status">
                        <thead>
                        <tr>
                            <th colspan="3"
                                data-export-label="Server Environment"><?php esc_html_e( 'Server Environment', 'redux-framework' ); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td data-export-label="Server Info"><?php esc_html_e( 'Server Info', 'redux-framework' ); ?>:</td>
                            <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'Information about the web server that is currently hosting your site.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                            <td><?php echo $sysinfo['server_info']; ?></td>
                        </tr>
                        <tr>
                            <td data-export-label="Localhost Environment"><?php esc_html_e( 'Localhost Environment', 'redux-framework' ); ?>:</td>
                            <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'Is the server running in a localhost environment.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                            <td><?php
                                if ( true == $sysinfo['localhost'] ) {
                                    echo '<mark class="yes">' . '&#10004;' . '</mark>';
                                } else {
                                    echo '<mark class="no">' . '&ndash;' . '</mark>';
                                }?>            
                            </td>
                        </tr>
                        <tr>
                            <td data-export-label="PHP Version"><?php esc_html_e( 'PHP Version', 'redux-framework' ); ?>:</td>
                            <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'The version of PHP installed on your hosting server.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                            <td><?php echo $sysinfo['php_ver']; ?></td>
                        </tr>
                        <tr>
                            <td data-export-label="ABSPATH"><?php esc_html_e( 'ABSPATH', 'redux-framework' ); ?>:</td>
                            <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'The ABSPATH variable on the server.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                            <td><?php echo '<code>' . $sysinfo['abspath'] . '</code>'; ?></td>
                        </tr>
                        
                        <?php if ( function_exists( 'ini_get' ) ) { ?>
                            <tr>
                                <td data-export-label="PHP Memory Limit"><?php esc_html_e( 'PHP Memory Limit', 'redux-framework' ); ?>:</td>
                                <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'The largest filesize that can be contained in one post.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                                <td><?php echo $sysinfo['php_mem_limit']; ?></td>
                            </tr>
                            <tr>
                                <td data-export-label="PHP Post Max Size"><?php esc_html_e( 'PHP Post Max Size', 'redux-framework' ); ?>:</td>
                                <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'The largest filesize that can be contained in one post.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                                <td><?php echo $sysinfo['php_post_max_size']; ?></td>
                            </tr>
                            <tr>
                                <td data-export-label="PHP Time Limit"><?php esc_html_e( 'PHP Time Limit', 'redux-framework' ); ?>:</td>
                                <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'The amount of time (in seconds) that your site will spend on a single operation before timing out (to avoid server lockups)', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                                <td><?php echo $sysinfo['php_time_limit']; ?></td>
                            </tr>
                            <tr>
                                <td data-export-label="PHP Max Input Vars"><?php esc_html_e( 'PHP Max Input Vars', 'redux-framework' ); ?>:</td>
                                <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'The maximum number of variables your server can use for a single function to avoid overloads.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                                <td><?php echo $sysinfo['php_max_input_var']; ?></td>
                            </tr>
                            <tr>
                                <td data-export-label="PHP Display Errors"><?php esc_html_e( 'PHP Display Errors', 'redux-framework' ); ?>:</td>
                                <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'Determines if PHP will display errors within the browser.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                                <td><?php
                                        if ( true == $sysinfo['php_display_errors'] ) {
                                            echo '<mark class="yes">' . '&#10004;' . '</mark>';
                                        } else {
                                            echo '<mark class="no">' . '&ndash;' . '</mark>';
                                        }
                                    ?></td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td data-export-label="SUHOSIN Installed"><?php esc_html_e( 'SUHOSIN Installed', 'redux-framework' ); ?>:</td>
                            <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'Suhosin is an advanced protection system for PHP installations.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                            <td><?php if ( $sysinfo['suhosin_installed'] == true ) {
                                    echo '<mark class="yes">' . '&#10004;' . '</mark>';
                                } else {
                                    echo '<mark class="no">' . '&ndash;' . '</mark>';
                                } ?></td>
                        </tr>
                
                        <tr>
                            <td data-export-label="MySQL Version"><?php esc_html_e( 'MySQL Version', 'redux-framework' ); ?>:</td>
                            <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'The version of MySQL installed on your hosting server.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                            <td><?php echo $sysinfo['mysql_ver']; ?></td>
                        </tr>
                        <tr>
                            <td data-export-label="Max Upload Size"><?php esc_html_e( 'Max Upload Size', 'redux-framework' ); ?>:</td>
                            <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'The largest filesize that can be uploaded to your WordPress installation.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                            <td><?php echo $sysinfo['max_upload_size']; ?></td>
                        </tr>
                        <tr>
                            <td data-export-label="Default Timezone is UTC"><?php esc_html_e( 'Default Timezone is UTC', 'redux-framework' ); ?>
                                :
                            </td>
                            <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'The default timezone for your server.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                            <td><?php
                                    if ( $sysinfo['def_tz_is_utc'] == false ) {
                                        echo '<mark class="error">' . '&#10005; ' . sprintf( esc_html__( 'Default timezone is %s - it should be UTC', 'redux-framework' ), date_default_timezone_get() ) . '</mark>';
                                    } else {
                                        echo '<mark class="yes">' . '&#10004;' . '</mark>';
                                    } ?>
                            </td>
                        </tr>
                        <?php
                            $posting = array();
                
                            // fsockopen/cURL
                            $posting['fsockopen_curl']['name'] = 'fsockopen/cURL';
                            $posting['fsockopen_curl']['help'] = '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'Used when communicating with remote services with PHP.', 'redux-framework' ) . '"><span>i</span></a>';
                
                            if ( $sysinfo['fsockopen_curl'] == true ) {
                                $posting['fsockopen_curl']['success'] = true;
                            } else {
                                $posting['fsockopen_curl']['success'] = false;
                                $posting['fsockopen_curl']['note']    = esc_html__( 'Your server does not have fsockopen or cURL enabled - cURL is used to communicate with other servers. Please contact your hosting provider.', 'redux-framework' ) . '</mark>';
                            }
                
                            /*
                            // SOAP
                            $posting['soap_client']['name'] = 'SoapClient';
                            $posting['soap_client']['help'] = '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'Some webservices like shipping use SOAP to get information from remote servers, for example, live shipping quotes from FedEx require SOAP to be installed.', 'redux-framework' ) . '"><span>i</span></a>';
                
                            if ( $sysinfo['soap_client'] == true ) {
                                $posting['soap_client']['success'] = true;
                            } else {
                                $posting['soap_client']['success'] = false;
                                $posting['soap_client']['note']    = sprintf( esc_html__( 'Your server does not have the <a href="%s">SOAP Client</a> class enabled - some gateway plugins which use SOAP may not work as expected.', 'redux-framework' ), 'http://php.net/manual/en/class.soapclient.php' ) . '</mark>';
                            }
                
                            // DOMDocument
                            $posting['dom_document']['name'] = 'DOMDocument';
                            $posting['dom_document']['help'] = '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'HTML/Multipart emails use DOMDocument to generate inline CSS in templates.', 'redux-framework' ) . '"><span>i</span></a>';
                
                            if ( $sysinfo['dom_document'] == true ) {
                                $posting['dom_document']['success'] = true;
                            } else {
                                $posting['dom_document']['success'] = false;
                                $posting['dom_document']['note']    = sprintf( esc_html__( 'Your server does not have the <a href="%s">DOMDocument</a> class enabled - HTML/Multipart emails, and also some extensions, will not work without DOMDocument.', 'redux-framework' ), 'http://php.net/manual/en/class.domdocument.php' ) . '</mark>';
                            }
                            */
                
                            //// GZIP
                            //$posting['gzip']['name'] = 'GZip';
                            //$posting['gzip']['help'] = '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'GZip (gzopen) is used to open the GEOIP database from MaxMind.', 'redux-framework' ) . '"><span>i</span></a>';
                            //
                            //if ( $sysinfo['gzip'] == true ) {
                            //    $posting['gzip']['success'] = true;
                            //} else {
                            //    $posting['gzip']['success'] = false;
                            //    $posting['gzip']['note']    = sprintf( esc_html__( 'Your server does not support the <a href="%s">gzopen</a> function - this is required to use the GeoIP database from MaxMind. The API fallback will be used instead for geolocation.', 'redux-framework' ), 'http://php.net/manual/en/zlib.installation.php' ) . '</mark>';
                            //}
                
                            // WP Remote Post Check
                            $posting['wp_remote_post']['name'] = esc_html__( 'Remote Post', 'redux-framework' );
                            $posting['wp_remote_post']['help'] = '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'Used to send data to remote servers.', 'redux-framework' ) . '"><span>i</span></a>';
                
                            if ( $sysinfo['wp_remote_post'] == true ) {
                                $posting['wp_remote_post']['success'] = true;
                            } else {
                                $posting['wp_remote_post']['note'] = esc_html__( 'wp_remote_post() failed. Many advanced features may not function. Contact your hosting provider.', 'redux-framework' );
                
                                if ( $sysinfo['wp_remote_post_error'] ) {
                                    $posting['wp_remote_post']['note'] .= ' ' . sprintf( esc_html__( 'Error: %s', 'redux-framework' ), rexux_clean( $sysinfo['wp_remote_post_error'] ) );
                                }
                
                                $posting['wp_remote_post']['success'] = false;
                            }
                
                            // WP Remote Get Check
                            $posting['wp_remote_get']['name'] = esc_html__( 'Remote Get', 'redux-framework' );
                            $posting['wp_remote_get']['help'] = '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'Used to grab information from remote servers for updates updates.', 'redux-framework' ) . '"><span>i</span></a>';
                
                            if ( $sysinfo['wp_remote_get'] == true ) {
                                $posting['wp_remote_get']['success'] = true;
                            } else {
                                $posting['wp_remote_get']['note'] = esc_html__( 'wp_remote_get() failed. This is needed to get information from remote servers. Contact your hosting provider.', 'redux-framework' );
                                if ( $sysinfo['wp_remote_get_error'] ) {
                                    $posting['wp_remote_get']['note'] .= ' ' . sprintf( esc_html__( 'Error: %s', 'redux-framework' ), redux_clean( $sysinfo['wp_remote_get_error'] ) );
                                }
                
                                $posting['wp_remote_get']['success'] = false;
                            }
                
                            $posting = apply_filters( 'redux_debug_posting', $posting );
                
                            foreach ( $posting as $post ) {
                                $mark = ! empty( $post['success'] ) ? 'yes' : 'error';
                                ?>
                                <tr>
                                    <td data-export-label="<?php echo esc_html( $post['name'] ); ?>"><?php echo esc_html( $post['name'] ); ?>
                                        :
                                    </td>
                                    <td><?php echo isset( $post['help'] ) ? $post['help'] : ''; ?></td>
                                    <td class="help">
                                        <mark class="<?php echo $mark; ?>">
                                            <?php echo ! empty( $post['success'] ) ? '&#10004' : '&#10005'; ?>
                                            <?php echo ! empty( $post['note'] ) ? wp_kses_data( $post['note'] ) : ''; ?>
                                        </mark>
                                    </td>
                                </tr>
                            <?php
                            }
                        ?>
                        </tbody>
                    </table>
                    <table class="redux_status_table widefat" cellspacing="0" id="status">
                        <thead>
                        <tr>
                            <th colspan="3"
                                data-export-label="Active Plugins (<?php echo count( (array) get_option( 'active_plugins' ) ); ?>)"><?php esc_html_e( 'Active Plugins', 'redux-framework' ); ?>
                                (<?php echo count( (array) get_option( 'active_plugins' ) ); ?>)
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            foreach ( $sysinfo['plugins'] as $name => $plugin_data ) {
                                $version_string = '';
                                $network_string = '';
                
                                if ( ! empty( $plugin_data['Name'] ) ) {
                                    // link the plugin name to the plugin url if available
                                    $plugin_name = esc_html( $plugin_data['Name'] );
                
                                    if ( ! empty( $plugin_data['PluginURI'] ) ) {
                                        $plugin_name = '<a href="' . esc_url( $plugin_data['PluginURI'] ) . '" title="' . esc_html__( 'Visit plugin homepage', 'redux-framework' ) . '">' . $plugin_name . '</a>';
                                    }
                                    ?>
                                    <tr>
                                        <td><?php echo $plugin_name; ?></td>
                                        <td class="help">&nbsp;</td>
                                        <td><?php echo sprintf( _x( 'by %s', 'by author', 'redux-framework' ), $plugin_data['Author'] ) . ' &ndash; ' . esc_html( $plugin_data['Version'] ) . $version_string . $network_string; ?></td>
                                    </tr>
                                <?php
                                }
                            }
                        ?>
                        </tbody>
                    </table>
                    
                    <table class="redux_status_table widefat" cellspacing="0" id="status">
                        <thead>
                        <tr>
                            <th colspan="3" data-export-label="Theme"><?php esc_html_e( 'Theme', 'redux-framework' ); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td data-export-label="Name"><?php esc_html_e( 'Name', 'redux-framework' ); ?>:</td>
                            <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'The name of the current active theme.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                            <td><?php echo $sysinfo['theme']['name']; ?></td>
                        </tr>
                        <tr>
                            <td data-export-label="Version"><?php esc_html_e( 'Version', 'redux-framework' ); ?>:</td>
                            <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'The installed version of the current active theme.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                            <td><?php
                                    echo $sysinfo['theme']['version'];
                
                                    if ( ! empty( $theme_version_data['version'] ) && version_compare( $theme_version_data['version'], $active_theme->Version, '!=' ) ) {
                                        echo ' &ndash; <strong style="color:red;">' . $theme_version_data['version'] . ' ' . esc_html__( 'is available', 'redux-framework' ) . '</strong>';
                                    }
                                ?></td>
                        </tr>
                        <tr>
                            <td data-export-label="Author URL"><?php esc_html_e( 'Author URL', 'redux-framework' ); ?>:</td>
                            <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'The theme developers URL.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                            <td><?php echo $sysinfo['theme']['author_uri']; ?></td>
                        </tr>
                        <tr>
                            <td data-export-label="Child Theme"><?php esc_html_e( 'Child Theme', 'redux-framework' ); ?>:</td>
                            <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'Displays whether or not the current theme is a child theme.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                            <td><?php
                                    echo is_child_theme() ? '<mark class="yes">' . '&#10004;' . '</mark>' : '&#10005;</em>';
                                ?></td>
                        </tr>
                        <?php
                
                            if ( is_child_theme() ) {
                                ?>
                                <tr>
                                    <td data-export-label="Parent Theme Name"><?php esc_html_e( 'Parent Theme Name', 'redux-framework' ); ?>:
                                    </td>
                                    <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'The name of the parent theme.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                                    <td><?php echo $sysinfo['theme']['parent_name']; ?></td>
                                </tr>
                                <tr>
                                    <td data-export-label="Parent Theme Version"><?php esc_html_e( 'Parent Theme Version', 'redux-framework' ); ?>
                                        :
                                    </td>
                                    <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'The installed version of the parent theme.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                                    <td><?php echo $sysinfo['theme']['parent_version']; ?></td>
                                </tr>
                                <tr>
                                    <td data-export-label="Parent Theme Author URL"><?php esc_html_e( 'Parent Theme Author URL', 'redux-framework' ); ?>
                                        :
                                    </td>
                                    <td class="help"><?php echo '<a href="#" class="bk-tipper-bottom bk-help" data-title="' . esc_attr__( 'The parent theme developers URL.', 'redux-framework' ) . '"><span>i</span></a>'; ?></td>
                                    <td><?php echo $sysinfo['theme']['parent_author_uri']; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <script type="text/javascript">
                        jQuery( 'a.redux-hint-qtip' ).click(
                            function() {
                                return false;
                            }
                        );
                
                        jQuery( 'a.debug-report' ).click(
                            function() {
                                var report = '';
                
                                jQuery( '#status thead, #status tbody' ).each(
                                    function() {
                                        if ( jQuery( this ).is( 'thead' ) ) {
                                            var label = jQuery( this ).find( 'th:eq(0)' ).data( 'export-label' ) || jQuery( this ).text();
                                            report = report + "\n### " + jQuery.trim( label ) + " ###\n\n";
                                        } else {
                                            jQuery( 'tr', jQuery( this ) ).each(
                                                function() {
                                                    var label = jQuery( this ).find( 'td:eq(0)' ).data( 'export-label' ) || jQuery( this ).find( 'td:eq(0)' ).text();
                                                    var the_name = jQuery.trim( label ).replace( /(<([^>]+)>)/ig, '' ); // Remove HTML
                                                    var the_value = jQuery.trim( jQuery( this ).find( 'td:eq(2)' ).text() );
                                                    var value_array = the_value.split( ', ' );
                
                                                    if ( value_array.length > 1 ) {
                                                        // If value have a list of plugins ','
                                                        // Split to add new line
                                                        var output = '';
                                                        var temp_line = '';
                                                        jQuery.each(
                                                            value_array, function( key, line ) {
                                                                temp_line = temp_line + line + '\n';
                                                            }
                                                        );
                
                                                        the_value = temp_line;
                                                    }
                
                                                    report = report + '' + the_name + ': ' + the_value + "\n";
                                                }
                                            );
                                        }
                                    }
                                );
                
                                try {
                                    jQuery( "#debug-report" ).slideDown();
                                    jQuery( "#debug-report textarea" ).val( report ).focus().select();
                                    jQuery( this ).fadeOut();
                
                                    return false;
                                } catch ( e ) {
                                    console.log( e );
                                }
                
                                return false;
                            }
                        );
                
                        jQuery( document ).ready(
                            function( $ ) {
                                $( 'body' ).on(
                                    'copy', '#copy-for-support', function( e ) {
                                        e.clipboardData.clearData();
                                        e.clipboardData.setData( 'text/plain', $( '#debug-report textarea' ).val() );
                                        e.preventDefault();
                                    }
                                );
                            }
                        );
                    </script>
                </div>
    		</div>
    	</div>
        <?php }?>
    </div>
	
	
	<br class="clear"/>

</div>

<?php
}
//add_action('after_switch_theme', 'bk_theme_options_menu');
//add_action( 'redux/loaded', array( $this, 'init' ) );