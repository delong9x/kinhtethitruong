<?php
function bk_welcome_template() {
?>
<br />
<div class="page-wrap" style="margin: 20px 30px 0 2px;">
    <div class="nav-tab-wrapper">
        <a href="admin.php?page=bk-theme-welcome" class="nav-tab nav-tab-active">Welcome</a>
        <a href="admin.php?page=bk-theme-plugins" class="nav-tab">Plugins</a>
        <?php if ( class_exists( 'ReduxFrameworkPlugin' ) ) {?>
            <a href="admin.php?page=bk-theme-demos" class="nav-tab">Install demos</a>
            <a href="admin.php?page=bk-system-status" class="nav-tab">System status</a>
            <a href="admin.php?page=_options" class="nav-tab">Theme Options</a>
        <?php }?>
    </div>   
    <div class="postbox bkpostbox">
    	<div class="hndle" style="padding: 15px 30px;">
            <h1><?php esc_html_e('Welcome to Gloria', 'gloria'); ?></h1>
            <p class="bk-admin-notice">
    			Thank you for using our theme, Gloria is now installed and ready to use!
    		</p>
            <?php if (! class_exists( 'ReduxFrameworkPlugin' ) ) {?>
            <p class="bk-admin-notice">
    			Note: You should install all required plugins and activate them to have Full Options of Gloria.
    		</p>
            <?php }?>
        </div>
    	<div class="inside" style="margin: 30px -15px 30px -15px;">
    		<div class="main gloria-welcome-main">
                <div class="introduce clearfix">
                    <div class="gloria-screenshot">
                        <img src="<?php echo (GLORIA_AD_PLUGIN_URL . 'demo_type_imgs/Pagebuilder.png');?>"/>
                    </div>
                    <div class="gloria-welcome-content">
                        <p>Gloria supports one click demo importer, quickly get your site to look like our demo page and get started easily.</p>
                        <p>Just following this <a href="#">tutorial video</a> to get the site ready now.</p>
                        <div class="video-tutorial"><iframe width="300" height="168" src="https://www.youtube.com/embed/Cq7IzSk7uqo" frameborder="0" allowfullscreen></iframe></div>
                    </div>
                </div>
                <div class="gloria-welcome-footer clearfix">
                    <div class="bk-footer-1">
                        <h2>Support Forum</h2>
                        <p>
                            Technical Support is free for all customers purchasing Gloria. For any questions or any bugs related to the setup of Gloria that are not covered by the theme documentation, please register and submit a ticket at our forum. Our support team is always ready to help you.
                        </p>
                        <div class="bk-button"><a target="_blank" href="http://forum.bk-ninja.com">Open Forum</a></div>
                    </div>
                    <div class="bk-footer-2">
                        <h2>Documentation and Video Tutorials</h2>
                        <p>
                            Our online documentation with Video Tutorials is an incredible resource for learning the ins and outs of using Gloria. Here you will find key points to manuals, tutorials and references that will come in handy easily. Watching a video is one of the best ways to understand well about Gloria. 
                        </p>
                        <div class="bk-button"><a target="_blank" href="http://forum.bk-ninja.com/documentation/">Open Documentation</a></div>
                    </div>
                </div>
    		</div>
    	</div>
    </div>
	
	
	<br class="clear"/>

</div>

<?php
}