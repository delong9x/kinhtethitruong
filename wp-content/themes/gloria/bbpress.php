<?php

/**
 * bbPress Forum Template 
 */

get_header();
$gloria_option = gloria_core::bk_get_global_var('gloria_option');
?>
<div class="page-wrap bkwrapper container">
    <div class="row bksection">
        <div id="page-content" class="<?php if (isset($gloria_option['bk-forum-sidebar']) && ($gloria_option['bk-forum-sidebar'] == 'on')){ echo 'col-md-8 col-sm-12 three-cols';}else { echo 'col-sm-12 four-cols'; }?>">
			<?php if (have_posts()) : ?>
				<?php while ( have_posts() ) : the_post(); ?>
                    <?php if ( is_page() && ! is_front_page() || ( function_exists( 'bp_current_component' ) && bp_current_component() )  ) : ?>
                    <div class="page-title">
					   <h2 itemprop="name"><span><?php the_title(); ?></span></h2>
                    </div>
					<?php endif; ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?>>
						<div class="post-content"><?php the_content(); ?></div>
					</article>
					
				<?php endwhile; ?>

			<?php else : ?>

				<h2><?php esc_html_e('Not Found', 'gloria') ?></h2>

			<?php endif; ?>
		</div>
        <?php
            if (isset($gloria_option['bk-forum-sidebar']) && ($gloria_option['bk-forum-sidebar'] == 'on')) {?>
                <div id="forum-sidebar" class="sidebar col-md-4 col-sm-12">
        			<aside class="sidebar-wrap <?php if($gloria_option['forum-stick-sidebar'] == 'enable') echo 'stick';?>" id="bk-forum-sidebar">
                        <?php get_sidebar(); ?>
                    </aside>
        		</div>
        <?php }?>                
    </div>
</div>
		
<?php get_footer(); ?>