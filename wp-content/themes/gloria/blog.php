<?php
/*
Template Name: Blog
*/
 ?> 
<?php get_header();?>
<?php
$gloria_option = gloria_core::bk_get_global_var('gloria_option');
$bk_layout = '';
if (isset($gloria_option) && ($gloria_option != '')): 
    $bk_layout = $gloria_option['bk-blog-layout'];
endif;
if ($bk_layout == '') {
    $bk_layout = 'classic-blog';
}
?>
<?php 
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
query_posts('post_type=post&post_status=publish&paged=' . $paged);
?>
<div id="body-wrapper" class="wp-page">
    <div class="bkwrapper container">		
        <div class="row bksection">			
            <div class="blog-page-content bkpage-content <?php if ((!($bk_layout == 'masonry-nosb')) && (!($bk_layout == 'square-grid-3'))): echo 'col-md-8 has-sb'; else: echo 'col-md-12 fullwidth';  endif;?>">
                <div class="page-title">
            		<h2 class="heading"><span><?php esc_html_e( 'Blog', 'gloria' );?></span></h2> 
                    <?php gloria_core::bk_breadcrumbs();?>
                </div>
                <div class="row">
                    <div id="main-content" class="bk-blog-content clear-fix" role="main">
                		
                    <?php
                        if (have_posts()) {                                       
/**
 *  Masonry with Sidebar
 * 
 */            
                            if ($bk_layout == 'masonry') { ?>
                                <?php $bk_contentout = new gloria_contentout4;?>
                                <?php
                                    $meta_ar = array('author', 'date');
                                    $post_args = array (
                                        'thumbnail_size'    => 'gloria_auto_size',
                                        'meta_ar'           => $meta_ar,
                                        'cat_meta'            => true,
                                        'except_length'     => 24,
                                        'rm_btn'            => true,
                                    );
                                ?>
                                <div class="content-wrap bk-masonry">
                                    <ul class="bk-masonry-content clearfix">
                                        <?php while (have_posts()): the_post(); ?>  
                                            <?php 
                                            if ( has_post_thumbnail(get_the_ID()) ) {
                                                $bkPostThumb = 'hasPostThumbnail';
                                                $post_args['review_score'] = get_post_meta(get_the_ID(), 'bk_final_score', true );
                                            }else {
                                                $bkPostThumb = 'noPostThumbnail';
                                                $post_args['review_score'] = '';
                                            }   
                                            ?>
                                            <li class="col-md-6 col-sm-6 item">
                                                <div class="row-type content_out <?php echo esc_attr($bkPostThumb);?>">
                                                    <?php 
                                                    $postFormat = gloria_core::bk_post_format_detect(get_the_ID());
                                                    if($postFormat['format'] === 'video') {
                                                        $post_args['video_icon'] = true;
                                                    }else {
                                                        $post_args['video_icon'] = '';
                                                    }
                                                    ?>	
                                                    <?php echo ($bk_contentout->render($post_args));?>
                                                </div>
                                            </li>
                                        <?php endwhile; ?>
                                    </ul>
                                </div>
                                <?php if (function_exists("gloria_paginate")) {?>
                                        <div class="col-md-12">
                                            <?php gloria_paginate();?>
                                        </div>
                                <?php }?>  
                            <?php } 
/**
 * Masonry No sidebar
 */   
                            else if ($bk_layout == 'masonry-nosb') { ?>
                                <?php $bk_contentout = new gloria_contentout4;?>
                                <?php
                                    $meta_ar = array('author', 'date');
                                    $post_args = array (
                                        'thumbnail_size'    => 'gloria_auto_size',
                                        'meta_ar'           => $meta_ar,
                                        'cat_meta'            => true,
                                        'except_length'     => 24,
                                        'rm_btn'            => true,
                                    );
                                ?>
                                <div class="content-wrap bk-masonry">
                                    <ul class="bk-masonry-content clearfix">
                                        <?php while (have_posts()): the_post(); ?>  
                                            <?php 
                                            if ( has_post_thumbnail(get_the_ID()) ) {
                                                $bkPostThumb = 'hasPostThumbnail';
                                                $post_args['review_score'] = get_post_meta(get_the_ID(), 'bk_final_score', true );
                                            }else {
                                                $bkPostThumb = 'noPostThumbnail';
                                                $post_args['review_score'] = '';
                                            }   
                                            ?>
                                            <li class="col-md-4 col-sm-6 item">
                                                <div class="row-type content_out <?php echo esc_attr($bkPostThumb);?>">
                                                    <?php 
                                                    $postFormat = gloria_core::bk_post_format_detect(get_the_ID());
                                                    if($postFormat['format'] === 'video') {
                                                        $post_args['video_icon'] = true;
                                                    }else {
                                                        $post_args['video_icon'] = '';
                                                    }
                                                    ?>
                                                    <?php echo ($bk_contentout->render($post_args));?>
                                                </div>
                                            </li>
                                        <?php endwhile; ?>
                                    </ul>
                                </div>
                                <?php if (function_exists("gloria_paginate")) {?>
                                        <div class="col-md-12">
                                            <?php gloria_paginate();?>
                                        </div>
                                <?php }?>  
                            <?php } 
/**
 * Classic Blog
 * 
 */                         
                             else if ($bk_layout == 'classic-blog') { ?>
                                <?php $bk_contentout = new gloria_contentout5;?>
                                <?php
                                    $meta_ar = array('author', 'date', 'postview', 'postcomment');
                                    $post_args = array (
                                        'thumbnail_size'    => 'gloria_620_420',
                                        'meta_ar'           => $meta_ar,
                                        'cat_meta'          => true,
                                        'except_length'     => 26,
                                        'rm_btn'            => true,
                                    ); 
                                ?>
                                <div class="module-classic-blog module-blog bk-blog-wrapper clearfix">
                                    <ul class="bk-blog-content clearfix">
                                        <?php while (have_posts()): the_post();?>  	
                                        <?php
                                        if ( has_post_thumbnail(get_the_ID()) ) {
                                            $bkPostThumb = 'hasPostThumbnail';
                                            $post_args['review_score'] = get_post_meta(get_the_ID(), 'bk_final_score', true );
                                        }else {
                                            $bkPostThumb = 'noPostThumbnail';
                                            $post_args['review_score'] = '';
                                        }
                                        ?>
                                        <li class="item col-md-12">
                                            <div class="classic-blog-type content_out clearfix <?php echo esc_attr($bkPostThumb);?>">
                                                <div class="classic-blog-post-wrapper">
                                                <?php 
                                                $postFormat = gloria_core::bk_post_format_detect(get_the_ID());
                                                if($postFormat['format'] === 'video') {
                                                    $post_args['video_icon'] = true;
                                                }else {
                                                    $post_args['video_icon'] = '';
                                                }
                                                ?>
                                                <?php echo ($bk_contentout->render($post_args));?>
                                                </div>
                                            </div>
                                        </li>
                                        <?php endwhile; ?>
                                    </ul>
                                </div>
                                <?php if (function_exists("gloria_paginate")) {?>
                                        <div class="col-md-12">
                                            <?php gloria_paginate();?>
                                        </div>
                                <?php }?>  
                             <?php }
/**
 * Large Blog
 */
                             else if ($bk_layout == 'large-blog') { ?>
                                <?php 
                                    $bk_contentout = new gloria_contentout5;
                                    $meta_ar = array('author', 'date', 'postview', 'postcomment');
                                    $post_args = array (
                                        'thumbnail_size'    => 'gloria_600_315',
                                        'meta_ar'           => $meta_ar,
                                        'cat_meta'          => true,
                                        'except_length'     => 55,
                                        'rm_btn'            => true,
                                    );  
                                ?>
                                <div class="content-wrap module-large-blog module-blog">
                                    <ul class="bk-blog-content clearfix">
                                        <?php while (have_posts()): the_post(); ?>  	
                                            <?php 
                                            if ( has_post_thumbnail(get_the_ID()) ) {
                                                $bkPostThumb = 'hasPostThumbnail';
                                                $post_args['review_score'] = get_post_meta(get_the_ID(), 'bk_final_score', true );
                                            }else {
                                                $bkPostThumb = 'noPostThumbnail';
                                                $post_args['review_score'] = '';
                                            }   
                                            ?>
                                            <li class="item col-md-12">
                                                <div class="content_out clearfix <?php echo esc_attr($bkPostThumb);?>">
                                                    <?php 
                                                    $postFormat = gloria_core::bk_post_format_detect(get_the_ID());
                                                    if($postFormat['format'] === 'video') {
                                                        $post_args['video_icon'] = true;
                                                    }else {
                                                        $post_args['video_icon'] = '';
                                                    }
                                                    ?>
                                                    <?php echo ($bk_contentout->render($post_args));?>
                                                </div>
                                            </li>
                                            <?php endwhile; ?>
                                    </ul>
                                </div>
                                <?php if (function_exists("gloria_paginate")) {?>
                                        <div class="col-md-12">
                                            <?php gloria_paginate();?>
                                        </div>
                                <?php }?>
                             <?php }
/**
* square-grid-3
*/
                         else if ($bk_layout == 'square-grid-3') { ?>
                                <?php 
                                    $bk_contentin1 = new gloria_contentin1;
                                    $post_args = array (
                                        'thumbnail_size'    => 'full',
                                        'cat_meta'            => true,
                                        'rm_btn'            => true,
                                    );    
                                ?>
                                <div class="content-wrap square-grid-3 module-square-grid">
                                    <ul class="bk-blog-content clearfix">
                                        <?php while (have_posts()): the_post(); ?>  	
                                            <li class="content_in col-md-4 col-sm-6">
                                                <div class="content_in_wrapper">
                                                    <?php 
                                                    $postFormat = gloria_core::bk_post_format_detect(get_the_ID());
                                                    if($postFormat['format'] === 'video') {
                                                        $post_args['video_icon'] = true;
                                                    }else {
                                                        $post_args['video_icon'] = '';
                                                    }
                                                    ?>                                                
                                                    <?php $bk_final_score = get_post_meta(get_the_ID(), 'bk_final_score', true );?>
                                                    <?php $post_args['review_score'] = $bk_final_score;?>
                                                    <?php echo ($bk_contentin1->render($post_args));?>
                                                </div>
                                            </li>
                                            <?php endwhile; ?>
                                    </ul>
                                </div>
                                <?php if (function_exists("gloria_paginate")) {?>
                                        <div class="col-md-12">
                                            <?php gloria_paginate();?>
                                        </div>
                                <?php }?>
                             <?php }
/**
* square-grid-2
*/
                             else if ($bk_layout == 'square-grid-2') { ?>
                                <?php 
                                    $bk_contentin1 = new gloria_contentin1;
                                    $post_args = array (
                                        'thumbnail_size'    => 'full',
                                        'cat_meta'            => true,
                                        'rm_btn'            => true,
                                    );    
                                ?>
                                <div class="content-wrap square-grid-2 module-square-grid">
                                    <ul class="bk-blog-content clearfix">
                                        <?php while (have_posts()): the_post(); ?>  	
                                            <li class="content_in col-md-6 col-sm-6">
                                                <div class="content_in_wrapper">
                                                    <?php 
                                                    $postFormat = gloria_core::bk_post_format_detect(get_the_ID());
                                                    if($postFormat['format'] === 'video') {
                                                        $post_args['video_icon'] = true;
                                                    }else {
                                                        $post_args['video_icon'] = '';
                                                    }
                                                    ?>  
                                                    <?php $bk_final_score = get_post_meta(get_the_ID(), 'bk_final_score', true );?>
                                                    <?php $post_args['review_score'] = $bk_final_score;?>
                                                    <?php echo ($bk_contentin1->render($post_args));?>
                                                </div>
                                            </li>
                                            <?php endwhile; ?>
                                    </ul>
                                </div>
                                <?php if (function_exists("gloria_paginate")) {?>
                                        <div class="col-md-12">
                                            <?php gloria_paginate();?>
                                        </div>
                                <?php }?>
                             <?php }
                        } else { esc_html_e('No post to display','gloria');} ?>
            
    	            </div> <!-- end #main -->
                </div>
            </div> <!-- end #bk-content -->
            <?php
                if ((!($bk_layout == 'masonry-nosb')) && (!($bk_layout == 'square-grid-3'))) {?>
                    <div class="sidebar col-md-4">
                        <aside class="sidebar-wrap <?php if($gloria_option['blog-stick-sidebar'] == 'enable') echo 'stick';?>" id="bk-blog-sidebar">
                            <?php get_sidebar(); ?>
                        </aside>
                    </div>
                <?php }
            ?>
        </div>
    </div>
</div>   
<?php get_footer(); ?>