<?php
    $gloria_option = gloria_core::bk_get_global_var('gloria_option');
    $bk_footer_type = '';
    if (isset ($gloria_option['bk-footer-instagram']) && ($gloria_option['bk-footer-instagram'] == 1)) {
        $bk_footer_type = '2';
    }else {
        $bk_footer_type = '1';
    }
    if ($bk_footer_type == '1') {
        get_template_part( 'library/templates/footers/footer1' );
    } else {
        get_template_part( 'library/templates/footers/footer2' );
    }
?>
    </div> <!-- Close Page inner Wrap -->

	</div> <!-- Close Page Wrap -->
    <?php wp_footer(); ?>
</body>

</html>