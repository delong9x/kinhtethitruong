<?php
/* 
 * Sections Configuration
 */
 if ( ! function_exists( 'bk_init_sections' ) ) {
	function bk_init_sections() {
		$sections = array(
            'fullwidth'=>esc_html__('FullWidth Section','gloria'), 'has-rsb' => esc_html__('Content Section', 'gloria')
		);
		wp_localize_script( 'gloria-page-builder-js', 'bk_sections', $sections );
        $modules = array(
            'main_feature' => array(
				'title' => esc_html__( 'BK Grid With Center Slider', 'gloria' ),
				'options' => array(
                    'title' => array(
						'title' => esc_html__('Title', 'gloria' ),
						'description' => esc_html__( 'The Module title', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'limit' => array(
						'title' => esc_html__('Number of Posts', 'gloria' ),
						'description' => esc_html__( 'Enter the number of posts', 'gloria' ),
						'field' => 'number',
						'default' => 7
					),
                    'offset' => array(
						'title' => esc_html__('Offset', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order' => array(
						'title' => esc_html__('Order', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'feature' => array(
						'title' => esc_html__('Display featured posts in slider', 'gloria'),
                        'description' => esc_html__( 'Yes: Display featured posts', 'gloria' ),
						'field' => 'select',
						'default' => 'no',
						'options' => array(
							'yes' => esc_html__( 'Yes', 'gloria' ),
							'no' => esc_html__( 'No', 'gloria' ),
						),
					),      
                    'bg_color' => array(
						'title' => esc_html__('Background Color', 'gloria' ),
						'description' => esc_html__( 'Choose Background Color', 'gloria' ),
						'field' => 'color',
						'default' => '#f8f8f8',
					),              
					'category' => array(
						'title' => esc_html__('Category', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'category',
						'default' => 'All',
					),
				),
			),   
            'grid_carousel' => array(
				'title' => esc_html__( 'BK Feature 1 (Grid Carousel)', 'gloria' ),
				'options' => array(
                    'title' => array(
						'title' => esc_html__('Title', 'gloria' ),
						'description' => esc_html__( 'The Module title', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'limit' => array(
						'title' => esc_html__('Number of Posts', 'gloria' ),
						'description' => esc_html__( 'Enter the number of posts', 'gloria' ),
						'field' => 'number',
						'default' => 8
					),
                    'offset' => array(
						'title' => esc_html__('Offset', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order' => array(
						'title' => esc_html__('Order', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'feature' => array(
						'title' => esc_html__('Display featured posts in slider', 'gloria'),
                        'description' => esc_html__( 'Yes: Display featured posts', 'gloria' ),
						'field' => 'select',
						'default' => 'no',
						'options' => array(
							'yes' => esc_html__( 'Yes', 'gloria' ),
							'no' => esc_html__( 'No', 'gloria' ),
						),
					),
					'category' => array(
						'title' => esc_html__('Category', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'category',
						'default' => 'All',
					),
				),
			),
            'feature_2' => array(
				'title' => esc_html__( 'BK Feature 2 (Slick Slider)', 'gloria' ),
				'options' => array(
                    'limit' => array(
						'title' => esc_html__('Number of Posts', 'gloria' ),
						'description' => esc_html__( 'Enter the number of posts', 'gloria' ),
						'field' => 'number',
						'default' => 7
					),
                    'offset' => array(
						'title' => esc_html__('Offset', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order' => array(
						'title' => esc_html__('Order', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'feature' => array(
						'title' => esc_html__('Display featured posts in slider', 'gloria'),
                        'description' => esc_html__( 'Yes: Display featured posts', 'gloria' ),
						'field' => 'select',
						'default' => 'no',
						'options' => array(
							'yes' => esc_html__( 'Yes', 'gloria' ),
							'no' => esc_html__( 'No', 'gloria' ),
						),
					),            
					'category' => array(
						'title' => esc_html__('Category', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'category',
						'default' => 'All',
					),
				),
			), 
            'grid' => array(
				'title' => esc_html__( 'BK Grid', 'gloria' ),
				'options' => array(
                    'title' => array(
						'title' => esc_html__('Title', 'gloria' ),
						'description' => esc_html__( 'The Module title', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'limit' => array(
						'title' => esc_html__('Number of Posts', 'gloria' ),
						'description' => esc_html__( 'Enter the number of posts', 'gloria' ),
						'field' => 'number',
						'default' => 8
					),
                    'offset' => array(
						'title' => esc_html__('Offset', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order' => array(
						'title' => esc_html__('Order', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'feature' => array(
						'title' => esc_html__('Display featured posts in slider', 'gloria'),
                        'description' => esc_html__( 'Yes: Display featured posts', 'gloria' ),
						'field' => 'select',
						'default' => 'no',
						'options' => array(
							'yes' => esc_html__( 'Yes', 'gloria' ),
							'no' => esc_html__( 'No', 'gloria' ),
						),
					),            
					'category' => array(
						'title' => esc_html__('Category', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'category',
						'default' => 'All',
					),
				),
			),
            'video_playlist' => array(
				'title' => esc_html__( 'BK Video Playlist', 'gloria' ),
				'options' => array(
                    'title' => array(
						'title' => esc_html__('Title', 'gloria' ),
						'description' => esc_html__( 'The Module title', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
					'category' => array(
						'title' => esc_html__('Category', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'category',
						'default' => 'All',
					),
                    'limit' => array(
						'title' => esc_html__('Number of Posts', 'gloria' ),
						'description' => esc_html__( 'Enter the number of posts', 'gloria' ),
						'field' => 'number',
						'default' => 1
					),
                    'offset' => array(
						'title' => esc_html__('Offset', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order' => array(
						'title' => esc_html__('Order', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
				),
			),
            'carousel' => array(
				'title' => esc_html__( 'BK Carousel', 'gloria' ),
				'options' => array(
                    'title' => array(
						'title' => esc_html__('Title', 'gloria' ),
						'description' => esc_html__( 'The Module title', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'limit' => array(
						'title' => esc_html__('Number of posts', 'gloria' ),
						'description' => esc_html__( 'Enter the number of posts ( Recommend at least 4 posts ', 'gloria' ),
						'field' => 'number',
						'default' => 5
					),
                    'offset' => array(
						'title' => esc_html__('Offset', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order' => array(
						'title' => esc_html__('Order', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'bg_color' => array(
						'title' => esc_html__('Background Color', 'gloria' ),
						'description' => esc_html__( 'Choose Background Color', 'gloria' ),
						'field' => 'color',
						'default' => '',
					),
					'category' => array(
						'title' => esc_html__('Category', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'category',
						'default' => 'All',
					),
				),
			),
            'row' => array(
				'title' => esc_html__( 'BK Row', 'gloria' ),
				'options' => array(
                    'title' => array(
						'title' => esc_html__('Title', 'gloria' ),
						'description' => esc_html__( 'The Module title', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'limit' => array(
						'title' => esc_html__('Number of Rows', 'gloria' ),
						'description' => esc_html__( 'Enter the number of post rows', 'gloria' ),
						'field' => 'number',
						'default' => 1
					),
                    'offset' => array(
						'title' => esc_html__('Offset', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order' => array(
						'title' => esc_html__('Order', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
					'category' => array(
						'title' => esc_html__('Category', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'category',
						'default' => 'All',
					),
                    'category2' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 2 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset2' => array(
						'title' => esc_html__('Offset for tab 2', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order2' => array(
						'title' => esc_html__('Order for tab 2', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category3' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 3 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset3' => array(
						'title' => esc_html__('Offset for tab 3', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order3' => array(
						'title' => esc_html__('Order for tab 3', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category4' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 4 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset4' => array(
						'title' => esc_html__('Offset for tab 4', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order4' => array(
						'title' => esc_html__('Order for tab 4', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category5' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 5 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset5' => array(
						'title' => esc_html__('Offset for tab 5', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order5' => array(
						'title' => esc_html__('Order for tab 5', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category6' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 6 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset6' => array(
						'title' => esc_html__('Offset for tab 6', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order6' => array(
						'title' => esc_html__('Order for tab 6', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'tabs_amount' => array(
						'title' => esc_html__('Tabs Amount', 'gloria' ),
                        'description' => esc_html__( 'Add number of tabs will be used in this module', 'gloria' ),
						'field' => 'select',
						'default' => '1',
						'options' => array(
                            '1' => esc_html__( '1', 'gloria' ),
							'2' => esc_html__( '2', 'gloria' ),
							'3' => esc_html__( '3', 'gloria' ),
                            '4' => esc_html__( '4', 'gloria' ),
							'5' => esc_html__( '5', 'gloria' ),
                            '6' => esc_html__( '6', 'gloria' ),
						),
					),
				),
			),
            'hero' => array(
				'title' => esc_html__( 'BK Hero', 'gloria' ),
				'options' => array(
                    'title' => array(
						'title' => esc_html__('Title', 'gloria' ),
						'description' => esc_html__( 'The Module title', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'limit' => array(
						'title' => esc_html__('Number of Posts', 'gloria' ),
						'description' => esc_html__( 'Enter the number of posts', 'gloria' ),
						'field' => 'number',
						'default' => 6
					),
                    'offset' => array(
						'title' => esc_html__('Offset', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order' => array(
						'title' => esc_html__('Order', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
					'category' => array(
						'title' => esc_html__('Category', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'category',
						'default' => 'All',
					),
                    'category2' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 2 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset2' => array(
						'title' => esc_html__('Offset for tab 2', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order2' => array(
						'title' => esc_html__('Order for tab 2', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category3' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 3 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset3' => array(
						'title' => esc_html__('Offset for tab 3', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order3' => array(
						'title' => esc_html__('Order for tab 3', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category4' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 4 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset4' => array(
						'title' => esc_html__('Offset for tab 4', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order4' => array(
						'title' => esc_html__('Order for tab 4', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category5' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 5 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset5' => array(
						'title' => esc_html__('Offset for tab 5', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order5' => array(
						'title' => esc_html__('Order for tab 5', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category6' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 6 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset6' => array(
						'title' => esc_html__('Offset for tab 6', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order6' => array(
						'title' => esc_html__('Order for tab 6', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'tabs_amount' => array(
						'title' => esc_html__('Tabs Amount', 'gloria' ),
                        'description' => esc_html__( 'Add number of tabs will be used in this module', 'gloria' ),
						'field' => 'select',
						'default' => '1',
						'options' => array(
                            '1' => esc_html__( '1', 'gloria' ),
							'2' => esc_html__( '2', 'gloria' ),
							'3' => esc_html__( '3', 'gloria' ),
                            '4' => esc_html__( '4', 'gloria' ),
							'5' => esc_html__( '5', 'gloria' ),
                            '6' => esc_html__( '6', 'gloria' ),
						),
					),
				),
			),
            'fw_slider' => array(
				'title' => esc_html__( 'BK Fullwidth Slider', 'gloria' ),
				'options' => array(
                    'limit' => array(
						'title' => esc_html__('Number of Posts', 'gloria' ),
						'description' => esc_html__( 'Enter the number of posts', 'gloria' ),
						'field' => 'number',
						'default' => 5
					),
                    'offset' => array(
						'title' => esc_html__('Offset', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order' => array(
						'title' => esc_html__('Order', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
					'category' => array(
						'title' => esc_html__('Category', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'category',
						'default' => 'All',
					),
				),
			),
            'masonry' => array(
				'title' => esc_html__( 'BK Masonry', 'gloria' ),
				'options' => array(
                    'title' => array(
						'title' => esc_html__('Title', 'gloria' ),
						'description' => esc_html__( 'The Module title', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'limit' => array(
						'title' => esc_html__('Number of Posts', 'gloria' ),
						'description' => esc_html__( 'Enter the number of posts will be displayed', 'gloria' ),
						'field' => 'number',
						'default' => 3
					),
                    'ajax_load_number' => array(
						'title' => esc_html__('Ajax post', 'gloria' ),
						'description' => esc_html__( 'Enter the number of posts will be displayed when click ajax load button', 'gloria' ),
						'field' => 'number',
						'default' => 4
					),
                    'load_more_button' => array(
						'title' => esc_html__('Load More Button', 'gloria' ),
                        'description' => esc_html__( 'Enable / Disable Load More Button', 'gloria' ),
						'field' => 'select',
						'default' => 'enable',
						'options' => array(
							'enable' => esc_html__( 'Enable', 'gloria' ),
							'disable' => esc_html__( 'Disable', 'gloria' ),
						),
					),
					'category' => array(
						'title' => esc_html__('Category', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'category',
						'default' => 'All',
					),
                    'offset' => array(
						'title' => esc_html__('Offset', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order' => array(
						'title' => esc_html__('Order', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category2' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 2 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset2' => array(
						'title' => esc_html__('Offset for tab 2', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order2' => array(
						'title' => esc_html__('Order for tab 2', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category3' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 3 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset3' => array(
						'title' => esc_html__('Offset for tab 3', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order3' => array(
						'title' => esc_html__('Order for tab 3', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category4' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 4 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset4' => array(
						'title' => esc_html__('Offset for tab 4', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order4' => array(
						'title' => esc_html__('Order for tab 4', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category5' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 5 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset5' => array(
						'title' => esc_html__('Offset for tab 5', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order5' => array(
						'title' => esc_html__('Order for tab 5', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category6' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 6 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset6' => array(
						'title' => esc_html__('Offset for tab 6', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order6' => array(
						'title' => esc_html__('Order for tab 6', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'tabs_amount' => array(
						'title' => esc_html__('Tabs Amount', 'gloria' ),
                        'description' => esc_html__( 'Add number of tabs will be used in this module', 'gloria' ),
						'field' => 'select',
						'default' => '1',
						'options' => array(
                            '1' => esc_html__( '1', 'gloria' ),
							'2' => esc_html__( '2', 'gloria' ),
							'3' => esc_html__( '3', 'gloria' ),
                            '4' => esc_html__( '4', 'gloria' ),
							'5' => esc_html__( '5', 'gloria' ),
                            '6' => esc_html__( '6', 'gloria' ),
						),
					),
				),
			),
            'square_grid' => array(
				'title' => esc_html__( 'BK Square Grid', 'gloria' ),
				'options' => array(
                    'title' => array(
						'title' => esc_html__('Title', 'gloria' ),
						'description' => esc_html__( 'The Module title', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'limit' => array(
						'title' => esc_html__('Number of posts', 'gloria' ),
						'description' => esc_html__( 'Enter the number of posts ( Recommend at least 4 posts ', 'gloria' ),
						'field' => 'number',
						'default' => 5
					),
                    'offset' => array(
						'title' => esc_html__('Offset', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order' => array(
						'title' => esc_html__('Order', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
					'category' => array(
						'title' => esc_html__('Category', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'category',
						'default' => 'All',
					),
                    'category2' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 2 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset2' => array(
						'title' => esc_html__('Offset for tab 2', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order2' => array(
						'title' => esc_html__('Order for tab 2', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category3' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 3 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset3' => array(
						'title' => esc_html__('Offset for tab 3', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order3' => array(
						'title' => esc_html__('Order for tab 3', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category4' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 4 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset4' => array(
						'title' => esc_html__('Offset for tab 4', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order4' => array(
						'title' => esc_html__('Order for tab 4', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category5' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 5 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset5' => array(
						'title' => esc_html__('Offset for tab 5', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order5' => array(
						'title' => esc_html__('Order for tab 5', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category6' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 6 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset6' => array(
						'title' => esc_html__('Offset for tab 6', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order6' => array(
						'title' => esc_html__('Order for tab 6', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'tabs_amount' => array(
						'title' => esc_html__('Tabs Amount', 'gloria' ),
                        'description' => esc_html__( 'Add number of tabs will be used in this module', 'gloria' ),
						'field' => 'select',
						'default' => '1',
						'options' => array(
                            '1' => esc_html__( '1', 'gloria' ),
							'2' => esc_html__( '2', 'gloria' ),
							'3' => esc_html__( '3', 'gloria' ),
                            '4' => esc_html__( '4', 'gloria' ),
							'5' => esc_html__( '5', 'gloria' ),
                            '6' => esc_html__( '6', 'gloria' ),
						),
					),
				),
			),
            'tiny_row' => array(
				'title' => esc_html__( 'BK Tiny Row', 'gloria' ),
				'options' => array(
                    'title' => array(
						'title' => esc_html__('Title', 'gloria' ),
						'description' => esc_html__( 'The Module title', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'limit' => array(
						'title' => esc_html__('Number of Rows', 'gloria' ),
						'description' => esc_html__( 'Enter the number of post rows', 'gloria' ),
						'field' => 'number',
						'default' => 1
					),
                    'offset' => array(
						'title' => esc_html__('Offset', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order' => array(
						'title' => esc_html__('Order', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
					'category' => array(
						'title' => esc_html__('Category', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'category',
						'default' => 'All',
					),
				),
			),
            'custom_html' => array(
				'title' => esc_html__( 'BK Custom HTML', 'gloria'),
				'options' => array(
                    'title' => array(
						'title' => esc_html__('Title', 'gloria'),
						'description' => esc_html__( 'The Module title', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'custom_html' => array(
						'title' => esc_html__('HTML Code', 'gloria'),
						'description' => esc_html__( 'Put your custom HTML code here', 'gloria'),
						'field' => 'textarea',
						'default' => '',
					),
				),
			), 
            'shortcode' => array(
				'title' => esc_html__( 'BK Short Code', 'gloria'),
				'options' => array(
                    'title' => array(
						'title' => esc_html__('Title', 'gloria'),
						'description' => esc_html__( 'The Module title', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'shortcode' => array(
						'title' => esc_html__('Shortcode', 'gloria'),
						'description' => esc_html__( 'Put Shortcode here', 'gloria'),
						'field' => 'textarea',
						'default' => '',
					),
				),
			),  
            'ads' => array(
				'title' => esc_html__( 'BK Single Ads', 'gloria' ),
				'options' => array(
                    'image_url' => array(
						'title' => esc_html__('Image Url','gloria' ),
						'description' => esc_html__( '', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'url' => array(
						'title' => esc_html__('Url','gloria' ),
						'description' => esc_html__( '', 'gloria' ),
						'field' => 'text',
						'default' => '',
					)
				),
            ),
            'two_col_ads' => array(
				'title' => esc_html__( 'BK Two Cols Ads', 'gloria' ),
				'options' => array(
                    'image_url1' => array(
						'title' => esc_html__('Image Url - (First Column)','gloria' ),
						'description' => esc_html__( '', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'url1' => array(
						'title' => esc_html__('Url - (First Column)','gloria' ),
						'description' => esc_html__( '', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'image_url2' => array(
						'title' => esc_html__('Image Url - (Last Column)','gloria' ),
						'description' => esc_html__( '', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'url2' => array(
						'title' => esc_html__('Url - (Last Column)','gloria' ),
						'description' => esc_html__( '', 'gloria' ),
						'field' => 'text',
						'default' => '',
					)
				),
            ),
            'three_col_ads' => array(
				'title' => esc_html__( 'BK Three Cols Ads', 'gloria' ),
				'options' => array(
                    'image_url1' => array(
						'title' => esc_html__('Image Url - (First Column)','gloria' ),
						'description' => esc_html__( '', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'url1' => array(
						'title' => esc_html__('Url - (First Column)','gloria' ),
						'description' => esc_html__( '', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'image_url2' => array(
						'title' => esc_html__('Image Url - (Middle Column)','gloria' ),
						'description' => esc_html__( '', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'url2' => array(
						'title' => esc_html__('Url - (Middle Column)','gloria' ),
						'description' => esc_html__( '', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'image_url3' => array(
						'title' => esc_html__('Image Url - (Last Column)','gloria' ),
						'description' => esc_html__( '', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'url3' => array(
						'title' => esc_html__('Url - (Last Column)','gloria' ),
						'description' => esc_html__( '', 'gloria' ),
						'field' => 'text',
						'default' => '',
					)
				),
            ),
            'adsense' => array(
				'title' => esc_html__( 'BK Adsense', 'gloria' ),
				'options' => array(
                    'adsense_code' => array(
						'title' => esc_html__('Adsense Code','gloria' ),
						'description' => esc_html__( 'Put your adsense code here', 'gloria' ),
						'field' => 'textarea',
						'default' => '',
					),
				),
            ),
        );
		wp_localize_script( 'gloria-page-builder-js', 'bk_fullwidth_modules', $modules );
        $modules = array(
            'grid' => array(
				'title' => esc_html__( 'BK Grid', 'gloria' ),
				'options' => array(
                    'title' => array(
						'title' => esc_html__('Title', 'gloria' ),
						'description' => esc_html__( 'The Module title', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'limit' => array(
						'title' => esc_html__('Number of Posts', 'gloria' ),
						'description' => esc_html__( 'Enter the number of posts', 'gloria' ),
						'field' => 'number',
						'default' => 7
					),
                    'offset' => array(
						'title' => esc_html__('Offset', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order' => array(
						'title' => esc_html__('Order', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'feature' => array(
						'title' => esc_html__('Display featured posts in slider', 'gloria'),
                        'description' => esc_html__( 'Yes: Display featured posts', 'gloria' ),
						'field' => 'select',
						'default' => 'no',
						'options' => array(
							'yes' => esc_html__( 'Yes', 'gloria' ),
							'no' => esc_html__( 'No', 'gloria' ),
						),
					),            
					'category' => array(
						'title' => esc_html__('Category', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'category',
						'default' => 'All',
					),
				),
			),
            'carousel' => array(
				'title' => esc_html__( 'BK Carousel', 'gloria' ),
				'options' => array(
                    'title' => array(
						'title' => esc_html__('Title', 'gloria' ),
						'description' => esc_html__( 'The Module title', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'limit' => array(
						'title' => esc_html__('Number of posts', 'gloria' ),
						'description' => esc_html__( 'Enter the number of posts ( Recommend at least 4 posts ', 'gloria' ),
						'field' => 'number',
						'default' => 5
					),
                    'offset' => array(
						'title' => esc_html__('Offset', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order' => array(
						'title' => esc_html__('Order', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'bg_color' => array(
						'title' => esc_html__('Background Color', 'gloria' ),
						'description' => esc_html__( 'Choose Background Color', 'gloria' ),
						'field' => 'color',
						'default' => '',
					),
					'category' => array(
						'title' => esc_html__('Category', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'category',
						'default' => 'All',
					),
				),
			),
            'square_grid' => array(
				'title' => esc_html__( 'BK Square Grid', 'gloria' ),
				'options' => array(
                    'title' => array(
						'title' => esc_html__('Title', 'gloria' ),
						'description' => esc_html__( 'The Module title', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'limit' => array(
						'title' => esc_html__('Number of posts', 'gloria' ),
						'description' => esc_html__( 'Enter the number of posts ( Recommend at least 4 posts ', 'gloria' ),
						'field' => 'number',
						'default' => 5
					),
                    'offset' => array(
						'title' => esc_html__('Offset', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order' => array(
						'title' => esc_html__('Order', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
					'category' => array(
						'title' => esc_html__('Category', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'category',
						'default' => 'All',
					),
                    'category2' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 2 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset2' => array(
						'title' => esc_html__('Offset for tab 2', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order2' => array(
						'title' => esc_html__('Order for tab 2', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category3' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 3 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset3' => array(
						'title' => esc_html__('Offset for tab 3', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order3' => array(
						'title' => esc_html__('Order for tab 3', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category4' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 4 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset4' => array(
						'title' => esc_html__('Offset for tab 4', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order4' => array(
						'title' => esc_html__('Order for tab 4', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category5' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 5 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset5' => array(
						'title' => esc_html__('Offset for tab 5', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order5' => array(
						'title' => esc_html__('Order for tab 5', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category6' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 6 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset6' => array(
						'title' => esc_html__('Offset for tab 6', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order6' => array(
						'title' => esc_html__('Order for tab 6', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'tabs_amount' => array(
						'title' => esc_html__('Tabs Amount', 'gloria' ),
                        'description' => esc_html__( 'Add number of tabs will be used in this module', 'gloria' ),
						'field' => 'select',
						'default' => '1',
						'options' => array(
                            '1' => esc_html__( '1', 'gloria' ),
							'2' => esc_html__( '2', 'gloria' ),
							'3' => esc_html__( '3', 'gloria' ),
                            '4' => esc_html__( '4', 'gloria' ),
							'5' => esc_html__( '5', 'gloria' ),
                            '6' => esc_html__( '6', 'gloria' ),
						),
					),
				),
			),
            'block_1' => array(
				'title' => esc_html__( 'BK Post Block 1', 'gloria' ),
				'options' => array(
                    'title' => array(
						'title' => esc_html__('Title', 'gloria' ),
						'description' => esc_html__( 'The Module title', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'offset' => array(
						'title' => esc_html__('Offset for tab 1', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order' => array(
						'title' => esc_html__('Order for tab 1', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
					'category' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 1 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'category',
						'default' => 'All',
					),
                    'category2' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 2 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset2' => array(
						'title' => esc_html__('Offset for tab 2', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order2' => array(
						'title' => esc_html__('Order for tab 2', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category3' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 3 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset3' => array(
						'title' => esc_html__('Offset for tab 3', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order3' => array(
						'title' => esc_html__('Order for tab 3', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category4' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 4 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset4' => array(
						'title' => esc_html__('Offset for tab 4', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order4' => array(
						'title' => esc_html__('Order for tab 4', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category5' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 5 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset5' => array(
						'title' => esc_html__('Offset for tab 5', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order5' => array(
						'title' => esc_html__('Order for tab 5', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category6' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 6 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset6' => array(
						'title' => esc_html__('Offset for tab 6', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order6' => array(
						'title' => esc_html__('Order for tab 6', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'tabs_amount' => array(
						'title' => esc_html__('Tabs Amount', 'gloria' ),
                        'description' => esc_html__( 'Add number of tabs will be used in this module', 'gloria' ),
						'field' => 'select',
						'default' => '1',
						'options' => array(
                            '1' => esc_html__( '1', 'gloria' ),
							'2' => esc_html__( '2', 'gloria' ),
							'3' => esc_html__( '3', 'gloria' ),
                            '4' => esc_html__( '4', 'gloria' ),
							'5' => esc_html__( '5', 'gloria' ),
                            '6' => esc_html__( '6', 'gloria' ),
						),
					),
				),
			),
            'block_2' => array(
				'title' => esc_html__( 'BK Post Block 2', 'gloria' ),
				'options' => array(
                    'title' => array(
						'title' => esc_html__('Title', 'gloria' ),
						'description' => esc_html__( 'The Module title', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'limit' => array(
						'title' => esc_html__('Number of Posts', 'gloria' ),
						'description' => esc_html__( 'Enter the number of posts will be displayed', 'gloria' ),
						'field' => 'number',
						'default' => 5
					),
                    'offset' => array(
						'title' => esc_html__('Offset', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order' => array(
						'title' => esc_html__('Order', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
					'category' => array(
						'title' => esc_html__('Category', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'category',
						'default' => 'All',
					),
                    'category2' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 2 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset2' => array(
						'title' => esc_html__('Offset for tab 2', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order2' => array(
						'title' => esc_html__('Order for tab 2', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category3' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 3 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset3' => array(
						'title' => esc_html__('Offset for tab 3', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order3' => array(
						'title' => esc_html__('Order for tab 3', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category4' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 4 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset4' => array(
						'title' => esc_html__('Offset for tab 4', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order4' => array(
						'title' => esc_html__('Order for tab 4', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category5' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 5 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset5' => array(
						'title' => esc_html__('Offset for tab 5', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order5' => array(
						'title' => esc_html__('Order for tab 5', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category6' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 6 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset6' => array(
						'title' => esc_html__('Offset for tab 6', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order6' => array(
						'title' => esc_html__('Order for tab 6', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'tabs_amount' => array(
						'title' => esc_html__('Tabs Amount', 'gloria' ),
                        'description' => esc_html__( 'Add number of tabs will be used in this module', 'gloria' ),
						'field' => 'select',
						'default' => '1',
						'options' => array(
                            '1' => esc_html__( '1', 'gloria' ),
							'2' => esc_html__( '2', 'gloria' ),
							'3' => esc_html__( '3', 'gloria' ),
                            '4' => esc_html__( '4', 'gloria' ),
							'5' => esc_html__( '5', 'gloria' ),
                            '6' => esc_html__( '6', 'gloria' ),
						),
					),
				),
			),
			'masonry' => array(
				'title' => esc_html__( 'BK Masonry', 'gloria' ),
				'options' => array(
                    'title' => array(
						'title' => esc_html__('Title', 'gloria' ),
						'description' => esc_html__( 'The Module title', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'limit' => array(
						'title' => esc_html__('Number of Posts', 'gloria' ),
						'description' => esc_html__( 'Enter the number of posts will be displayed', 'gloria' ),
						'field' => 'number',
						'default' => 4
					),
                    'ajax_load_number' => array(
						'title' => esc_html__('Ajax post', 'gloria' ),
						'description' => esc_html__( 'Enter the number of posts will be displayed when click ajax load button', 'gloria' ),
						'field' => 'number',
						'default' => 4
					),
                    'load_more_button' => array(
						'title' => esc_html__('Load More Button', 'gloria' ),
                        'description' => esc_html__( 'Enable / Disable Load More Button', 'gloria' ),
						'field' => 'select',
						'default' => 'enable',
						'options' => array(
							'enable' => esc_html__( 'Enable', 'gloria' ),
							'disable' => esc_html__( 'Disable', 'gloria' ),
						),
					),
					'category' => array(
						'title' => esc_html__('Category', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'category',
						'default' => 'All',
					),
                    'offset' => array(
						'title' => esc_html__('Offset', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order' => array(
						'title' => esc_html__('Order', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category2' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 2 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset2' => array(
						'title' => esc_html__('Offset for tab 2', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order2' => array(
						'title' => esc_html__('Order for tab 2', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category3' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 3 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset3' => array(
						'title' => esc_html__('Offset for tab 3', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order3' => array(
						'title' => esc_html__('Order for tab 3', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category4' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 4 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset4' => array(
						'title' => esc_html__('Offset for tab 4', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order4' => array(
						'title' => esc_html__('Order for tab 4', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category5' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 5 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset5' => array(
						'title' => esc_html__('Offset for tab 5', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order5' => array(
						'title' => esc_html__('Order for tab 5', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category6' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 6 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset6' => array(
						'title' => esc_html__('Offset for tab 6', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order6' => array(
						'title' => esc_html__('Order for tab 6', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'tabs_amount' => array(
						'title' => esc_html__('Tabs Amount', 'gloria' ),
                        'description' => esc_html__( 'Add number of tabs will be used in this module', 'gloria' ),
						'field' => 'select',
						'default' => '1',
						'options' => array(
                            '1' => esc_html__( '1', 'gloria' ),
							'2' => esc_html__( '2', 'gloria' ),
							'3' => esc_html__( '3', 'gloria' ),
                            '4' => esc_html__( '4', 'gloria' ),
							'5' => esc_html__( '5', 'gloria' ),
                            '6' => esc_html__( '6', 'gloria' ),
						),
					),
				),
			),
            'classic_blog' => array(
				'title' => esc_html__( 'BK Classic Blog', 'gloria' ),
				'options' => array(
                    'title' => array(
						'title' => esc_html__('Title', 'gloria' ),
						'description' => esc_html__( 'The Module title', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'limit' => array(
						'title' => esc_html__('Number of Posts', 'gloria' ),
						'description' => esc_html__( 'Enter the number of posts will be displayed', 'gloria' ),
						'field' => 'number',
						'default' => 4
					),
                    'ajax_load_number' => array(
						'title' => esc_html__('Ajax post', 'gloria' ),
						'description' => esc_html__( 'Enter the number of posts will be displayed when click ajax load button', 'gloria' ),
						'field' => 'number',
						'default' => 4
					),
                    'load_more_button' => array(
						'title' => esc_html__('Load More Button', 'gloria' ),
                        'description' => esc_html__( 'Enable / Disable Load More Button', 'gloria' ),
						'field' => 'select',
						'default' => 'enable',
						'options' => array(
							'enable' => esc_html__( 'Enable', 'gloria' ),
							'disable' => esc_html__( 'Disable', 'gloria' ),
						),
					),
					'category' => array(
						'title' => esc_html__('Category', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'category',
						'default' => 'All',
					),
                    'offset' => array(
						'title' => esc_html__('Offset', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order' => array(
						'title' => esc_html__('Order', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category2' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 2 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset2' => array(
						'title' => esc_html__('Offset for tab 2', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order2' => array(
						'title' => esc_html__('Order for tab 2', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category3' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 3 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset3' => array(
						'title' => esc_html__('Offset for tab 3', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order3' => array(
						'title' => esc_html__('Order for tab 3', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category4' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 4 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset4' => array(
						'title' => esc_html__('Offset for tab 4', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order4' => array(
						'title' => esc_html__('Order for tab 4', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category5' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 5 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset5' => array(
						'title' => esc_html__('Offset for tab 5', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order5' => array(
						'title' => esc_html__('Order for tab 5', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category6' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 6 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset6' => array(
						'title' => esc_html__('Offset for tab 6', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order6' => array(
						'title' => esc_html__('Order for tab 6', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'tabs_amount' => array(
						'title' => esc_html__('Tabs Amount', 'gloria' ),
                        'description' => esc_html__( 'Add number of tabs will be used in this module', 'gloria' ),
						'field' => 'select',
						'default' => '1',
						'options' => array(
                            '1' => esc_html__( '1', 'gloria' ),
							'2' => esc_html__( '2', 'gloria' ),
							'3' => esc_html__( '3', 'gloria' ),
                            '4' => esc_html__( '4', 'gloria' ),
							'5' => esc_html__( '5', 'gloria' ),
                            '6' => esc_html__( '6', 'gloria' ),
						),
					),
				),
            ),
            'large_blog' => array(
				'title' => esc_html__( 'BK Large Blog', 'gloria' ),
				'options' => array(
                    'title' => array(
						'title' => esc_html__('Title', 'gloria' ),
						'description' => esc_html__( 'The Module title', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'limit' => array(
						'title' => esc_html__('Number of Posts', 'gloria' ),
						'description' => esc_html__( 'Enter the number of posts will be displayed', 'gloria' ),
						'field' => 'number',
						'default' => 4
					),
                    'ajax_load_number' => array(
						'title' => esc_html__('Ajax post', 'gloria' ),
						'description' => esc_html__( 'Enter the number of posts will be displayed when click ajax load button', 'gloria' ),
						'field' => 'number',
						'default' => 4
					),
                    'load_more_button' => array(
						'title' => esc_html__('Load More Button', 'gloria' ),
                        'description' => esc_html__( 'Enable / Disable Load More Button', 'gloria' ),
						'field' => 'select',
						'default' => 'enable',
						'options' => array(
							'enable' => esc_html__( 'Enable', 'gloria' ),
							'disable' => esc_html__( 'Disable', 'gloria' ),
						),
					),
					'category' => array(
						'title' => esc_html__('Category', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'category',
						'default' => 'All',
					),
                    'offset' => array(
						'title' => esc_html__('Offset', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order' => array(
						'title' => esc_html__('Order', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category2' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 2 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset2' => array(
						'title' => esc_html__('Offset for tab 2', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order2' => array(
						'title' => esc_html__('Order for tab 2', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category3' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 3 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset3' => array(
						'title' => esc_html__('Offset for tab 3', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order3' => array(
						'title' => esc_html__('Order for tab 3', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category4' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 4 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset4' => array(
						'title' => esc_html__('Offset for tab 4', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order4' => array(
						'title' => esc_html__('Order for tab 4', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category5' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 5 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset5' => array(
						'title' => esc_html__('Offset for tab 5', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order5' => array(
						'title' => esc_html__('Order for tab 5', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'category6' => array(
						'title' => esc_html__('------------------------------------ Choose Category for tab 6 ------------------------------------ ', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'categorytab',
						'default' => -1,
					),
                    'offset6' => array(
						'title' => esc_html__('Offset for tab 6', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order6' => array(
						'title' => esc_html__('Order for tab 6', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
                    'tabs_amount' => array(
						'title' => esc_html__('Tabs Amount', 'gloria' ),
                        'description' => esc_html__( 'Add number of tabs will be used in this module', 'gloria' ),
						'field' => 'select',
						'default' => '1',
						'options' => array(
                            '1' => esc_html__( '1', 'gloria' ),
							'2' => esc_html__( '2', 'gloria' ),
							'3' => esc_html__( '3', 'gloria' ),
                            '4' => esc_html__( '4', 'gloria' ),
							'5' => esc_html__( '5', 'gloria' ),
                            '6' => esc_html__( '6', 'gloria' ),
						),
					),
				),
            ), 
            'tiny_row' => array(
				'title' => esc_html__( 'BK Tiny Row', 'gloria' ),
				'options' => array(
                    'title' => array(
						'title' => esc_html__('Title', 'gloria' ),
						'description' => esc_html__( 'The Module title', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'limit' => array(
						'title' => esc_html__('Number of Rows', 'gloria' ),
						'description' => esc_html__( 'Enter the number of post rows', 'gloria' ),
						'field' => 'number',
						'default' => 1
					),
                    'offset' => array(
						'title' => esc_html__('Offset', 'gloria' ),
						'description' => esc_html__( 'Enter the offset number', 'gloria' ),
						'field' => 'number',
						'default' => 0
					),
                    'order' => array(
						'title' => esc_html__('Order', 'gloria' ),
                        'description' => esc_html__( 'Display random posts or latest posts from categories - (IMPORTANT) IF ORDER = Random, Ajax Load More Button (If have) will be disabled', 'gloria' ),
						'field' => 'select',
						'default' => 'date',
						'options' => array(
							'date' => esc_html__( 'Latest', 'gloria' ),
							'rand' => esc_html__( 'Random', 'gloria' ),
						),
					),
					'category' => array(
						'title' => esc_html__('Category', 'gloria' ),
						'description' => esc_html__( 'Choose a post category to be shown up', 'gloria' ),
						'field' => 'category',
						'default' => 'All',
					),
				),
			),
            'custom_html' => array(
				'title' => esc_html__( 'BK Custom HTML', 'gloria'),
				'options' => array(
                    'title' => array(
						'title' => esc_html__('Title', 'gloria'),
						'description' => esc_html__( 'The Module title', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'custom_html' => array(
						'title' => esc_html__('HTML Code', 'gloria'),
						'description' => esc_html__( 'Put your custom HTML code here', 'gloria'),
						'field' => 'textarea',
						'default' => '',
					),
				),
			), 
            'shortcode' => array(
				'title' => esc_html__( 'BK Short Code', 'gloria'),
				'options' => array(
                    'title' => array(
						'title' => esc_html__('Title', 'gloria'),
						'description' => esc_html__( 'The Module title', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'shortcode' => array(
						'title' => esc_html__('Shortcode', 'gloria'),
						'description' => esc_html__( 'Put Shortcode here', 'gloria'),
						'field' => 'textarea',
						'default' => '',
					),
				),
			),  
            'ads' => array(
				'title' => esc_html__( 'BK Single Ads', 'gloria' ),
				'options' => array(
                    'image_url' => array(
						'title' => esc_html__('Image Url','gloria' ),
						'description' => esc_html__( '', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'url' => array(
						'title' => esc_html__('Url','gloria' ),
						'description' => esc_html__( '', 'gloria' ),
						'field' => 'text',
						'default' => '',
					)
				),
            ),
            'two_col_ads' => array(
				'title' => esc_html__( 'BK Two Cols Ads', 'gloria' ),
				'options' => array(
                    'image_url1' => array(
						'title' => esc_html__('Image Url - (First Column)','gloria' ),
						'description' => esc_html__( '', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'url1' => array(
						'title' => esc_html__('Url - (First Column)','gloria' ),
						'description' => esc_html__( '', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'image_url2' => array(
						'title' => esc_html__('Image Url - (Last Column)','gloria' ),
						'description' => esc_html__( '', 'gloria' ),
						'field' => 'text',
						'default' => '',
					),
                    'url2' => array(
						'title' => esc_html__('Url - (Last Column)','gloria' ),
						'description' => esc_html__( '', 'gloria' ),
						'field' => 'text',
						'default' => '',
					)
				),
            ),
            'adsense' => array(
				'title' => esc_html__( 'BK Adsense', 'gloria' ),
				'options' => array(
                    'adsense_code' => array(
						'title' => esc_html__('Adsense Code','gloria' ),
						'description' => esc_html__( 'Put your adsense code here', 'gloria' ),
						'field' => 'textarea',
						'default' => '',
					),
				),
            ),
        );
		wp_localize_script( 'gloria-page-builder-js', 'bk_has_rsb_modules', $modules );
        $modules = array(
        
        );
		wp_localize_script( 'gloria-page-builder-js', 'bk_has_innersb_left_modules', $modules );
        $modules = array(

        );
		wp_localize_script( 'gloria-page-builder-js', 'bk_has_innersb_right_modules', $modules );
	}
} 