<?php
add_action( 'widgets_init', 'bk_feedburner_widget_register' );
function bk_feedburner_widget_register() {
	register_widget( 'bk_feedburner_widget' );
}
class bk_feedburner_widget extends WP_Widget {

	function __construct() {
		$widget_ops = array( 'classname' => 'widget-feedburner' , 'description' => 'Subscribe to feedburner via email' );
		parent::__construct( 'widget-feedburner',esc_html__('*BK: Widget Feedburner','gloria') , $widget_ops );
	}
	
	function widget( $args, $instance ) {
		extract( $args );
		$title = apply_filters('widget_title', $instance['title'] );
		$text_description = $instance['text'] ;
		$feedburner = $instance['feedburner'];
        echo ($before_widget);?>
        <div class="feedburner-inner">
		<?php
        if ( $title ) {?>
            <h3>
                <?php echo esc_html($title);?>
            </h3>
        <?php }
        if ( $text_description ) {?>
            <p>
                <?php echo esc_html($text_description);?>
            </p>
        <?php }?>
		<form class="clearfix" action="http://feedburner.google.com/fb/a/mailverify" method="post" target="popupwindow" onsubmit="window.open('http://feedburner.google.com/fb/a/mailverify?uri=<?php echo ($feedburner) ; ?>', 'popupwindow', 'scrollbars=yes,width=550,height=520');return true">
			<input class="feedburner-email" type="text" name="email" value="<?php esc_html_e( 'Enter your e-mail address', 'gloria' ) ; ?>" onfocus="if (this.value == '<?php esc_html_e( 'Enter your e-mail address', 'gloria' ) ; ?>') {this.value = '';}" onblur="if (this.value == '') {this.value = '<?php esc_html_e( 'Enter your e-mail address', 'gloria' ) ; ?>';}">
			<input type="hidden" value="<?php echo ($feedburner) ; ?>" name="uri">
			<input type="hidden" name="loc" value="en_US">		
            <span class="feedburner-subscribe">	
            <button class="" type="submit">
                <i class="fa fa-envelope-o"></i>
            </button> 
            </span>
		</form>
        </div>
		<?php
		echo $after_widget;			
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['text'] = $new_instance['text'] ;
		$instance['feedburner'] = strip_tags( $new_instance['feedburner'] );

		return $instance;
	}

	function form( $instance ) {
		$defaults = array( 'title' =>esc_html__( 'FeedBurner Widget' , 'gloria'), 'text' => esc_html__( "Sign up to receive email updates and to hear what's going on with our magazine!" , 'gloria'));
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e( 'Title:' , 'gloria') ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php if( !empty($instance['title']) ) echo $instance['title']; ?>" class="widefat" type="text" />
		</p>
        <p>
			<label for="<?php echo $this->get_field_id( 'text' ); ?>"><?php esc_html_e( 'Text description:' , 'gloria') ?></label>
			<input id="<?php echo $this->get_field_id( 'text' ); ?>" name="<?php echo $this->get_field_name( 'text' ); ?>" value="<?php if( !empty($instance['text']) ) echo $instance['text']; ?>" class="widefat" type="text" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'feedburner' ); ?>"><?php esc_html_e( 'Feedburner ID:' , 'gloria') ?></label>
			<input id="<?php echo $this->get_field_id( 'feedburner' ); ?>" name="<?php echo $this->get_field_name( 'feedburner' ); ?>" value="<?php if( !empty($instance['feedburner']) )  echo $instance['feedburner']; ?>" class="widefat" type="text" />
		</p>


	<?php
	}
}
?>