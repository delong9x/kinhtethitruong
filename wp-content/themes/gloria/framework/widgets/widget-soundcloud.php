<?php
add_action( 'widgets_init', 'bk_register_soundcloud_widget' );
function bk_register_soundcloud_widget() {
	register_widget( 'bk_soundcloud' );
}
class bk_soundcloud extends WP_Widget {

    function __construct() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'bk-soundcloud', 'description' => esc_html__('Displays SoundCloud widget in Sidebar or Footer.', 'gloria') );
		/* Create the widget. */
		parent::__construct( 'bk-soundcloud-widget', esc_html__('*BK: SoundCloud Widget', 'gloria'), $widget_ops);
	}
	
	function widget( $args, $instance ) {
		extract( $args );

		$title = apply_filters('widget_title', $instance['title'] );
		$url = $instance['url'];
		$autoplay = $instance['autoplay'];
		
		$play = 'false';
		if( !empty( $autoplay )) $play = 'true';
        echo ($before_widget);
        if ( $title ) {?>
            <div class="widget-title-wrap">
                <?php echo ($before_title . esc_html($title) . $after_title);?>
            </div>
        <?php }
			echo gloria_core::bk_soundcloud_iframe_output( $url , $play );
			echo ($after_widget);
				
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['url'] = $new_instance['url'] ;
		$instance['autoplay'] = strip_tags( $new_instance['autoplay'] );
		return $instance;
	}

	function form( $instance ) {
		$defaults = array( 'title' => 'SoundCloud'  );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e( 'Title:' , 'gloria') ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php if( !empty($instance['title']) ) echo $instance['title']; ?>" class="widefat" type="text" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'url' ); ?>"><?php esc_html_e( 'URL:' , 'gloria') ?></label>
			<input id="<?php echo $this->get_field_id( 'url' ); ?>" name="<?php echo $this->get_field_name( 'url' ); ?>" value="<?php if( !empty($instance['url']) ) echo $instance['url']; ?>" type="text" class="widefat" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'autoplay' ); ?>"><?php esc_html_e( 'Autoplay:' , 'gloria') ?></label>
			<input id="<?php echo $this->get_field_id( 'autoplay' ); ?>" name="<?php echo $this->get_field_name( 'autoplay' ); ?>" value="true" <?php if( !empty( $instance['autoplay'] ) ) echo 'checked="checked"'; ?> type="checkbox" />
		</p>


	<?php
	}
}
?>