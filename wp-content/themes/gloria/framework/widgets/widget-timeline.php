<?php
/**
 * Plugin Name: BK-Ninja: Timeline Widget
 * Plugin URI: http://bk-ninja.com
 * Description: Displays ads in any section.
 * Version: 1.0
 * Author: BK-Ninja
 * Author URI: http://BK-Ninja.com
 *
 */
add_action( 'widgets_init', 'bk_register_timeline_widget' );
function bk_register_timeline_widget() {
	register_widget( 'bk_timeline_widget' );
}
class bk_timeline_widget extends WP_Widget {

	function __construct() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'timeline-posts', 'description' => esc_html__('Displays Timeline posts in Sidebar or Footer.', 'gloria') );

		/* Create the widget. */
		parent::__construct( 'timeline-posts-widget', esc_html__('*BK: Timeline Widget', 'gloria'), $widget_ops);
	}
	
	function widget( $args, $instance ) {
		extract( $args );

		$title = apply_filters('widget_title', $instance['title'] );
		$no_of_posts = $instance['no_of_posts'];
		$cat_id = $instance['category'];
        echo ($before_widget);
        if ( $title ) {?>
            <div class="widget-title-wrap">
                <?php echo ($before_title . esc_html($title) . $after_title);?>
            </div>
        <?php }?>
			<ul>
				<?php gloria_core::bk_posts_timeline($no_of_posts , $cat_id)?>	
			</ul>
	<?php 
		echo ($after_widget);
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['no_of_posts'] = strip_tags( $new_instance['no_of_posts'] );
		
		$instance['category'] = implode(',' , $new_instance['category']  );

		return $instance;
	}

	function form( $instance ) {
		$defaults = array( 'title' =>esc_html__( 'Timeline' , 'gloria'), 'no_of_posts' => '5' , 'category' => 'all' );
		$instance = wp_parse_args( (array) $instance, $defaults );
		?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e( 'Title:' , 'gloria') ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" class="widefat" type="text" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'no_of_posts' ); ?>"><?php esc_html_e( 'Number of posts to show:' , 'gloria') ?></label>
			<input id="<?php echo $this->get_field_id( 'no_of_posts' ); ?>" name="<?php echo $this->get_field_name( 'no_of_posts' ); ?>" value="<?php echo $instance['no_of_posts']; ?>" type="text" size="3" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('category'); ?>"><strong><?php esc_html_e('Filter by Category: ','gloria');?></strong></label> 
			<select id="<?php echo $this->get_field_id('category'); ?>" name="<?php echo $this->get_field_name('category'); ?>" class="widefat categories" style="width:100%;">
				<option value='all' <?php if ('all' == $instance['category']) echo 'selected="selected"'; ?>><?php esc_html_e( 'All Categories', 'gloria' ); ?></option>
				<?php $categories = get_categories('hide_empty=1&type=post'); ?>
				<?php foreach($categories as $category) { ?>
				<option value='<?php echo $category->term_id; ?>' <?php if ($category->term_id == $instance['category']) echo 'selected="selected"'; ?>><?php echo $category->cat_name; ?></option>
				<?php } ?>
			</select>
		</p>

	<?php
	}
}
?>