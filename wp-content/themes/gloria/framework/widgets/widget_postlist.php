<?php
/**
 * Plugin Name: BK-Ninja: Post list Widget
 * Plugin URI: http://bk-ninja.com
 * Description: Displays List of Posts in sidebar or Footer.
 * Version: 1.0
 * Author: BK-Ninja
 * Author URI: http://BK-Ninja.com
 *
 */
/**
 * Add function to widgets_init that'll load our widget.
 */
add_action('widgets_init', 'bk_register_postlist_widget');

function bk_register_postlist_widget(){
	register_widget('bk_postlist_widget');
}

class bk_postlist_widget extends WP_Widget {
    
/**
 * Widget setup.
 */
	function __construct() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'widget-postlist', 'description' => esc_html__('Displays List of Posts in sidebar or Footer.', 'gloria') );

		/* Create the widget. */
		parent::__construct( 'bk_postlist_widget', esc_html__('*BK: Widget Post List', 'gloria'), $widget_ops);
	}

	function widget( $args, $instance ) {
	extract($args);
        $title = apply_filters('widget_title', $instance['title'] );
		$entries_display = esc_attr($instance['entries_display']);
        $posts_order = $instance['posts_order'];
		$cat_id = $instance['category'];
        echo ($before_widget);
        if ( $title ) {?>
            <div class="widget-title-wrap">
                <?php echo ($before_title . esc_html($title) . $after_title);?>
            </div>
        <?php }
        $render_modules = '';
        $bk_contentout2 = new gloria_contentout2;  
        $bk_contentout3 = new gloria_contentout3; 
           
		$args = array(
				'cat' => $cat_id,
				'post_status' => 'publish',
				'ignore_sticky_posts' => 1,
				'posts_per_page' => $entries_display
                );
        if ( $posts_order == 'random' ) {
			$args[ 'orderby' ] = 'rand';
		}else if ( $posts_order == 'popular_comments' ) {
			$args[ 'orderby' ] = 'comment_count';		
		}else if ( $posts_order == 'popular_views' ) {
            $args[ 'meta_key' ] = 'post_views_count';		
			$args[ 'orderby' ] = 'meta_value_num';		
		}else if ( $posts_order == 'best' ) {
			$args[ 'meta_key' ] = 'bk_final_score';		
            $args[ 'orderby' ] = 'meta_value_num';	
            $args[ 'meta_query' ] = array(
                				array(
                					'key' => 'bk_review_checkbox',
                					'value' => '1',
                				)
                             );
		}
        ?>
        <?php $the_query = new WP_Query( $args ); 
		$render_modules .= '<div class="post-list-wrap">';

                if ( $the_query->have_posts() ) :

                    foreach( range( 1, 1) as $i ):
                        $the_query->the_post();
                        $bkThumbId = get_post_thumbnail_id( get_the_ID() );
                        $bkThumbUrl = wp_get_attachment_image_src( $bkThumbId, 'full' );
                        $bkReviewSW = get_post_meta(get_the_ID(),'bk_review_checkbox',true);
                        $render_modules .= '<div class="large-post" itemscope itemtype="http://schema.org/Article">';
                        $render_modules .=  '<a href="'.get_permalink(get_the_ID()).'"><div class="thumb" data-type="background" style="background-image: url('.$bkThumbUrl[0].')"></div></a>';

                        $bk_final_score = get_post_meta(get_the_ID(), 'bk_final_score', true );
                        if(( $bkReviewSW == '1' ) && isset($bk_final_score) && ($bk_final_score != '') && ($bk_final_score != 0)) {$render_modules .=  gloria_core::bk_get_review_score($bk_final_score);}
 
                        $render_modules .=  '<div class="post-c-wrap">';
                        $render_modules .=   gloria_core::bk_meta_cases('cat', get_the_ID());
                        $render_modules .=   gloria_core::bk_get_post_title(get_the_ID(), 15);
                        $render_modules .=   gloria_core::bk_get_post_excerpt(30);
                        $render_modules .=  '</div>';
                        $render_modules .=   gloria_core::bk_get_article_info(get_the_ID());
                        $render_modules .= '</div>';
                    endforeach;        
        
                endif;
                
                if ( $the_query->have_posts() ) :
                    $meta_ar = array('author', 'date');
                    $post_args = array (
                        'thumbnail_size'    => 'gloria_130_130',
                        'meta_ar'           => $meta_ar,
                        'cat_meta'            => '',
                        'rm_btn'            => true,
                    );
                    $render_modules .= '<div class="clearfix">';
                    $render_modules .= '<div class="list-small-post"><ul>';
                    foreach( range( 1, $the_query->post_count - 1) as $i ):
                        $the_query->the_post();
                        $postFormat = gloria_core::bk_post_format_detect(get_the_ID());
                        if($postFormat['format'] === 'video') {
                            $post_args['video_icon'] = true;
                        }else {
                            $post_args['video_icon'] = '';
                        }
                        $render_modules .= '<li class="small-post content_out clearfix">';
                        $render_modules .= $bk_contentout3->render($post_args);
                        $render_modules .= '</li><!-- End post -->';        
                    endforeach;
                    $render_modules .= '</ul> <!-- End list-post -->';
                    $render_modules .= '</div></div><!-- End Column -->';
                endif;  

        $render_modules .= '</div>';
        echo ($render_modules);
		echo ($after_widget);
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$new_instance = wp_parse_args( (array) $new_instance, $this->default );
		$instance['title'] = strip_tags($new_instance['title']); 
		$instance['category'] = $new_instance['category'];
        $instance['posts_order'] = strip_tags( $new_instance['posts_order'] );
		$instance['entries_display'] = $new_instance['entries_display'];

		return $instance;
	}

	function form( $instance ) {
        $defaults = array('title' => '', 'category' => 'all', 'posts_order' => 'latest', 'entries_display' => 5);
		$instance = wp_parse_args((array) $instance, $defaults); ?>
        
        <p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><strong><?php esc_html_e('Title:', 'gloria'); ?></strong></label>
			<input type="text" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
		</p>
        
        <p>
			<label for="<?php echo $this->get_field_id('category'); ?>"><strong><?php esc_html_e('Filter by Category: ','gloria');?></strong></label> 
			<select id="<?php echo $this->get_field_id('category'); ?>" name="<?php echo $this->get_field_name('category'); ?>" class="widefat categories" style="width:100%;">
				<option value='all' <?php if ('all' == $instance['category']) echo 'selected="selected"'; ?>><?php esc_html_e( 'All Categories', 'gloria' ); ?></option>
				<?php $categories = get_categories('hide_empty=1&type=post'); ?>
				<?php foreach($categories as $category) { ?>
				<option value='<?php echo $category->term_id; ?>' <?php if ($category->term_id == $instance['category']) echo 'selected="selected"'; ?>><?php echo $category->cat_name; ?></option>
				<?php } ?>
			</select>
		</p>
        
        <p>
			<label for="<?php echo $this->get_field_id( 'posts_order' ); ?>">Posts order : </label>
			<select id="<?php echo $this->get_field_id( 'posts_order' ); ?>" name="<?php echo $this->get_field_name( 'posts_order' ); ?>" >
				<option value="latest" <?php if( !empty($instance['posts_order']) && $instance['posts_order'] == 'latest' ) echo "selected=\"selected\""; else echo ""; ?>>Most recent</option>
				<option value="random" <?php if( !empty($instance['posts_order']) && $instance['posts_order'] == 'random' ) echo "selected=\"selected\""; else echo ""; ?>>Random</option>
				<option value="popular_views" <?php if( !empty($instance['posts_order']) && $instance['posts_order'] == 'popular_views' ) echo "selected=\"selected\""; else echo ""; ?>>Popular/Views</option>
                <option value="popular_comments" <?php if( !empty($instance['posts_order']) && $instance['posts_order'] == 'popular_comments' ) echo "selected=\"selected\""; else echo ""; ?>>Popular/Comments</option>
				<option value="best" <?php if( !empty($instance['posts_order']) && $instance['posts_order'] == 'best' ) echo "selected=\"selected\""; else echo ""; ?>>Best Reviews</option>
			</select>
		</p>
        
		<p><label for="<?php echo $this->get_field_id( 'entries_display' ); ?>"><strong><?php esc_html_e('Number of entries to display: ', 'gloria'); ?></strong></label>
		<input type="text" id="<?php echo $this->get_field_id('entries_display'); ?>" name="<?php echo $this->get_field_name('entries_display'); ?>" value="<?php echo $instance['entries_display']; ?>" style="width:100%;" /></p>

<?php
	}
}
