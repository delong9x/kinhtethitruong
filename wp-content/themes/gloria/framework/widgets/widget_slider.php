<?php
/**
 * Plugin Name: BK-Ninja: Slider Widget
 * Plugin URI: http://bk-ninja.com
 * Description: Slider widget in sidebar
 * Version: 1.0
 * Author: BK-Ninja
 * Author URI: http://bk-ninja.com
 *
 */

/**
 * Add function to widgets_init that'll load our widget.
 */
add_action('widgets_init', 'bk_register_slider_widget');

function bk_register_slider_widget(){
	register_widget('bk_slider');
}

/**
 * This class handles everything that needs to be handled with the widget:
 * the settings, form, display, and update.  Nice!
 *
 */ 
class bk_slider extends WP_Widget {
	
	/**
	 * Widget setup.
	 */
	function __construct(){
		/* Widget settings. */	
		$widget_ops = array('classname' => 'widget_slider', 'description' => esc_html__('[Sidebar widget] Displays a slider in sidebar.', 'gloria'));
		
		/* Create the widget. */
		parent::__construct('bk_slider', esc_html__('*BK: Widget Slider','gloria'), $widget_ops);
	}
	
	/**
	 * display the widget on the screen.
	 */
	function widget($args, $instance){	
		extract($args);
        $title = apply_filters('widget_title', $instance['title'] );
		$entries_display = esc_attr($instance['entries_display']);
		$cat_id = $instance['category'];
        echo ($before_widget);
        if ( $title ) {?>
            <div class="widget-title-wrap">
                <?php echo ($before_title . esc_html($title) . $after_title);?>
            </div>
        <?php }
              
		$args = array(
				'cat' => $cat_id,
				'post_status' => 'publish',
				'ignore_sticky_posts' => 1,
				'posts_per_page' => $entries_display
                );
        ?>
		<div class="flexslider">
			<ul class="slides">
				<?php $query = new WP_Query( $args ); ?>
				<?php while($query->have_posts()): $query->the_post(); ?>
                        <?php $postid = get_the_ID();?>		
                        <li class="content_in">
                            <div class="bk-article-wrapper" itemscope itemtype="http://schema.org/Article">
                                <?php 
                                    $bkThumbId = get_post_thumbnail_id( get_the_ID() );
                                    $bkThumbUrl = wp_get_attachment_image_src( $bkThumbId, 'full' );
                                    $bkReviewSW = get_post_meta(get_the_ID(),'bk_review_checkbox',true);
                                    echo '<a href="'.get_permalink(get_the_ID()).'"><div class="thumb" data-type="background" style="background-image: url('.$bkThumbUrl[0].')"></div></a>';
                                ?>
                                <?php 
                                    $bk_final_score = get_post_meta(get_the_ID(), 'bk_final_score', true );
                                    if(( $bkReviewSW == '1' ) && isset($bk_final_score) && ($bk_final_score != '') && ($bk_final_score != 0)) {echo gloria_core::bk_get_review_score($bk_final_score);}
                                ?> 
                                <div class="post-c-wrap">
                                    <?php echo gloria_core::bk_meta_cases('cat', get_the_ID());?>  
                                    <?php echo gloria_core::bk_get_post_title(get_the_ID(), 15);?>
                                    <?php echo gloria_core::bk_get_post_excerpt(30);?>
                                </div>
                                <?php echo gloria_core::bk_get_article_info(get_the_ID());?>	
                            </div>
						</li>	
                    																				
				<?php endwhile; ?>
			</ul>
		</div>			
		<?php
		echo ($after_widget);
	}
	
	/**
	 * update widget settings
	 */
	function update($new_instance, $old_instance){
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']); 
		$instance['category'] = $new_instance['category'];
		$instance['entries_display'] = $new_instance['entries_display'];
		return $instance;
	}
	
	/**
	 * Displays the widget settings controls on the widget panel.
	 * Make use of the get_field_id() and get_field_name() function
	 * when creating your form elements. This handles the confusing stuff.
	 */	
	function form($instance){
		$defaults = array('title' => '', 'category' => 'all', 'entries_display' => 5);
		$instance = wp_parse_args((array) $instance, $defaults); ?>
        
		<!-- Title: Text Input -->     
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><strong><?php esc_html_e('Title: ','gloria');?></strong></label>
            <input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
		</p>
        		
		<p>
			<label for="<?php echo $this->get_field_id('category'); ?>"><strong><?php esc_html_e('Filter by Category: ','gloria');?></strong></label> 
			<select id="<?php echo $this->get_field_id('category'); ?>" name="<?php echo $this->get_field_name('category'); ?>" class="widefat categories" style="width:100%;">
				<option value='all' <?php if ('all' == $instance['category']) echo 'selected="selected"'; ?>><?php esc_html_e( 'All Categories', 'gloria' ); ?></option>
				<?php $categories = get_categories('hide_empty=1&type=post'); ?>
				<?php foreach($categories as $category) { ?>
				<option value='<?php echo $category->term_id; ?>' <?php if ($category->term_id == $instance['category']) echo 'selected="selected"'; ?>><?php echo $category->cat_name; ?></option>
				<?php } ?>
			</select>
		</p>
        
		<p><label for="<?php echo $this->get_field_id( 'entries_display' ); ?>"><strong><?php esc_html_e('Number of entries to display: ', 'gloria'); ?></strong></label>
		<input type="text" id="<?php echo $this->get_field_id('entries_display'); ?>" name="<?php echo $this->get_field_name('entries_display'); ?>" value="<?php echo $instance['entries_display']; ?>" style="width:100%;" /></p>

	<?php }
}
?>