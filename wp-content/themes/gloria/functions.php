<?php
require_once (get_template_directory().'/framework/page-builder/controller/bk_pd_template.php');
require_once (get_template_directory().'/framework/page-builder/controller/bk_pd_cfg.php');
require_once (get_template_directory().'/framework/page-builder/controller/bk_pd_save.php');
require_once (get_template_directory().'/framework/page-builder/controller/bk_pd_del.php');
require_once (get_template_directory().'/framework/page-builder/controller/render-sections.php');
require_once (get_template_directory().'/framework/page-builder/bk_pd_start.php');
require_once (get_template_directory().'/plugins/sitelinks-search-box.php');

require_once (get_template_directory().'/framework/taxonomy-meta/taxonomy-meta.php');
require_once (get_template_directory().'/library/taxonomy-meta-config.php');

/**-------------------------------------------------------------------------------------------------------------------------
 * remove redux sample config & notice
 */
if ( ! function_exists( 'gloria_redux_remove_notice' ) ) {
	function gloria_redux_remove_notice() {
		if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
			remove_filter( 'plugin_row_meta', array( ReduxFrameworkPlugin::instance(), 'plugin_metalinks' ), null, 2 );
			remove_action( 'admin_notices', array( ReduxFrameworkPlugin::get_instance(), 'admin_notices' ) );
		}
	}
	add_action( 'redux/loaded', 'gloria_redux_remove_notice' );
}
/**-------------------------------------------------------------------------------------------------------------------------
 * remove redux admin page
 */
if ( ! function_exists( 'gloria_remove_redux_page' ) ) {
	function gloria_remove_redux_page() {
		remove_submenu_page( 'tools.php', 'redux-about' );
	}
	add_action( 'admin_menu', 'gloria_remove_redux_page', 12 );
}

/**
 * Load the TGM Plugin Activator class to notify the user
 * to install the Envato WordPress Toolkit Plugin
 */
require_once( get_template_directory() . '/inc/class-tgm-plugin-activation.php' );
if ( ! function_exists( 'tgmpa_register_toolkit' ) ) {
    function tgmpa_register_toolkit() {
        // Specify the Envato Toolkit plugin
        $plugins = array(
            array(
                'name' => esc_html__('Login With Ajax', 'gloria'),
                'slug' => 'login-with-ajax',
                'title' => esc_html__('Login With Ajax - Optional', 'gloria'),
                'img' => get_template_directory_uri() . '/images/plugins/login-with-ajax.jpg',
                'source' => get_template_directory() . '/plugins/login-with-ajax.zip',
                'required' => false,
                'version' => '3.1.6',
                'force_activation' => false,
                'force_deactivation' => false,
                'external_url' => '',
            ),
            array(
                'name' => esc_html__('Sidebar Generator', 'gloria'),
                'slug' => 'gloria-sidebar-generator',
                'title' => esc_html__('Sidebar Generator - Optional', 'gloria'),
                'img' => get_template_directory_uri() . '/images/plugins/sidebar-generator.jpg',
                'source' => get_template_directory() . '/plugins/gloria-sidebar-generator.zip',
                'required' => false,
                'version' => '3.0',
                'force_activation' => false,
                'force_deactivation' => false,
                'external_url' => '',
            ),
            array(
                'name' => esc_html__('Contact Form 7', 'gloria'),
                'slug' => 'contact-form-7',
                'title' => esc_html__('Contact Form 7 - Optional', 'gloria'),
                'img' => get_template_directory_uri() . '/images/plugins/contact-form-7.jpg',
                'source' => get_template_directory() . '/plugins/contact-form-7.zip',
                'required' => false,
                'version' => '4.4.2',
                'force_activation' => false,
                'force_deactivation' => false,
                'external_url' => '',
            ),
            array(
                'name' => esc_html__('BK Shortcode', 'gloria'),
                'slug' => 'short-code',
                'title' => esc_html__('BK Shortcode - Optional', 'gloria'),
                'img' => get_template_directory_uri() . '/images/plugins/shortcode.jpg',
                'source' => get_template_directory() . '/plugins/short-code.zip',
                'required' => false,
                'version' => '1.0',
                'force_activation' => false,
                'force_deactivation' => false,
                'external_url' => '',
            ),
            array(
                'name' => esc_html__('Gloria Admin Panel', 'gloria'),
                'slug' => 'gloria-admin-panel',
                'title' => esc_html__('Gloria Admin Panel', 'gloria'),
                'source' => get_template_directory() . '/plugins/gloria-admin-panel.zip',
                'required' => true,
                'version' => '1.0',
                'force_activation' => false,
                'force_deactivation' => false,
                'external_url' => '',
            ),
            array(
                'name' => esc_html__('ReduxFramework', 'gloria'),
                'slug' => 'redux-framework',
                'title' => esc_html__('ReduxFramework - Required', 'gloria'),
                'img' => get_template_directory_uri() . '/images/plugins/Redux.jpg',
                'source' => get_template_directory() . '/plugins/redux-framework.zip',
                'required' => true,
                'version' => '3.6.0.2',
                'force_activation' => false,
                'force_deactivation' => false,
                'external_url' => '',
            ),
            array(
                'name' => esc_html__('Meta Box', 'gloria'),
                'slug' => 'meta-box',
                'title' => esc_html__('Meta Box - Required', 'gloria'),
                'img' => get_template_directory_uri() . '/images/plugins/meta-box.jpg',
                'source' => get_template_directory() . '/plugins/meta-box.zip',
                'required' => true,
                'version' => '4.8.5',
                'force_activation' => false,
                'force_deactivation' => false,
                'external_url' => '',
            ),
        );
        // i18n text domain used for translation purposes
        $theme_text_domain = 'gloria';
         
        // Configuration of TGM
        $config = array(
            'domain'           => 'gloria',
            'default_path'     => '',
            'parent_menu_slug' => 'admin.php',
            'parent_url_slug'  => 'admin.php',
            'menu'             => 'install-required-plugins',
            'has_notices'      => true,
            'is_automatic'     => true,
            'message'          => '',
            'strings'          => array(
                'page_title'                      => esc_html__( 'Install Required Plugins', 'gloria' ),
                'menu_title'                      => esc_html__( 'Install Plugins', 'gloria' ),
                'installing'                      => esc_html__( 'Installing Plugin: %s', 'gloria' ),
                'oops'                            => esc_html__( 'Something went wrong with the plugin API.', 'gloria' ),
                'notice_can_install_required'     => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'gloria' ),
                'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'gloria' ),
                'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'gloria' ),
                'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'gloria' ),
                'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'gloria' ),
                'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'gloria' ),
                'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'gloria' ),
                'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'gloria' ),
                'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'gloria' ),
                'activate_link'                   => _n_noop( 'Activate installed plugin', 'Activate installed plugins', 'gloria' ),
                'return'                          => esc_html__( 'Return to Required Plugins Installer', 'gloria' ),
                'plugin_activated'                => esc_html__( 'Plugin activated successfully.', 'gloria' ),
                'complete'                        => esc_html__( 'All plugins installed and activated successfully. %s', 'gloria' ),
                'nag_type'                        => 'updated'
            )
        );
        tgmpa( $plugins, $config );
    }
}
add_action( 'tgmpa_register', 'tgmpa_register_toolkit' );
/**
 * Load the Envato WordPress Toolkit Library check for updates
 * and direct the user to the Toolkit Plugin if there is one
 */
if ( ! function_exists( 'envato_toolkit_admin_init' ) ) {
    function envato_toolkit_admin_init() {
     
        // Include the Toolkit Library
        include_once( get_template_directory() . '/inc/envato-wordpress-toolkit-library/class-envato-wordpress-theme-upgrader.php' );
    
    /**
     * Display a notice in the admin to remind the user to enter their credentials
     */
        function envato_toolkit_credentials_admin_notices() {
            $message = sprintf( esc_html__( "To enable theme update notifications, please enter your Envato Marketplace credentials in the %s", "gloria" ),
                "<a href='" . admin_url() . "admin.php?page=envato-wordpress-toolkit'>Envato WordPress Toolkit Plugin</a>" );
            echo "<div id='message' class='updated below-h2'><p>{$message}</p></div>";
        }
        
        // Use credentials used in toolkit plugin so that we don't have to show our own forms anymore
        $credentials = get_option( 'envato-wordpress-toolkit' );
        if ( empty( $credentials['user_name'] ) || empty( $credentials['api_key'] ) ) {
            //add_action( 'admin_notices', 'envato_toolkit_credentials_admin_notices' );
            return;
        }
    
        // Check updates only after a while
        $lastCheck = get_option( 'toolkit-last-toolkit-check' );
        if ( false === $lastCheck ) {
            update_option( 'toolkit-last-toolkit-check', time() );
            return;
        }
        
        // Check for an update every 3 hours
        if ( 10800 < ( time() - $lastCheck ) ) {
            return;
        }
        
        // Update the time we last checked
        update_option( 'toolkit-last-toolkit-check', time() );
        
        // Check for updates
        $upgrader = new Envato_WordPress_Theme_Upgrader( $credentials['user_name'], $credentials['api_key'] );
        $updates = $upgrader->check_for_theme_update();
         
        // If $updates->updated_themes_count == true then we have an update!
        
        // Add update alert, to update the theme
        if ((isset($updates->updated_themes_count)) && ( $updates->updated_themes_count )) {
            //add_action( 'admin_notices', 'envato_toolkit_admin_notices' );
        }
        
        /**
         * Display a notice in the admin that an update is available
         */
        function envato_toolkit_admin_notices() {
            $message = sprintf( esc_html__( "An update to the theme is available! Head over to %s to update it now.", "gloria" ),
                "<a href='" . admin_url() . "admin.php?page=envato-wordpress-toolkit'>Envato WordPress Toolkit Plugin</a>" );
            echo "<div id='message' class='updated below-h2'><p>{$message}</p></div>";
        }
    }
}
add_action( 'admin_init', 'envato_toolkit_admin_init' );

$gloria_justified_ids = array();
$gloria_ajax_btnstr = array();
/**
 * http://codex.wordpress.org/Content_Width
 */
if ( ! isset($content_width)) {
	$content_width = 1200;
}
/**
 * Get ajaxurl
 *---------------------------------------------------
 */
if ( ! function_exists( 'gloria_ajaxurl' ) ) {
    function gloria_ajaxurl() {
    ?>
        <script type="text/javascript">
            var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
        </script>
    <?php
    }
}
add_action('wp_head','gloria_ajaxurl');

if ( ! function_exists( 'gloria_scripts_method' ) ) {
    function gloria_scripts_method() {
        $gloria_option = gloria_core::bk_get_global_var('gloria_option');
        
        wp_enqueue_script('jquery-ui-widget');
        wp_enqueue_script('jquery-ui-tabs');
        wp_enqueue_script('jquery-ui-accordion');
        
        wp_enqueue_style( 'jquery-core-css', 'http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css', array(), '' );
        wp_enqueue_script( 'ui-core-js', 'http://code.jquery.com/ui/1.11.4/jquery-ui.js', '', true );
              
        if($gloria_option['bk-rtl-sw']) {                
            wp_enqueue_style( 'bootstrap-css', get_template_directory_uri().'/framework/bootstrap/css/bootstrap-rtl.css', array(), '' );
        }else {
            wp_enqueue_style( 'bootstrap-css', get_template_directory_uri().'/framework/bootstrap/css/bootstrap.css', array(), '' );        
        }
        wp_enqueue_style('fa', get_template_directory_uri() . '/css/fonts/awesome-fonts/css/font-awesome.min.css');
                        
        wp_enqueue_style('gloria-theme-plugins', get_template_directory_uri() . '/css/theme_plugins.css');

        if($gloria_option['bk-rtl-sw']) {
            wp_enqueue_style('gloria-style', get_template_directory_uri() . '/css/bkrtl.css');
            wp_enqueue_style('gloria-woocommerce', get_template_directory_uri() . '/css/woocommerce_css/woocommerce_rtl.css');
            wp_enqueue_style('gloria-bbpress', get_template_directory_uri() . '/css/bbpress_css/bbpress_rtl.css');
            wp_enqueue_style('gloria-responsive', get_template_directory_uri() . '/css/responsive_rtl.css');
        }else {
            wp_enqueue_style('gloria-style', get_template_directory_uri() . '/css/bkstyle.css');
            wp_enqueue_style('gloria-woocommerce', get_template_directory_uri() . '/css/woocommerce_css/woocommerce.css');
            wp_enqueue_style('gloria-bbpress', get_template_directory_uri() . '/css/bbpress_css/bbpress.css');
            wp_enqueue_style('gloria-responsive', get_template_directory_uri() . '/css/responsive.css');
        }
        
        wp_enqueue_style('gloria-wpstyle', get_stylesheet_uri());
                             
        wp_enqueue_script('gloria-theme-plugins', get_template_directory_uri() . '/js/theme_plugins.js', array('jquery'),'', true);  
        
        wp_enqueue_script( 'gloria-onviewport', get_template_directory_uri().'/js/onviewport.js', array( 'jquery' ), false, true );

        wp_enqueue_script('gloria-module-load-post', get_template_directory_uri() . '/js/module-load-post.js', array('jquery'),false,true); 
        
        wp_enqueue_script( 'gloria-menu', get_template_directory_uri().'/js/menu.js', array( 'jquery' ), false, true );
        
        if($gloria_option['bk-rtl-sw']) {
            wp_enqueue_script( 'gloria-customjs', get_template_directory_uri().'/js/customjs_rtl.js', array( 'jquery' ), false, true );
        }else {
            wp_enqueue_script( 'gloria-customjs', get_template_directory_uri().'/js/customjs.js', array( 'jquery' ), false, true );
        }
        
        if ( is_singular() ) {wp_enqueue_script('comment-reply');}
        
     }
}

add_action('wp_enqueue_scripts', 'gloria_scripts_method');

if ( ! function_exists( 'gloria_post_admin_scripts_and_styles' ) ) {
    function gloria_post_admin_scripts_and_styles($hook) {        
    	if( $hook == 'post.php' || $hook == 'post-new.php' ) {
            wp_enqueue_script( 'bootstrap-admin-js', get_template_directory_uri().'/framework/bootstrap-admin/bootstrap.js', array(), '', true );
            wp_enqueue_style( 'bootstrap-admin-css', get_template_directory_uri().'/framework/bootstrap-admin/bootstrap.css', array(), '' );
            wp_enqueue_style( 'jquery-core-css', 'http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css', array(), '' );
            wp_enqueue_script( 'ui-core-js', 'http://code.jquery.com/ui/1.11.2/jquery-ui.js', '', true );
            wp_register_script( 'gloria-post-review-admin',  get_template_directory_uri() . '/js/post-review-admin.js', array(), '', true);
            wp_enqueue_script( 'gloria-post-review-admin' ); // enqueue it
   		}
        wp_enqueue_script('ckeditor-js', get_template_directory_uri() . '/js/ckeditor/ckeditor.js', array('jquery'));
        
        wp_enqueue_script('bootstrap-colorpicker-js', get_template_directory_uri() . '/js/bootstrap-colorpicker.js', array('jquery')); 
        
        wp_enqueue_style('bootstrap-colorpicker-css', get_template_directory_uri() . '/css/bootstrap-colorpicker.css');
        
        wp_enqueue_style( 'gloria-admin-css', get_template_directory_uri().'/css/admin.css', array(), '' );
        
        add_editor_style('css/editorstyle.css');
        
        wp_enqueue_style('fa-admin', get_template_directory_uri() . '/css/fonts/awesome-fonts/css/font-awesome.min.css');
        
        wp_enqueue_style( 'rwmb-group', get_template_directory_uri() .'/framework/meta-box-group/group.css');
        
        wp_enqueue_script( 'gloria-autosize-js', get_template_directory_uri().'/js/jquery.autosize.min.js', array(), '', true );
        
        wp_enqueue_script( 'gloria-admin-js', get_template_directory_uri().'/js/admin.js', array('jquery-ui-sortable'), '', true );

        
    }
}
add_action('admin_enqueue_scripts', 'gloria_post_admin_scripts_and_styles');
 if ( ! function_exists( 'gloria_page_builder_js' ) ) {
    function gloria_page_builder_js($hook) {
        if( $hook == 'post.php' || $hook == 'post-new.php' ) {
            wp_enqueue_script( 'gloria-page-builder-js', get_template_directory_uri().'/framework/page-builder/controller/js/page-builder.js', array( 'jquery' ), null, true );
            wp_localize_script( 'gloria-page-builder-js', 'bkpb_ajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
  		}
    }
 }
 add_action('admin_enqueue_scripts', 'gloria_page_builder_js', 9);
/**
 * Register sidebars and widgetized areas.
 *---------------------------------------------------
 */
 if ( ! function_exists( 'gloria_widgets_init' ) ) {
    function gloria_widgets_init() {
        register_sidebar( array(
    		'name' => esc_html__('Home Sidebar 1', 'gloria'),
    		'id' => 'home_sidebar1',
    		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    		'after_widget' => '</aside>',
    		'before_title' => '<div class="bk-header"><div class="widget-title"><h3>',
    		'after_title' => '</h3></div></div>',
    	) );
        register_sidebar( array(
    		'name' => esc_html__('Home Sidebar 2', 'gloria'),
    		'id' => 'home_sidebar2',
    		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    		'after_widget' => '</aside>',
    		'before_title' => '<div class="bk-header"><div class="widget-title"><h3>',
    		'after_title' => '</h3></div></div>',
    	) );
        register_sidebar( array(
    		'name' => esc_html__('Page Sidebar', 'gloria'),
    		'id' => 'page_sidebar',
    		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    		'after_widget' => '</aside>',
    		'before_title' => '<div class="bk-header"><div class="widget-title"><h3>',
    		'after_title' => '</h3></div></div>',
    	) );
        register_sidebar( array(
    		'name' => esc_html__('Single Sidebar', 'gloria'),
    		'id' => 'single_sidebar',
    		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    		'after_widget' => '</aside>',
    		'before_title' => '<div class="bk-header"><div class="widget-title"><h3>',
    		'after_title' => '</h3></div></div>',
    	) );
        register_sidebar( array(
    		'name' => esc_html__('Default Template Sidebar', 'gloria'),
    		'id' => 'default_template_sidebar',
    		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    		'after_widget' => '</aside>',
    		'before_title' => '<div class="bk-header"><div class="widget-title"><h3>',
    		'after_title' => '</h3></div></div>',
    	) );
        register_sidebar( array(
    		'name' => esc_html__('Sidebar', 'gloria'),
    		'id' => 'home_sidebar',
    		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    		'after_widget' => '</aside>',
    		'before_title' => '<div class="bk-header"><div class="widget-title"><h3>',
    		'after_title' => '</h3></div></div>',
    	) );

        register_sidebar( array(
    		'name' => esc_html__('Footer Sidebar 1', 'gloria'),
    		'id' => 'footer_sidebar_1',
    		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    		'after_widget' => '</aside>',
    		'before_title' => '<div class="bk-header"><div class="widget-title"><h3>',
    		'after_title' => '</h3></div></div>',
    	) );
        
        register_sidebar( array(
    		'name' => esc_html__('Footer Sidebar 2', 'gloria'),
    		'id' => 'footer_sidebar_2',
    		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    		'after_widget' => '</aside>',
    		'before_title' => '<div class="bk-header"><div class="widget-title"><h3>',
    		'after_title' => '</h3></div></div>',
    	) );
        
        register_sidebar( array(
    		'name' => esc_html__('Footer Sidebar 3', 'gloria'),
    		'id' => 'footer_sidebar_3',
    		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    		'after_widget' => '</aside>',
    		'before_title' => '<div class="bk-header"><div class="widget-title"><h3>',
    		'after_title' => '</h3></div></div>',
    	) );
    }
}
add_action( 'widgets_init', 'gloria_widgets_init' );

/**
 * Add php library file.
 */
require_once(get_template_directory()."/library/core.php");
require_once(get_template_directory()."/library/mega_menu.php");
require_once(get_template_directory()."/library/custom_css.php");
require_once(get_template_directory()."/library/translation.php");
require_once(get_template_directory()."/inc/controller.php");

/**
 * Add widgets
 */
include(get_template_directory()."/framework/widgets/widget_tabs.php");
include(get_template_directory()."/framework/widgets/widget_most_commented.php");
include(get_template_directory()."/framework/widgets/widget_post_review.php");
include(get_template_directory()."/framework/widgets/widget_social_counter.php");
include(get_template_directory()."/framework/widgets/widget_latest_comments.php");
include(get_template_directory()."/framework/widgets/widget_twitter.php");
include(get_template_directory()."/framework/widgets/widget_flickr.php");
include(get_template_directory()."/framework/widgets/widget_slider.php");
include(get_template_directory()."/framework/widgets/widget_facebook.php");
include(get_template_directory()."/framework/widgets/widget_google_badge.php");
include(get_template_directory()."/framework/widgets/widget_ads.php");
include(get_template_directory()."/framework/widgets/widget-timeline.php");
include(get_template_directory()."/framework/widgets/widget-soundcloud.php");
include(get_template_directory()."/framework/widgets/widget-feedburner.php");
include(get_template_directory()."/framework/widgets/widget_social.php");
include(get_template_directory()."/framework/widgets/widget_postlist.php");

gloria_initWpFilesystem();

if ( class_exists('bbpress') ) {
    require_once ( get_template_directory().'/framework/widgets/widget-recent-replies-bbp.php' ); 
    require_once ( get_template_directory().'/framework/widgets/widget-recent-topics-bbp.php' );
}
/**
 * Meta box
 */
require_once (get_template_directory() . '/framework/meta-box-group/meta-box-group.php');
require_once (get_template_directory() . '/library/meta_box_config.php');

//Add support tag title
add_theme_support('title-tag');

/**
 * Add support for the featured images (also known as post thumbnails).
 */
if ( function_exists( 'add_theme_support' ) ) { 
	add_theme_support( 'post-thumbnails' );
}

add_action( 'after_setup_theme', 'gloria_thumbnail_setup' );
if ( ! function_exists( 'gloria_thumbnail_setup' ) ){

    function gloria_thumbnail_setup() {
        add_image_size( 'gloria_660_400', 660, 400, true );
        add_image_size( 'gloria_320_218', 320, 218, true );
        add_image_size( 'gloria_160_110', 160, 110, true );
        add_image_size( 'gloria_600_315', 600, 315, true );
        add_image_size( 'gloria_620_420', 620, 420, true ); 
        add_image_size( 'gloria_130_130', 130, 130, true );
        add_image_size( 'gloria_330_380', 330, 380, true );
        add_image_size( 'gloria_fw_slider', 1000, 485, true );
        add_image_size( 'gloria_auto_size', 400, 99999, false );
        add_image_size( 'gloria_s_feat_img', 99999, 600, false );
    }
}
/**
 * Remove standard image sizes so that these sizes are not
 * created during the Media Upload process
 */
function gloria_filter_image_sizes( $sizes) {
		
	unset( $sizes['thumbnail']);
	unset( $sizes['medium']);
	unset( $sizes['large']);
	
	return $sizes;
}
//add_filter('intermediate_image_sizes_advanced', 'gloria_filter_image_sizes');

/**
 * Post Format 
 */
 add_action('after_setup_theme', 'gloria_add_theme_format', 11);
function gloria_add_theme_format() {
    add_theme_support( 'post-formats', array( 'gallery', 'video', 'image', 'audio' ) );
}
/**
 * Add support for the featured images (also known as post thumbnails).
 */
if ( function_exists( 'add_theme_support' ) ) { 
	add_theme_support( 'post-thumbnails' );
    add_theme_support( 'automatic-feed-links' );
}
/**
 * Title
 */
add_filter( 'wp_title', 'gloria_wp_title', 10, 2 );
if ( ! function_exists( 'gloria_wp_title' ) ) {
    function gloria_wp_title( $title, $sep ) {
    	global $paged, $page;
    
    	if ( is_feed() ) {
    		return $title;
    	}
    
    	// Add the site name.
    	$title .= get_bloginfo( 'name' );
    
    	// Add the site description for the home/front page.
    	$site_description = get_bloginfo( 'description', 'display' );
    	if ( $site_description && ( is_home() || is_front_page() ) ) {
    		$title = "$title $sep $site_description";
    	}
    
    	// Add a page number if necessary.
    	if ( $paged >= 2 || $page >= 2 ) {
    		$title = "$title $sep " . sprintf( esc_html__( 'Page %s', 'gloria' ), max( $paged, $page ) );
    	}
    
    	return $title;
    }
}
/**
 * Register menu locations
 *---------------------------------------------------
 */
if ( ! function_exists( 'gloria_register_menu' ) ) {
    function gloria_register_menu() {
        register_nav_menu('main-menu',esc_html__( 'Main Menu', 'gloria' ));
        register_nav_menu('menu-top',esc_html__( 'Top Menu', 'gloria' ));
        register_nav_menu('menu-footer',esc_html__( 'Footer Menu', 'gloria' )); 
    }
}
add_action( 'init', 'gloria_register_menu' );

function gloria_category_nav_class( $classes, $item ){
    if( 'category' == $item->object ){
        $classes[] = 'menu-category-' . $item->object_id;
    }
    return $classes;
}
add_filter( 'nav_menu_css_class', 'gloria_category_nav_class', 10, 2 );

function gloria_custom_excerpt_length( $length ) {
	return 100;
}
add_filter( 'excerpt_length', 'gloria_custom_excerpt_length', 999 );

/**
 * ReduxFramework
 */
if ( !isset( $gloria_option ) && file_exists( get_template_directory() . '/library/theme-option.php' ) ) {
    require_once( get_template_directory() . '/library/theme-option.php' );
}

if ( ! function_exists( 'gloria_check_retina_display' ) ) {
    function gloria_check_retina_display() {
        $gloria_option = gloria_core::bk_get_global_var('gloria_option');
        if (isset($gloria_option['bk-retina-display'])) {
            $retina_display = $gloria_option['bk-retina-display'];
        }else {
            $retina_display = 0;
        }
        return $retina_display;
    }
}

if (class_exists('Woocommerce')) {
    /**
     * Woocommerce Setup
     */
    if ( ! function_exists( 'bk_woocommerce_setup' ) ) {
    	function bk_woocommerce_setup() {
    		add_theme_support( 'woocommerce' );
    
    		// remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
    		// remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
    
    		add_filter( 'woocommerce_show_page_title', '__return_false' );
    	}
    	add_action( 'after_setup_theme', 'bk_woocommerce_setup' );
    }
    if ( ! function_exists( 'bk_setup_woocommerce_image_dimensions' ) ) {
    	function bk_setup_woocommerce_image_dimensions() {
    		$bk_woo_catalog = array( // square_medium size
    			'width' 	=> '550',	// px
    			'height'	=> '550',	// px
    			'crop'		=> 1 		// true
    		);
    	 
    		$bk_woo_single = array(
    			'width' 	=> '550',	// px
    			'height'	=> '550',	// px
    			'crop'		=> 1 		// true
    		);
    	 
    		$bk_woo_thumbnail = array(
    			'width' 	=> '360',	// px
    			'height'	=> '360',	// px
    			'crop'		=> 0 		// false
    		);
    	 
    		// Image sizes
    		update_option( 'shop_catalog_image_size', $bk_woo_catalog ); 		// Product category thumbs
    		update_option( 'shop_single_image_size', $bk_woo_single ); 		// Single product image
    		update_option( 'shop_thumbnail_image_size', $bk_woo_thumbnail ); 	// Image gallery thumbs
    	}
    	add_action( 'after_switch_theme', 'bk_setup_woocommerce_image_dimensions' );
    }
    // Redefine woocommerce_output_related_products()
    function woocommerce_output_related_products() {
        $gloria_option = gloria_core::bk_get_global_var('gloria_option');
        if(isset($gloria_option['bk-shop-sidebar']) && ($gloria_option['bk-shop-sidebar'] == 'on')) {
            $args = array(
                'posts_per_page' => 3,
                'columns'        => 3,
                'orderby'        => 'rand',
              );
        }else {
            $args = array(
                'posts_per_page' => 4,
                'columns'        => 4,
                'orderby'        => 'rand',
              );
        }
        woocommerce_related_products($args, false, false); // Display 4 products in rows
    }
    
    add_filter( 'single_product_small_thumbnail_size', 'bk_single_product_small_thumbnail_size', 25, 1 );
    function bk_single_product_small_thumbnail_size( $size ) {
        $size = 'full';
        return $size;
    }
    
    add_filter('add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');
    
    function woocommerce_header_add_to_cart_fragment( $fragments ) {
        global $woocommerce;
        ob_start();
        ?>
        <a class="cart-contents" href="<?php echo esc_url($woocommerce->cart->get_cart_url()); ?>" title="<?php esc_html_e('View your shopping cart', 'woothemes'); ?>"><i class="fa fa-shopping-cart"></i><span><?php echo sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?></span></a>
        <?php
        
        $fragments['a.cart-contents'] = ob_get_clean();
        
        return $fragments;
    
    }
    register_sidebar( array(
            'name'          => esc_html__( 'Small Cart', 'gloria' ),
            'id'            => 'cart-1',
            'description'   => 'Do not drop any widget to this place because it will not be affected, this is just used for header Woocommerce small cart (you can enable / disable small cart ON in Theme Options -> Header Setting). If you want another widget holder, please create more by using the sidebar generator',
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        ) );
    $sidebars_widgets = get_option( 'sidebars_widgets' );      
    
    $woo_cart_data = get_option('widget_woocommerce_widget_cart');  
    
    $bkdata = array (
                    'title'=>'Cart',
                    'hide_if_empty'=>0
                  );
    if ($woo_cart_data != '') {
        $woo_cart_data[1] = $bkdata;
        update_option( 'widget_woocommerce_widget_cart', $woo_cart_data );   
    }else {
        $woo_cart_data[1] = $bkdata;
        add_option( 'widget_woocommerce_widget_cart', $woo_cart_data );   
    }

	//empty the default sidebar
	$sidebars_widgets['cart-1'] = array();
    $new_instance_id = 'woocommerce_widget_cart' . '-' . 1; // use ID number from new widget instance
    $sidebars_widgets['cart-1'][] = $new_instance_id;
    update_option( 'sidebars_widgets', $sidebars_widgets ); // save the amended data

}
$bk_retina_display = gloria_check_retina_display();

if ($bk_retina_display) {
    /**
     * Enqueueing retina.js
     *
     * This function is attached to the 'wp_enqueue_scripts' action hook.
     */
    add_action( 'wp_enqueue_scripts', 'gloria_retina_support_enqueue_scripts' );
    function gloria_retina_support_enqueue_scripts() {
        wp_enqueue_script( 'retina_js', get_template_directory_uri() . '/js/retina.min.js', '', '', true );
    }
    
    add_filter( 'wp_generate_attachment_metadata', 'gloria_retina_support_attachment_meta', 10, 2 );
    /**
     * Retina images
     *
     * This function is attached to the 'wp_generate_attachment_metadata' filter hook.
     */
    function gloria_retina_support_attachment_meta( $metadata, $attachment_id ) {
        foreach ( $metadata as $key => $value ) {
            if ( is_array( $value ) ) {
                foreach ( $value as $image => $attr ) {
                    if ( is_array( $attr ) && isset ($attr['width']) && isset ($attr['height'])) {
                        gloria_retina_support_create_images( get_attached_file( $attachment_id ), $attr['width'], $attr['height'], true );
                    }
                }
            }
        }
     
        return $metadata;
    }
    
    /**
     * Create retina-ready images
     *
     * Referenced via retina_support_attachment_meta().
     */
    function gloria_retina_support_create_images( $file, $width, $height, $crop = false ) {
        if ( $width || $height ) {
            $resized_file = wp_get_image_editor( $file );
            if ( ! is_wp_error( $resized_file ) ) {
                $filename = $resized_file->generate_filename( $width . 'x' . $height . '@2x' );
     
                $resized_file->resize( $width * 2, $height * 2, $crop );
                $resized_file->save( $filename );
     
                $info = $resized_file->get_size();
     
                return array(
                    'file' => wp_basename( $filename ),
                    'width' => $info['width'],
                    'height' => $info['height'],
                );
            }
        }
        return false;
    }
    
    add_filter( 'delete_attachment', 'gloria_delete_retina_support_images' );
    /**
     * Delete retina-ready images
     *
     * This function is attached to the 'delete_attachment' filter hook.
     */
    function gloria_delete_retina_support_images( $attachment_id ) {
        $meta = wp_get_attachment_metadata( $attachment_id );
        $upload_dir = wp_upload_dir();
        if(isset($meta['file'])) {
            $path = pathinfo( $meta['file'] );
            foreach ( $meta as $key => $value ) {
                if ( 'sizes' === $key ) {
                    foreach ( $value as $sizes => $size ) {
                        $original_filename = $upload_dir['basedir'] . '/' . $path['dirname'] . '/' . $size['file'];
                        $retina_filename = substr_replace( $original_filename, '@2x.', strrpos( $original_filename, '.' ), strlen( '.' ) );
                        if ( file_exists( $retina_filename ) )
                            unlink( $retina_filename );
                    }
                }
            }
        }
    }
}
function gloria_remove_pages_from_search() {
    $gloria_option = gloria_core::bk_get_global_var('gloria_option');
    if(($gloria_option != null) && isset($gloria_option['bk-search-result']) && ($gloria_option['bk-search-result'] == 'yes')) {
        global $wp_post_types;
        $wp_post_types['page']->exclude_from_search = true;
        $wp_post_types['attachment']->exclude_from_search = true;
    }
}
add_action('init', 'gloria_remove_pages_from_search');