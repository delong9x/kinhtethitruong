<?php
if (!class_exists('gloria_carousel')) {
    class gloria_carousel extends bk_section_parent  {
        
        public function render( $page_info ) {
            $block_str = '';
            $cfg_ops = array();
            $cfg_ops = $this->cfg_options(); 
            $module_cfg = bk_get_cfg::configs($cfg_ops['fullwidth']['bk_carousel'], $page_info);    //get block config
            $sec = '';
            $has_bkwrapper = '';
            if (substr( $page_info['block_prefix'], 0, 10 ) == 'bk_has_rsb') {
                $has_bkwrapper = 'carousel_2';                 
            }else {
                $has_bkwrapper = 'bkwrapper container carousel_3';   
            }
            $the_query = bk_get_query::query($module_cfg);//get query
    
            $block_str .= '<div class="bkmodule '.$has_bkwrapper.' module-carousel hide">';
            if($the_query->post_count < 3) {
                $block_str .= 'Found '.$the_query->post_count.' posts that corresponds to your conditions. This module (bk carousel) need at least 3 posts to work properly. Please try to create more posts';
            }else {
                if ( $the_query->have_posts() ) :
                    $block_str .= gloria_core::bk_get_block_title($page_info);  //render block title
                endif;
                $block_str .= '<div class="bk-carousel-wrap flexslider" style="background-color: '.$module_cfg['bg_color'].'">';
                $block_str .= $this->render_modules($the_query);
                $block_str .= '</div>';
            }
            $block_str .= '</div>';
            
            unset($cfg_ops); unset($module_cfg); unset($the_query);
            wp_reset_postdata();
            return $block_str;
    	}
        static function render_modules ($the_query){
            $render_modules = '';
            $bk_contentin1 = new gloria_contentin1;
            if ( $the_query->have_posts() ) :
                $render_modules .= '<ul class="slides clearfix">';
                $post_args = array (
                    'thumbnail_size'    => 'full',
                    'cat_meta'            => true,
                );
                while ( $the_query->have_posts() ): $the_query->the_post();
                    $bk_final_score = get_post_meta(get_the_ID(), 'bk_final_score', true );
                    $post_args['review_score'] = $bk_final_score;
                    $postFormat = gloria_core::bk_post_format_detect(get_the_ID());
                    if($postFormat['format'] === 'video') {
                        $post_args['video_icon'] = true;
                    }else {
                        $post_args['video_icon'] = '';
                    }
                    $render_modules .= '<li class="content_in">';
                    $render_modules .= '<div class="content_in_wrapper">';
                    $render_modules .= $bk_contentin1->render($post_args);
                    $render_modules .= '</div></li>';
                endwhile;
                $render_modules .= '</ul> <!-- Close render slider -->';
            endif;
            return $render_modules;
        }
        
    }
}