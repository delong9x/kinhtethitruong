<?php
if (!class_exists('gloria_feature_2')) {
    class gloria_feature_2 extends bk_section_parent  {
        
        public function render( $page_info ) {
            $block_str = '';
            $cfg_ops = array();
            $cfg_ops = $this->cfg_options(); 
            $module_cfg = bk_get_cfg::configs($cfg_ops['fullwidth']['bk_feature_2'], $page_info);    //get block config
            
            $the_query = bk_get_query::query($module_cfg);              //get query
    
            $block_str .= '<div class="bkmodule module-feature-2 background-preload "><div class="feature-2-wrapper feature-2-runner opacity-zero">';
            $block_str .= $this->render_modules($the_query);            //render modules
            $block_str .= '</div><div class="bk-preload"></div></div>';
            
            unset($cfg_ops); unset($module_cfg); unset($the_query);     //free
            wp_reset_postdata();
            return $block_str;
    	}
        static function render_modules ($the_query){
            $render_modules = '';
            $bk_contentin2 = new gloria_contentin2;
            if ( $the_query->have_posts() ) :   
                $meta_ar = array('author', 'date');
                $post_args = array (
                    'thumbnail_size'    => 'gloria_660_400',
                    'meta_ar'           => $meta_ar,
                    'cat_meta'            => true,
                    'except_length'     => '',
                    'rm_btn'            => true,
                );
                while ( $the_query->have_posts() ): $the_query->the_post();
                    $render_modules .= '<div class="item content_in">';
                    $render_modules .= $bk_contentin2->render($post_args);
                    $render_modules .= '</div>';
                endwhile;
                
            endif;
            return $render_modules;
        }
        
    }
}