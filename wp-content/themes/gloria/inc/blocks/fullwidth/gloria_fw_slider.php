<?php
if (!class_exists('gloria_fw_slider')) {
    class gloria_fw_slider extends bk_section_parent  {
        
        public function render( $page_info ) {
            $block_str = '';
            $cfg_ops = array();
            $cfg_ops = $this->cfg_options(); 
            $module_cfg = bk_get_cfg::configs($cfg_ops['fullwidth']['bk_fw_slider'], $page_info);    //get block config
            
            $the_query = bk_get_query::query($module_cfg);              //get query
            
            if ( $the_query->have_posts() ) : 
            $block_str .= '<div class="bkmodule module-fw-slider bk-slider-module">';
            $block_str .= '<div class="row flexslider">';
            $block_str .= '<ul class="col-md-12 slides">';
            $block_str .= $this->render_modules($the_query);            //render modules
            $block_str .= '</ul></div> <!-- Close render modules -->';
            $block_str .= '</div>';
            endif;
            unset($cfg_ops); unset($module_cfg); unset($the_query);     //free
            wp_reset_postdata();
            return $block_str;
    	}
        static function render_modules ($the_query){
            $render_modules = '';
            $bk_contentin3 = new gloria_contentin3; 
            $meta_ar = array('author', 'date');
            $post_args = array (
                'thumbnail_size'    => 'full',
                'meta_ar'           => $meta_ar,
                'cat_meta'            => true,
                'except_length'     => '',
                'rm_btn'            => true,
            );         
            
            while ( $the_query->have_posts() ): $the_query->the_post();
                $render_modules .= '<li class="item content_in">';
                $render_modules .= $bk_contentin3->render($post_args);
                $render_modules .= '</li>';
            endwhile;
                
            return $render_modules;
        }
        
    }
}