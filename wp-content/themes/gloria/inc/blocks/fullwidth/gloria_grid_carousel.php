<?php
if (!class_exists('gloria_grid_carousel')) {
    class gloria_grid_carousel extends bk_section_parent  {
        
        public function render( $page_info ) {
            $block_str = '';
            $cfg_ops = array();
            $cfg_ops = $this->cfg_options(); 
            $module_cfg = bk_get_cfg::configs($cfg_ops['fullwidth']['bk_grid_carousel'], $page_info);    //get block config
                
            $the_query = bk_get_query::query($module_cfg);//get query
            
            //Carousel Flow
            $grid_carousel_item = array();
            $post_count = $the_query->post_count;
            $carousel_item_count = 0;
            while($post_count > 0) {
                //1
                if($post_count > 1) {
                    $grid_carousel_item[$carousel_item_count] = 'item_2_half';
                    $post_count = $post_count - 2;
                }else {
                    $grid_carousel_item[$carousel_item_count] = 'item_1';
                    $post_count = $post_count - 1;
                    break;
                }
                $carousel_item_count ++;
                //2
                if($post_count > 2) {
                    $grid_carousel_item[$carousel_item_count] = 'item_3';
                    $post_count = $post_count - 3;
                }else if($post_count == 2) {
                    $grid_carousel_item[$carousel_item_count] = 'item_2_half';
                    $post_count = $post_count - 2;
                    break;
                }else if($post_count == 1){
                    $grid_carousel_item[$carousel_item_count] = 'item_1';
                    $post_count = $post_count - 1;
                    break;
                }
                $carousel_item_count ++;
                //3
                if($post_count > 1) {
                    $grid_carousel_item[$carousel_item_count] = 'item_2_big';
                    $post_count = $post_count - 2;
                }else if($post_count == 1){
                    $grid_carousel_item[$carousel_item_count] = 'item_1';
                    $post_count = $post_count - 1;
                    break;
                }
                $carousel_item_count ++;
                //4
                if($post_count > 1) {
                    $grid_carousel_item[$carousel_item_count] = 'item_2_small';
                    $post_count = $post_count - 2;
                }else if($post_count == 1){
                    $grid_carousel_item[$carousel_item_count] = 'item_1';
                    $post_count = $post_count - 1;
                    break;
                }
                $carousel_item_count ++;
                if($post_count > 0) {
                    $grid_carousel_item[$carousel_item_count] = 'item_1';
                    $post_count = $post_count - 1;
                }
                $carousel_item_count ++;
            }
            $block_str .= '<div class="bkmodule bkwrapper container module-grid-carousel">';
            if($the_query->post_count < 8) {
                $block_str .= 'Found '.$the_query->post_count.' posts that corresponds to your conditions. This module (bk grid carousel) need at least 8 posts to work properly. Please try to create more posts';
            }else {
                if ( $the_query->have_posts() ) :
                    $block_str .= gloria_core::bk_get_block_title($page_info);  //render block title
                endif;
                $block_str .= '<div class="bk-grid-carousel-inner background-preload">';
                $block_str .= '<div class="bk-carousel-wrap flexslider opacity-zero">';
                $block_str .= $this->render_modules($the_query, $grid_carousel_item);
                $block_str .= '</div><div class="bk-preload"></div></div>';
            }
            $block_str .= '</div>';
            unset($cfg_ops); unset($module_cfg); unset($the_query);
            wp_reset_postdata();
            return $block_str;
    	}
        public function render_modules($the_query, $grid_carousel_item){
            $render_modules = '';
            $bk_contentin1 = new gloria_contentin1;
            if ( $the_query->have_posts() ) :
                $render_modules .= '<ul class="slides clearfix">';
                $post_args = array (
                    'thumbnail_size'    => 'full',
                    'cat_meta'            => true,
                );
                $loop_count = count($grid_carousel_item);
                $carousel_item_count = 0;
                for($carousel_item_count = 0; $carousel_item_count < $loop_count; $carousel_item_count++) {
                    if($grid_carousel_item[$carousel_item_count] == 'item_1') {
                        $render_modules .= self::item_1($the_query, $post_args);
                    }else if($grid_carousel_item[$carousel_item_count] == 'item_2_half') {
                        $render_modules .= self::item_2_half($the_query, $post_args);
                    }else if($grid_carousel_item[$carousel_item_count] == 'item_2_big') {
                        $render_modules .= self::item_2_big($the_query, $post_args);
                    }else if($grid_carousel_item[$carousel_item_count] == 'item_2_small') {
                        $render_modules .= self::item_2_small($the_query, $post_args);
                    }else if($grid_carousel_item[$carousel_item_count] == 'item_3') {
                        $render_modules .= self::item_3($the_query, $post_args);
                    }
                }
                $render_modules .= '</ul> <!-- Close render slider -->';
            endif;
            return $render_modules;
        }
        static function item_1($the_query, $post_args){
            $bk_contentin1 = new gloria_contentin1;
            $item_html = '';
            $item_html .= '<li class="content_in item_1">';
            $item_html .= '<div class="item-child"><div class="item-child-inner">';
            $the_query->the_post();
            $bk_final_score = get_post_meta(get_the_ID(), 'bk_final_score', true );
            $post_args['review_score'] = $bk_final_score;
            $postFormat = gloria_core::bk_post_format_detect(get_the_ID());
            if($postFormat['format'] === 'video') {
                $post_args['video_icon'] = true;
            }else {
                $post_args['video_icon'] = '';
            }
            $item_html .= $bk_contentin1->render($post_args);
            $item_html .= '</div></div>';
            $item_html .= '</li>';
            return $item_html;
        }
        static function item_2_half($the_query, $post_args){
            $bk_contentin1 = new gloria_contentin1;
            $item_html = '';
            $item_html .= '<li class="content_in item_2 item_2_half">';
            
            $item_html .= '<div class="item-child"><div class="item-child-inner">';
            $the_query->the_post();
            $bk_final_score = get_post_meta(get_the_ID(), 'bk_final_score', true );
            $post_args['review_score'] = $bk_final_score;
            $postFormat = gloria_core::bk_post_format_detect(get_the_ID());
            if($postFormat['format'] === 'video') {
                $post_args['video_icon'] = true;
            }else {
                $post_args['video_icon'] = '';
            }
            $item_html .= $bk_contentin1->render($post_args);
            $item_html .= '</div></div>';
            
            $item_html .= '<div class="item-child"><div class="item-child-inner">';
            $the_query->the_post();
            $bk_final_score = get_post_meta(get_the_ID(), 'bk_final_score', true );
            $post_args['review_score'] = $bk_final_score;
            $postFormat = gloria_core::bk_post_format_detect(get_the_ID());
            if($postFormat['format'] === 'video') {
                $post_args['video_icon'] = true;
            }else {
                $post_args['video_icon'] = '';
            }
            $item_html .= $bk_contentin1->render($post_args);
            $item_html .= '</div></div>';
            
            $item_html .= '</li>';
            return $item_html;
        }
        static function item_2_big($the_query, $post_args){
            $bk_contentin1 = new gloria_contentin1;
            $item_html = '';
            $item_html .= '<li class="content_in item_2 item_2_big">';
            
            $item_html .= '<div class="item-child"><div class="item-child-inner">';
            $the_query->the_post();
            $bk_final_score = get_post_meta(get_the_ID(), 'bk_final_score', true );
            $post_args['review_score'] = $bk_final_score;
            $postFormat = gloria_core::bk_post_format_detect(get_the_ID());
            if($postFormat['format'] === 'video') {
                $post_args['video_icon'] = true;
            }else {
                $post_args['video_icon'] = '';
            }
            $item_html .= $bk_contentin1->render($post_args);
            $item_html .= '</div></div>';
            
            $item_html .= '<div class="item-child"><div class="item-child-inner">';
            $the_query->the_post();
            $bk_final_score = get_post_meta(get_the_ID(), 'bk_final_score', true );
            $post_args['review_score'] = $bk_final_score;
            $postFormat = gloria_core::bk_post_format_detect(get_the_ID());
            if($postFormat['format'] === 'video') {
                $post_args['video_icon'] = true;
            }else {
                $post_args['video_icon'] = '';
            }
            $item_html .= $bk_contentin1->render($post_args);
            $item_html .= '</div></div>';
            
            $item_html .= '</li>';
            return $item_html;
        }
        static function item_2_small($the_query, $post_args){
            $bk_contentin1 = new gloria_contentin1;
            $item_html = '';
            $item_html .= '<li class="content_in item_2 item_2_small">';
            
            $item_html .= '<div class="item-child"><div class="item-child-inner">';
            $the_query->the_post();
            $bk_final_score = get_post_meta(get_the_ID(), 'bk_final_score', true );
            $post_args['review_score'] = $bk_final_score;
            $postFormat = gloria_core::bk_post_format_detect(get_the_ID());
            if($postFormat['format'] === 'video') {
                $post_args['video_icon'] = true;
            }else {
                $post_args['video_icon'] = '';
            }
            $item_html .= $bk_contentin1->render($post_args);
            $item_html .= '</div></div>';
            
            $item_html .= '<div class="item-child"><div class="item-child-inner">';
            $the_query->the_post();
            $bk_final_score = get_post_meta(get_the_ID(), 'bk_final_score', true );
            $post_args['review_score'] = $bk_final_score;
            $postFormat = gloria_core::bk_post_format_detect(get_the_ID());
            if($postFormat['format'] === 'video') {
                $post_args['video_icon'] = true;
            }else {
                $post_args['video_icon'] = '';
            }
            $item_html .= $bk_contentin1->render($post_args);
            $item_html .= '</div></div>';
            
            $item_html .= '</li>';
            return $item_html;
        }
        static function item_3($the_query, $post_args){
            $bk_contentin1 = new gloria_contentin1;
            $item_html = '';
            $item_html .= '<li class="content_in item_3">';
            
            $item_html .= '<div class="item-child"><div class="item-child-inner">';
            $the_query->the_post();
            $bk_final_score = get_post_meta(get_the_ID(), 'bk_final_score', true );
            $post_args['review_score'] = $bk_final_score;
            $postFormat = gloria_core::bk_post_format_detect(get_the_ID());
            if($postFormat['format'] === 'video') {
                $post_args['video_icon'] = true;
            }else {
                $post_args['video_icon'] = '';
            }
            $item_html .= $bk_contentin1->render($post_args);
            $item_html .= '</div></div>';
            
            $item_html .= '<div class="item-child"><div class="item-child-inner">';
            $the_query->the_post();
            $bk_final_score = get_post_meta(get_the_ID(), 'bk_final_score', true );
            $post_args['review_score'] = $bk_final_score;
            $postFormat = gloria_core::bk_post_format_detect(get_the_ID());
            if($postFormat['format'] === 'video') {
                $post_args['video_icon'] = true;
            }else {
                $post_args['video_icon'] = '';
            }
            $item_html .= $bk_contentin1->render($post_args);
            $item_html .= '</div></div>';
            
            $item_html .= '<div class="item-child"><div class="item-child-inner">';
            $the_query->the_post();
            $bk_final_score = get_post_meta(get_the_ID(), 'bk_final_score', true );
            $post_args['review_score'] = $bk_final_score;
            $postFormat = gloria_core::bk_post_format_detect(get_the_ID());
            if($postFormat['format'] === 'video') {
                $post_args['video_icon'] = true;
            }else {
                $post_args['video_icon'] = '';
            }
            $item_html .= $bk_contentin1->render($post_args);
            $item_html .= '</div></div>';
            
            $item_html .= '</li>';
            return $item_html;
        }
        
    }
}