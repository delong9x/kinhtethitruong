<?php
if (!class_exists('gloria_hero')) {
    class gloria_hero extends bk_section_parent  {
        
        public function render( $page_info ) {
            $block_str = '';
            $cfg_ops = array();
            $cfg_ops = $this->cfg_options(); 
            $module_cfg = bk_get_cfg::configs($cfg_ops['fullwidth']['bk_hero'], $page_info);    //get block config
            $tabs_amount = intval($module_cfg['tabs_amount']);
            
            $i = 0; $tabs = array();
            $uid = uniqid('hero-', true);
            $tabs[1] = $module_cfg['category_id'];
            bk_section_parent::$bk_ajax_c[$uid]['tab1']['cat'] = $module_cfg['category_id'];
            bk_section_parent::$bk_ajax_c[$uid]['tab1']['offset'] = $module_cfg['offset'];
            bk_section_parent::$bk_ajax_c[$uid]['tab1']['order'] = $module_cfg['order'];
            bk_section_parent::$bk_ajax_c[$uid]['tab1']['content'] = '';
            
            if($tabs_amount > 1) {
                $tab_cnt = 2;
                for ($i=2; $i<= $tabs_amount; $i++) {
                    if (($module_cfg['category_id'.$i] != -1) && ($module_cfg['category_id'.$i] != null)) {
                        $tabs[$tab_cnt] = $module_cfg['category_id'.$i];
                        bk_section_parent::$bk_ajax_c[$uid]['tab'.$tab_cnt]['cat'] = $module_cfg['category_id'.$i];
                        bk_section_parent::$bk_ajax_c[$uid]['tab'.$tab_cnt]['offset'] = $module_cfg['offset'.$i];
                        bk_section_parent::$bk_ajax_c[$uid]['tab'.$tab_cnt]['order'] = $module_cfg['order'.$i];
                        bk_section_parent::$bk_ajax_c[$uid]['tab'.$tab_cnt]['content'] = '';
                        $tab_cnt ++;
                    }
                }
            }
            
            $the_query = bk_get_query::query($module_cfg, $uid);              //get query
    
            $block_str .= '<div id="'.$uid.'" class="bkmodule container bkwrapper module-hero clearfix">';
            if ( $the_query->have_posts() ) :
                $block_str .= gloria_core::bk_get_block_title($page_info, $tabs, $uid);  //render block title
            endif;
            $block_str .= '<div class="bk-module-inner clearfix">';
            $block_str .= $this->render_modules($the_query);            //render modules
            $block_str .= '</div></div>';
            
            unset($cfg_ops); unset($module_cfg); unset($the_query);     //free
            wp_reset_postdata();
            return $block_str;
    	}
        static function render_modules ($the_query){
            $render_modules = '';
            $bk_contentout2 = new gloria_contentout2;
            $bk_contentout3 = new gloria_contentout3;
            if ( $the_query->have_posts() ) :            
                $render_modules .= '<div class="row clearfix">';
                
                if ( $the_query->have_posts() ) :
                    $meta_ar = array('author', 'date', 'postview', 'postcomment');
                    $post_args = array (
                        'thumbnail_size'    => 'gloria_320_218',
                        'meta_ar'           => $meta_ar,
                        'cat_meta'            => true,
                        'except_length'     => 35,
                        'rm_btn'            => true,
                        'title_length'      => ''
                    );
                    $render_modules .= '<ul class="hero-main-post clearfix col-md-8">';
                    foreach( range( 1, 2) as $i ):
                        $the_query->the_post();
                        if ( has_post_thumbnail(get_the_ID()) ) {
                            $bkPostThumb = 'hasPostThumbnail';
                            $post_args['review_score'] = get_post_meta(get_the_ID(), 'bk_final_score', true );
                        }else {
                            $bkPostThumb = 'noPostThumbnail';
                            $post_args['review_score'] = '';
                        }   
                        $postFormat = gloria_core::bk_post_format_detect(get_the_ID());
                        if($postFormat['format'] === 'video') {
                            $post_args['video_icon'] = true;
                        }else {
                            $post_args['video_icon'] = '';
                        }                                        
                        $render_modules .= '<li class="large-post row-type content_out col-md-6 col-sm-6 '.$bkPostThumb.'"><div class="row-type-inner">';
                        $render_modules .= $bk_contentout2->render($post_args);               
                        $render_modules .=  '</div></li>';      
                    endforeach;        
                    $render_modules .= '</ul>';
    
                endif;
                
                if ( $the_query->have_posts() ) :
                    $render_modules .= '<div class="col-md-4 col-sm-12 list-small-post clearfix">';
                    $render_modules .= '<ul>';
                    foreach( range( 1, $the_query->post_count - 2) as $i ):
                        $the_query->the_post();
                        $meta_ar = array('author', 'date');
                        $post_args = array (
                            'thumbnail_size'    => 'gloria_130_130',
                            'meta_ar'           => $meta_ar,
                            'cat_meta'            => '',
                            'except_length'     => '',
                            'rm_btn'            => true,
                            'review_score'      => '',                        
                        );          
                        $postFormat = gloria_core::bk_post_format_detect(get_the_ID());
                        if($postFormat['format'] === 'video') {
                            $post_args['video_icon'] = true;
                        }else {
                            $post_args['video_icon'] = '';
                        }                                                 
                        $render_modules .= '<li class="small-post content_out clearfix">';
                        $render_modules .= $bk_contentout3->render($post_args);
                        $render_modules .= '</li><!-- End post -->';        
                    endforeach;
                    $render_modules .= '</ul> <!-- End list-post -->';
                    $render_modules .= '</div><!-- End Column -->';
                endif;
                
                $render_modules .= '</div><!-- Close render modules -->';
                
            endif;
            return $render_modules;
        }
        
    }
}