<?php
if (!class_exists('gloria_main_feature')) {
    class gloria_main_feature extends bk_section_parent  {
        
        public function render( $page_info ) {
            $block_str = '';
            $cfg_ops = array();
            $cfg_ops = $this->cfg_options(); 
            $module_cfg = bk_get_cfg::configs($cfg_ops['fullwidth']['bk_main_feature'], $page_info);    //get block config
    
            $the_query = bk_get_query::query($module_cfg);              //get query
    
            $block_str .= '<div class="bkmodule module-main-feature clearfix" style="background-color: '.$module_cfg['bg_color'].'">';
            $block_str .= '<div class="bk-main-feature-inner container bkwrapper clearfix">';
            if($the_query->post_count < 5) {
                $block_str .= 'Found '.$the_query->post_count.' posts that corresponds to your conditions. This module (bk main feature) need at least 5 posts to work properly. Please try to create more posts';
            }else {
                if ( $the_query->have_posts() ) :
                    $block_str .= gloria_core::bk_get_block_title($page_info);  //render block title
                endif;
                $block_str .= $this->render_modules($the_query, $module_cfg);            //render modules
            }
            $block_str .= '</div></div>';
            
            unset($cfg_ops); unset($module_cfg); unset($the_query);     //free
            wp_reset_postdata();
            return $block_str;
    	}
        public function render_modules ($the_query, $module_cfg){
            $render_modules = '';
            $bk_contentout2 = new gloria_contentout2;
            $bk_contentout3 = new gloria_contentout3;
            if ( $the_query->have_posts() ) :   
                $render_modules .= '<div class="bk-left-block">';
                $render_modules .= '<div class="flexslider" style="background-color: '.$module_cfg['bg_color'].'">';
                if ( $the_query->have_posts() ) :
                    $render_modules .= '<ul class="slides">';
                    $meta_ar = array('author', 'date');
                    $post_args = array (
                        'thumbnail_size'    => 'gloria_660_400',
                        'meta_ar'           => $meta_ar,
                        'cat_meta'            => true,
                        'except_length'     => 32,
                        'rm_btn'            => true,
                    );
                    foreach( range( 1, $the_query->post_count - 4) as $i ):
                        $the_query->the_post();
                        if ( has_post_thumbnail(get_the_ID()) ) {
                            $bkPostThumb = 'hasPostThumbnail';
                        }else {
                            $bkPostThumb = 'noPostThumbnail';
                        }
                        $bk_final_score = get_post_meta(get_the_ID(), 'bk_final_score', true );
                        $post_args['review_score'] = $bk_final_score;
                        $render_modules .= '<li class="large-post row-type content_out '.$bkPostThumb.'">';
                        $render_modules .= $bk_contentout2->render($post_args);
                        $render_modules .=  '</li>';
                    endforeach;
                    $render_modules .= '</ul>';
    
                endif;
                $render_modules .= '</div><!-- Close slider -->';
                if ( $the_query->have_posts() ) :
                    $post_args = array (
                        'thumbnail_size'    => 'gloria_320_218',
                        'meta_ar'           => '',
                        'cat_meta'            => true,
                        'except_length'     => '',
                        'rm_btn'            => true,
                        'title_length'      => 15
                    );
                    $render_modules .= '<div class="bk-small-group clearfix">';
                    $render_modules .= '<ul>';
                    foreach( range( 1, 2) as $i ):
                        $the_query->the_post();
                        if ( has_post_thumbnail(get_the_ID()) ) {
                            $bkPostThumb = 'hasPostThumbnail';
                            $post_args['review_score'] = get_post_meta(get_the_ID(), 'bk_final_score', true );
                        }else {
                            $bkPostThumb = 'noPostThumbnail';
                            $post_args['review_score'] = '';
                        }
                        $postFormat = gloria_core::bk_post_format_detect(get_the_ID());
                        if($postFormat['format'] === 'video') {
                            $post_args['video_icon'] = true;
                        }else {
                            $post_args['video_icon'] = '';
                        }
                        $render_modules .= '<li class="content_out clearfix '.$bkPostThumb.'">';
                        $render_modules .= $bk_contentout3->render($post_args);
                        $render_modules .= '</li><!-- End post -->';        
                    endforeach;
                    $render_modules .= '</ul> <!-- End list-post -->';
                    $render_modules .= '</div><!-- End Column -->';
                endif;
                $render_modules .= '</div><!-- Close render left-block -->';
                if ( $the_query->have_posts() ) :
                    $post_args = array (
                        'thumbnail_size'    => 'gloria_320_218',
                        'meta_ar'           => '',
                        'cat_meta'            => true,
                        'except_length'     => '',
                        'rm_btn'            => true,
                        'title_length'      => 15
                    );
                    $render_modules .= '<div class="bk-right-block bk-small-group clearfix">';
                    $render_modules .= '<ul>';
                    foreach( range( 1, 2) as $i ):
                        $the_query->the_post();
                        if ( has_post_thumbnail(get_the_ID()) ) {
                            $bkPostThumb = 'hasPostThumbnail';
                            $post_args['review_score'] = get_post_meta(get_the_ID(), 'bk_final_score', true );
                        }else {
                            $bkPostThumb = 'noPostThumbnail';
                            $post_args['review_score'] = '';
                        }
                        
                        $postFormat = gloria_core::bk_post_format_detect(get_the_ID());
                        if($postFormat['format'] === 'video') {
                            $post_args['video_icon'] = true;
                        }else {
                            $post_args['video_icon'] = '';
                        }
                        $render_modules .= '<li class="content_out clearfix '.$bkPostThumb.'">';
                        $render_modules .= $bk_contentout3->render($post_args);
                        $render_modules .= '</li><!-- End post -->';        
                    endforeach;
                    $render_modules .= '</ul> <!-- End list-post -->';
                    $render_modules .= '</div><!-- Close render left-block -->';
                endif;
                
            endif;
            return $render_modules;
        }
        
    }
}