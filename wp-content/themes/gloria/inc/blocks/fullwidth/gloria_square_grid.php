<?php
if (!class_exists('gloria_square_grid')) {
    class gloria_square_grid extends bk_section_parent  {
        
        public function render( $page_info ) {
            $block_str = '';
            $cfg_ops = array();
            $cfg_ops = $this->cfg_options(); 
            $module_cfg = bk_get_cfg::configs($cfg_ops['fullwidth']['bk_square_grid'], $page_info);    //get block config
            $tabs_amount = intval($module_cfg['tabs_amount']);
            $uid = uniqid('square_grid-', true);
            $i = 0; $tabs = array();
            $has_bkwrapper = '';
            $columns = "";
            
            if (substr( $page_info['block_prefix'], 0, 10 ) == 'bk_has_rsb') {
                $has_bkwrapper = 'square-grid-2';
                $columns = "col-md-6 col-sm-6";               
            }else {
                $has_bkwrapper = 'bkwrapper container square-grid-3';  
                $columns = "col-md-4 col-sm-6";       
            }
            
            $tabs[1] = $module_cfg['category_id'];
            bk_section_parent::$bk_ajax_c[$uid]['tab1']['cat'] = $module_cfg['category_id'];
            bk_section_parent::$bk_ajax_c[$uid]['tab1']['offset'] = $module_cfg['offset'];
            bk_section_parent::$bk_ajax_c[$uid]['tab1']['order'] = $module_cfg['order'];
            bk_section_parent::$bk_ajax_c[$uid]['tab1']['content'] = '';
            bk_section_parent::$bk_ajax_c[$uid]['columns'] = $columns;
            
            if($tabs_amount > 1) {
                $tab_cnt = 2;
                for ($i=2; $i<= $tabs_amount; $i++) {
                    if (($module_cfg['category_id'.$i] != -1) && ($module_cfg['category_id'.$i] != null)) {
                        $tabs[$tab_cnt] = $module_cfg['category_id'.$i];
                        bk_section_parent::$bk_ajax_c[$uid]['tab'.$tab_cnt]['cat'] = $module_cfg['category_id'.$i];
                        bk_section_parent::$bk_ajax_c[$uid]['tab'.$tab_cnt]['offset'] = $module_cfg['offset'.$i];
                        bk_section_parent::$bk_ajax_c[$uid]['tab'.$tab_cnt]['order'] = $module_cfg['order'.$i];
                        bk_section_parent::$bk_ajax_c[$uid]['tab'.$tab_cnt]['content'] = '';
                        $tab_cnt ++;
                    }
                }
            }
            
            $the_query = bk_get_query::query($module_cfg, $uid);              //get query
    
            $block_str .= '<div id="'.$uid.'" class="bkmodule '.$has_bkwrapper.' module-square-grid">';
            if ( $the_query->have_posts() ) :
                $block_str .= gloria_core::bk_get_block_title($page_info, $tabs, $uid);  //render block title
            endif;
            $block_str .= '<div class="bk-module-inner clearfix">';
            $block_str .= $this->render_modules($the_query, $columns);            //render modules
            $block_str .= '</div><!-- Close render modules -->';
            $block_str .= '</div>';
            
            unset($cfg_ops); unset($module_cfg); unset($the_query);
            wp_reset_postdata();
            return $block_str;
    	}
        static function render_modules ($the_query, $columns){
            $render_modules = '';
            $bk_contentin1 = new gloria_contentin1;
            if ( $the_query->have_posts() ) :
               $render_modules .= '<ul class="row clearfix">';
                $post_args = array (
                    'thumbnail_size'    => 'full',
                    'cat_meta'            => true,
                    'rm_btn'            => true,
                );
                while ( $the_query->have_posts() ): $the_query->the_post();
                    $bk_final_score = get_post_meta(get_the_ID(), 'bk_final_score', true );
                    $post_args['review_score'] = $bk_final_score;
                    $postFormat = gloria_core::bk_post_format_detect(get_the_ID());
                    if($postFormat['format'] === 'video') {
                        $post_args['video_icon'] = true;
                    }else {
                        $post_args['video_icon'] = '';
                    }
                    $render_modules .= '<li class="content_in '.$columns.'">';
                    $render_modules .= '<div class="content_in_wrapper">';
                    $render_modules .= $bk_contentin1->render($post_args);
                    $render_modules .= '</div></li><!-- end post item -->';
                endwhile;
                
                $render_modules .= '</ul><!-- Close render modules -->';
            endif;
            return $render_modules;
        }
        
    }
}