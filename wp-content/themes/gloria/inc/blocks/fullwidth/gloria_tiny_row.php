<?php
if (!class_exists('gloria_tiny_row')) {
    class gloria_tiny_row extends bk_section_parent  {
        
        public function render( $page_info ) {
            $block_str = '';
            $cfg_ops = array();
            $cfg_ops = $this->cfg_options();      
            $module_cfg = bk_get_cfg::configs($cfg_ops['fullwidth']['bk_row'], $page_info);    //get block config
            
            if (substr( $page_info['block_prefix'], 0, 10 ) == 'bk_has_rsb') {
                $container = '';  
                $module_cfg['limit'] = $module_cfg['limit']*4;         
                $columns = 'col-sm-3 col-xs-6';                        
            }else {
                $container = 'bkwrapper container'; 
                $module_cfg['limit'] = $module_cfg['limit']*6;         
                $columns = 'col-md-2 col-sm-3 col-xs-6';        
            }   
            
            $the_query = bk_get_query::query($module_cfg);              //get query
            
            $block_str .= '<div class="bkmodule module-tiny-row '.$container.'">';
            if ( $the_query->have_posts() ) :
                $block_str .= gloria_core::bk_get_block_title($page_info);  //render block title
            endif;
            $block_str .= $this->render_modules($the_query, $columns);            //render modules
            $block_str .= '</div><!-- Close render modules -->';
            
            unset($cfg_ops); unset($module_cfg); unset($the_query);     //free
            wp_reset_postdata();
            return $block_str;
    	}
        static function render_modules ($the_query, $columns){
            $render_modules = '';
            $bk_contentout2 = new gloria_contentout2;
            if ( $the_query->have_posts() ) :            
                $render_modules .= '<ul class="row clearfix">';
                $meta_ar = array('author', 'date');
                $post_args = array (
                    'thumbnail_size'    => 'gloria_160_110',                
                );
                while ( $the_query->have_posts() ): $the_query->the_post();
                    if ( has_post_thumbnail(get_the_ID()) ) {
                        $bkPostThumb = 'hasPostThumbnail';
                    }else {
                        $bkPostThumb = 'noPostThumbnail';
                    }  
                    $postFormat = gloria_core::bk_post_format_detect(get_the_ID());                
                    if($postFormat['format'] == 'video') {
                        $post_args['video_icon'] = true;
                    }else {
                        $post_args['video_icon'] = '';
                                        
                    }                
                    $render_modules .= '<li class="content_out '.$columns.' '.$bkPostThumb.'">';
                    $render_modules .= $bk_contentout2->render($post_args);
                    $render_modules .= '</li><!-- end post item -->';                
                endwhile;
                
                $render_modules .= '</ul><!-- Close render modules -->';
                
            endif;
            return $render_modules;
        }
        
    }
}