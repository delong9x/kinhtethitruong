<?php
if (!class_exists('gloria_video_playlist')) {
    class gloria_video_playlist extends bk_section_parent  {
        
        public function render( $page_info ) {
            $block_str = '';
            $cfg_ops = array();
            $cfg_ops = $this->cfg_options(); 
            $module_cfg = bk_get_cfg::configs($cfg_ops['fullwidth']['bk_video_playlist'], $page_info);    //get block config
            $args = array(
                'post_type' => 'post',
    			'orderby' => 'post_date',
    			'ignore_sticky_posts' => 1,
                'post_status' => 'publish',
    			'posts_per_page' => $module_cfg['limit'],
                'offset' => $module_cfg['offset'],
                'orderby' =>  $module_cfg['order'],
            	'tax_query' => array(
            		array(
            			'taxonomy' => 'post_format',
            			'field'    => 'slug',
            			'terms' => array('post-format-video' ),
            		),
            	),
            );
            if ( $module_cfg['category_id'] >= 1 ) {
    			$args[ 'cat' ] = $module_cfg['category_id'];
    		}
            $the_query = new WP_Query($args);
            if (!( $the_query->have_posts() )) {
                return '';
            }
            $block_str .= '<div class="bkmodule container bkwrapper module-video-playlist clearfix">';
            if ( $the_query->have_posts() ) :
                $block_str .= gloria_core::bk_get_block_title($page_info);  //render block title
            endif;
            $block_str .= '<div class="bk-video-playlist-wrap">';
            $block_str .= $this->render_modules($the_query);
            $block_str .= '</div></div>';
            
            unset($cfg_ops); unset($module_cfg); unset($the_query);
            wp_reset_postdata();
            return $block_str;
    	}
        static function render_modules ($the_query){
            $render_modules = '';
            $meta_ar = array('cat', 'postview', 'postcomment');
            if ( $the_query->have_posts() ) :
                $render_modules .= '<div class="row clearfix">';
                $render_modules .= '<div class="bk-current-video col-md-8"><div class="bk-frame-wrap"></div></div>';
                $render_modules .= '<div class="bk-playlist-wrap col-md-4"><ul class="bk-playlist">';
                while ( $the_query->have_posts() ): $the_query->the_post();
                    $bkURL = get_post_meta(get_the_ID(), 'bk_media_embed_code_post', true);
                    $bkUrlParse = parse_url($bkURL);
                    if(isset($bkUrlParse['host'])) {
                        if (($bkUrlParse['host'] == 'www.youtube.com')||($bkUrlParse['host'] == 'youtube.com')) { 
                            $video_id = gloria_core::bk_parse_youtube($bkURL);
                            $dataID = 'y&+'.$video_id;
                        }else if (($bkUrlParse['host'] == 'www.vimeo.com')||($bkUrlParse['host'] == 'vimeo.com')) {
                            $video_id = gloria_core::bk_parse_vimeo($bkURL);
                            $dataID = 'v&+'.$video_id;
                        }
                        $render_modules .= '<li class="bk-video-item" data-id="'.$dataID.'">';
                        $render_modules .= '<div class="post-c-wrap clearfix">';    
                        $render_modules .= gloria_core::bk_get_feature_image('gloria_130_130', '', true);
                        $render_modules .= '<div class="video-details">';
                        $render_modules .= gloria_core::bk_get_post_meta($meta_ar, get_the_ID());
                        $render_modules .= gloria_core::bk_get_post_title(get_the_ID(), 15);
                        $render_modules .= '</div></div>';
                        $render_modules .= '</li>';
                    }
                endwhile;
                $render_modules .= '</ul></div> <!-- Close render slider -->';
                $render_modules .= '</div>';
            endif;
            return $render_modules;
        }
        
    }
}