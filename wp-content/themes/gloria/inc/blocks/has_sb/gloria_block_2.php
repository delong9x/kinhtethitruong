<?php
if (!class_exists('gloria_block_2')) {
    class gloria_block_2 extends bk_section_parent  {
        
        public function render( $page_info ) {
            $block_str = '';
            $cfg_ops = array();
            $cfg_ops = $this->cfg_options(); 
            $i = 0; $tabs = array(); $tab_cnt = 0;
            $uid = uniqid('block_2-', true);
            $module_cfg = bk_get_cfg::configs($cfg_ops['has_sb']['bk_block_2'], $page_info);    //get block config
            $tabs_amount = intval($module_cfg['tabs_amount']);
                    
            $tabs[1] = $module_cfg['category_id'];
            bk_section_parent::$bk_ajax_c[$uid]['tab1']['cat'] = $module_cfg['category_id'];
            bk_section_parent::$bk_ajax_c[$uid]['tab1']['offset'] = $module_cfg['offset'];
            bk_section_parent::$bk_ajax_c[$uid]['tab1']['order'] = $module_cfg['order'];
            bk_section_parent::$bk_ajax_c[$uid]['tab1']['content'] = '';
            
            if($tabs_amount > 1) {
                $tab_cnt = 2;
                for ($i=2; $i<= $tabs_amount; $i++) {
                    if (($module_cfg['category_id'.$i] != -1) && ($module_cfg['category_id'.$i] != null)) {
                        $tabs[$tab_cnt] = $module_cfg['category_id'.$i];
                        bk_section_parent::$bk_ajax_c[$uid]['tab'.$tab_cnt]['cat'] = $module_cfg['category_id'.$i];
                        bk_section_parent::$bk_ajax_c[$uid]['tab'.$tab_cnt]['offset'] = $module_cfg['offset'.$i];
                        bk_section_parent::$bk_ajax_c[$uid]['tab'.$tab_cnt]['order'] = $module_cfg['order'.$i];
                        bk_section_parent::$bk_ajax_c[$uid]['tab'.$tab_cnt]['content'] = '';
                        $tab_cnt ++;
                    }
                }
            }
            
            $the_query = bk_get_query::query($module_cfg, $uid);              //get query
            
            $block_str .= '<div id="'.$uid.'" class="bkmodule module-block-2 clearfix">';
            if ( $the_query->have_posts() ) :
                $block_str .= gloria_core::bk_get_block_title($page_info, $tabs, $uid);  //render block title
            endif;
            $block_str .= '<div class="bk-module-inner row clearfix">';
            $block_str .= $this->render_modules($the_query);            //render modules
            $block_str .= '</div><!-- Close render modules -->';
            $block_str .= '</div>';
            
            unset($cfg_ops); unset($module_cfg); unset($the_query);     //free
            wp_reset_postdata();
            return $block_str;
    	}
        static function render_modules ($the_query){
            $render_modules = '';
            $bk_contentout5 = new gloria_contentout5;
            $bk_contentout3 = new gloria_contentout3;
            if ( $the_query->have_posts() ) : 
                $meta_ar = array('author', 'date', 'postview', 'postcomment');
                $post_args = array (
                    'thumbnail_size'    => 'gloria_320_218',
                    'meta_ar'           => $meta_ar,
                    'cat_meta'          => true,
                    'except_length'     => 26,
                    'rm_btn'            => true,
                );        
                $the_query->the_post();   
                if ( has_post_thumbnail(get_the_ID()) ) {
                    $bkPostThumb = 'hasPostThumbnail';
                    $post_args['review_score'] = get_post_meta(get_the_ID(), 'bk_final_score', true );
                }else {
                    $bkPostThumb = 'noPostThumbnail';
                    $post_args['review_score'] = '';
                }
                $postFormat = gloria_core::bk_post_format_detect(get_the_ID());
                if($postFormat['format'] === 'video') {
                    $post_args['video_icon'] = true;
                }else {
                    $post_args['video_icon'] = '';
                }
                $render_modules .= '<div class="classic-blog-type content_out col-md-12 clearfix '.$bkPostThumb.'"><div class="classic-blog-type-inner">';
                $render_modules .= $bk_contentout5->render($post_args);
                $render_modules .= '</div></div><!-- end post item -->';            
            endif;
            if ( $the_query->have_posts() ) :        
                $meta_ar = array('author', 'date');
                $post_args = array (
                    'thumbnail_size'    => 'gloria_130_130',
                    'meta_ar'           => $meta_ar,
                    'cat_meta'            => '',
                    'rm_btn'            => true,
                );    
                $render_modules .= '<div class="col-md-12 col-sm-12 clearfix">';
                $render_modules .= '<div class="list-small-post"><ul>';
                while ( $the_query->have_posts() ): $the_query->the_post();
                    $postFormat = gloria_core::bk_post_format_detect(get_the_ID());
                    if($postFormat['format'] === 'video') {
                        $post_args['video_icon'] = true;
                    }else {
                        $post_args['video_icon'] = '';
                    }
                    $render_modules .= '<li class="small-post content_out clearfix">';
                    $render_modules .= $bk_contentout3->render($post_args);
                    $render_modules .= '</li><!-- End post -->';        
                endwhile;
                $render_modules .= '</ul> <!-- End list-post -->';
                $render_modules .= '</div></div><!-- End Column -->';
            endif;
            return $render_modules;
        }
        
    }
}