<?php
if (!class_exists('gloria_classic_blog')) {
    class gloria_classic_blog extends bk_section_parent  {
    
        public function render( $page_info ) {
            $block_str = '';
            $uid = uniqid('classic_blog-', true);
            $i = 0; $tabs = array(); $tab_cnt = 0;
            $cfg_ops = array();
            $cfg_ops = $this->cfg_options(); 
            $hide_loadmore_class = '';
            $loadmoreButton = '';
            
    // prepare ajax vars 
            $ajax_load_number = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_ajax_load_number', true );
            
            parent::$bk_ajax_c[$uid]['entries'] = $ajax_load_number;
            parent::$bk_ajax_c[$uid]['sec'] = ''; 
    ///////////////////////
                    
            $module_cfg = bk_get_cfg::configs($cfg_ops['has_sb']['bk_classic_blog'], $page_info);    //get block config
            $tabs_amount = intval($module_cfg['tabs_amount']);
            
            $tabs[1] = $module_cfg['category_id'];
            bk_section_parent::$bk_ajax_c[$uid]['tab1']['cat'] = $module_cfg['category_id'];
            bk_section_parent::$bk_ajax_c[$uid]['tab1']['offset'] = $module_cfg['offset'];
            bk_section_parent::$bk_ajax_c[$uid]['tab1']['order'] = $module_cfg['order'];
            bk_section_parent::$bk_ajax_c[$uid]['tab1']['content'] = '';
    
            if($tabs_amount > 1) {
                $tab_cnt = 2;
                for ($i=2; $i<= $tabs_amount; $i++) {
                    if (($module_cfg['category_id'.$i] != -1) && ($module_cfg['category_id'.$i] != null)) {
                        $tabs[$tab_cnt] = $module_cfg['category_id'.$i];
                        bk_section_parent::$bk_ajax_c[$uid]['tab'.$tab_cnt]['cat'] = $module_cfg['category_id'.$i];
                        bk_section_parent::$bk_ajax_c[$uid]['tab'.$tab_cnt]['offset'] = $module_cfg['offset'.$i];
                        bk_section_parent::$bk_ajax_c[$uid]['tab'.$tab_cnt]['order'] = $module_cfg['order'.$i];
                        bk_section_parent::$bk_ajax_c[$uid]['tab'.$tab_cnt]['content'] = '';
                        $tab_cnt ++;
                    }
                }
            }
            
            if($module_cfg['load_more_button'] === 'disable') {
                $loadmoreButton = 'disable';
            }else {
                $loadmoreButton = 'enable';
            }
            if($module_cfg['order'] === 'rand') {
                $hide_loadmore_class = 'hide';
            }else {
                $hide_loadmore_class = '';
            }
            $the_query = bk_get_query::query($module_cfg, $uid);              //get query
    
    // locallize ajax vars 
            //wp_localize_script( 'gloria-module-load-post', 'ajax_c', parent::$bk_ajax_c );
    
            $block_str .= '<div id="'.$uid.'" class="bkmodule module-classic-blog module-blog">';
            if ( $the_query->have_posts() ) :
                $block_str .= gloria_core::bk_get_block_title($page_info, $tabs, $uid);  //render block title
            endif;
            $block_str .= '<div class="bk-blog-wrapper row clearfix">';
            $block_str .= '<ul class="bk-blog-content clearfix">';
            $block_str .= $this::render_modules($the_query);            //render modules
            $block_str .= '</ul></div>';
            //Loadmore button   
            if ($loadmoreButton !== 'disable') { 
                $block_str .= '<div class="blog-ajax loadmore '.$hide_loadmore_class.'">';
                $block_str .= '<span class="ajaxtext ajax-load-btn">'.esc_html__("Load More","gloria").'</span>';
                $block_str .= '<span class="loading-animation"></span>';
                $block_str .= '</div>';
            }
            $block_str .= '</div>';
            unset($cfg_ops); unset($module_cfg); unset($the_query);     //free
            wp_reset_postdata();
            return $block_str;
    	}
        static function render_modules ($the_query){
            $render_modules = '';
            $bk_contentout = new gloria_contentout5;
            if ( $the_query->have_posts() ) :            
                $meta_ar = array('author', 'date', 'postview', 'postcomment');
                $post_args = array (
                    'thumbnail_size'    => 'gloria_320_218',
                    'meta_ar'           => $meta_ar,
                    'cat_meta'          => true,
                    'except_length'     => 26,
                    'rm_btn'            => true,
                );    
                while ( $the_query->have_posts() ): $the_query->the_post();
                    if ( has_post_thumbnail(get_the_ID()) ) {
                        $bkPostThumb = 'hasPostThumbnail';
                        $post_args['review_score'] = get_post_meta(get_the_ID(), 'bk_final_score', true );
                    }else {
                        $bkPostThumb = 'noPostThumbnail';
                        $post_args['review_score'] = '';
                    }
                    $postFormat = gloria_core::bk_post_format_detect(get_the_ID());
                    if($postFormat['format'] === 'video') {
                        $post_args['video_icon'] = true;
                    }else {
                        $post_args['video_icon'] = '';
                    }
                    $render_modules .= '<li class="item col-md-12">';
                    $render_modules .= '<div class="classic-blog-type content_out clearfix '.$bkPostThumb.'">';
                    $render_modules .= '<div class="classic-blog-post-wrapper">';
                    $render_modules .= $bk_contentout->render($post_args);
                    $render_modules .= '</div></div></li><!-- end post item -->';
                endwhile;
                
            endif;
            return $render_modules;
        }
    }
}