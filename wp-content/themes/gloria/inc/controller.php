<?php
// Section
    require_once(get_template_directory()."/inc/blocks/gloria_parent.php");
    
// Block Fullwidth
    require_once(get_template_directory()."/inc/blocks/fullwidth/gloria_main_feature.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/gloria_feature_2.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/gloria_grid_carousel.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/gloria_grid.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/gloria_video.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/gloria_row.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/gloria_tiny_row.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/gloria_hero.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/gloria_fw_slider.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/gloria_masonry.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/gloria_carousel.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/gloria_square_grid.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/gloria_ads.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/gloria_ads_2cols.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/gloria_ads_3cols.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/gloria_adsense.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/gloria_custom_html.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/gloria_short_code.php");
// Block has sidebar
    require_once(get_template_directory()."/inc/blocks/has_sb/gloria_block_1.php");
    require_once(get_template_directory()."/inc/blocks/has_sb/gloria_block_2.php");
    require_once(get_template_directory()."/inc/blocks/has_sb/gloria_classic_blog.php");
    require_once(get_template_directory()."/inc/blocks/has_sb/gloria_large_blog.php");
        
// Modules    
    require_once(get_template_directory()."/inc/modules/gloria_contentin1.php"); //Module Grid Square
    require_once(get_template_directory()."/inc/modules/gloria_contentin2.php"); //Module feature 2
    require_once(get_template_directory()."/inc/modules/gloria_contentin3.php"); //FW slider
    require_once(get_template_directory()."/inc/modules/gloria_contentin4.php"); //Header slider
    require_once(get_template_directory()."/inc/modules/gloria_contentout1.php"); // feature1
    require_once(get_template_directory()."/inc/modules/gloria_contentout2.php"); //row
    require_once(get_template_directory()."/inc/modules/gloria_contentout3.php"); //hero
    require_once(get_template_directory()."/inc/modules/gloria_contentout4.php"); //masonry
    require_once(get_template_directory()."/inc/modules/gloria_contentout5.php"); //classic blog & large Blog
//Libs        
    require_once(get_template_directory()."/inc/libs/gloria_get_configs.php");
    require_once(get_template_directory()."/inc/libs/gloria_core.php");
    require_once(get_template_directory()."/inc/libs/gloria_query.php");