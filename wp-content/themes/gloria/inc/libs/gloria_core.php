<?php
$wp_upload_arr = wp_upload_dir();
$gloria_default_options = array
(
    'bk-primary-color' => '#d13030',
    'bk-secondary-color' => '#333946',
    'bk-retina-display' => '1',
    'bk-smooth-scroll' => '0',
    'bk-site-layout' => 'wide',
    'bk-rtl-sw' => '0',
    'bk-sb-location-sw' => 'right',
    'bk-sb-responsive-sw' => '1',
    'bk-main-nav-layout' => 'left',
    'bk-header-type' => 'header-1',
    'bk-ajaxlogin-switch' => '1',
    'bk-headerslider-number' => '5',
    'bk-headerslider-featured' => '0',
    'bk-headerslider-featured-option' => 'Sticky',
    'bk-header-top-switch' => '1',
    'bk-cart-icon-switch' => '1',
    'bk-social-header-switch' => '0',
    'bk-social-header' => array
        (
            'fb' => '',
            'twitter' => '',
            'gplus' => '',
            'linkedin' => '',
            'pinterest' => '',
            'instagram' => '',
            'dribbble' => '',
            'youtube' => '',
            'vimeo' => '',
            'vk' => '',
            'rss' => '',
        ),

    'bk-header-logo-position' => 'left',
    'bk-header-banner-switch' => '0',
    'bk-header-banner' => array
        (
            'imgurl' => 'http://',
            'linkurl' => 'http://',
        ),

    'bk-banner-script' => '',
    'bk-fixed-nav-switch' => '1',
    'bk-header-ticker' => '0',
    'bk-ticker-title' => 'Breaking News',
    'bk-ticker-number' => '5',
    'bk-ticker-featured' => '0',
    'bk-ticker-featured-option' => 'Sticky',
    'bk-footer-instagram' => '0',
    'bk-backtop-switch' => '1',
    'cr-text' => '',
    'bk-top-menu-font' => array
        (
            'font-weight' => '600',
            'font-family' => 'Open Sans',
            'google' => '1',
        ),

    'bk-main-menu-font' => array
        (
            'font-weight' => '400',
            'font-family' => 'Open Sans',
            'google' => '1',
        ),

    'bk-review-font' => array
        (
            'font-weight' => '700',
            'font-family' => 'Archivo Narrow',
            'google' => '1',
        ),

    'bk-meta-font' => array
        (
            'font-weight' => '400',
            'font-family' => 'Archivo Narrow',
            'google' => '1',
        ),

    'bk-title-font' => array
        (
            'font-weight' => '700',
            'font-family' => 'Roboto Slab',
            'google' => '1',
        ),

    'bk-body-font' => array
        (
            'font-weight' => '400',
            'font-family' => 'Open Sans',
            'google' => '1',
        ),

    'feat-tag' => '',
    'bk-meta-review-sw' => '1',
    'bk-meta-author-sw' => '1',
    'bk-meta-date-sw' => '1',
    'bk-meta-comments-sw' => '1',
    'bk-breadcrumbs' => 'enable',
    'default-template-layout' => 'has-sb',
    'default-template-sidebar-sticky' => 'disable',
    'pagebuilder-sidebar' => 'disable',
    'bk-blog-layout' => 'classic-blog',
    'blog-stick-sidebar' => 'disable',
    'bk-author-layout' => 'classic-blog',
    'author-stick-sidebar' => 'disable',
    'bk-page-authorbox-sw' => '0',
    'bk-category-layout' => 'masonry',
    'category-stick-sidebar' => 'disable',
    'bk-archive-layout' => 'classic-blog',
    'archive-stick-sidebar' => 'disable',
    'bk-search-layout' => 'classic-blog',
    'search-stick-sidebar' => 'disable',
    'bk-search-result' => 'yes',
    'bk-shop-sidebar' => 'off',
    'shop-stick-sidebar' => 'disable',
    'bk-forum-sidebar' => 'off',
    'forum-stick-sidebar' => 'disable',
    'single-stick-sidebar' => 'disable',
    'bk-og-tag' => '0',
    'bk-sharebox-sw' => '1',
    'bk-fb-sw' => '1',
    'bk-tw-sw' => '1',
    'bk-gp-sw' => '1',
    'bk-pi-sw' => '1',
    'bk-stu-sw' => '1',
    'bk-li-sw'  => '1',
    'bk-authorbox-sw' => '0',
    'bk-postnav-sw' => '1',
    'bk-related-sw' => '1',
    'bk-comment-sw' => '1',
    'bk-recommend-box' => '1',
    'recommend-box-title' => '',
    'recommend-number' => '',
    'bk-css-code' => '',
    'REDUX_last_saved' => '1461380541',
    'REDUX_LAST_SAVE' => '1461380541'
);
if (!class_exists('gloria_core')) {
    class gloria_core {
        static function bk_get_global_var($bk_var){  
            if ($bk_var == 'gloria_option') {
                if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                    global $gloria_option;
                    return $gloria_option;
                }else {
                    global $gloria_default_options;
                    return $gloria_default_options;
                }
            }else if ($bk_var == 'justified_ids') {
                global $gloria_justified_ids;
                return $gloria_justified_ids;
            }else if ($bk_var == 'ajax_btnstr') {
                global $gloria_ajax_btnstr;
                return $gloria_ajax_btnstr;
            }
        }
    // Static variable
        static $current_template = '';
                
        static function bk_get_article_info($bkPostId){  
            global $post;
            $gloria_option = gloria_core::bk_get_global_var('gloria_option');
            $article_info = '';
            $bk_logo = array();
            $bk_logo_url = '';
            if ((isset($gloria_option['bk-logo'])) && (($gloria_option['bk-logo']) != NULL)){ 
                $bk_logo = $gloria_option['bk-logo'];
                if (($bk_logo != null) && (array_key_exists('url',$bk_logo))) {
                    $bk_logo_url = $bk_logo['url'];
                }
            };

            $bk_publisher_name = get_bloginfo('name');
            if (empty($bk_publisher_name)){
                $bk_publisher_name = esc_attr(get_the_author_meta('display_name', $post->post_author));
            }
            $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $bkPostId ), 'full' );
            $article_info .= '<meta itemprop="author" content="'.$post->post_author.'">';    
            $article_info .= '<span style="display: none;" itemprop="author" itemscope itemtype="https://schema.org/Person">' ;
            $article_info .= '<meta itemprop="name" content="' . esc_attr(get_the_author_meta('display_name', $post->post_author)) . '">' ;
            $article_info .= '</span>' ;    
            $article_info .= '<meta itemprop="headline " content="'.get_the_title($bkPostId).'">';
            $article_info .= '<meta itemprop="datePublished" content="'.date(DATE_W3C, get_the_time('U', $bkPostId)).'">';
            $article_info .= '<meta itemprop="dateModified" content="' . the_modified_date('c', '', '', false) . '">';
            $article_info .= '<meta itemscope itemprop="mainEntityOfPage" content="" itemType="https://schema.org/WebPage" itemid="' . get_permalink($bkPostId) .'"/>';
            $article_info .= '<span style="display: none;" itemprop="publisher" itemscope itemtype="https://schema.org/Organization">';
            $article_info .= '<span style="display: none;" itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">';
            $article_info .= '<meta itemprop="url" content="' . $bk_logo_url . '">';
            $article_info .= '</span>';
            $article_info .= '<meta itemprop="name" content="' . $bk_publisher_name . '">';
            $article_info .= '</span>';
            $article_info .= '<span style="display: none;" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">';
            $article_info .= '<meta itemprop="url" content="' . $thumbnail_src[0] . '">';
            $article_info .= '<meta itemprop="width" content="' . $thumbnail_src[1] . '">';
            $article_info .= '<meta itemprop="height" content="' . $thumbnail_src[2] . '">';
            $article_info .= '</span>';
            $article_info .= '<meta itemprop="interactionCount" content="UserComments:' . get_comments_number($bkPostId) . '"/>';
            return $article_info;
        }
    /*-----------------------------------------------------------------------------------*/
    # Timeline
    /*-----------------------------------------------------------------------------------*/
        static function bk_posts_timeline($posts_number = 5 , $cat = ''){
        	global $post;
        	$original_post = $post;
        
        	$args = array(
        		'posts_per_page'		 => $posts_number,
        		'cat'					 => $cat,
                'post_status'            => 'publish',
    			'ignore_sticky_posts'    => 1,
        	);
        
        	$get_posts_query = new WP_Query( $args );
        
        	if ( $get_posts_query->have_posts() ):
        		while ( $get_posts_query->have_posts() ) : $get_posts_query->the_post()?>
        		<li>
                    <div class="bk-article-wrapper" itemscope itemtype="http://schema.org/Article">
            			<a itemprop="url" href="<?php the_permalink(); ?>">
                            <div class="meta"><?php echo gloria_core::bk_meta_cases('date', get_the_ID());?></div>                     
            				<h3 itemprop="name"><?php the_title();?></h3>
            			</a>
                        <?php echo gloria_core::bk_get_article_info(get_the_ID());?>	
                    </div>
        		</li>
        		<?php
        		endwhile;
        	endif;
        	
        	$post = $original_post;
        	wp_reset_postdata();
        }
        
    /*-----------------------------------------------------------------------------------*/
    # Soundcloud Function
    /*-----------------------------------------------------------------------------------*/
        static function bk_soundcloud_iframe_output($url , $autoplay = 'false' ) {
        	$gloria_option = gloria_core::bk_get_global_var('gloria_option');;
        	
        	$color = $tie_post_color = '';
            
            $tie_post_color = $gloria_option['bk-primary-color'];
           	
        	if( !empty( $tie_post_color ) ){
        		$tie_post_color = str_replace ( '#' , '' , $tie_post_color );
        		$color = '&amp;color='.$tie_post_color;
        	}
        	
        	return '<iframe width="335" height="166" src="https://w.soundcloud.com/player/?url='.$url.$color.'&amp;auto_play='.$autoplay.'&amp;show_artwork=true"></iframe>';
        }	
        
    /*-----------------------------------------------------------------------------------*/
    # Get Ticker
    /*-----------------------------------------------------------------------------------*/		
        static function bk_get_ticker() {
            $gloria_option = gloria_core::bk_get_global_var('gloria_option');
            global $bk_ticker;
            $feat_tag = array();
            $ticker_cat = array();
            
            if (isset($gloria_option)){
                $title = $gloria_option['bk-ticker-title'];
                if ( $title == '' ) { $title = 'Breaking News';}
                
                $ticker_num = $gloria_option['bk-ticker-number'];
                if ( $ticker_num == '' ) { $ticker_num = 5;}                
                
                $featured_enable = $gloria_option['bk-ticker-featured'];
                if ($featured_enable == 1) {
                    $feat_ops =  $gloria_option['bk-ticker-featured-option'];
                    if ($feat_ops == 'Sticky') {
                        $args = array(
                			'post__in'  => get_option( 'sticky_posts' ),
                			'post_status' => 'publish',
                			'ignore_sticky_posts' => 1,
                			'posts_per_page' => $ticker_num,
                        );
                    }else {
                        $feat_tag = $gloria_option['ticker-featured-tags'];
                         if (sizeof($feat_tag)) { 
                            $args = array(
                    			'tag__in' => $feat_tag,
                    			'post_status' => 'publish',
                    			'ignore_sticky_posts' => 1,
                    			'posts_per_page' => $ticker_num,
                                );   
                        }
                    }
                }else {
                    if(isset($gloria_option['ticker-category'])) {
                        $ticker_cat = $gloria_option['ticker-category'];
                    }else {
                        $ticker_cat = '';
                    }
                        if (sizeof($ticker_cat)) {
                        $args = array(
                            'category__in'  => $ticker_cat,
                            'post_status' => 'publish',
                            'ignore_sticky_posts' => 1,
                            'posts_per_page' => $ticker_num,
                        );
                    }
                }
    
                $ticker_query = new WP_Query( $args );
                $uid = uniqid('ticker-');
                bk_section_parent::$bk_ajax_c['tickerid'] = $uid;
                bk_section_parent::$bk_ajax_c['tickertitle'] = $title;
                ?>
                <div class=" bkwrapper container">
                    <div class="bk-ticker-inner">
                        <ul id="<?php echo ($uid);?>" class="bk-ticker-wrapper">
                            <?php while ( $ticker_query->have_posts() ): $ticker_query->the_post(); ?>
                                <li class="news-item">
                                    <div class="bk-article-wrapper" itemscope itemtype="http://schema.org/Article">
                                        <?php echo gloria_core::bk_get_post_title(get_the_ID(), '');?>
                                        <?php echo gloria_core::bk_get_article_info(get_the_ID());?>	
                                    </div>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                    </div>
                </div>                        
            <?php
            wp_reset_postdata();
            }
        }
    /**
     * ************* Post Views *********************
     *---------------------------------------------------
     */ 
        static function bk_getPostViews($postID){
            $count_key = 'post_views_count';
            $count = get_post_meta($postID, $count_key, true);
            if($count==''){
                delete_post_meta($postID, $count_key);
                add_post_meta($postID, $count_key, '0');
                return "0";
           }
           return $count;
        }
        static function bk_setPostViews($postID){
            $count_key = 'post_views_count';
            $count = get_post_meta($postID, $count_key, true);
            if($count==''){
                $count = 0;
                delete_post_meta($postID, $count_key);
                add_post_meta($postID, $count_key, '0');
            }else{
                $count++;
                update_post_meta($postID, $count_key, $count);
            }
            return false;   
        }
    
        static function bk_get_block_title($page_info, $tabs = null, $uid = null){  
            $block_title = '';
            $i = 0;
            $title = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_title', true ); 
            if((strlen($title)!=0) || (($tabs != null) && (count($tabs) > 1))) {
                $block_title .= '<div class="module-title">';
                $block_title .= '<div class="main-title clearfix">';
                if(strlen($title)!=0) {
                    $block_title .= '<h2>'.$title.'</h2>';
                }
                $tab_count = count($tabs);
                if(($tabs != null) && ($tab_count > 1)) {
                    $block_title .= '<div class="bk-tabs-wrapper">';
                    $block_title .= '<div class="bk-current-tab">';
                        if ($tabs[1] == 0) {
                            $block_title .= '<span>All</span>';
                        }else {
                            $block_title .= '<span>'.get_cat_name($tabs[1]).'</span>';
                        }
                        $block_title .= '<i class="fa fa-chevron-down"></i>';                    
                    $block_title .= '</div>';
                    $block_title .= '<ul class="bk-module-tabs">';
                    if ($tabs[1] == 0) {
                        $block_title .= '<li class="bk-tab-1 bk-tabs active"><a href="#">All</a></li>';
                    }else {
                        $block_title .= '<li class="bk-tab-1 bk-tabs active"><a href="#">'.get_cat_name($tabs[1]).'</a></li>';
                    }
                    for($i=2; $i<=$tab_count; $i++) {
                        if($tabs[$i] != -1) {
                            if ($tabs[$i] == 0) {
                                $block_title .= '<li class="bk-tab-'.$i.' bk-tabs"><a href="#">All</a></li>';
                            }else {
                                $block_title .= '<li class="bk-tab-'.$i.' bk-tabs"><a href="#">'.get_cat_name($tabs[$i]).'</a></li>';
                            }
                        }
                    }
                    $block_title .= '</ul>';
                    $block_title .= '</div><!-- Close module tabs -->';                
                }
                $block_title .= '</div>';
    			$block_title .= '</div>';
            }
            return $block_title;
        }
        static function bk_get_feature_image($thumbSize, $clickable = '', $video_icon = '', $atl = '', $bkPostID = ''){
            $feat_img = '';
            if ($bkPostID == '') {
                $bkPostID = get_the_ID();
            }
            if ( has_post_thumbnail($bkPostID) ||  ($atl !== '') ) {
                $video_icon_str = '';        
                if($video_icon == true) {
                    $video_icon_str .= '<span class="play-icon"></span>';
                }       
                $feat_img .= '<div class="thumb hide-thumb">';
                if($clickable == true) {
                    $feat_img .= '<a href="'.get_permalink($bkPostID).'">';
                }
                if ( has_post_thumbnail($bkPostID)) {
                    $feat_img .= $video_icon_str;            
                    $feat_img .= get_the_post_thumbnail($bkPostID, $thumbSize);
                }else {
                    $feat_img .= '<img width="684" height="452" src="'.get_template_directory_uri().'/images/bkdefaultimg.jpg">';
                }
                if($clickable == true) {
                    $feat_img .= '</a> <!-- close a tag -->';
                }
                $feat_img .= '</div> <!-- close thumb -->';
            }
            return $feat_img;
        }
     
    
        static function bk_meta_cases( $meta_type, $bkpostID ='' ) {
            $bk_meta = $meta_type;
            $bk_meta_str = '';
            if ($bkpostID === ''){
                $bkpostID = get_the_ID();
            }        
            switch ($bk_meta) {
                case "cat":
                    $bk_meta_str .= '<div class="post-category">';
                    $bk_meta_str .= gloria_get_category_link($bkpostID);
                    $bk_meta_str .= '</div>';
                    break;
                case "date":
                    $bk_meta_str .= '<div class="post-date"><i class="fa fa-clock-o"></i>';
                    $bk_meta_str .=  get_the_date('', $bkpostID);
                    $bk_meta_str .= '</div>';
                    break;
                case "author":
                    $bk_meta_str .=  '<div class="post-author">';
                    $bk_meta_str .=  esc_html__('By', 'gloria').' <a href="'. get_author_posts_url(get_the_author_meta( 'ID' )).'">'. get_the_author() .'</a>';          
                    $bk_meta_str .=  '</div>';
                    break;
                case "bg":
                    $thumb130 = wp_get_attachment_image_src( get_post_thumbnail_id($bkpostID), 'gloria_130_130');
                    $bk_meta_str .=  '<div class="meta-bg" style="background-image:url('.$thumb130['0'].');background-size:cover;background-position:50% 50%;background-repeat:no-repeat;"></div>';
                    break;
                case "postview":
                    $bk_meta_str .=  '<div class="views"><i class="fa fa-eye"></i>'.self::bk_getPostViews($bkpostID).'</div>';
                    break;
                case "postcomment":
                    $bk_meta_str .=  '<div class="comments"><i class="fa fa-comment-o"></i>'.get_comments_number($bkpostID).'</div>';
                    break;
                default:
                    echo "No Case Matched!";
            }
            return $bk_meta_str;
        }
    
        static function bk_get_post_meta( $meta_arg, $bkpostID ) {
            $bk_meta = '';
            $bk_meta .= '<div class="meta">';
            if ((isset($meta_arg[0])) && ($meta_arg[0] != null)) {
                $bk_meta .= self::bk_meta_cases($meta_arg[0], $bkpostID);
            }
            if ((isset($meta_arg[1])) && ($meta_arg[1] != null)) {
                $bk_meta .= self::bk_meta_cases($meta_arg[1], $bkpostID);
            }
            if ((isset($meta_arg[2])) && ($meta_arg[2] != null)) {
                $bk_meta .= self::bk_meta_cases($meta_arg[2], $bkpostID);
            }
            if ((isset($meta_arg[3])) && ($meta_arg[3] != null)) {
                $bk_meta .= self::bk_meta_cases($meta_arg[3], $bkpostID);
            }
            if ((isset($meta_arg[4])) && ($meta_arg[4] != null)) {
                $bk_meta .= self::bk_meta_cases($meta_arg[4], $bkpostID);
            }
            if ((isset($meta_arg[5])) && ($meta_arg[5] != null)) {
                $bk_meta .= self::bk_meta_cases($meta_arg[5], $bkpostID);
            }
            $bk_meta .= '</div>';
            return $bk_meta;
        }
        
        static function bk_excerpt_limit_by_word($string, $word_limit){
            $words = explode(' ', $string, ($word_limit + 1));
            if(count($words) > $word_limit)
            array_pop($words);
            $strout = implode(' ', $words);
            if (strlen($strout) < strlen($string))
                $strout .=" ...";
            return $strout;
        }
        
        static function bk_get_post_title ($bkPostId, $length){
            $bk_title = '';
            $bk_title .= '<h4 itemprop="name" class="title">';
            $bk_title .= self::bk_get_title_link($bkPostId, $length);
            $bk_title .= '</h4>';
            return $bk_title;
        }
        
        static function bk_get_title_link( $bkPostId, $length ) {
            $titleLink = '';
            $titleLink .= '<a itemprop="url" href="'.get_permalink($bkPostId).'">';
            if($length != null) {
                $titleLink .= self::bk_excerpt_limit_by_word(get_the_title($bkPostId),$length);
            }else {
                $titleLink .= get_the_title($bkPostId);
            }
            $titleLink .= '</a>';
            return $titleLink;
        }
        
        static function bk_get_post_excerpt($length) {
            $bk_excerpt = '';
            $the_excerpt = get_the_excerpt();
            $bk_excerpt .= '<div class="excerpt">';
            $bk_excerpt .= self::bk_excerpt_limit_by_word($the_excerpt, $length); 
            $bk_excerpt .= '</div>';
            return $bk_excerpt;
        }
        static function bk_readmore_btn($bkPostId, $icon = '') {
            $readmore = '';
            $readmore .= '<div class="readmore">';
            if($icon != null) {
                $readmore .= $icon;
            }
            $readmore .= '<a href="'.get_permalink($bkPostId).'">'.esc_html__("Read More", "gloria").'</a>';
            $readmore .= '</div>';
            return $readmore;
        }
        
        static function bk_get_review_score($bk_final_score) {
            $bk_review = '';
        
            if (isset($bk_final_score) && ($bk_final_score != null)){
                $arc_percent = $bk_final_score * 10;
                $bk_review .= '<div class="rating-wrap"><canvas class="rating-canvas" data-rating="'.$arc_percent.'"></canvas><span>'.$bk_final_score.'</span></div>';
            }
            return $bk_review;
        }
    //Single Function
    /**
     * Video Post Format
     */
        static function bk_get_video_postFormat($postFormat) { 
            $videoFormat = '';
            if ($postFormat['url'] != null) {
                $videoFormat .= '<div class="bk-embed-video">';
                $videoFormat .= $postFormat['iframe'];
                $videoFormat .= '</div> <!-- End embed-video -->';
            }else {
                $videoFormat .= '';
            }
            return $videoFormat;
        }
    /**
     * Audio Post Format
     */
        static function bk_get_audio_postFormat($bkPostId, $postFormat, $audioType) { 
            $audioFormat = '';
            if ($postFormat['url'] != null) {
                preg_match('/src="([^"]+)"/', wp_oembed_get( $postFormat['url'] ), $match);
                
                if(isset($match[1])) {
                    $bkNewURL = $match[1];
                }else {
                    return null;
                }
    
                $audioFormat .= '<div class="bk-embed-audio"><div class="bk-frame-wrap">';
                $audioFormat .= wp_oembed_get( $postFormat['url'] );
                $audioFormat .= '</div></div>';
            }else {
                $audioFormat .= '';
            }
            return $audioFormat;
        }
     /**
     * Gallery Post Format
     */
        static function bk_get_gallery_postFormat($galleryImages) { 
            $galleryFormat = '';
            $galleryFormat .= '<div class="gallery-wrap background-preload"><div class="gallery-runner opacity-zero">';
            foreach ( $galleryImages as $image ){
                $attachment = get_post($image['ID']);
                $caption = apply_filters('the_title', $attachment->post_excerpt);
                $galleryFormat .= '<div class="item">';
                    $galleryFormat .= wp_get_attachment_image($attachment->ID, 'gloria_s_feat_img');
                    if (strlen($caption) > 0) {
                        $galleryFormat .= '<div class="img-caption">'.$caption.'</div>';
                    }
                $galleryFormat .= '</div>';
            }
            $galleryFormat .= '';
            $galleryFormat .= '</div><div class="bk-preload"></div></div><!-- Close gallery-wrap -->';
            return $galleryFormat; 
        }
     /**
     * Image Post Format
     */
        static function bk_get_image_postFormat($bkPostId) { 
            $attachmentID = get_post_meta($bkPostId, 'bk_image_upload', true );
            $thumb_url = wp_get_attachment_image_src($attachmentID, true);
            $imageFormat = '';
            $imageFormat .= '<div class="icon-play zoomer"><a class="img-popup-link" href="'.$thumb_url[0].'"><i class="fa fa-camera"></i></a></div>'; 
            return $imageFormat;
        }
    /**
     * Standard Post Format
     */
        static function bk_get_standard_postFormat($bkPostId) { 
            $imageFormat = '';
            $imageFormat .= '<div class="s-feat-img">'.get_the_post_thumbnail($bkPostId, 'gloria_660_400').'</div>';
            return $imageFormat;        
        }
    /**
     * Post Format Detect
     */
        static function bk_post_format_detect($bkPostId) { 
            $bk_format = array();
    /** Video Post Format **/
            if(function_exists('has_post_format') && ( get_post_format( $bkPostId ) == 'video')){
                $bkURL = get_post_meta($bkPostId, 'bk_media_embed_code_post', true);
                $bkUrlParse = parse_url($bkURL);
                $bk_format['format'] = 'video';
                if (isset($bkUrlParse['host']) && (($bkUrlParse['host'] == 'www.youtube.com')||($bkUrlParse['host'] == 'youtube.com'))) { 
                    $video_id = self::bk_parse_youtube($bkURL);
                    $bk_format['iframe'] = '<iframe width="1050" height="591" src="http://www.youtube.com/embed/'.$video_id.'" allowFullScreen ></iframe>';
                    $bk_format['url'] = $bkURL;
                }else if (isset($bkUrlParse['host']) && (($bkUrlParse['host'] == 'www.vimeo.com')||($bkUrlParse['host'] == 'vimeo.com'))) {
                    $video_id = self::bk_parse_vimeo($bkURL);
                    $bk_format['iframe'] = '<iframe src="//player.vimeo.com/video/'.$video_id.'?api=1&amp;title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff"></iframe>';
                    $bk_format['url'] = $bkURL;
                }else {
                    $bk_format['iframe'] = null;
                    $bk_format['url'] = null;
                    $bk_format['notice'] = esc_html__('please put youtube or vimeo video link to the video post format section', 'gloria');
                }
            }
    /** Audio Post Format **/        
            else if(function_exists('has_post_format') && (has_post_format('audio'))) {
                $bkURL = get_post_meta($bkPostId, 'bk_media_embed_code_post', true);
                $bkUrlParse = parse_url($bkURL);
                $bk_format['format'] = 'audio';
                if (isset($bkUrlParse['host']) && (($bkUrlParse['host'] == 'www.soundcloud.com')||($bkUrlParse['host'] == 'soundcloud.com'))) { 
                    $bk_format['url'] = $bkURL;
                }else {
                    $bk_format['url'] = null;
                }
            }
    /** Gallery post format **/
            else if( function_exists('has_post_format') && has_post_format( 'gallery') ) {
                $bk_format['format'] = 'gallery';
            }
    /** Image Post Format **/
            else if( function_exists('has_post_format') && has_post_format( 'image') ) {
                $bk_format['format'] = 'image';
            }
    /** Standard Post **/
            else {
                $bk_format['format'] = 'standard';
            }
            return $bk_format;
            
        }
    
    /**
     * ************* Display Post format *****************
     *---------------------------------------------------
     */ 
    
        static function bk_post_format_display($bkPostId, $bk_post_layout) { 
            $gloria_option = gloria_core::bk_get_global_var('gloria_option');
            $postFormat = array();
            $postFormat = self::bk_post_format_detect($bkPostId);
            $postFormatOut = '';
    
    /** Video **/
            if($postFormat['format'] == 'video') {
                $postFormatOut .= self::bk_get_video_postFormat($postFormat);
            }
    /** Audio **/
            else if($postFormat['format'] == 'audio') {
                $postFormatOut .= self::bk_get_audio_postFormat($bkPostId, $postFormat, null);
            }
    /** Gallery **/
            else if($postFormat['format'] == 'gallery') {
                $galleryImages = rwmb_meta( 'bk_gallery_content', $args = array('type' => 'image'), $bkPostId );
                $galleryLength = count($galleryImages); 
                if ($galleryLength == 0) {
                    return null;
                }else {
                    $postFormatOut .= self::bk_get_gallery_postFormat($galleryImages);
                }
            }
    /** Image **/
            else if($postFormat['format'] == 'image') {
                $attachmentID = get_post_meta($bkPostId, 'bk_image_upload', true );
                $thumb_url = wp_get_attachment_image_src($attachmentID, true);
                if(isset($thumb_url[0])) {
                    $postFormatOut .=  '<div class="thumb">';
                    $postFormatOut .= '<img src="'.$thumb_url[0].'" class="attachment-full wp-post-image" alt="4359236389_7da6b11ac5_o">';
                    $postFormatOut .= self::bk_get_image_postFormat($bkPostId);
                    $postFormatOut .=  '</div> <!-- End Thumb -->';
                }
            }
    /** Standard **/
            else if(($postFormat['format'] == 'standard') && ($bk_post_layout != 'fw-feat-img')) {
                $postFormatOut .= self::bk_get_standard_postFormat($bkPostId);
            }else {
                $postFormatOut .= '';
            }
            return $postFormatOut;
            
        }
    /**
     * Single Tags
     */
        static function bk_single_tags($tags) {
            $single_tags = '';
            $single_tags .= '<div class="s-tags">';
            $single_tags .= '<span>'.esc_html__('Tags', 'gloria').'</span>';
        		foreach ($tags as $tag):
        			$single_tags .= '<a href="'. get_tag_link($tag->term_id) .'" title="'. esc_attr(sprintf(esc_html__("View all posts tagged %s",'gloria'), $tag->name)) .'">'. $tag->name.'</a>';
        		endforeach;
            $single_tags .= '</div>';
            return $single_tags;
        }
    /**
     * Post Navigation
     */
        static function bk_single_post_nav($next_post, $prev_post){
            $post_nav = '';
            $post_nav .= '<div class="s-post-nav clearfix">';  
            if (!empty($prev_post)):
                $post_nav .= '<div class="nav-btn nav-prev">';
        		$post_nav .= '<div class="nav-title clearfix">';
                $post_nav .= '<span class="icon"><i class="fa fa-long-arrow-left"></i></span>';
                $post_nav .= '<span>'.esc_html__("Previous Article","gloria").'</span>';
                $post_nav .= '<h3>';
                $post_nav .= '<a href="'.get_permalink( $prev_post->ID ).'">'.self::bk_excerpt_limit_by_word(get_the_title($prev_post->ID),7).'</a>';
                $post_nav .= '</h3>';
                $post_nav .= '</div></div>';
    		endif;
            if (!empty($next_post)):
                $post_nav .= '<div class="nav-btn nav-next">';
                $post_nav .= '<div class="nav-title clearfix">';
                $post_nav .= '<span class="icon"><i class="fa fa-long-arrow-right"></i></span>';
                $post_nav .= '<span>'.esc_html__("Next Article","gloria").'</span>';
                $post_nav .= '<h3>';
                $post_nav .= '<a href="'.get_permalink( $next_post->ID ).'">'.self::bk_excerpt_limit_by_word(get_the_title($next_post->ID),7).'</a>';
                $post_nav .= '</h3>';
                $post_nav .= '</div></div>';
            endif;                
            $post_nav .= '</div>';
            return $post_nav;
        }     
        
    /**
     * Share Box
     */
        static function bk_share_box($bkPostId, $social_share){
            $share_box = '';
            $titleget = get_the_title($bkPostId);
            $bk_url = get_permalink($bkPostId);
            $bk_social_count = array();
            $bk_social_count = self::bk_social_counter($bk_url);
              
            $fb_oc = "window.open('http://www.facebook.com/sharer.php?u=".urlencode(get_permalink())."','Facebook','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;";
            $tw_oc = "window.open('http://twitter.com/share?url=".urlencode(get_permalink())."&amp;text=".str_replace(" ", "%20", $titleget)."','Twitter share','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;";
            $gp_oc = "window.open('https://plus.google.com/share?url=".urlencode(get_permalink())."','Google plus','width=585,height=666,left='+(screen.availWidth/2-292)+',top='+(screen.availHeight/2-333)+''); return false;";
            $stu_oc = "window.open('http://www.stumbleupon.com/submit?url=".urlencode(get_permalink())."','Stumbleupon','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;";
            $li_oc = "window.open('http://www.linkedin.com/shareArticle?mini=true&amp;url=".urlencode(get_permalink())."','Linkedin','width=863,height=500,left='+(screen.availWidth/2-431)+',top='+(screen.availHeight/2-250)+''); return false;";

            $share_box .= '<div class="share-box-wrap">';
            $share_box .= '<div class="share-box">';
            if (sizeof($social_share)):  
                $share_box .= '<div class="share-total-wrap">';              
                $share_box .= '<div class="share-total">';
    			$share_box .= '<div class="share-total__value">'.$bk_social_count['all'].'</div>';
    			$share_box .= '<div class="share-total__title">'.esc_html__("Shares", "gloria").'</div>';
                $share_box .= '</div></div><!-- End share-total-wrap -->'; 
            endif;
            $share_box .= '<ul class="social-share">';
                    if ($social_share['fb']):
                        $share_box .= '<li id="facebook" class="bk-share bk_facebook_share" data-url="'.$bk_url.'" data-text="'.get_the_title($bkPostId).'" data-title="Like"><div class="share-item__icon"><a onClick="'.$fb_oc.'" href="http://www.facebook.com/sharer.php?u='.urlencode($bk_url).'"><i class="fa fa-facebook " title="Facebook"></i></a></div><div class="share-item__value">'.$bk_social_count['facebook'].'</div></li>';
                    endif;
                    if ($social_share['tw']):
                        $share_box .= '<li class="bk_twitter_share"><div class="share-item__icon"><a onClick="'.$tw_oc.'" href="http://twitter.com/share?url='.urlencode(get_permalink()).'&amp;text='.str_replace(" ", "%20", $titleget).'"><i class="fa fa-twitter " title="Tweet"></i></a></div><div class="bk-twitter-share-icon">+</div></li>';
                    endif;
                    if ($social_share['gp']):
                        $share_box .= '<li id="gplus" class="bk-share bk_gplus_share" data-url="'.$bk_url.'" data-text="'.get_the_title($bkPostId).'" data-title="G+"><div class="share-item__icon"><a onClick="'.$gp_oc.'" href="https://plus.google.com/share?url='.urlencode($bk_url).'"><i class="fa fa-google-plus " title="Google Plus"></i></a></div><div class="share-item__value">'.$bk_social_count['plus_one'].'</div></li>';
                    endif;
                    if ($social_share['pi']):
                        $share_box .= '<li id="pinterest" class="bk-share bk_pinterest_share" data-url="'.$bk_url.'" data-text="'.get_the_title($bkPostId).'" data-title="Pinterest"><div class="share-item__icon"><a href="javascript:void((function()%7Bvar%20e=document.createElement(&apos;script&apos;);e.setAttribute(&apos;type&apos;,&apos;text/javascript&apos;);e.setAttribute(&apos;charset&apos;,&apos;UTF-8&apos;);e.setAttribute(&apos;src&apos;,&apos;http://assets.pinterest.com/js/pinmarklet.js?r=&apos;+Math.random()*99999999);document.body.appendChild(e)%7D)());"><i class="fa fa-pinterest " title="Pinterest"></i></a></div><div class="share-item__value">'.$bk_social_count['pinterest'].'</div></li>';
                    endif;
                    if ($social_share['stu']): 
                        $share_box .= '<li id="stumbleupon" class="bk-share bk_stumbleupon_share" data-url="'.$bk_url.'" data-text="'.get_the_title($bkPostId).'" data-title="Stumbleupon"><div class="share-item__icon"><a onClick="'.$stu_oc.'" href="http://www.stumbleupon.com/submit?url='.urlencode($bk_url).'"><i class="fa fa-stumbleupon " title="Stumbleupon"></i></a></div><div class="share-item__value">'.$bk_social_count['stumbledupon'].'</div></li>';
                    endif;
                    if ($social_share['li']):
                        $share_box .= '<li id="linkedin" class="bk-share bk_linkedin_share" data-url="'.$bk_url.'" data-text="'.get_the_title($bkPostId).'" data-title="Linkedin"><div class="share-item__icon"><a onClick="'.$li_oc.'" href="http://www.linkedin.com/shareArticle?mini=true&amp;url='.urlencode($bk_url).'"><i class="fa fa-linkedin " title="Linkedin"></i></a></div><div class="share-item__value">'.$bk_social_count['linkedin'].'</div></li>';
                    endif;         
            $share_box .= '</ul>';
      
            $share_box .= '</div></div>';
            return $share_box;
        }
    /**
     * Share Box
     */
        static function bk_sticky_share($post_id, $social_share) {
            $titleget = get_the_title($post_id);
            ?>
            <div class="share-sticky enable">
                <div class="total-share-wrap">
                    <span><i class="fa fa-share-alt"></i></span>
                    <span class="total-share">0</span>
                </div>
                <ul class="social-share">
                    <?php if ($social_share['fb']): ?>
                        <li><a class="bk-share bk_facebook_share" onClick="window.open('http://www.facebook.com/sharer.php?u=<?php echo urlencode(get_permalink($post_id));?>','Facebook','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode(get_permalink($post_id));?>"><i class="fa fa-facebook " title="Facebook"></i></a></li>
                    <?php endif; ?>
                    <?php if ($social_share['tw']): ?>
                        <li><a class="bk-share bk_twitter_share" onClick="window.open('http://twitter.com/share?url=<?php echo urlencode(get_permalink($post_id));?>&amp;text=<?php echo str_replace(" ", "%20", esc_attr($titleget)); ?>','Twitter share','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://twitter.com/share?url=<?php echo urlencode(get_permalink($post_id));?>&amp;text=<?php echo str_replace(" ", "%20", esc_attr($titleget)); ?>"><i class="fa fa-twitter " title="Twitter"></i></a></li>
                    <?php endif; ?>
                    <?php if ($social_share['gp']): ?>
                        <li><a class="bk-share bk_gplus_share" onClick="window.open('https://plus.google.com/share?url=<?php echo urlencode(get_permalink($post_id));?>','Google plus','width=585,height=666,left='+(screen.availWidth/2-292)+',top='+(screen.availHeight/2-333)+''); return false;" href="https://plus.google.com/share?url=<?php echo urlencode(get_permalink($post_id));?>"><i class="fa fa-google-plus " title="Google Plus"></i></a></li>
                    <?php endif; ?>
                    <?php if ($social_share['pi']): ?>
                        <li><a class="bk-share bk_pinterest_share" onClick="window.open('http://pinterest.com/pin/create/button/?url=<?php echo urlencode(get_permalink($post_id));?>', 'pinterest','width=600,height=300'); return false;" href="http://pinterest.com/pin/create/button/?url=<?php echo urlencode(get_permalink($post_id));?>"><i class="fa fa-pinterest" title="Pinterest"></i></a></li>
                    <?php endif; ?>
                    <?php if ($social_share['stu']): ?>
                        <li><a class="bk-share bk_stumbleupon_share" onClick="window.open('http://www.stumbleupon.com/submit?url=<?php echo urlencode(get_permalink($post_id));?>&amp;name=<?php echo str_replace(" ", "%20", esc_attr($titleget)); ?>','Stumbleupon','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://www.stumbleupon.com/submit?url=<?php echo urlencode(get_permalink($post_id)); ?>&amp;name=<?php echo str_replace(" ", "%20", esc_attr($titleget)); ?>"><i class="fa fa-stumbleupon " title="Stumbleupon"></i></a></li>
                    <?php endif; ?>
                    <?php if ($social_share['li']): ?>
                        <li><a class="bk-share bk_linkedin_share" onClick="window.open('http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo urlencode(get_permalink($post_id));?>','Linkedin','width=863,height=500,left='+(screen.availWidth/2-431)+',top='+(screen.availHeight/2-250)+''); return false;" href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo urlencode(get_permalink($post_id));?>"><i class="fa fa-linkedin " title="Linkedin"></i></a></li>
                    <?php endif; ?>                        
                </ul>
                <div class="share-sticky-close">
                    <i class="fa fa-backward"></i>
                </div>
            </div>
            <div class="share-sticky-open">
                <i class="fa fa-forward"></i>
            </div>
         <?php   
        }
    /**
    * ************* Author box *****************
    *---------------------------------------------------
    */  
        static function bk_author_details($bk_author_id, $bk_desc = true) {
            
            $bk_author_email = get_the_author_meta('publicemail', $bk_author_id);
            $bk_author_name = get_the_author_meta('display_name', $bk_author_id);
            $bk_author_tw = get_the_author_meta('twitter', $bk_author_id);
            $bk_author_go = get_the_author_meta('googleplus', $bk_author_id);
            $bk_author_fb = get_the_author_meta('facebook', $bk_author_id);
            $bk_author_yo = get_the_author_meta('youtube', $bk_author_id);
            $bk_author_www = get_the_author_meta('url', $bk_author_id);
            $bk_author_desc = get_the_author_meta('description', $bk_author_id);
            $bk_author_posts = count_user_posts( $bk_author_id ); 
        
            $bk_author = NULL;
            $bk_author .= '<div class="bk-author-box clearfix"><div class="bk-author-avatar"><a href="'.get_author_posts_url($bk_author_id).'">'. get_avatar($bk_author_id, '90').'</a></div><div class="author-info"><h3><a href="'.get_author_posts_url($bk_author_id).'">'.$bk_author_name.'</a></h3>';
                                
            if (($bk_author_desc != NULL) && ($bk_desc == true)) { $bk_author .= '<p class="bk-author-bio">'. $bk_author_desc .'</p>'; }                    
            if (($bk_author_email != NULL) || ($bk_author_www != NULL) || ($bk_author_tw != NULL) || ($bk_author_go != NULL) || ($bk_author_fb != NULL) ||($bk_author_yo != NULL)) {$bk_author .= '<div class="bk-author-page-contact">';}
            if ($bk_author_email != NULL) { $bk_author .= '<a class="bk-tipper-bottom" data-title="Email" href="mailto:'. $bk_author_email.'"><i class="fa fa-envelope " title="'.esc_html__('Email', 'gloria').'"></i></a>'; } 
            if ($bk_author_www != NULL) { $bk_author .= ' <a class="bk-tipper-bottom" data-title="Website" href="'. $bk_author_www .'" target="_blank"><i class="fa fa-globe " title="'.esc_html__('Website', 'gloria').'"></i></a> '; } 
            if ($bk_author_tw != NULL) { $bk_author .= ' <a class="bk-tipper-bottom" data-title="Twitter" href="//www.twitter.com/'. $bk_author_tw.'" target="_blank" ><i class="fa fa-twitter " title="Twitter"></i></a>'; } 
            if ($bk_author_go != NULL) { $bk_author .= ' <a class="bk-tipper-bottom" data-title="Google Plus" href="'. $bk_author_go .'" rel="publisher" target="_blank"><i title="Google+" class="fa fa-google-plus " ></i></a>'; }
            if ($bk_author_fb != NULL) { $bk_author .= ' <a class="bk-tipper-bottom" data-title="Facebook" href="'.$bk_author_fb. '" target="_blank" ><i class="fa fa-facebook " title="Facebook"></i></a>'; }
            if ($bk_author_yo != NULL) { $bk_author .= ' <a class="bk-tipper-bottom" data-title="Youtube" href="http://www.youtube.com/user/'.$bk_author_yo. '" target="_blank" ><i class="fa fa-youtube " title="Youtube"></i></a>'; }
            if (($bk_author_email != NULL) || ($bk_author_www != NULL) || ($bk_author_go != NULL) || ($bk_author_tw != NULL) || ($bk_author_fb != NULL) ||($bk_author_yo != NULL)) {$bk_author .= '</div>';}                          
            
            $bk_author .= '</div></div><!-- close author-infor-->';
                 
            return $bk_author;
        }
    /**
     * Related Layout
     *---------------------------------------------------
     */
        static function bk_related_posts_layout($bk_related_posts, $bk_number_related) {
            $meta_ar = array('date','author');
            $bk_related_layout = '';
            $bk_related_layout .= '<ul class="related-posts row clearfix">';                                            
                foreach ( $bk_related_posts as $key => $relatedpost ) { //setup global post
                    if($key > ($bk_number_related - 1))
                        break;                                   
                    setup_postdata($relatedpost);
                    if ( has_post_thumbnail($relatedpost->ID) ) {
                        $bkPostThumb = 'hasPostThumbnail';
                    }else {
                        $bkPostThumb = 'noPostThumbnail';
                    }
                    $postFormat = gloria_core::bk_post_format_detect($relatedpost->ID);
                    if($postFormat['format'] === 'video') {
                        $video_icon = true;
                    }else {
                        $video_icon = '';
                    }
                    $bk_related_layout .= '<li class="item row-type content_out col-md-4 col-sm-4 '.$bkPostThumb.'">';
                    $bk_related_layout .= self::bk_get_feature_image('gloria_660_400', true, $video_icon, '', $relatedpost->ID);
                    $bk_related_layout .= self::bk_meta_cases('cat', $relatedpost->ID);
                    $bk_related_layout .= '<div class="post-c-wrap">';
                    $bk_related_layout .= '<h4>';
                    $bk_related_layout .= self::bk_get_title_link( $relatedpost->ID, 20 );
                    $bk_related_layout .= '</h4>';
                    //$bk_related_layout .= '<h4><a href='.get_the_permalink($relatedpost->ID).'>'.get_the_title($relatedpost->ID).'</a></h4>';
                    $bk_related_layout .= self::bk_get_post_meta($meta_ar, $relatedpost->ID);
                    $bk_related_layout .= '</div>';
                    $bk_related_layout .= '</li>';
                }
            $bk_related_layout .= '</ul>';
            wp_reset_postdata();    
            return $bk_related_layout;
        }
    /**
     * ************* Related Post *****************
     *---------------------------------------------------
     */            
        static function bk_related_posts($bk_number_related) {
            global $post;
            $bkPostId = $post->ID;
            if (is_attachment() && ($post->post_parent)) { $bkPostId = $post->post_parent; };
            $i = 1;
            $bk_related_posts = array();
            $bk_relate_tags = array();
            $bk_relate_categories = array();
            $excludeid = array();
            $bk_number_related_remain = 0;
            $bk_related_output = '';
            array_push($excludeid, $bkPostId);
    
            $bk_tags = wp_get_post_tags($bkPostId);
            $tag_length = sizeof($bk_tags);                               
            $bk_tag_check = $bk_all_cats = NULL;
     
     // Get tag post
            if ($tag_length > 0){
                foreach ( $bk_tags as $bk_tag ) { $bk_tag_check .= $bk_tag->slug . ','; }             
                    $bk_related_args = array(   'numberposts' => $bk_number_related, 
                                                'tag' => $bk_tag_check, 
                                                'post__not_in' => $excludeid,
                                                'post_status' => 'publish',
                                                'orderby' => 'rand'  );
                $bk_relate_tags_posts = get_posts( $bk_related_args );
                $bk_number_related_remain = $bk_number_related - sizeof($bk_relate_tags_posts);
                if(sizeof($bk_relate_tags_posts) > 0){
                    foreach ( $bk_relate_tags_posts as $bk_relate_tags_post ) {
                        array_push($excludeid, $bk_relate_tags_post->ID);
                        array_push($bk_related_posts, $bk_relate_tags_post);
                    }
                }
            }
     // Get categories post
            $bk_categories = get_the_category($bkPostId);  
            $category_length = sizeof($bk_categories);       
            if ($category_length > 0){       
                foreach ( $bk_categories as $bk_category ) { $bk_all_cats .= $bk_category->term_id  . ','; }
                    $bk_related_args = array(  'numberposts' => $bk_number_related_remain, 
                                            'category' => $bk_all_cats, 
                                            'post__not_in' => $excludeid, 
                                            'post_status' => 'publish', 
                                            'orderby' => 'rand'  );
                $bk_relate_categories = get_posts( $bk_related_args );
    
                if(sizeof($bk_relate_categories) > 0){
                    foreach ( $bk_relate_categories as $bk_relate_category ) {
                        array_push($bk_related_posts, $bk_relate_category);
                    }
                }
            }
            if ( $bk_related_posts != NULL ) {
                $bk_related_output .= '<div class="bk-related-posts">';
                $bk_related_output .= self::bk_related_posts_layout($bk_related_posts, $bk_number_related);
                $bk_related_output .= '</div><!--End related posts containter-->';
            }    
            return $bk_related_output;
        }
    /**
     * BK Get Feature Image Background (Full Width)
     */ 
        static function bk_get_feature_image_bg($bkPostId) {
            $parallax_featImg = get_post_meta(get_the_ID(),'bk_parallax_single',true);
            $style = get_post_meta(get_the_ID(),'bk_fwstyle',true);   
            $bk_feat_img = '';
            if($parallax_featImg == 1) {
                $parallax_class = 'parallax_enable';
            }else {
                $parallax_class = '';
            }
            $bkThumbId = get_post_thumbnail_id( $bkPostId );
            $bkThumbUrl = wp_get_attachment_image_src( $bkThumbId, 'full' );
            $bk_feat_img .= '<div class="fw-feat-img '.$style.' '.$parallax_class.'" data-type="background" style="background-image: url('.$bkThumbUrl[0].')"></div>';
            return $bk_feat_img;
        }
    /**
     *  BK Get Feature Image
     */
        static function bk_get_single_interface($bkPostId, $bk_post_layout) {
            $postFormat = gloria_core::bk_post_format_detect($bkPostId);
            $bkReviewSW = get_post_meta($bkPostId, 'bk_review_checkbox',true);
            $feat_image_sts = get_post_meta($bkPostId,'bk_feat_img_status',true);
            $bk_feat_img = '';
    
            if ($bk_post_layout != 'fw-feat-img') {
                if(($postFormat['format'] == 'standard') && (($feat_image_sts == 'off') || ( !has_post_thumbnail($bkPostId) ))) {
                    return $bk_feat_img;
                }else {
                    if($bk_post_layout == 'bk-normal2-feat') {
                        $bk_feat_img .= '<header id="bk-normal2-feat" class="clearfix">'; 
                    }else {
                        $bk_feat_img .= '<header id="bk-normal-feat" class="clearfix">'; 
                    }    
                    
                    $bk_feat_img .= self::bk_post_format_display($bkPostId, $bk_post_layout);
                    $bk_feat_img .= '</header>';     
                }
    
            }else {
                if(($postFormat['format'] == 'video') || ($postFormat['format'] == 'audio') || ($postFormat['format'] == 'image')) {
                    $bk_feat_img .= '<header id="bk-normal-feat" class="clearfix">'; 
                    $bk_feat_img .= self::bk_post_format_display($bkPostId, $bk_post_layout);
                    $bk_feat_img .= '</header>';
                }
            }
            return $bk_feat_img;
        }
    /**
     * BK WP Native Gallery
     */
         static function bk_native_gallery($bkPostId, $attachment_ids){
            global $gloria_justified_ids;
            $uid = rand();
            $gloria_justified_ids[]=$uid;
            wp_localize_script( 'gloria-customjs', 'justified_ids', $gloria_justified_ids );
            $ret = '';
            
            $ret .= '<div class="zoom-gallery justifiedgall_'.$uid.' justified-gallery" style="margin: 0px 0px 1.5em;">';
    						if ($attachment_ids) :					
    						foreach ($attachment_ids as $id) :
    							$attachment_url = wp_get_attachment_image_src( $id, 'full' );
                                $attachment = get_post($id);
    							$caption = apply_filters('the_title', $attachment->post_excerpt);
    					
                                $ret .= '<a class="zoomer" title="'.$caption.'" data-source="'.$attachment_url[0].'" href="'.$attachment_url[0].'">'.wp_get_attachment_image($attachment->ID, 'full').'</a>';
    
    						endforeach;
    						endif;
    			$ret .= '</div>';
                return $ret;
         }
    /**
     *  Single Content
     */    
        static function bk_single_content($bkPostId){
            $the_content = '';
            $the_content = apply_filters( 'the_content', get_the_content($bkPostId) );
            $the_content = str_replace( ']]>', ']]&gt;', $the_content );
    
            $post_content_str = get_the_content($bkPostId);
            $gallery_flag = 0;
            $i = 0;
            $ids = null;
            for ($i=0; $i < 10; $i++) {
                preg_match('/<style(.+(\n))+.*?<\/div>/', $the_content, $match);
                    
                preg_match('/\[gallery.*ids=.(.*).\]/', $post_content_str, $ids);             
                
                if ($ids != null) {
                    $the_content = str_replace($match[0], $ids[0] ,$the_content);          
                       
                    $attachment_ids = explode(",", $ids[1]);
                    $post_content_str = str_replace($ids[0], self::bk_native_gallery ($bkPostId, $attachment_ids),$post_content_str);
                    $the_content = str_replace($ids[0], self::bk_native_gallery ($bkPostId, $attachment_ids),$the_content);
                    $gallery_flag = 1;
                }
            }
            if($gallery_flag == 1) {
                return $the_content;
            }else {
                return the_content($bkPostId);
            }
        }
    /**
     * Review Box
     */
    /**
     * Canvas box 
     */
        static function bk_user_review($bk_post_id){
            $gloria_option = gloria_core::bk_get_global_var('gloria_option');
            $bk_user_rating = get_post_meta($bk_post_id, 'bk_user_rating', true );
            if($bk_user_rating) {
                $bk_user_review = '';
                $bk_number_rates = get_post_meta($bk_post_id, "bk_rates", true);
                $bk_user_score = get_post_meta($bk_post_id, "bk_user_score_output", true); 
                if ($bk_number_rates == NULL) {$bk_number_rates = 0;}
                if ($bk_user_score == NULL) {$bk_user_score = 0;}
                $bk_average_score = '<span class="bk-criteria-score bk-average-score hide">'.  floatval($bk_user_score) .'</span>'; 
                if(isset($_COOKIE["bk_user_rating"])) { $bk_current_rates = $_COOKIE["bk_user_rating"]; } else { $bk_current_rates = NULL; }
                if(isset($_COOKIE["bk_score_rating"])) { $bk_current_score = $_COOKIE["bk_score_rating"]; } else { $bk_current_score = NULL; }
    
                if ( preg_match('/\b' .$bk_post_id . '\b/', $bk_current_rates) ) {
                     $bk_class = " bk-rated"; 
                     $bk_tip_class = ' bk-tipper-bottom'; 
                     $bk_tip_title = ' data-title= "You have rated '.$bk_current_score.' points for this post"'; 
                } else {
                     $bk_class = $bk_tip_title = $bk_tip_class = NULL; 
                }
    
                if ( $bk_number_rates == '1' ) {
                    $bk_rate_str = esc_html__("Rate", "gloria");
                }  else {
                    $bk_rate_str = esc_html__("Rates", "gloria");
                }             
                $bk_user_review .= '<div class="bk-bar bk-user-rating clearfix"><div id="bk-rate" class="bg bk-criteria-wrap clearfix '. $bk_class .'"><span class="bk-criteria">'. esc_html__("Reader Rating", "gloria"). ': (<span>'. $bk_number_rates .'</span> '. $bk_rate_str .')</span>';
                
                $bk_user_review .= '<div class="bk-overlay'. $bk_tip_class .'"'. $bk_tip_title .'><span class="user-score-initial bk-user-ani" style="width:'. $bk_user_score*10 .'%">'.$bk_average_score.'</span></div></div></div>';
    
                 if ( function_exists('wp_nonce_field') ) { $bk_user_review .= wp_nonce_field('rating_nonce', 'rating_nonce', true, false); } 
                 
                 return $bk_user_review;
            }
        }
    /**
     * Canvas box 
     */
         static function bk_canvas_box($bk_final_score){
            $gloria_option = gloria_core::bk_get_global_var('gloria_option');
            $bk_best_rating = '10';
            $bk_review_final_score = floatval($bk_final_score);  
            $arc_percent = $bk_final_score*10;
            $bk_canvas_ret = '';                                       
            $score_circle = '<canvas class="rating-canvas" data-rating="'.$arc_percent.'"></canvas>';           
            $bk_canvas_ret .= '<div class="bk-score-box" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">'.$score_circle.'<meta itemprop="worstRating" content="1"><meta itemprop="bestRating" content="'. $bk_best_rating .'"><span class="score" itemprop="ratingValue">'.$bk_review_final_score.'</span></div><!--close canvas-->';
            return $bk_canvas_ret;
        }
    /**
    * ************* Display post review box ********
    *---------------------------------------------------
    */
        static function bk_post_review_boxes($bk_post_id, $reviewPos){
            $gloria_option = gloria_core::bk_get_global_var('gloria_option');
            if (isset($gloria_option)){
                $primary_color = $gloria_option['bk-primary-color'];
            }
            $bk_custom_fields = get_post_custom();
            $bk_review_checkbox = get_post_meta($bk_post_id, 'bk_review_checkbox', true );
    
            if ( $bk_review_checkbox == '1' ) {
                 $bk_review_checkbox = 'on'; 
            } else {
                 $bk_review_checkbox = 'off';
            }
            if ($bk_review_checkbox == 'on') {
                $bk_summary = get_post_meta($bk_post_id, 'bk_summary', true );                
                $bk_final_score = get_post_meta($bk_post_id, 'bk_final_score', true );        
                $criteria_val = rwmb_meta( 'reviewcriterias' );
    
                for ($i = 0; $i < count($criteria_val); $i++) {
                    ${"bk_rating_".$i."_title"} = $criteria_val[$i]['bk_ct'];
                    ${"bk_rating_".$i."_score"} = $criteria_val[$i]['bk_cs'];
                }
                
                $bk_ratings = array();  
            
                for( $i = 0; $i < count($criteria_val); $i++ ) {
                     if ( isset(${"bk_rating_".$i."_score"}) ) { $bk_ratings[] =  ${"bk_rating_".$i."_score"};}
                }
                $bk_review_ret = '<div class="bk-review-box '.$reviewPos.' clearfix"><div class="bk-detail-rating clearfix">'; 
                for( $j = 0; $j < count($criteria_val); $j++ ) {
                     if ((isset(${"bk_rating_".$j."_title"})) && (isset(${"bk_rating_".$j."_score"})) ) {                       
                            $bk_review_ret .= '<div class="bk-criteria-wrap"><span class="bk-criteria">'. ${"bk_rating_".$j."_title"}.'</span>';                                     
                            $bk_review_ret .= '<div class="bk-bar clearfix"><span class="bk-overlay"><span class="bk-zero-trigger" style="width:'. ( ${"bk_rating_".$j."_score"}*10).'%"><span class="bk-criteria-score hide">'. $bk_ratings[$j].'</span></span></span></div></div>';
                     }
                }
                $bk_review_ret .= '</div>';
                $bk_review_ret .= '<div class="summary-wrap clearfix">';
                $bk_review_ret .= self::bk_canvas_box($bk_final_score);
                if ( $bk_summary != NULL ) { $bk_review_ret .= '<div class="bk-summary"><div id="bk-conclusion" itemprop="description">'.$bk_summary.'</div></div>'; }                                    
                $bk_review_ret .= '</div><!-- /bk-author-review-box -->';
                $bk_review_ret .= self::bk_user_review($bk_post_id);
                
                //Pros & Cons List
                if (isset($bk_custom_fields['bk_pros_cons_enable']) && ($bk_custom_fields['bk_pros_cons_enable'][0] === 'enable')) {
                    
                    if(isset($bk_custom_fields['bk_pro_title'])) {
                        $bk_pros_title = $bk_custom_fields['bk_pro_title'][0];
                    }
                    if(isset($bk_custom_fields['bk_pros'])){
                        $bk_pros_length[] = unserialize($bk_custom_fields['bk_pros'][0]);
                    }
                    if(isset($bk_custom_fields['bk_con_title'])) {
                        $bk_cons_title = $bk_custom_fields['bk_con_title'][0];
                    }
                    if(isset($bk_custom_fields['bk_cons'])) {
                        $bk_cons_length[] = unserialize($bk_custom_fields['bk_cons'][0]);
                    }
        
                    $bk_review_ret .= '<div class="bk-pros-cons clearfix">';
                    $bk_review_ret .= '<div class="bk-pros-wrap">';
                    if(isset($bk_pros_title) && ($bk_pros_title != '')){
                        $bk_review_ret .= '<h3 class="pros-cons-title">'.$bk_pros_title.'</h3>';
                    }
                    if(count($bk_pros_length[0] > 0)) {
                        $bk_review_ret .= '<ul class="bk-pros-list">';
                        
                            for($i=0; $i<count($bk_pros_length[0]);$i++){
                                $bk_review_ret .= '<li class="bk-pro">'.$bk_pros_length[0][$i].'</li>';
                            }
                        $bk_review_ret .= '</ul>';
                    }
                    
                    $bk_review_ret .= '</div><!--bk-pros close-->';
                    
                    $bk_review_ret .= '<div class="bk-cons-wrap">';
                    if(isset($bk_cons_title) && ($bk_cons_title != '')){
                        $bk_review_ret .= '<h3 class="pros-cons-title">'.$bk_cons_title.'</h3>';
                    }
                    if(count($bk_cons_length[0] > 0)) {
                    $bk_review_ret .= '<ul class="bk-cons-list">';
                        for($i=0; $i<count($bk_cons_length[0]);$i++){
                            $bk_review_ret .= '<li class="bk-con">'.$bk_cons_length[0][$i].'</li>';
                        }
                    $bk_review_ret .= '</ul>';
                    }
                    $bk_review_ret .= '</div><!--bk-cons close-->';
                    
                    $bk_review_ret .= '</div><!--bk-pros-cons close-->';
                }
                $bk_review_ret .= '</div> <!--bk-review-box close-->';
                return $bk_review_ret;
            }
        }
    /**
    * ************* Get youtube ID  *****************
    *---------------------------------------------------
    */ 
      
        static function bk_parse_youtube($link){
         
            $regexstr = '~
                # Match Youtube link and embed code
                (?:                             # Group to match embed codes
                    (?:<iframe [^>]*src=")?       # If iframe match up to first quote of src
                    |(?:                        # Group to match if older embed
                        (?:<object .*>)?      # Match opening Object tag
                        (?:<param .*</param>)*  # Match all param tags
                        (?:<embed [^>]*src=")?  # Match embed tag to the first quote of src
                    )?                          # End older embed code group
                )?                              # End embed code groups
                (?:                             # Group youtube url
                    https?:\/\/                 # Either http or https
                    (?:[\w]+\.)*                # Optional subdomains
                    (?:                         # Group host alternatives.
                    youtu\.be/                  # Either youtu.be,
                    | youtube\.com              # or youtube.com
                    | youtube-nocookie\.com     # or youtube-nocookie.com
                    )                           # End Host Group
                    (?:\S*[^\w\-\s])?           # Extra stuff up to VIDEO_ID
                    ([\w\-]{11})                # $1: VIDEO_ID is numeric
                    [^\s]*                      # Not a space
                )                               # End group
                "?                              # Match end quote if part of src
                (?:[^>]*>)?                       # Match any extra stuff up to close brace
                (?:                             # Group to match last embed code
                    </iframe>                 # Match the end of the iframe
                    |</embed></object>          # or Match the end of the older embed
                )?                              # End Group of last bit of embed code
                ~ix';
        
            preg_match($regexstr, $link, $matches);
        
            return $matches[1];
        
        }
        
    /**
     * ************* Get vimeo ID *****************
     *---------------------------------------------------
     */  
        
        static function bk_parse_vimeo($link){
         
            $regexstr = '~
                # Match Vimeo link and embed code
                (?:<iframe [^>]*src=")?       # If iframe match up to first quote of src
                (?:                         # Group vimeo url
                    https?:\/\/             # Either http or https
                    (?:[\w]+\.)*            # Optional subdomains
                    vimeo\.com              # Match vimeo.com
                    (?:[\/\w]*\/videos?)?   # Optional video sub directory this handles groups links also
                    \/                      # Slash before Id
                    ([0-9]+)                # $1: VIDEO_ID is numeric
                    [^\s]*                  # Not a space
                )                           # End group
                "?                          # Match end quote if part of src
                (?:[^>]*></iframe>)?        # Match the end of the iframe
                (?:<p>.*</p>)?              # Match any title information stuff
                ~ix';
        
            preg_match($regexstr, $link, $matches);
        
            return $matches[1];
        }
        /**
         * ************* Get Dailymotion ID *****************
         *---------------------------------------------------
         */  
        static function bk_parse_dailymotion($link){
            preg_match('#<object[^>]+>.+?http://www.dailymotion.com/swf/video/([A-Za-z0-9]+).+?</object>#s', $link, $matches);
        
                // Dailymotion url
                if(!isset($matches[1])) {
                    preg_match('#http://www.dailymotion.com/video/([A-Za-z0-9]+)#s', $link, $matches);
                }
        
                // Dailymotion iframe
                if(!isset($matches[1])) {
                    preg_match('#http://www.dailymotion.com/embed/video/([A-Za-z0-9]+)#s', $link, $matches);
                }
            return $matches[1];
        }
        
        /**
         * Breadcrumbs
         */
        static function bk_breadcrumbs($options = array()){
        		
    		global $post;
     
    		$text['home']     = _x('Home', 'breadcrumbs', 'gloria'); // text for the 'Home' link
    		$text['category'] = esc_html__('Category: "%s"', 'gloria'); // text for a category page
    		$text['tax'] 	  = esc_html__('Archive for "%s"', 'gloria'); // text for a taxonomy page
    		$text['search']   = esc_html__('Search Results for "%s"', 'gloria'); // text for a search results page
    		$text['tag']      = esc_html__('Posts Tagged "%s"', 'gloria'); // text for a tag page
    		$text['author']   = esc_html__('Author: %s', 'gloria'); // text for an author page
    		$text['404']      = esc_html__('Error 404', 'gloria'); // text for the 404 page
    	
    		$defaults = array(
    			'show_current' => 1, // 1 - show current post/page title in breadcrumbs, 0 - don't show
    			'show_on_home' => 0, // 1 - show breadcrumbs on the homepage, 0 - don't show
    			'delimiter' => '<span class="delim">&rsaquo;</span>',
    			'before' => '<span class="current">',
    			'after' => '</span>',
    			
    			'home_before' => '',
    			'home_after' => '',
    			'home_link' => home_url('/') . '/',
    
    			'link_before' => '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">',
    			'link_after'  => '</span>',
    			'link_attr'   => ' itemprop="url" ',
    			'link_in_before' => '<span itemprop="title">',
    			'link_in_after'  => '</span>'
    		);
    		
    		extract($defaults);
    		
    		$link = '<a itemprop="url" href="%1$s">' . $link_in_before . '%2$s' . $link_in_after . '</a>';
    				
    		// form whole link option
    		$link = $link_before . $link . $link_after;
    
    		if (isset($options['text'])) {
    			$options['text'] = array_merge($text, (array) $options['text']);
    		}
    
    		// override defaults
    		extract($options);
    
    		// regex replacement
    		$replace = $link_before . '<a' . $link_attr . '\\1>' . $link_in_before . '\\2' . $link_in_after . '</a>' . $link_after;
    		
            /*
    		 * Use bbPress's breadcrumbs when available
    		 */
    		if (function_exists('bbp_breadcrumb') && is_bbpress()) {		
    			$bbp_crumbs = 
    				bbp_get_breadcrumb(array(
    					'home_text' => $text['home'],
    					'sep' => '<span class="delim">&rsaquo;</span>',
    					'sep_before' => '',
    					'sep_after'  => '',
    					'pad_sep' => 0,
    					'before' => '<div class="bk-breadcrumbs-wrap"><div class="breadcrumbs">' . $home_before,
    					'after' => $home_after . '</div></div>',
    					'current_before' => $before,
    					'current_after'  => $after,
    				));
    			
    			if ($bbp_crumbs) {
    				echo ($bbp_crumbs);
    				return;
    			}
    		}
            
    		/*
    		 * Use WooCommerce's breadcrumbs when available
    		 */
    		if (function_exists('woocommerce_breadcrumb') && (is_woocommerce() OR is_cart() OR is_shop())) {
    			woocommerce_breadcrumb(array(
    				'delimiter' => '<span class="delim">&rsaquo;</span>',
    				'before' => '',
    				'after' => '',
    				'wrap_before' => '<div class="bk-breadcrumbs-wrap"><div class="breadcrumbs">' . $home_before,
    				'wrap_after' => $home_after . '</div></div>',
    				'home' => $text['home'],
    			));
    			
    			return;
    		}
    		
    		// normal breadcrumbs
    		if ((is_home() || is_front_page())) {
    			
    			if ($show_on_home == 1) {
    				echo '<div class="bk-breadcrumbs-wrap"><div class="breadcrumbs">'. $home_before . '<a href="' . $home_link . '">' . $text['home'] . '</a>'. $home_after .'</div>';
    			}
    	
    		} else {
    	
    			echo '<div class="bk-breadcrumbs-wrap"><div class="breadcrumbs">' . $home_before . sprintf($link, $home_link, $text['home']) . $home_after . $delimiter;
    			
    			if (is_category() || is_tax()) 
    			{
    				$the_cat = get_category(get_query_var('cat'), false);
    				
    				// have parents?
    				if ($the_cat->parent != 0) {
    					
    					$cats = get_category_parents($the_cat->parent, true, $delimiter);
    					$cats = preg_replace('#<a([^>]+)>([^<]+)</a>#', $replace, $cats);
    					
    					echo ($cats);
    				}
    								
    				// print category
    				echo $before . sprintf((is_category() ? $text['category'] : $text['tax']), single_cat_title('', false)) . $after;
    	
    			}
    			else if (is_search()) {
    				
    				echo $before . sprintf($text['search'], get_search_query()) . $after;
    	
    			}
    			else if (is_day()) {
    				
    				echo  sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter
    					. sprintf($link, get_month_link(get_the_time('Y'),get_the_time('m')), get_the_time('F')) . $delimiter
    					. $before . get_the_time('d') . $after;
    	
    			}
    			else if (is_month()) {
    				
    				echo  sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter
    					. $before . get_the_time('F') . $after;
    	
    			}
    			else if (is_year()) {
    				
    				echo $before . get_the_time('Y') . $after;
    	
    			}
    			// single post or page
    			else if (is_single() && !is_attachment()) {
    				
    				// custom post type
    				if (get_post_type() != 'post') {
    					
    					$post_type = get_post_type_object(get_post_type());
    					printf($link, get_post_type_archive_link(get_post_type()), $post_type->labels->name);
    					
    					if ($show_current == 1) {
    						echo $delimiter . $before . get_the_title() . $after;	
    					}
    				}
    				else {
    					
    					$cat = get_the_category();
    					$cats = get_category_parents($cat[0], true, $delimiter);
    					
    					if ($show_current == 0) {
    						$cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);	
    					}
    
    					$cats = preg_replace('#<a([^>]+)>([^<]+)</a>#', $replace, $cats);
    					
    					echo ($cats);
    					
    					if ($show_current == 1) {
    						echo ($before . get_the_title() . $after);	
    					}
    				}
    	
    			}
    			elseif (!is_single() && !is_page() && get_post_type() != 'post' && !is_404()) {
    
    				$post_type = get_post_type_object(get_post_type());
    				
    				echo $before . $post_type->labels->name . $after;
    	
    			} 
    			elseif (is_attachment()) {
    				
    				$parent = get_post($post->post_parent);
    				$cat = current(get_the_category($parent->ID));
    				$cats = get_category_parents($cat, true, $delimiter);
    				
    				if (!is_wp_error($cats)) {
    					$cats = preg_replace('#<a([^>]+)>([^<]+)</a>#', $replace, $cats);
    					echo ($cats);
    				}
    				
    				printf($link, get_permalink($parent), $parent->post_title);
    				
    				if ($show_current == 1) {
    					echo $delimiter . $before . get_the_title() . $after;	
    				}
    	
    			}
    			elseif (is_page() && !$post->post_parent && $show_current == 1) {
    	
    				echo $before . get_the_title() . $after;
    	
    			} 
    			elseif (is_page() && $post->post_parent) {
    				
    				$parent_id  = $post->post_parent;
    				$breadcrumbs = array();
    				
    				while ($parent_id) {
    					$page = get_post($parent_id);
    					$breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
    					$parent_id  = $page->post_parent;
    				}
    				
    				$breadcrumbs = array_reverse($breadcrumbs);
    				
    				for ($i = 0; $i < count($breadcrumbs); $i++) {
    					
    					echo ($breadcrumbs[$i]);
    					
    					if ($i != count($breadcrumbs)-1) {
    						echo ($delimiter);	
    					}
    				}
    				
    				if ($show_current == 1) {
    					echo $delimiter . $before . get_the_title() . $after;	
    				}
    	
    			}
    			elseif (is_tag()) {
    				echo $before . sprintf($text['tag'], single_tag_title('', false)) . $after;
    	
    			}
    			elseif (is_author()) {
    				
    				global $author;
    				
    				$userdata = get_userdata($author);
    				echo $before . sprintf($text['author'], $userdata->display_name) . $after;
    	
    			}
    			elseif (is_404()) {
    				echo $before . $text['404'] . $after;
    			}
    	
    			// have pages?
    			if (get_query_var('paged')) {
    				
    				if (is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author()) {
    					echo ' (' . esc_html__('Page', 'gloria') . ' ' . get_query_var('paged') . ')';
    				}
    			}
    	
    			echo '</div></div>';
    		}
    	
    	} // breadcrumbs()
    // Feature Posts Section Category pages
        static function bk_query_posts_from_category($bk_cat_id){
            $gloria_option = gloria_core::bk_get_global_var('gloria_option');
            $feat_tag = '';
            if (isset($gloria_option['feat-tag']) && ($gloria_option['feat-tag'] != '')){
                $feat_tag = $gloria_option['feat-tag'];
            }                                
            if ($feat_tag != '') {
                $args = array(
    				'tag__in' => $feat_tag,
                    'cat' => $bk_cat_id,
        			'post_status' => 'publish',
        			'ignore_sticky_posts' => 1,
        			'posts_per_page' => 10,
                );   
            } else {
                $args = array(
    				'post__in'  => get_option( 'sticky_posts' ),
                    'cat' => $bk_cat_id,
    				'post_status' => 'publish',
    				'ignore_sticky_posts' => 1,
    				'posts_per_page' => 10,
                );
            }
            $the_query = new WP_Query( $args );
            wp_reset_postdata();
            return $the_query;
        }
        static function bk_cat_feature_posts_section($bk_cat_id, $bk_layout){
            $the_query = gloria_core::bk_query_posts_from_category($bk_cat_id);
            $bk_output = '';
            if ($bk_layout == 'layout1') {
                    if($the_query->post_count > 1) {
                    $bk_feat_cat = new gloria_fw_slider;
                    $bk_output .= '<div class="bkmodule module-fw-slider bk-slider-module bk-cat-feat-section bk-nomargin-top">';
                    $bk_output .= '<div class="row flexslider">';
                    $bk_output .= '<ul class="col-md-12 slides">';
                    $bk_output .= $bk_feat_cat->render_modules($the_query);
                    $bk_output .= '</ul></div> <!-- Close render modules -->';
                    $bk_output .= '</div>';
                }
            }else if ($bk_layout == 'layout2') {
                if($the_query->post_count > 4) {
                    $bk_feat_cat = new gloria_main_feature;
                    $background_cfg['bg_color'] = '#f8f8f8';            
                    $bk_output .= '<div class="bkmodule module-main-feature clearfix bk-cat-feat-section" style="background-color: '.$background_cfg['bg_color'].'">';
                    $bk_output .= '<div class="bk-main-feature-inner container bkwrapper clearfix">';            
                    $bk_output .= $bk_feat_cat->render_modules($the_query, $background_cfg);
                    $bk_output .= '</div></div>';  
                }                                  
            }else if ($bk_layout == 'layout3') {  
                if($the_query->post_count > 2) {
                    $bk_feat_cat = new gloria_feature_2;
                    $bk_output .= '<div class="bkmodule module-feature-2 bk-nomargin-top background-preload "><div class="feature-2-wrapper feature-2-runner opacity-zero">';
                    $bk_output .= $bk_feat_cat->render_modules($the_query);            //render modules
                    $bk_output .= '</div><div class="bk-preload"></div></div>';
                }
            }else if ($bk_layout == 'layout4') {  
                if($the_query->post_count > 4) {
                    $bk_feat_cat = new gloria_grid;
                    $bk_output .= '<div class="bkmodule module-grid clearfix bkwrapper container">';
                    $bk_output .= $bk_feat_cat->render_modules($the_query);            //render modules
                    $bk_output .= '</div>';
                }
            }
            echo ($bk_output);
        }
        
/**
 * ************* Social Share Box *****************
 *---------------------------------------------------
 */
              
        static function bk_share_box_top($bkPostId, $social_share) {
            $titleget = get_the_title($bkPostId);
        	?>
            <div class="share-box-wrap">
                <div class="share-box">
                    <ul class="social-share">
                        <?php if ($social_share['fb']): ?>
                            <li class="bk_facebook_share"><a onClick="window.open('http://www.facebook.com/sharer.php?u=<?php echo urlencode(get_permalink());?>','Facebook','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode(get_permalink());?>"><div class="share-item__icon"><i class="fa fa-facebook " title="Facebook"></i></div></a></li>
                        <?php endif; ?>
                        <?php if ($social_share['tw']): ?>
                            <li class="bk_twitter_share"><a onClick="window.open('http://twitter.com/share?url=<?php echo urlencode(get_permalink());?>&amp;text=<?php echo str_replace(" ", "%20", $titleget); ?>','Twitter share','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://twitter.com/share?url=<?php echo urlencode(get_permalink());?>&amp;text=<?php echo str_replace(" ", "%20", $titleget); ?>"><div class="share-item__icon"><i class="fa fa-twitter " title="Twitter"></i></div></a></li>
                        <?php endif; ?>
                        <?php if ($social_share['gp']): ?>
                            <li class="bk_gplus_share"><a onClick="window.open('https://plus.google.com/share?url=<?php echo urlencode(get_permalink());?>','Google plus','width=585,height=666,left='+(screen.availWidth/2-292)+',top='+(screen.availHeight/2-333)+''); return false;" href="https://plus.google.com/share?url=<?php echo urlencode(get_permalink());?>"><div class="share-item__icon"><i class="fa fa-google-plus " title="Google Plus"></i></div></a></li>
                        <?php endif; ?>
                        <?php if ($social_share['pi']): ?>
                            <li class="bk_pinterest_share"><a href='javascript:void((function()%7Bvar%20e=document.createElement(&apos;script&apos;);e.setAttribute(&apos;type&apos;,&apos;text/javascript&apos;);e.setAttribute(&apos;charset&apos;,&apos;UTF-8&apos;);e.setAttribute(&apos;src&apos;,&apos;http://assets.pinterest.com/js/pinmarklet.js?r=&apos;+Math.random()*99999999);document.body.appendChild(e)%7D)());'><div class="share-item__icon"><i class="fa fa-pinterest " title="Pinterest"></i></div></a></li>
                        <?php endif; ?>
                        <?php if ($social_share['stu']):?>
                            <li class="bk_stumbleupon_share"><a onClick="window.open('http://www.stumbleupon.com/submit?url=<?php echo urlencode(get_permalink());?>','Stumbleupon','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://www.stumbleupon.com/submit?url=<?php echo urlencode(get_permalink());?>"><div class="share-item__icon"><i class="fa fa-stumbleupon " title="Stumbleupon"></i></div></a></li>
                        <?php endif; ?>
                        <?php if ($social_share['li']): ?>
                            <li class="bk_linkedin_share"><a onClick="window.open('http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo urlencode(get_permalink());?>','Linkedin','width=863,height=500,left='+(screen.availWidth/2-431)+',top='+(screen.availHeight/2-250)+''); return false;" href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo urlencode(get_permalink());?>"><div class="share-item__icon"><i class="fa fa-linkedin " title="Linkedin"></i></div></a></li>
                        <?php endif; ?>             
                    </ul>
                </div>
            </div>
         <?php   
        }
/**
 * ************* Get Images Instagram *****************
 *---------------------------------------------------
 */
        
        static function bk_get_instagram( $username, $cache_hours, $nr_images, $attachment ) {
        	if ( isset( $username ) && !empty( $username ) ) {
        		$search = 'user';
        		$search_string = $username;
        	} else {
        		return __( 'Nothing to search for', 'jrinstaslider');
        	}
        	
        	
        	$opt_name  = 'jr_insta_' . md5( $search . '_' . $search_string );
        	$instaData = get_transient( $opt_name );
        	$user_opt  = (array) get_option( $opt_name );
        
        	if ( false === $instaData || $user_opt['search_string'] != $search_string || $user_opt['search'] != $search || $user_opt['cache_hours'] != $cache_hours || $user_opt['nr_images'] != $nr_images || $user_opt['attachment'] != $attachment ) {
        		
        		$instaData = array();
        		$user_opt['search']        = $search;
        		$user_opt['search_string'] = $search_string;
        		$user_opt['cache_hours']   = $cache_hours;
        		$user_opt['nr_images']     = $nr_images;
        		$user_opt['attachment']    = $attachment;
        
        		if ( 'user' == $search ) {
        			$response = wp_remote_get( 'https://www.instagram.com/' . trim( $search_string ), array( 'sslverify' => false, 'timeout' => 60 ) );
        		} 
        		if ( is_wp_error( $response ) ) {
        
        			return $response->get_error_message();
        		}
        		if ( $response['response']['code'] == 200 ) {
        			
        			$json = str_replace( 'window._sharedData = ', '', strstr( $response['body'], 'window._sharedData = ' ) );
        			
        			// Compatibility for version of php where strstr() doesnt accept third parameter
        			if ( version_compare( PHP_VERSION, '5.3.0', '>=' ) ) {
        				$json = strstr( $json, '</script>', true );
        			} else {
        				$json = substr( $json, 0, strpos( $json, '</script>' ) );
        			}
        			
        			$json = rtrim( $json, ';' );
        			// Function json_last_error() is not available before PHP * 5.3.0 version
        			if ( function_exists( 'json_last_error' ) ) {
        				
        				( $results = json_decode( $json, true ) ) && json_last_error() == JSON_ERROR_NONE;
        				
        			} else {
        				
        				$results = json_decode( $json, true );
        			}
        			
        			if ( $results && is_array( $results ) ) {
        
        				if ( 'user' == $search ) {
        					$entry_data = isset( $results['entry_data']['ProfilePage'][0]['user']['media']['nodes'] ) ? $results['entry_data']['ProfilePage'][0]['user']['media']['nodes'] : array();
        				} else {
        					$entry_data = isset( $results['entry_data']['TagPage'][0]['tag']['media']['nodes'] ) ? $results['entry_data']['TagPage'][0]['tag']['media']['nodes'] : array();
        				}
        				
        				if ( empty( $entry_data ) ) {
        					return __( 'No images found', 'jrinstaslider');
        				}
        
        				foreach ( $entry_data as $current => $result ) {
        
        					if ( $result['is_video'] == true ) {
        						$nr_images++;
        						continue;
        					}
        
        					if ( $current >= $nr_images ) {
        						break;
        					}
        
        					$image_data['code']       = $result['code'];
        					$image_data['username']   = 'user' == $search ? $search_string : false;
        					$image_data['user_id']    = $result['owner']['id'];
        					$image_data['caption']    = '';
        					$image_data['id']         = $result['id'];
        					$image_data['link']       = 'https://www.instagram.com/p/'. $result['code'] . '/';
        					$image_data['popularity'] = (int) ( $result['comments']['count'] ) + ( $result['likes']['count'] );
        					$image_data['timestamp']  = (float) $result['date'];
        					$image_data['url']        = $result['display_src'];
        					$image_data['url_thumbnail'] = $result['thumbnail_src'];
        
        						
        					$instaData[] = $image_data;
        
        					
        				} // end -> foreach
        				
        			} // end -> ( $results ) && is_array( $results ) )
        			
        		} else { 
        
        			return $response['response']['message'];
        
        		} // end -> $response['response']['code'] === 200 )
                //print_R($instaData);
        		update_option( $opt_name, $user_opt );
        		
        		if ( is_array( $instaData ) && !empty( $instaData )  ) {
        
        			//set_transient( $opt_name, $instaData, $cache_hours * 60 * 60 );
        		}
        		
        	} // end -> false === $instaData
        
        	return $instaData;
        }
        static function bk_count_social( $user, $social_type ) {
    		//check options
    		if ( empty( $user ) ) {
    			return false;
    		}
    
    		$params = array(
    			'timeout'   => 120,
    			'sslverify' => false
    		);
            if($social_type == 'twitter') {
        		$filter   = array(
        			'start_1' => 'ProfileNav-item--followers',
        			'start_2' => 'title',
        			'end'     => '>'
        		);
        		$response = wp_remote_get( 'https://twitter.com/' . $user, $params );
            }else if($social_type == 'dribbble') {
                $filter   = array(
        			'start_1' => 'full-tabs-links',
        			'start_2' => 'envato/followers',
        			'end'     => '</a>'
        		);
        		$response = wp_remote_get( 'https://dribbble.com/' . $user, $params );   
            }
    		//check & return
    		if ( is_wp_error( $response ) || empty( $response['response']['code'] ) || '200' != $response['response']['code'] ) {
    			return false;
    		}
    		//get content
    		$response = wp_remote_retrieve_body( $response );
    
    		if ( ! empty( $response ) && $response !== false ) {
    			foreach ( $filter as $key => $value ) {
    
    				$key = explode( '_', $key );
    				$key = $key[0];
    
    				if ( $key == 'start' ) {
    					$key = false;
    				} else if ( $value == 'end' ) {
    					$key = true;
    				}
    				$key = (bool) $key;
    
    				$index = strpos( $response, $value );
    				if ( $index === false ) {
    					return false;
    				}
    				if ( $key ) {
    					$response = substr( $response, 0, $index );
    				} else {
    					$response = substr( $response, $index + strlen( $value ) );
    				}
    			}
    
    			if ( strlen( $response ) > 100 ) {
    				return false;
    			}
    
    			$count = self::bk_extract_one_number( $response );
    
    			if ( ! is_numeric( $count ) || strlen( number_format( $count ) ) > 15 ) {
    				return false;
    			}
    
    			$count = intval( $count );
    
    			return $count;
    		} else {
    			return false;
    		}
    
    	}
        static function bk_extract_one_number( $str ) {
        	return intval( preg_replace( '/[^0-9]+/', '', $str ), 10 );
        }
        
        static function bk_get_footer_instagram() {
            $gloria_option = gloria_core::bk_get_global_var('gloria_option');
            $photos_arr = array();
            $photostream_title = '';
            
            $pp_instagram_username = $gloria_option['bk-footer-instagram-username'];
        	$photos_arr = gloria_core::bk_get_instagram( $pp_instagram_username, 5, 8, false );
        	$photostream_title = $gloria_option['bk-footer-instagram-title'];
            ?>
            <div class="footer_photostream_wrapper">
            	<h3><?php echo esc_attr($photostream_title); ?></h3>
            	<ul class="footer_photostream clearfix">
            		<?php
            			foreach($photos_arr as $photo)
            			{
            		?>
            			<li><a target="_blank" href="<?php echo esc_url($photo['link']); ?>"><img src="<?php echo esc_url($photo['url_thumbnail']); ?>" alt="<?php echo esc_attr($photo['id']); ?>" /></a></li>
            		<?php
            			}
            		?>
            	</ul>
            </div>
        <?php
        }
        static function bk_get_footer_widgets() {
            if (is_active_sidebar('footer_sidebar_1') 
            || is_active_sidebar('footer_sidebar_2')
            || is_active_sidebar('footer_sidebar_3')) { ?>
                <div class="footer-content bkwrapper clearfix container">
                    <div class="row">
                        <div class="footer-sidebar col-sm-4">
                            <?php dynamic_sidebar( 'footer_sidebar_1' ); ?>
                        </div>
                        <div class="footer-sidebar col-sm-4">
                            <?php dynamic_sidebar( 'footer_sidebar_2' ); ?>
                        </div>
                        <div class="footer-sidebar col-sm-4">
                            <?php dynamic_sidebar( 'footer_sidebar_3' ); ?>
                        </div>
                    </div>
                </div>
        <?php } 
        }
        static function bk_get_footer_lower() {
            $gloria_option = gloria_core::bk_get_global_var('gloria_option');
            if ($gloria_option != null):                
                $bk_allow_html = array(
                                        'a' => array(
                                            'href' => array(),
                                            'title' => array()
                                        ),
                                        'br' => array(),
                                        'em' => array(),
                                        'strong' => array(),
                                    );
                $cr_text = $gloria_option['cr-text'];
            endif;?>
            <div class="footer-lower">
                <div class="container">
                    <div class="footer-inner clearfix">
                        <?php if ( has_nav_menu('menu-footer') ) {?> 
                            <?php wp_nav_menu(array('theme_location' => 'menu-footer', 'depth' => '1', 'container_id' => 'footer-menu'));?>  
                        <?php }?>  
                        <div class="bk-copyright"><?php echo wp_kses($cr_text, $bk_allow_html);?></div>
                    </div>
                </div>
            </div>    
        <?php   
        }
        static function bk_footer_localize() {
            $gloria_option = gloria_core::bk_get_global_var('gloria_option');
            $gloria_justified_ids = gloria_core::bk_get_global_var('justified_ids');
            $gloria_ajax_btnstr = gloria_core::bk_get_global_var('ajax_btnstr');
            
            $gloria_ajax_btnstr['loadmore'] = esc_html__('Load More', 'gloria');
            $gloria_ajax_btnstr['nomore'] = esc_html__('No More Posts', 'gloria');
            
            $sharrre_path = get_template_directory_uri().'/js/sharrre.php';
            
            if (isset($gloria_option['bk-smooth-scroll'])) {
                $bkSmoothScroll['status'] = $gloria_option['bk-smooth-scroll'];
            }else {
                $bkSmoothScroll['status'] = 0;
            }
            
            wp_localize_script( 'gloria-theme-plugins', 'bkSmoothScroll', $bkSmoothScroll );
            
            wp_localize_script( 'gloria-module-load-post', 'ajax_btn_str', $gloria_ajax_btnstr );
            
            wp_localize_script( 'gloria-customjs', 'justified_ids', $gloria_justified_ids );
        
            wp_localize_script( 'gloria-customjs', 'ajax_c', bk_section_parent::$bk_ajax_c );
            
            wp_localize_script( 'gloria-customjs', 'sharrre_path', $sharrre_path );
            
            if ($gloria_option != null):            
                $fixed_nav = $gloria_option['bk-fixed-nav-switch'];            
                wp_localize_script( 'gloria-customjs', 'fixed_nav', $fixed_nav );
            endif;
        }
        static function bk_social_counter( $url ) {
			$url_id                  = preg_replace('/[^A-Za-z0-9]/', '', $url);
			$bk_url_shares_transient = 'bk_share_' . $url_id;
			$bk_cache_result            = get_transient( $bk_url_shares_transient );
            $bk_social_count = array();
            /*
			if ( $bk_cache_result !== false ) {
				return $bk_cache_result;
			} else {*/

                $bk_social_count['facebook'] = 0;
				//facebook
				$json_string = wp_remote_get( 'http://graph.facebook.com/?ids=' . $url, array( 'timeout' => 100 ) );
				if ( ! is_wp_error( $json_string ) && isset( $json_string['body'] ) ) {
					$json              = json_decode( $json_string['body'], true );
					$bk_social_count['facebook'] = isset( $json[ $url ]['shares'] ) ? intval( ( $json[ $url ]['shares'] ) ) : 0;
				} else {
					$bk_social_count['facebook'] = 0;
				}

				//remove twitter counter API
				$bk_social_count['twitter'] = 0;

				//linkedin
				$json_string = wp_remote_get( "http://www.linkedin.com/countserv/count/share?url=$url&format=json", array( 'timeout' => 100 ) );
				if ( ! is_wp_error( $json_string ) && isset( $json_string['body'] ) ) {
					$json              = json_decode( $json_string['body'], true );
					$bk_social_count['linkedin'] = isset( $json['count'] ) ? intval( $json['count'] ) : 0;
				} else {
					$bk_social_count['linkedin'] = 0;
				}

                
				//Pinterest
                $bk_social_count['pinterest'] = 0;
				$json_string = wp_remote_get( 'http://api.pinterest.com/v1/urls/count.json?url=' . $url, array( 'timeout' => 100 ) );
				if ( ! is_wp_error( $json_string ) && isset( $json_string['body'] ) ) {
					$json_string        = preg_replace( '/^receiveCount\((.*)\)$/', "\\1", $json_string['body'] );
					$json               = json_decode( $json_string, true );
					$bk_social_count['pinterest'] = isset( $json['count'] ) ? intval( $json['count'] ) : 0;
				} else {
					$bk_social_count['pinterest'] = 0;
				}

				//google plus
				$bk_social_count['plus_one'] = 0;
				$google_args       = array(
					'headers' => array( 'Content-type' => 'application/json-rpc' ),
					'body'    => '[{"method":"pos.plusones.get","id":"p","params":{"nolog":true,"id":"' . $url . '","source":"widget","userId":"@viewer","groupId":"@self"},"jsonrpc":"2.0","key":"p","apiVersion":"v1"}]',
				);
				$google_url        = 'https://clients6.google.com/rpc?key=AIzaSyCKSbrvQasunBoV16zDH9R33D88CeLr9gQ';
				$json_string       = wp_remote_post( $google_url, $google_args, array( 'timeout' => 100 ) );
				if ( ! is_wp_error( $json_string ) && isset( $json_string['body'] ) ) {
					$data = json_decode( $json_string['body'] );
					if ( ! is_null( $data ) ) {
						if ( is_array( $data ) && count( $data ) == 1 ) {
							$data = array_shift( $data );
						}
						if ( isset( $data->result->metadata->globalCounts->count ) ) {
							$bk_social_count['plus_one'] = $data->result->metadata->globalCounts->count;
						}
					}
				}
                
                /** StumbledUpon **/
                $bk_social_count['stumbledupon'] = 0;
				$json_string = wp_remote_get( 'http://www.stumbleupon.com/services/1.01/badge.getinfo?url='.$url, array( 'timeout' => 100 ) );
				if ( ! is_wp_error( $json_string ) && isset( $json_string['body'] ) ) {
					$json              = json_decode( $json_string['body'], true );
					$bk_social_count['stumbledupon'] = isset( $json['result']['views'] ) ? intval( $json['result']['views'] ) : 0;
				} else {
					$bk_social_count['stumbledupon'] = 0;
				}
                
				//count all
				$bk_social_count['all'] = $bk_social_count['twitter'] + $bk_social_count['pinterest'] + $bk_social_count['plus_one'] + $bk_social_count['facebook'] + $bk_social_count['linkedin'] + $bk_social_count['stumbledupon'];

				set_transient( $bk_url_shares_transient, $bk_social_count, 60 * 60 * 4 );

				return $bk_social_count;
			 }
		/*}*/  
    } // Close bk_core class
}