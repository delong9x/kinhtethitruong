<?php
class bk_get_cfg {
    static function configs($atts, $page_info) {
        $bk_configs = array();
        $i = 2;
        $atts = shortcode_atts(
                array(
                    'category_id'   => true,
                    'limit'         => false,
                    'feature'       => false,
                    'bg_color'      => false,
                    'offset'        => false,
                    'order'         => false, 
                    'category_id2'   => true,
                    'offset2'        => false,
                    'order2'         => false, 
                    'category_id3'   => true,
                    'offset3'        => false,
                    'order3'         => false, 
                    'category_id4'   => true,
                    'offset4'        => false,
                    'order4'         => false, 
                    'category_id5'   => true,
                    'offset5'        => false,
                    'order5'         => false, 
                    'category_id6'   => true,
                    'offset6'        => false,
                    'order6'         => false,    
                    'load_more_button' => false, 
                    'tabs_amount'   => false,               
                ),$atts);               
        $bk_configs['category_id'] = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_category', true );
        if($atts['limit'] == true){
            $bk_configs['limit'] = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_limit', true );
        }
        if($atts['feature'] == true) {
            $bk_configs['feature'] = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_feature', true );
        }
        if($atts['bg_color'] == true) {
            $bk_configs['bg_color'] = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_bg_color', true );
        }
        if($atts['offset'] == true) {
            $bk_configs['offset'] = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_offset', true );
        }
        if($atts['order'] == true) {
            $bk_configs['order'] = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_order', true );
        }  
        if($atts['load_more_button'] == true) {
            $bk_configs['load_more_button'] = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_load_more_button', true );
        }  
        if($atts['tabs_amount'] == true) {
            $bk_configs['tabs_amount'] = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_tabs_amount', true );
        } 
        for ($i=2; $i<=6; $i++) {
            if($atts['category_id'.$i] == true) {
                $bk_configs['category_id'.$i] = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_category'.$i, true );
            }
            if($atts['order'.$i] == true) {
            $bk_configs['order'.$i] = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_order'.$i, true );
            }
            if($atts['offset'.$i] == true) {
                $bk_configs['offset'.$i] = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_offset'.$i, true );
            }            
        }
        return $bk_configs;
    }
}