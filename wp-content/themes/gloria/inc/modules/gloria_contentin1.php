<?php
if (!class_exists('gloria_contentin1')) {
    class gloria_contentin1 {
        
        function render($post_args) {
            ob_start();        
            $video_icon_str = '';
            $bkReviewSW = get_post_meta(get_the_ID(),'bk_review_checkbox',true);
            $bkThumbId = get_post_thumbnail_id( get_the_ID() );
            $bkThumbUrl = wp_get_attachment_image_src( $bkThumbId, $post_args['thumbnail_size'] );
            if (isset($post_args['video_icon']) && ($post_args['video_icon'] != '')) {
                $video_icon_str = '<span class="play-icon"></span>';
            }?>
            <div class="bk-article-wrapper" itemscope itemtype="http://schema.org/Article">
        
                <?php echo '<a href="'.get_permalink(get_the_ID()).'">'.$video_icon_str.'<div class="thumb" data-type="background" style="background-image: url('.$bkThumbUrl[0].')"></div></a>';?>
                <?php if(( $bkReviewSW == '1' ) && isset($post_args['review_score']) && ($post_args['review_score'] != '') && ($post_args['review_score'] != 0)) {echo gloria_core::bk_get_review_score($post_args['review_score']);}?>
                <div class="post-c-wrap">
                    <?php if(isset($post_args['cat_meta']) && ($post_args['cat_meta'] != '')) {echo gloria_core::bk_meta_cases('cat', get_the_ID());}?>      
                    <?php 
                        if (isset($post_args['title_length']) && ($post_args['title_length'] > 0)) {
                            echo gloria_core::bk_get_post_title(get_the_ID(), $post_args['title_length']);
                        }else {
                            echo gloria_core::bk_get_post_title(get_the_ID(), '');
                        }
                    ?> 
                    <?php if (isset($post_args['meta_ar']) && ($post_args['meta_ar'] != null)) {echo gloria_core::bk_get_post_meta($post_args['meta_ar'], get_the_ID());}?>
                </div>
                <?php echo gloria_core::bk_get_article_info(get_the_ID());?>
            </div>
            <?php return ob_get_clean();
        }
        
    }
}