<?php
if (!class_exists('gloria_contentin3')) {
    class gloria_contentin3 {
        
        function render($post_args) {
            ob_start();
            $meta_ar = array('author', 'date');
            $bkThumbId = get_post_thumbnail_id( get_the_ID() );
            $bkThumbUrl = wp_get_attachment_image_src( $bkThumbId, $post_args['thumbnail_size'] );?>
            <div class="bk-article-wrapper" itemscope itemtype="http://schema.org/Article">
                <?php
                echo '<div class="thumb" data-type="background" style="background-image: url('.$bkThumbUrl[0].')"></div>';
                ?>
                <div class="post-c-wrap">
                    <div class="inner">
                        <div class="inner-cell">   
                            <div class="innerwrap">  
                                <?php if(isset($post_args['cat_meta']) && ($post_args['cat_meta'] != '')) {echo gloria_core::bk_meta_cases('cat', get_the_ID());}?>   
                                <?php 
                                    if (isset($post_args['title_length']) && ($post_args['title_length'] > 0)) {
                                        echo gloria_core::bk_get_post_title(get_the_ID(), $post_args['title_length']);
                                    }else {
                                        echo gloria_core::bk_get_post_title(get_the_ID(), '');
                                    }
                                ?>
                                <?php if (isset($post_args['meta_ar']) && ($post_args['meta_ar'] != null)) {echo gloria_core::bk_get_post_meta($post_args['meta_ar'], get_the_ID());}?>
                                <?php if (isset($post_args['rm_btn']) && ($post_args['rm_btn'] > 0)) {echo gloria_core::bk_readmore_btn(get_the_ID());}?>   
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo gloria_core::bk_get_article_info(get_the_ID());?>
            </div>
            <?php return ob_get_clean();
        }
        
    }
}