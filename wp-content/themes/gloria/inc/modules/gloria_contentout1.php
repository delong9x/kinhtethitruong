<?php
if (!class_exists('gloria_contentout1')) {
    class gloria_contentout1 {
        
        function render() {
            ob_start();
            $meta_ar = array('bg', 'cat', 'date');
            ?>
            <div class="bk-article-wrapper" itemscope itemtype="http://schema.org/Article">
                <div class="col-md-7 col-sm-12 feat-img">
                    <a href="<?php echo get_permalink(get_the_ID());?>">
                    <?php echo gloria_core::bk_get_feature_image('gloria_620_420');?>
                    </a>
                </div>
                <div class="post-c-wrap col-md-5 col-sm-12">
                    <div class="head-wrap row">
                        <div class="col-md-4">
                            <?php echo gloria_core::bk_get_post_meta($meta_ar, get_the_ID());?>                                            
                        </div>
                        <div class="col-md-8">
                            <?php echo gloria_core::bk_get_post_title(get_the_ID(), 15);?>
                            <div class="post-author">
                                By <?php the_author_posts_link();?>           
                            </div>
                        </div>
                    </div>
                    <?php echo gloria_core::bk_get_post_excerpt(50);?>
                    <?php 
                        $icon = '<i class="fa fa-long-arrow-right"></i>';
                        echo gloria_core::bk_readmore_btn(get_the_ID(), $icon);
                    ?>
                </div>
                <?php echo gloria_core::bk_get_article_info(get_the_ID());?>
            </div>
            <?php return ob_get_clean();
        }
        
    }
}