<?php
if (!class_exists('gloria_contentout5')) {
    class gloria_contentout5 {
        
        function render($post_args) {
            ob_start();
            if (isset($post_args['video_icon']) && ($post_args['video_icon'] == true)) {
                $video_icon = true;
            }else {
                $video_icon = '';
            }
            $bkReviewSW = get_post_meta(get_the_ID(),'bk_review_checkbox',true);
            ?>
            <div class="bk-article-wrapper" itemscope itemtype="http://schema.org/Article">
                <div class="bk-mask">
                    <?php echo gloria_core::bk_get_feature_image($post_args['thumbnail_size'], true, $video_icon);?>
                    <?php if (( $bkReviewSW == '1' ) && isset($post_args['review_score']) && ($post_args['review_score'] != '') && ($post_args['review_score'] != 0)) {echo gloria_core::bk_get_review_score($post_args['review_score']);}?>
                </div>
                <?php if(isset($post_args['cat_meta']) && ($post_args['cat_meta'] != '')) {echo gloria_core::bk_meta_cases('cat', get_the_ID());}?>      
                <div class="post-c-wrap">  
                    <?php if (isset($post_args['meta_ar']) && ($post_args['meta_ar'] != null)) {echo gloria_core::bk_get_post_meta($post_args['meta_ar'], get_the_ID());}?>
                    <?php 
                        if (isset($post_args['title_length']) && ($post_args['title_length'] > 0)) {
                            echo gloria_core::bk_get_post_title(get_the_ID(), $post_args['title_length']);
                        }else {
                            echo gloria_core::bk_get_post_title(get_the_ID(), '');
                        }
                    ?>
                    <?php if (isset($post_args['except_length']) && ($post_args['except_length'] > 0)) {echo gloria_core::bk_get_post_excerpt($post_args['except_length']);}?>
                    <?php if (isset($post_args['rm_btn']) && ($post_args['rm_btn'] > 0)) {echo gloria_core::bk_readmore_btn(get_the_ID());}?>      
                </div>
                <?php echo gloria_core::bk_get_article_info(get_the_ID());?>
            </div>
            <?php return ob_get_clean();
        }
        
    }
}