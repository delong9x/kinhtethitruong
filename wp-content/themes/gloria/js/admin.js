/* -----------------------------------------------------------------------------
 * Page Template Meta-box
 * -------------------------------------------------------------------------- */
/*** Tipper ***/
!function(t){"use strict";function e(e){return c.formatter=r,a=t("body"),t(this).not(".tipper-attached").addClass("tipper-attached").on("mouseenter.tipper",t.extend({},c,e||{}),o)}function o(e){var o=t(this),r=t.extend(!0,{},e.data,o.data("tipper-options"));r.$target=o,f={left:e.pageX,top:e.pageY},r.delay?(s(r.timer),r.timer=setTimeout(function(){i(r.$target,r)},r.delay)):i(r.$target,r),r.$target.one("mouseleave.tipper",r,n),!r.follow&&r.match&&r.$target.on("mousemove.tipper",r,p).trigger("mousemove")}function i(e,o){var i="";i+='<div class="tipper '+o.direction+'">',i+='<div class="tipper-content">',i+=o.formatter.apply(a,[e]),i+='<span class="tipper-caret"></span>',i+="</div>",i+="</div>",o.$target=e,o.$tipper=t(i),a.append(o.$tipper),o.$content=o.$tipper.find(".tipper-content"),o.$caret=o.$tipper.find(".tipper-caret"),o.offset=e.offset(),o.height=e.outerHeight(),o.width=e.outerWidth(),o.tipperPos={},o.caretPos={},o.contentPos={};var r=o.$caret.outerHeight(!0),n=o.$caret.outerWidth(!0),s=o.$content.outerHeight(!0),c=o.$content.outerWidth(!0);"right"===o.direction||"left"===o.direction?(o.caretPos.top=(s-r)/2,o.contentPos.top=-s/2,"right"===o.direction?o.contentPos.left=o.margin:"left"===o.direction&&(o.contentPos.left=-(c+o.margin))):(o.caretPos.left=(c-n)/2,o.contentPos.left=-c/2,"bottom"===o.direction?o.contentPos.top=o.margin:"top"===o.direction&&(o.contentPos.top=-(s+o.margin))),o.$content.css(o.contentPos),o.$caret.css(o.caretPos),o.follow?o.$target.on("mousemove.tipper",o,p).trigger("mousemove"):o.match?("right"===o.direction||"left"===o.direction?(o.tipperPos.top=f.top,"right"===o.direction?o.tipperPos.left=o.offset.left+o.width:"left"===o.direction&&(o.tipperPos.left=o.offset.left)):(o.tipperPos.left=f.left,"bottom"===o.direction?o.tipperPos.top=o.offset.top+o.height:"top"===o.direction&&(o.tipperPos.top=o.offset.top)),o.$tipper.css(o.tipperPos)):("right"===o.direction||"left"===o.direction?(o.tipperPos.top=o.offset.top+o.height/2,"right"===o.direction?o.tipperPos.left=o.offset.left+o.width:"left"===o.direction&&(o.tipperPos.left=o.offset.left)):(o.tipperPos.left=o.offset.left+o.width/2,"bottom"===o.direction?o.tipperPos.top=o.offset.top+o.height:"top"===o.direction&&(o.tipperPos.top=o.offset.top)),o.$tipper.css(o.tipperPos))}function r(t){return t.data("title")}function p(t){var e=t.data;f={left:t.pageX,top:t.pageY},e.follow&&"undefined"!=typeof e.$tipper&&e.$tipper.css({left:f.left,top:f.top})}function n(t){var e=t.data;s(e.timer),"undefined"!=typeof e.$tipper&&(e.$tipper.remove(),e.$target.off("mousemove.tipper mouseleave.tipper"))}function s(t){t&&(clearTimeout(t),t=null)}var a,f,c={delay:0,direction:"top",follow:!1,formatter:t.noop,margin:15,match:!1},l={defaults:function(e){return c=t.extend(c,e||{}),t(this)},destroy:function(){return t(this).trigger("mouseleave.tipper").off(".tipper").removeClass("tipper-attached")}};t.fn.tipper=function(t){return l[t]?l[t].apply(this,Array.prototype.slice.call(arguments,1)):"object"!=typeof t&&t?this:e.apply(this,arguments)},t.tipper=function(t){"defaults"===t&&l.defaults.apply(this,Array.prototype.slice.call(arguments,1))}}(jQuery);	

;(function( $, window, document, undefined ){
	"use strict";
	$( document ).ready( function () {
	   $(".bk-tipper-bottom").tipper({
            direction: "bottom"
        });
		/* -----------------------------------------------------------------------------
		 * Page template
		 * -------------------------------------------------------------------------- */
		$( '#page_template' ).change( function() {
			var template = $( '#page_template' ).val();

			// Page Composer Template
			if ( 'page_builder.php' == template ) {
				
				$.page_builder( 'show' );
				$( '#bk_page_options' ).hide();

			} else {
				$.page_builder( 'hide' );
				$( '#bk_page_options' ).show();
			}
		} ).triggerHandler( 'change' );

		// -----------------------------------------------------------------------------
		// Fitvids - keep video ratio
		// 
//		$( '.postbox .embed-code' ).fitVids( { customSelector: "iframe[src*='maps.google.'], iframe[src*='soundcloud.']" });
        /*
        $(function() {
            $( ".datepicker" ).datepicker();
            $('.timepicker').timepicker({
                minuteStep: 5,
                showInputs: false,
                disableFocus: true
            });
         });
         */
        $(function() {
            if ($('input[name=post_format]:checked', '#post-formats-select').val() == 0) {
                $("#bk_format_options").hide();
                $("#bk_feature_image").show();
            }else {
                var value = $('input[name=post_format]:checked', '#post-formats-select').val(); 
                $("#bk_feature_image").hide();
                $("#bk_format_options").show();
                if (value == "gallery"){
                    $("#bk_media_embed_code_post_description").parents(".rwmb-field").css("display", "none");
                    $("#bk_image_upload_description").parents(".rwmb-field").css("display", "none");
                    $("#bk_gallery_content_description").parents(".rwmb-field").css("display", "block");
                }else if ((value == "video")||(value == "audio")){
                    $("#bk_media_embed_code_post_description").parents(".rwmb-field").css("display", "block");
                    $("#bk_image_upload_description").parents(".rwmb-field").css("display", "none");
                    $("#bk_gallery_content_description").parents(".rwmb-field").css("display", "none");
                }else if (value == "image"){
                    $("#bk_media_embed_code_post_description").parents(".rwmb-field").css("display", "none");
                    $("#bk_image_upload_description").parents(".rwmb-field").css("display", "block");
                    $("#bk_gallery_content_description").parents(".rwmb-field").css("display", "none");
                }
            }
            $('#post-formats-select input').on('change', function() { 
                var value = $('input[name=post_format]:checked', '#post-formats-select').val(); 
                if (value == 0){
                    $("#bk_format_options").hide();
                    $("#bk_feature_image").show();
                }else {
                    $("#bk_format_options").show();
                    $("#bk_feature_image").hide();
                } 
                if (value == "gallery"){
                    $("#bk_media_embed_code_post_description").parents(".rwmb-field").css("display", "none");
                    $("#bk_image_upload_description").parents(".rwmb-field").css("display", "none");
                    $("#bk_gallery_content_description").parents(".rwmb-field").css("display", "block");
                }else if ((value == "video")||(value == "audio")){
                    $("#bk_media_embed_code_post_description").parents(".rwmb-field").css("display", "block");
                    $("#bk_image_upload_description").parents(".rwmb-field").css("display", "none");
                    $("#bk_gallery_content_description").parents(".rwmb-field").css("display", "none");
                }else if (value == "image"){
                    $("#bk_media_embed_code_post_description").parents(".rwmb-field").css("display", "none");
                    $("#bk_image_upload_description").parents(".rwmb-field").css("display", "block");
                    $("#bk_gallery_content_description").parents(".rwmb-field").css("display", "none");
                }
            });
            var e = document.getElementById("bk_post_layout");
            if( e != null) {
                var layout_selected = e.options[e.selectedIndex].text;
                if(layout_selected === 'Full Width Feature Image') {
                    $("#bk_parallax_single_description").parents(".rwmb-field").css("display", "block");
                    $("#bk_fwstyle").parents(".rwmb-field").css("display", "block");
                    $("#bk_feat_img_status_description").parents(".rwmb-field").css("display", "none");
                }else {
                    $("#bk_parallax_single_description").parents(".rwmb-field").css("display", "none");
                    $("#bk_fwstyle").parents(".rwmb-field").css("display", "none");
                    $("#bk_feat_img_status_description").parents(".rwmb-field").css("display", "block");
                }
                document.getElementById("bk_post_layout").onchange = function(e) {
                    if(this[this.selectedIndex].text === 'Full Width Feature Image') {
                        $("#bk_parallax_single_description").parents(".rwmb-field").slideDown();
                        $("#bk_fwstyle").parents(".rwmb-field").slideDown();
                        $("#bk_feat_img_status_description").parents(".rwmb-field").slideUp();
                    }else {
                        $("#bk_parallax_single_description").parents(".rwmb-field").slideUp();
                        $("#bk_fwstyle").parents(".rwmb-field").slideUp();
                        $("#bk_feat_img_status_description").parents(".rwmb-field").slideDown();
                    }
                };
            }
            var bkSidebarRemove = $('#bk_remove_sidebar');
            if ( bkSidebarRemove.is(":checked") ) {
                $("#bk_post_sb_select_description").parents(".rwmb-field").hide();
                $("#bk_sidebar_pos_description").parents(".rwmb-field").hide();
            }
            bkSidebarRemove.click(function(){
                $("#bk_post_sb_select_description").parents(".rwmb-field").slideToggle('slow');
                $("#bk_sidebar_pos_description").parents(".rwmb-field").slideToggle('slow');
            });
        });
        
    // Pagebuilder tabs
        var module_type = new Array;
        module_type = $('.bk-module-type');
        var module_count = 0;
        var module_length = $('.bk-module-type').length;
        function pagebuilderSetbg(new_module_type) {
            //console.log(new_module_type.val());
            if((new_module_type.val() === 'masonry') || (new_module_type.val() === 'large_blog') || (new_module_type.val() === 'classic_blog')){

                new_module_type.siblings('.bk-module-options').find('.bk-module-option-wrap:lt(10):gt(6)').css("background-color", "#eeeeee"); //nth: 8 9 10
                new_module_type.siblings('.bk-module-options').find('.bk-module-option-wrap:lt(16):gt(12)').css("background-color", "#eeeeee");//nth: 14 15 16
                new_module_type.siblings('.bk-module-options').find('.bk-module-option-wrap:lt(22):gt(18)').css("background-color", "#eeeeee");//nth: 20 21 22      
                new_module_type.siblings('.bk-module-options').find('.bk-module-option-wrap:nth-child(23)').css({"background-color" : "#2490D0", "color" : "#fff", "margin-top" :"30px"});
            
            }else if((new_module_type.val() === 'row') || (new_module_type.val() === 'hero') || (new_module_type.val() === 'square_grid')
                || (new_module_type.val() === 'block_2')) {
                    
                new_module_type.siblings('.bk-module-options').find('.bk-module-option-wrap:lt(8):gt(4)').css("background-color", "#eeeeee"); //nth: 6 7 8
                new_module_type.siblings('.bk-module-options').find('.bk-module-option-wrap:lt(14):gt(10)').css("background-color", "#eeeeee");//nth: 12 13 14
                new_module_type.siblings('.bk-module-options').find('.bk-module-option-wrap:lt(20):gt(16)').css("background-color", "#eeeeee");//nth: 18 19 20       
                new_module_type.siblings('.bk-module-options').find('.bk-module-option-wrap:nth-child(21)').css({"background-color" : "#2490D0", "color" : "#fff", "margin-top" :"30px"});

            }else if(new_module_type.val() === 'block_1') {
                
                new_module_type.siblings('.bk-module-options').find('.bk-module-option-wrap:lt(7):gt(3)').css("background-color", "#eeeeee"); //nth: 5 6 7
                new_module_type.siblings('.bk-module-options').find('.bk-module-option-wrap:lt(13):gt(9)').css("background-color", "#eeeeee");//nth: 11 12 13
                new_module_type.siblings('.bk-module-options').find('.bk-module-option-wrap:lt(19):gt(15)').css("background-color", "#eeeeee");//nth: 17 18 19     
                new_module_type.siblings('.bk-module-options').find('.bk-module-option-wrap:nth-child(20)').css({"background-color" : "#2490D0", "color" : "#fff", "margin-top" :"30px"});

            }
        }
        function pagebuilderSetupTabs(new_module_type) {
            //console.log(new_module_type.val());
            if((new_module_type.val() === 'masonry') || (new_module_type.val() === 'large_blog') || (new_module_type.val() === 'classic_blog')){
                var el = new_module_type.siblings('.bk-module-options').find('.bk-module-option-wrap:nth-child(23)').find('select');
                var tabs_amount = el.val() - 1;
                var $gt_val = 6 + tabs_amount*3;
                new_module_type.siblings('.bk-module-options').find('.bk-module-option-wrap:lt(22):gt('+$gt_val+')').hide(); //hide all tabs   
                el.change(function() {
                    var new_tabs_amount = parseInt(this[this.selectedIndex].text) - 1;
                    var new_gt_val = 6 + new_tabs_amount*3;
                    var new_lt_val = new_gt_val + 1;
                    new_module_type.siblings('.bk-module-options').find('.bk-module-option-wrap:lt(22):gt('+new_gt_val+')').slideUp('slow');
                    new_module_type.siblings('.bk-module-options').find('.bk-module-option-wrap:lt('+new_lt_val+'):gt(6)').slideDown('slow');
                });
            }else if((new_module_type.val() === 'row') || (new_module_type.val() === 'hero') || (new_module_type.val() === 'square_grid')
                || (new_module_type.val() === 'block_2')) {;
                var el = new_module_type.siblings('.bk-module-options').find('.bk-module-option-wrap:nth-child(21)').find('select');
                var tabs_amount = el.val() - 1;
                var $gt_val = 4 + tabs_amount*3;
                new_module_type.siblings('.bk-module-options').find('.bk-module-option-wrap:lt(20):gt('+$gt_val+')').hide(); //hide all tabs   
                el.change(function() {
                    var new_tabs_amount = parseInt(this[this.selectedIndex].text) - 1;
                    var new_gt_val = 4 + new_tabs_amount*3;
                    var new_lt_val = new_gt_val + 1;
                    new_module_type.siblings('.bk-module-options').find('.bk-module-option-wrap:lt(20):gt('+new_gt_val+')').slideUp('slow');
                    new_module_type.siblings('.bk-module-options').find('.bk-module-option-wrap:lt('+new_lt_val+'):gt(4)').slideDown('slow');
                });
            }else if(new_module_type.val() === 'block_1') {
                var el = new_module_type.siblings('.bk-module-options').find('.bk-module-option-wrap:nth-child(20)').find('select');
                var tabs_amount = el.val() - 1;
                var $gt_val = 3 + tabs_amount*3;
                new_module_type.siblings('.bk-module-options').find('.bk-module-option-wrap:lt(19):gt('+$gt_val+')').hide(); //hide all tabs   
                el.change(function() {
                    var new_tabs_amount = parseInt(this[this.selectedIndex].text) - 1;
                    var new_gt_val = 3 + new_tabs_amount*3;
                    var new_lt_val = new_gt_val + 1;
                    new_module_type.siblings('.bk-module-options').find('.bk-module-option-wrap:lt(19):gt('+new_gt_val+')').slideUp('slow');
                    new_module_type.siblings('.bk-module-options').find('.bk-module-option-wrap:lt('+new_lt_val+'):gt(3)').slideDown('slow');
                });
            }
        }
        function eventInsertDom(){
            $('.bk-modules-wrap').find($('.bk-modules')).bind("DOMNodeInserted",function(){
                var new_module_type = $(this).find('.bk-module-item:last-child').find('.bk-module-type');
                pagebuilderSetupTabs(new_module_type);
                pagebuilderSetbg(new_module_type);
            });
        }
        //Initial
        $('.bk-module-type').each(function(index,element){
            pagebuilderSetupTabs($(this));
            pagebuilderSetbg($(this));
            //console.log($(this).siblings('.bk-module-options'));
        });
        $('.bk-sections').bind("DOMNodeInserted",function(){
            eventInsertDom();
        });
        eventInsertDom();
	} );
    $.uuid = function() {
		return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
			var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
			return v.toString(16);
		});
	};
})( jQuery, window , document );