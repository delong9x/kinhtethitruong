<?php
if ( ! function_exists( 'gloria_get_header' ) ) {
    function gloria_get_header () {
        $gloria_option = gloria_core::bk_get_global_var('gloria_option');
        if ((isset($gloria_option['bk-header-type'])) && (($gloria_option['bk-header-type']) != NULL)){ 
            $header_type = $gloria_option['bk-header-type'];
        }else {
            $header_type = 'header-1';
        }  
        if ($header_type == 'header-1') {
            get_template_part( 'library/templates/headers/bk_header_one' );
        }else if ($header_type == 'header-2') {
            get_template_part( 'library/templates/headers/bk_header_two' );
        }else if ($header_type == 'header-3') {
            get_template_part( 'library/templates/headers/bk_header_three' );
        }else if ($header_type == 'header-4') {
            get_template_part( 'library/templates/headers/bk_header_four' );
        }else if ($header_type == 'header-5') {
            get_template_part( 'library/templates/headers/bk_header_five' );
        }else if ($header_type == 'header-6') {
            get_template_part( 'library/templates/headers/bk_header_six' );
        }
    }
}
if ( ! function_exists( 'gloria_review_score' ) ) {
    function gloria_review_score ($post_id) {
        $ret = '';
        if (strlen(get_post_meta($post_id, 'bk_final_score', true)) > 0) {
            $ret .= '<div class="review-score">';
            $ret .= '<span><i class="fa fa-star-o"></i></span>';
            $ret .= get_post_meta($post_id, 'bk_final_score', true);
            $ret .= '</div>';
        }
        return $ret;
    }
}
if ( ! function_exists('gloria_initWpFilesystem')){ 
    function gloria_initWpFilesystem() {
        global $wp_filesystem;
    
        // Initialize the Wordpress filesystem, no more using file_put_contents function
        if ( empty( $wp_filesystem ) ) {
            require_once ABSPATH . '/wp-admin/includes/file.php';
            WP_Filesystem();
        }
    }
}
/**
 * ********* Get Post Category ************
 *---------------------------------------------------
 */ 
if ( ! function_exists('gloria_get_category_link')){
    function gloria_get_category_link($postid){ 
        $html = '';
        $category = get_the_category($postid); 
        if(isset($category[0]) && $category[0]){
            foreach ($category as $key => $value) {
                $html.= '<a href="'.get_category_link($value->term_id ).'">'.$value->cat_name.'</a>';  
            }
        						
        }
        return $html;
    }
}
/*** AJAX LOAD ***/
// Single Related Section
add_action( 'wp_ajax_author_posts_load', 'gloria_ajax_author_posts_load' );
add_action('wp_ajax_nopriv_author_posts_load', 'gloria_ajax_author_posts_load');
if ( ! function_exists( 'gloria_ajax_author_posts_load' ) ) {
    function gloria_ajax_author_posts_load() {
        $authorID = isset( $_POST['author_id'] ) ? $_POST['author_id'] : null;   
        $args = array (
            'post_type'             => 'post',
			'orderby'               => 'rand',
			'ignore_sticky_posts'   => 1,
            'post_status'           => 'publish',
			'posts_per_page'        => 6,
            'author'                => $authorID
        );
                
        $bk_posts = get_posts( $args );
        echo gloria_core::bk_related_posts_layout($bk_posts, $args['posts_per_page']);
        die();
    }
}
add_action( 'wp_ajax_classic_blog_load', 'gloria_ajax_classic_blog_load' );
add_action('wp_ajax_nopriv_classic_blog_load', 'gloria_ajax_classic_blog_load');
if ( ! function_exists( 'gloria_ajax_classic_blog_load' ) ) {
    function gloria_ajax_classic_blog_load() {
        $post_offset = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : 0;
        $entries = isset( $_POST['entries'] ) ? $_POST['entries'] : 0;
        $currentCatID = isset( $_POST['currentCatID'] ) ? $_POST['currentCatID'] : null;   
        $args = isset( $_POST['args'] ) ? $_POST['args'] : null;
        $args[ 'posts_per_page' ] = $entries;
        $args[ 'offset' ] = $post_offset;
        $args[ 'cat' ] = $currentCatID;
                
        $the_query = new WP_Query( $args );
        echo gloria_classic_blog::render_modules($the_query);
        die();
    }
}
add_action( 'wp_ajax_large_blog_load', 'gloria_ajax_large_blog_load' );
add_action('wp_ajax_nopriv_large_blog_load', 'gloria_ajax_large_blog_load');
if ( ! function_exists( 'gloria_ajax_large_blog_load' ) ) {
    function gloria_ajax_large_blog_load() {
        $post_offset = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : 0;
        $entries = isset( $_POST['entries'] ) ? $_POST['entries'] : 0;
        $currentCatID = isset( $_POST['currentCatID'] ) ? $_POST['currentCatID'] : null;   
        $args = isset( $_POST['args'] ) ? $_POST['args'] : null;
        $args[ 'posts_per_page' ] = $entries;
        $args[ 'offset' ] = $post_offset;
        $args[ 'cat' ] = $currentCatID;
                
        $the_query = new WP_Query( $args );
        echo gloria_large_blog::render_modules($the_query);
        die();
    }
}
add_action( 'wp_ajax_masonry_load', 'gloria_ajax_masonry_load' );
add_action('wp_ajax_nopriv_masonry_load', 'gloria_ajax_masonry_load');
if ( ! function_exists( 'gloria_ajax_masonry_load' ) ) {
    function gloria_ajax_masonry_load() {
        $post_offset = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : 0;
        $entries = isset( $_POST['entries'] ) ? $_POST['entries'] : 0;
        $columns = isset( $_POST['columns'] ) ? $_POST['columns'] : 0;
        $currentCatID = isset( $_POST['currentCatID'] ) ? $_POST['currentCatID'] : null;   
        $args = isset( $_POST['args'] ) ? $_POST['args'] : null;    
        $args[ 'posts_per_page' ] = $entries;
        $args[ 'offset' ] = $post_offset;
        $args[ 'cat' ] = $currentCatID;
                
        $the_query = new WP_Query( $args );
        echo gloria_masonry::render_modules($the_query, $columns);
        die();
    }
}
add_action('wp_ajax_nopriv_bk_ajax_search', 'gloria_ajax_search');
add_action('wp_ajax_bk_ajax_search', 'gloria_ajax_search');
if (!function_exists('gloria_ajax_search')) {
    function gloria_ajax_search()
    {
        $html_render = new gloria_contentout3;
        $post_cnt = 0;
        $str = '';
        if (!empty($_POST['s'])) {
            $s = ($_POST['s']);
        } else {
            $s = '';
        }
        $meta_ar = array('author', 'date');
        $post_args = array (
            'thumbnail_size'    => 'gloria_130_130',
            'meta_ar'           => $meta_ar,
            'cat_meta'            => '',
            'except_length'     => '',
            'rm_btn'            => true,
            'review_score'      => '',                        
        );    
        $the_query = gloria_query_search($s);
        if ( $the_query->have_posts() ) {
            if ($the_query->post_count >= 3) {
                $post_cnt = 3;
            }else {
                $post_cnt = $the_query->post_count;
            }
            $str .= '<ul class="s-list">';
            foreach( range( 1, $post_cnt) as $i ):
                $the_query->the_post();
                $str .= '<li class="small-post content_out clearfix">';
                $str .= $html_render->render($post_args);
                $str .= '</li><!-- End post -->';        
            endforeach;
            $str .= '</ul>';
            $str .= '<div class="result-msg"><a href="' . get_search_link($s) . '">' . esc_html__('View all results', 'gloria') . '</a></div>';
        } else $str .= '<div class="ajax-not-found">'.esc_html__('Not found', 'gloria').'</div>';
        $data = array(
            'content' => $str,
        );
        die(json_encode($data));
    }
}
add_action('wp_ajax_nopriv_bk_ajax_share', 'gloria_ajax_share');
add_action('wp_ajax_bk_ajax_share', 'gloria_ajax_share');
if (!function_exists('gloria_ajax_share')) {
    function gloria_ajax_share()
    {

    }
}
//Search Query
if (!function_exists('gloria_query_search')) {
    function gloria_query_search($search_data)
    {
        $args = array(
            's' => esc_sql($search_data),
            'post_type' => array('post'),
            'post_status' => 'publish',
        );

        $bk_query = new WP_Query($args);
        return $bk_query;
    }
};
//render search form
if (!function_exists('gloria_ajax_form_search')) {
    function gloria_ajax_form_search()
    {
        $str = '';
        $str .= '<div class="ajax-search-wrap">';
        $str .= '<div id="ajax-form-search" class="ajax-search-icon"><i class="fa fa-search"></i></div>';
        $str .= '<form class="ajax-form" method="get" action="' . esc_url(home_url('/')) . '">';
        $str .= '<fieldset>';
        $str .= '<input id="search-form-text" type="text" autocomplete="off" class="field" name="s" value="' . get_search_query() . '" placeholder="' . esc_html__('Search this Site...', 'gloria') . '">';
        $str .= '</fieldset>';
        $str .= '</form>';
        $str .= ' <div id="ajax-search-result"></div></div>';
        return $str;
    }
}

/**
 * ************* update image feature for image post  *****************
 *---------------------------------------------------
 */ 
if ( ! function_exists( 'gloria_set_image_post_as_featured_image') ) {
    
    function gloria_set_image_post_as_featured_image($bkPostId) {
        $format = get_post_format( $bkPostId );
        if(($format == 'image') && (!has_post_thumbnail($bkPostId))){
            $attachment_id = get_post_meta($bkPostId, 'bk_image_upload', true );
            set_post_thumbnail( $bkPostId, $attachment_id );
        }
    }
}    
add_action('save_post', 'gloria_set_image_post_as_featured_image', 100);
/**
 * BK Comments
 */
if ( ! function_exists( 'gloria_comments') ) {
    function gloria_comments($comment, $args, $depth) {
		$GLOBALS['comment'] = $comment; ?>
		<li <?php comment_class(); ?>>
			<div id="comment-<?php comment_ID(); ?>" class="comment-article  media">
                <div class="comment-author clear-fix">
                    <div class="comment-avatar">
                        <?php echo get_avatar( get_comment_author_email(), 60 ); ?>  
                    </div>
                        <?php printf('<span class="comment-author-name">%s</span>', get_comment_author_link()) ?>
    					<span class="comment-time"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>" class="comment-timestamp"><?php comment_time(esc_html__('j F, Y \a\t H:i', 'gloria')); ?> </a></span>
                        <span class="comment-links">
                            <?php
                                edit_comment_link(esc_html__('Edit', 'gloria'),'  ','');
                                comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth'])));
                            ?>
                        </span>
                </div><!-- .comment-meta -->
                
                <div class="comment-text">
                    				
    				<?php if ($comment->comment_approved == '0') : ?>
    				<div class="alert info">
    					<p><?php esc_html_e('Your comment is awaiting moderation.', 'gloria') ?></p>
    				</div>
    				<?php endif; ?>
    				<div class="comment-content">
    					<?php comment_text() ?>
    				</div>
                </div>
			</div>
		<!-- </li> is added by WordPress automatically -->
		<?php
    }
}
add_action('wp_footer', 'gloria_user_rating');

// User Rating System
if ( ! function_exists( 'gloria_user_rating' ) ) {
    function gloria_user_rating() {
        if (is_single()) {
            global $wp_query;
            $gloria_option = gloria_core::bk_get_global_var('gloria_option');
            $post = $wp_query->post;
            $bk_review_checkbox = get_post_meta( $post->ID, 'bk_review_checkbox', true );
            if ($bk_review_checkbox == 1) {
                $rtl = $gloria_option['bk-rtl-sw'];
                $user_rating_script = "";            
                $user_rating_script.= " <script type='text/javascript'>
                var bkExistingOverlay=0, bkWidthDivider = 1, old_val=0, new_val=1;
                old_val = jQuery('#bk-rate').find('.bk-overlay').width();
                old_x = jQuery('#bk-rate').find('.bk-overlay').children().width();
                jQuery(window).resize(function(){
                    x = jQuery('#bk-rate').find('.bk-overlay').children().width();
                    y = jQuery('#bk-rate').find('.bk-overlay').width();
                    new_val = y;
                    //console.log(x);
                    //console.log(old_x);
                    //console.log(y);
                    //console.log(new_val);
                    //console.log(old_val);
                    if ((new_val != old_val) || ((x != old_x) && (x != 0))) {
                        bkExistingOverlay = ((x/old_val)*y).toFixed(0)+'px';
                        old_val = new_val;
                    }
                    bkWidthDivider = jQuery('#bk-rate').width() / 100;
                    //console.log(bkExistingOverlay);
                    jQuery('#bk-rate').find('.bk-overlay').children().css( {'width': bkExistingOverlay} );
                });
                (function ($) {'use strict';
                    var bkRate = $('#bk-rate'), 
                        bkCriteriaAverage = $('.bk-criteria-score.bk-average-score'),
                        bkRateCriteria = bkRate.find('.bk-criteria'),
                        bkRateOverlay = bkRate.find('.bk-overlay');
                            
                        var bkExistingOverlaySpan = bkRateOverlay.children(),
                            bkNotRated = bkRate.not('.bk-rated').find('.bk-overlay');
                        //console.log(bkExistingOverlaySpan);
                        bkExistingOverlay = bkExistingOverlaySpan.css('width');
                        bkExistingOverlaySpan.addClass('bk-zero-trigger');
                        
                    var bkExistingScore =  $(bkCriteriaAverage).text(),
                        bkExistingRateLine = $(bkRateCriteria).html(),
                        bkRateAmount  = $(bkRate).find('.bk-criteria span').text();
                        bkWidthDivider = ($(bkRate).width() / 100);
                        
                    if ( typeof bkExistingRateLine !== 'undefined' ) {
                        var bkExistingRatedLine = bkExistingRateLine.substr(0, bkExistingRateLine.length-1) + ')'; 
                    }
                    var bk_newRateAmount = parseInt(bkRateAmount) + 1;
                    if ( (bkRateAmount) === '0' ) {
                        var bkRatedLineChanged = '". esc_html__('Reader Rating', 'gloria') .": (' + (bk_newRateAmount) + ' ". esc_html__('Rate', 'gloria') .")';
                    } else {
                        var bkRatedLineChanged = '". esc_html__('Reader Rating', 'gloria') .": (' + (bk_newRateAmount) + ' ". esc_html__('Rates', 'gloria') .")';      
                    }
    
                    if (bkRate.hasClass('bk-rated')) {
                        bkRate.find('.bk-criteria').html(bkExistingRatedLine); 
                    }
    
                    bkNotRated.on('mousemove click mouseleave mouseenter', function(e) {
                        var bkParentOffset = $(this).parent().offset();  
                        ";
                        if($gloria_option['bk-rtl-sw']) {
                            $user_rating_script.= "
                            var bkBaseX =  100 - (Math.ceil((e.pageX - bkParentOffset.left) / bkWidthDivider));";
                        }else {
                            $user_rating_script.= "
                            var bkBaseX = Math.ceil((e.pageX - bkParentOffset.left) / bkWidthDivider);";
                        }
                        $user_rating_script.= "
                        var bkFinalX = (bkBaseX / 10).toFixed(1);
                        bkCriteriaAverage.text(bkFinalX);
                        
                        bkExistingOverlaySpan.css( 'width', (bkBaseX +'%') );
     
                        if ( e.type == 'mouseleave' ) {
                            bkExistingOverlaySpan.animate( {'width': bkExistingOverlay}, 300); 
                            bkCriteriaAverage.text(bkExistingScore); 
                        }
                        
                        if ( e.type == 'click' ) {
                                var bkFinalX = bkFinalX;
                                //console.log(bkRatedLineChanged);
                                bkRateCriteria.fadeOut(550, function () {  $(this).fadeIn(550).html(bkRatedLineChanged);  });
                                var bkParentOffset = $(this).parent().offset(),
                                    nonce = $('input#rating_nonce').val(),
                                    bk_data_rates = { 
                                            action  : 'bk_rate_counter', 
                                            nonce   : nonce, 
                                            postid  : '". $post->ID ."' 
                                    },
                                    bk_data_score = { 
                                            action: 'bk_add_user_score', 
                                            nonce: nonce, 
                                            bkCurrentRates: bkRateAmount, 
                                            bkNewScore: bkFinalX, 
                                            postid: '". $post->ID ."' 
                                    };
                                
                                bkRateOverlay.off('mousemove click mouseleave mouseenter'); 
                                        
                                $.post('". admin_url('admin-ajax.php'). "', bk_data_rates, function(bk_rates) {
                                    if ( bk_rates !== '-1' ) {
                                        
                                        var bk_checker = cookie.get('bk_user_rating'); 
                                       
                                        if (!bk_checker) {
                                            var bk_rating_c = '" . $post->ID . "';
                                        } else {
                                            var bk_rating_c = bk_checker + '," . $post->ID . "';
                                        }
                                       cookie.set('bk_user_rating', bk_rating_c, { expires: 1, }); 
                                    } 
                                });
                                        
                                $.post('". admin_url('admin-ajax.php') ."', bk_data_score, function(bk_score) {
                                        var res = bk_score.split(' ');
                                        if ( ( res[0] !== '-1' ) && ( res[0] !=='null' ) ) {
                                            
                                            var bkScoreOverlay = (res[0]*10);
                                            var latestScore = res[1];
                                            var bkScore_display = (parseFloat(res[0])).toFixed(1);
                                            var overlay_w = jQuery('#bk-rate').find('.bk-overlay').width();
                                            
                                            var bkScoreOverlay_px = (bkScoreOverlay*overlay_w)/100;
                                            
                                            old_val = overlay_w;
                                            
                                            bkCriteriaAverage.html( bkScore_display ); 
                                           
                                            bkExistingOverlaySpan.css( 'width', bkScoreOverlay_px +'px' );
                                            bkRate.addClass('bk-rated');
                                            bkRateOverlay.addClass('bk-tipper-bottom').attr('data-title', '". esc_html__('You have rated ', 'gloria') . "' + latestScore + ' points for this post');
                                            bkRate.off('click');
                                            $('.bk-tipper-bottom').tipper({
                                                direction: 'bottom'
                                            });
                                        } 
                                });
                                cookie.set('bk_score_rating', bkFinalX, { expires: 1, }); 
                                
                                return false;
                       }
                    });
                })(jQuery);
                </script>";
                echo ($user_rating_script);
            }
        }
    }
}
if ( ! function_exists( 'gloria_rate_counter' ) ) {
    function gloria_rate_counter() {
        if ( ! wp_verify_nonce($_POST['nonce'], 'rating_nonce') ) { return; }
    
        $bk_post_id = $_POST['postid'];   
        $bk_current_rates = get_post_meta($bk_post_id, "bk_rates", true); 
        
        if ($bk_current_rates == NULL) {
             $bk_current_rates = 0; 
        }
        
        $bk_current_rates = intval($bk_current_rates);       
        $bk_new_rates = $bk_current_rates + 1;
        
        update_post_meta($bk_post_id, 'bk_rates', $bk_new_rates);
            
        die(0);
    }
}
add_action('wp_ajax_bk_rate_counter', 'gloria_rate_counter');
add_action('wp_ajax_nopriv_bk_rate_counter', 'gloria_rate_counter');

if ( ! function_exists( 'gloria_add_user_score' ) ) {
    function gloria_add_user_score() {
        
        if ( ! wp_verify_nonce($_POST['nonce'], 'rating_nonce')) { return; }

        $bk_post_id = $_POST['postid'];
        $bk_latest_score = floatval($_POST['bkNewScore']);
        $bk_current_rates = floatval($_POST['bkCurrentRates']);   
        
        $current_score = get_post_meta($bk_post_id, "bk_user_score_output", true);    

        if ($bk_current_rates == NULL) {
            $bk_current_rates = 0; 
        }

        if ($bk_current_rates == 0) {
            $bk_new_score =  $bk_latest_score ;
        }
        
        if ($bk_current_rates == 1) {
            $bk_new_score = round(floatval(( $current_score + $bk_latest_score  ) / 2),1);
        }
        if ($bk_current_rates > 1) {
            $current_score_total = ($current_score * $bk_current_rates );
            $bk_new_score = round(floatval(($current_score_total + $bk_latest_score) / ($bk_current_rates + 1)),1) ;
        }

        update_post_meta($bk_post_id, 'bk_user_score_output', $bk_new_score);
        $score_return = array();
        $score_return['bk_new_score'] = $bk_new_score;
        $score_return['bk_latest_score'] = $bk_latest_score;                 
        echo implode(" ",$score_return);
        die();
    }
}
add_action('wp_ajax_bk_add_user_score', 'gloria_add_user_score');
add_action('wp_ajax_nopriv_bk_add_user_score', 'gloria_add_user_score');
/**
* ************* Author Page.*****************
*---------------------------------------------------
*/ 
if ( ! function_exists( 'gloria_contact_data' ) ) {  
    function gloria_contact_data($contactmethods) {
    
        unset($contactmethods['aim']);
        unset($contactmethods['yim']);
        unset($contactmethods['jabber']);
        $contactmethods['publicemail'] = 'Public Email';
        $contactmethods['twitter'] = 'Twitter Username';
        $contactmethods['facebook'] = 'Facebook URL';
        $contactmethods['youtube'] = 'Youtube Username';
        $contactmethods['googleplus'] = 'Google+ (Entire URL)';
         
        return $contactmethods;
    }
}
add_filter('user_contactmethods', 'gloria_contact_data');

/**
 * wp_get_attachment
 * -------------------------------------------------
 */
if ( ! function_exists( 'wp_get_attachment' ) ) {
    function wp_get_attachment( $attachment_id ) {
    
        $attachment = get_post( $attachment_id );
        return array(
        	'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
        	'caption' => $attachment->post_excerpt,
        	'description' => $attachment->post_content,
        	'href' => get_permalink( $attachment->ID ),
        	'src' => $attachment->guid,
        	'title' => $attachment->post_title
        );
    }
}

/**
 * ************* Pagination *****************
 *---------------------------------------------------
 */ 
if ( ! function_exists( 'gloria_paginate') ) {
    function gloria_paginate(){  
        global $wp_query, $wp_rewrite;
        $gloria_option = gloria_core::bk_get_global_var('gloria_option'); 
        if ( $wp_query->max_num_pages > 1 ) : ?>
        <div id="pagination" class="clear-fix">
        	<?php
        		$wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
                if($gloria_option['bk-rtl-sw']) {
                    $pagination = array(
            			'base' => add_query_arg( 'paged','%#%' ),
            			'format' => '',
            			'total' => $wp_query->max_num_pages,
            			'current' => $current,
            			'prev_text' => '<i class="fa fa-long-arrow-right"></i>',
            			'next_text' => '<i class="fa fa-long-arrow-left"></i>',
            			'type' => 'plain'
            		); 
                }else {
            		$pagination = array(
            			'base' => add_query_arg( 'paged','%#%' ),
            			'format' => '',
            			'total' => $wp_query->max_num_pages,
            			'current' => $current,
            			'prev_text' => '<i class="fa fa-long-arrow-left"></i>',
            			'next_text' => '<i class="fa fa-long-arrow-right"></i>',
            			'type' => 'plain'
            		);
                }
        		
        		if( $wp_rewrite->using_permalinks() )
        			$pagination['base'] = user_trailingslashit( trailingslashit( esc_url(remove_query_arg( 's', get_pagenum_link( 1 ) )) ) . 'page/%#%/', 'paged' );
        
        		if( !empty( $wp_query->query_vars['s'] ) )
        			$pagination['add_args'] = array( 's' => get_query_var( 's' ) );
        
        		echo paginate_links( $pagination );

        	?>
        </div>
<?php
    endif;
    }
}
/* Convert hexdec color string to rgb(a) string */

function gloria_hex2rgba($color, $opacity = false) {

	$default = 'rgb(0,0,0)';

	//Return default if no color provided
	if(empty($color))
          return $default; 

	//Sanitize $color if "#" is provided 
        if ($color[0] == '#' ) {
        	$color = substr( $color, 1 );
        }

        //Check if color has 6 or 3 characters and get values
        if (strlen($color) == 6) {
                $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
        } elseif ( strlen( $color ) == 3 ) {
                $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
        } else {
                return $default;
        }

        //Convert hexadec to rgb
        $rgb =  array_map('hexdec', $hex);

        //Check if opacity is set(rgba or rgb)
        if($opacity){
        	if(abs($opacity) > 1)
        		$opacity = 1.0;
        	$output = 'rgba('.implode(",",$rgb).','.$opacity.')';
        } else {
        	$output = 'rgb('.implode(",",$rgb).')';
        }

        //Return rgb(a) color string
        return $output;
}
/*
//Adding the Open Graph in the Language Attributes
function add_opengraph_doctype( $output ) {
		return $output . ' xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml"';
	}
add_filter('language_attributes', 'add_opengraph_doctype');

//Lets add Open Graph Meta Info

function insert_fb_in_head() {
	global $post;
	if ( !is_singular()) //if it is not a post or a page
		return;
        echo '<meta property="og:title" content="' . get_the_title() . '"/>';
        echo '<meta property="og:type" content="article"/>';
        echo '<meta property="og:url" content="' . get_permalink() . '"/>';
	if(!has_post_thumbnail( $post->ID )) { //the post does not have featured image, use a default image
		echo '<meta property="og:image" content=""/>';
	}
	else{
		$thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'gloria_600_315' );
		echo '<meta property="og:image" content="' . esc_attr( $thumbnail_src[0] ) . '"/>';
	}
	echo "
";
}
add_action( 'wp_head', 'insert_fb_in_head', 5 );
*/
// Block 1
add_action( 'wp_ajax_block_1', 'gloria_ajax_block_1' );
add_action('wp_ajax_nopriv_block_1', 'gloria_ajax_block_1');
if ( ! function_exists( 'gloria_ajax_block_1' ) ) {
    function gloria_ajax_block_1() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['offset'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['offset'];
        $args['orderby'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['order'];
        $args['cat'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $the_query = new WP_Query( $args );
        echo gloria_block_1::render_modules($the_query);
        die();
    }
}
// Block 2
add_action( 'wp_ajax_block_2', 'gloria_ajax_block_2' );
add_action('wp_ajax_nopriv_block_2', 'gloria_ajax_block_2');
if ( ! function_exists( 'gloria_ajax_block_2' ) ) {
    function gloria_ajax_block_2() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['offset'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['offset'];
        $args['orderby'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['order'];
        $args['cat'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $the_query = new WP_Query( $args );
        echo gloria_block_2::render_modules($the_query);
        die();
    }
}
// Square Grid
add_action( 'wp_ajax_square_grid', 'gloria_ajax_square_grid' );
add_action('wp_ajax_nopriv_square_grid', 'gloria_ajax_square_grid');
if ( ! function_exists( 'gloria_ajax_square_grid' ) ) {
    function gloria_ajax_square_grid() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['offset'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['offset'];
        $args['orderby'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['order'];
        $args['cat'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $the_query = new WP_Query( $args );
        echo gloria_square_grid::render_modules($the_query, $bk_ajax_c[$blockID]['columns']);
        die();
    }
}
// Row
add_action( 'wp_ajax_row', 'gloria_ajax_row' );
add_action('wp_ajax_nopriv_row', 'gloria_ajax_row');
if ( ! function_exists( 'gloria_ajax_row' ) ) {
    function gloria_ajax_row() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['offset'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['offset'];
        $args['orderby'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['order'];
        $args['cat'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $the_query = new WP_Query( $args );
        echo gloria_row::render_modules($the_query);
        die();
    }
}
// Row
add_action( 'wp_ajax_hero', 'gloria_ajax_hero' );
add_action('wp_ajax_nopriv_hero', 'gloria_ajax_hero');
if ( ! function_exists( 'gloria_ajax_hero' ) ) {
    function gloria_ajax_hero() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['offset'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['offset'];
        $args['orderby'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['order'];
        $args['cat'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $the_query = new WP_Query( $args );
        echo gloria_hero::render_modules($the_query);
        die();
    }
}
// Large Blog
add_action( 'wp_ajax_large_blog', 'gloria_ajax_large_blog' );
add_action('wp_ajax_nopriv_large_blog', 'gloria_ajax_large_blog');
if ( ! function_exists( 'gloria_ajax_large_blog' ) ) {
    function gloria_ajax_large_blog() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['offset'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['offset'];
        $args['orderby'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['order'];
        $args['cat'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $the_query = new WP_Query( $args );
        echo gloria_large_blog::render_modules($the_query);
        die();
    }
}
// Classic Blog
add_action( 'wp_ajax_classic_blog', 'gloria_ajax_classic_blog' );
add_action('wp_ajax_nopriv_classic_blog', 'gloria_ajax_classic_blog');
if ( ! function_exists( 'gloria_ajax_classic_blog' ) ) {
    function gloria_ajax_classic_blog() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['offset'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['offset'];
        $args['orderby'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['order'];
        $args['cat'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $the_query = new WP_Query( $args );
        echo gloria_classic_blog::render_modules($the_query);
        die();
    }
}
// Masonry
add_action( 'wp_ajax_masonry', 'gloria_ajax_masonry' );
add_action('wp_ajax_nopriv_masonry', 'gloria_ajax_masonry');
if ( ! function_exists( 'gloria_ajax_masonry' ) ) {
    function gloria_ajax_masonry() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['offset'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['offset'];
        $args['orderby'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['order'];
        $args['cat'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $the_query = new WP_Query( $args );
        echo gloria_masonry::render_modules($the_query, $bk_ajax_c[$blockID]['columns']);
        die();
    }
}

//Adding the Open Graph in the Language Attributes
function add_opengraph_doctype( $output ) {
    $gloria_option = gloria_core::bk_get_global_var('gloria_option');
    if($gloria_option['bk-og-tag']) {
		return $output . ' xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml"';
    }else{
        return '';
    }
}
add_filter('language_attributes', 'add_opengraph_doctype');

function insert_fb_in_head() {
    $gloria_option = gloria_core::bk_get_global_var('gloria_option');
    if($gloria_option['bk-og-tag']) {
    	global $post;
    	if ( !is_singular()) //if it is not a post or a page
    		return;
            echo '<meta property="og:title" content="' . get_the_title() . '"/>';
            echo '<meta property="og:type" content="article"/>';
            echo '<meta property="og:url" content="' . get_permalink() . '"/>';
            echo '<meta property="og:site_name" content="' . get_bloginfo("name") . '"/>';
    	if(!has_post_thumbnail( $post->ID )) { //the post does not have featured image, use a default image
    		echo '<meta property="og:image" content=""/>';
    	}
    	else{
    		$thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'bk600_315' );
    		echo '<meta property="og:image" content="' . esc_attr( $thumbnail_src[0] ) . '"/>';
    	}
    	echo "
    ";
    }
}
add_action( 'wp_head', 'insert_fb_in_head', 5 );
/** One Click Demo Import **/

function gloria_get_server_request($key) {
	$value = '';
	if ($key) {
		if (isset($_GET[$key])) {
			$value = $_GET[$key];
		} else if (isset($_POST[$key])) {
			$value = $_POST[$key];
		}
	}
	return $value;
}
if ( ! function_exists( 'gloria_install_demo_ajax_callback' ) ) {
    function gloria_install_demo_ajax_callback() {
    	$reset = gloria_get_server_request('reset');
    	if ($reset) {
    		update_option('bk_demo_log', '');
    		update_option('bk_demo_percent', 0);
    		echo 'DONE';
    	} else {
    		$log = get_option('bk_demo_log');
    		$percent = get_option('bk_demo_percent');
    		echo ($percent.'#####'.$log);
    	}
    	die();
    }
}
if (is_admin()) :
	add_action( 'wp_ajax_nopriv_bk_install_demo_ajax', 'gloria_install_demo_ajax_callback' );
	add_action( 'wp_ajax_bk_install_demo_ajax', 'gloria_install_demo_ajax_callback' );
endif;// is_admin for ajax

get_template_part('./library/templates/admin/gloria_admin_panel');

function gloria_activate_admin_panel() {
    if (current_user_can('manage_options')) {  
        add_theme_page('Activate Admin Panel', 'Activate Admin Panel', 'edit_theme_options', 'activate-admin-panel', 'gloria_admin_panel');
    }
}

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if ( !is_plugin_active( 'gloria-admin-panel/gloria-admin-panel.php' ) ) {
    add_action('admin_menu', 'gloria_activate_admin_panel');
}

function gloria_call_admin_install_template() {
    global $pagenow;
    if ( is_admin() && 'themes.php' == $pagenow && isset( $_GET['activated'] ) ) {
        if ( !is_plugin_active( 'gloria-admin-panel/gloria-admin-panel.php' ) ) {
            wp_redirect(admin_url('admin.php?page=activate-admin-panel'));
        }else {
            wp_redirect(admin_url('admin.php?page=bk-theme-welcome'));
        }
        exit;
    }
}
gloria_call_admin_install_template();