<?php
add_action('wp_head','custom_css',20);
if ( ! function_exists( 'custom_css' ) ) {
    function custom_css() {
        $gloria_option = gloria_core::bk_get_global_var('gloria_option');
        if ( isset($gloria_option)):
            $primary_color = $gloria_option['bk-primary-color'];
            $secondary_color = $gloria_option['bk-secondary-color'];
            $bg_switch = $gloria_option['bk-site-layout'];
            $cat_opt = get_option('bk_cat_opt');  
            $sb_location = $gloria_option['bk-sb-location-sw']; 
            $sb_responsive_sw = $gloria_option['bk-sb-responsive-sw'];
            $main_nav_layout = $gloria_option['bk-main-nav-layout'];   
            $meta_review = $gloria_option['bk-meta-review-sw'];
            $meta_author = $gloria_option['bk-meta-author-sw'];
            $meta_date = $gloria_option['bk-meta-date-sw'];
            $meta_comments = $gloria_option['bk-meta-comments-sw'];        
            $custom_css = $gloria_option['bk-css-code'];    
            $bkbreadcrumbs = $gloria_option['bk-breadcrumbs'];  
?>
    
    <style type='text/css' media="all">
        <?php
            if ( ($meta_review) == 0) echo ('.review-score {display: none !important;}'); 
            if ( ($meta_author) == 0) echo ('.post-author {display: none !important;}'); 
            if ( ($meta_date) == 0) echo ('.post-date {display: none !important;}'); 
            if ( ($meta_comments) == 0) echo ('.meta-comment {display: none !important;}');
            if ( ($sb_location) == 'left') echo ('.has-sb > div > .content-wrap {float: right;} .has-sb .sidebar {float: left; padding-left: 15px; padding-right: 30px;}'); 
            if ( ($bkbreadcrumbs) == 'disable') echo ('.bk-breadcrumbs-wrap{display: none;}');
        ?>
        ::selection {color: #FFF; background: <?php echo esc_attr($primary_color); ?>}
        ::-webkit-selection {color: #FFF; background: <?php echo esc_attr($primary_color); ?>}
        <?php if ( ($primary_color) != null) {?> 
             p > a, .article-content p a, .article-content p a:visited, .article-content p a:focus, .article-content li a, .article-content li a:visited, 
             .article-content li a:focus, .content_out.small-post .meta .post-category a, .ticker-title, #top-menu>ul>li:hover, 
             #top-menu>ul>li .sub-menu li:hover, .content_in .meta > div.post-category a,
            .meta .post-category a, .top-nav .bk-links-modal:hover, .bk-lwa-profile .bk-user-data > div:hover,
            .s-post-header .meta > .post-category a, .breadcrumbs .location,
            .error-number h4, .redirect-home,
            .bk-author-box .author-info .bk-author-page-contact a:hover, .bk-blog-content .meta .post-category a, .widget-social-counter ul li .social-icon,
            #pagination .page-numbers, .post-page-links a, .single-page .icon-play:hover, .bk-author-box .author-info h3,
            #wp-calendar tbody td a, #wp-calendar tfoot #prev, .widget-feedburner > h3, 
            a.bk_u_login:hover, a.bk_u_logout:hover, .widget-feedburner .feedburner-inner > h3,
            .meta .post-author a, .content_out.small-post .post-category a, .widget-tabs .cm-header .author-name, blockquote, blockquote:before, 
            /* Title hover */
            .bk-main-feature-inner .bk-small-group .title:hover, .row-type h4:hover, .content_out.small-post h4:hover, 
            .widget-tabs .author-comment-wrap h4:hover, .widget_comment .post-title:hover, .classic-blog-type .post-c-wrap .title:hover, 
            .module-large-blog .post-c-wrap h4:hover, .widget_reviews_tabs .post-list h4:hover, .module-tiny-row .post-c-wrap h4:hover, .pros-cons-title, 
            .article-content p a:hover, .article-content p a:visited, .article-content p a:focus, .s-post-nav .nav-btn h3:hover,
            .widget_recent_entries a:hover, .widget_archive a:hover, .widget_categories a:hover, .widget_meta a:hover, .widget_pages a:hover, .widget_recent_comments a:hover, .widget_nav_menu > div a:hover,
            .widget_rss li a:hover, .widget.timeline-posts li a:hover, .widget.timeline-posts li a:hover .post-date, 
            .bk-header-2 .header-social .social-icon a:hover, .bk-header-90 .header-social .social-icon a:hover,
            /*** Woocommerce ***/
            .woocommerce-page .star-rating span, .woocommerce-page p.stars a, .woocommerce-page div.product form.cart table td .reset_variations:hover,
            .bk_small_cart .widget_shopping_cart .cart_list a:hover,
            /*** BBPRESS ***/
            #subscription-toggle, #subscription-toggle:hover, #bbpress-forums li > a:hover,
            .widget_recent_topics .details .comment-author a, .bbp-author-name, .bbp-author-name:hover, .bbp-author-name:visited, 
            .widget_latest_replies .details .comment-author, .widget_recent_topics .details .post-title:hover, .widget_display_views ul li a:hover, .widget_display_topics ul li a:hover, 
            .widget_display_replies ul li a:hover, .widget_display_forums ul li a:hover, 
            .widget_latest_replies .details h4:hover
            {color: <?php echo esc_attr($primary_color); ?>}
            
            .widget_tag_cloud .tagcloud a:hover,
            #comment-submit:hover, .main-nav, 
            #pagination .page-numbers, .post-page-links a, .post-page-links > span, .widget_latest_comments .flex-direction-nav li a:hover,
            #mobile-inner-header, input[type="submit"]:hover, #pagination .page-numbers, .post-page-links a, .post-page-links > span, .bk-login-modal, .lwa-register.lwa-register-default,
            .button:hover, .bk-back-login:hover, .footer .widget-title h3, .footer .widget-tab-titles li.active h3,
            #mobile-inner-header, .readmore a:hover, .loadmore span.ajaxtext:hover, .result-msg a:hover, .top-bar, .widget.timeline-posts li a:hover .meta:before,
            .button:hover, .woocommerce-page input.button.alt:hover, .woocommerce-page input.button:hover, .woocommerce-page div.product form.cart .button:hover,
            .woocommerce-page .woocommerce-message .button:hover, .woocommerce-page a.button:hover, .woocommerce-cart .wc-proceed-to-checkout a.checkout-button:hover,
            .bk_small_cart .woocommerce.widget_shopping_cart .buttons a:hover, .recommend-box h3:after
            {border-color: <?php echo esc_attr($primary_color); ?>;}

            .flex-direction-nav li a:hover, #back-top, .module-fw-slider .flex-control-nav li a.flex-active, .related-box h3 a.active,
            .footer .cm-flex .flex-control-paging li a.flex-active, .main-nav #main-menu .menu > li:hover, #main-menu > ul > li.current-menu-item,
            .module-title h2, .page-title h2, .row-type .post-category a, .bk-small-group .post-category a, .module-grid-carousel .bk-carousel-wrap .item-child .post-category a,
            .bk-review-box .bk-overlay span, .bk-score-box, .share-total, #pagination .page-numbers.current, .post-page-links > span, .widget_latest_comments .flex-direction-nav li a:hover,
            .searchform-wrap .search-icon, .module-square-grid .content_in_wrapper, .module-large-blog .post-category a, .result-msg a:hover,
            .readmore a:hover, .module-fw-slider .post-c-wrap .post-category a, .rating-wrap, .inner-cell .innerwrap .post-category a, .module-carousel .post-c-wrap .post-category a, 
            .widget_slider .post-category a, .module-square-grid .post-c-wrap .post-category a, .module-grid .post-c-wrap .post-category a,.module-title .bk-tabs.active a, .classic-blog-type .post-category a, .sidebar-wrap .widget-title h3, .widget-tab-titles li.active h3, 
            .module-fw-slider .post-c-wrap .readmore a:hover, .loadmore span.ajaxtext:hover, .widget_tag_cloud .tagcloud a:hover, .widget.timeline-posts li a:hover .meta:before,
            .s-tags a:hover, .singletop .post-category a, .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar, .mCSB_scrollTools .mCSB_dragger:hover .mCSB_dragger_bar, 
            .mCSB_scrollTools .mCSB_dragger:active .mCSB_dragger_bar, .mCSB_scrollTools .mCSB_dragger.mCSB_dragger_onDrag .mCSB_dragger_bar, .widget-postlist .large-post .post-category a,
            input[type="submit"]:hover, .widget-feedburner .feedburner-subscribe:hover button, .bk-back-login:hover, #comment-submit:hover,
            .bk-header-slider .post-c-wrap .readmore a,
            /** Woocommerce **/
            .woocommerce span.onsale, .woocommerce-page span.onsale, .button:hover, .woocommerce-page input.button.alt:hover, .woocommerce-page input.button:hover, .woocommerce-page div.product form.cart .button:hover,
            .woocommerce-page .woocommerce-message .button:hover, .woocommerce-page a.button:hover, .woocommerce-cart .wc-proceed-to-checkout a.checkout-button:hover, 
            .woocommerce-page div.product .summary .product_title span, .woocommerce-page div.product .woocommerce-tabs ul.tabs li.active, 
            .related.products > h2 span, .woocommerce-page #reviews h3 span, .upsells.products > h2 span, .cross-sells > h2 span, .woocommerce-page .cart-collaterals .cart_totals h2 span, 
            .woocommerce-page div.product .summary .product_title span, .woocommerce-page .widget_price_filter .ui-slider .ui-slider-range, 
            .woocommerce-page .widget_price_filter .ui-slider .ui-slider-handle, .product_meta .post-tags a:hover, .widget_display_stats dd strong, 
            .bk_small_cart .woocommerce.widget_shopping_cart .buttons a:hover, .bk_small_cart .cart-contents span,
            /*** BBPRESS ***/
            #bbpress-forums #bbp-search-form .search-icon, .widget_display_search .search-icon, #bbpress-forums div.bbp-topic-tags a:hover
            {background-color: <?php echo esc_attr($primary_color); ?>;}
            @-webkit-keyframes rotateplane {
                0% {
                    -webkit-transform: perspective(120px) scaleX(1) scaleY(1);
                    background-color: <?php echo esc_attr($primary_color); ?>;
                }
                25% {
                    -webkit-transform: perspective(120px) rotateY(90deg) scaleX(1) scaleY(1);
                    background-color: <?php echo esc_attr($primary_color); ?>;
                }
                25.1% {
                    -webkit-transform: perspective(120px) rotateY(90deg) scaleX(-1) scaleY(1);
                    background-color: #333333;
                }
                50% {
                    -webkit-transform: perspective(120px) rotateY(180deg) scaleX(-1) scaleY(1);
                    background-color: #333333;
                }
                75% {
                    -webkit-transform: perspective(120px) rotateY(180deg) rotateX(90deg) scaleX(-1) scaleY(1);
                    background-color: #333333;
                }
                75.1% {
                    -webkit-transform: perspective(120px) rotateY(180deg) rotateX(90deg) scaleX(-1) scaleY(-1);
                    background-color: <?php echo esc_attr($primary_color); ?>;
                }
                100% {
                    -webkit-transform: perspective(120px) rotateY(180deg) rotateX(180deg) scaleX(-1) scaleY(-1);
                    background-color: <?php echo esc_attr($primary_color); ?>;
                }
            }
            @keyframes rotateplane {
                0% {
                    transform: perspective(120px) rotateX(0deg) rotateY(0deg) scaleX(1) scaleY(1);
                    -webkit-transform: perspective(120px) rotateX(0deg) rotateY(0deg) scaleX(1) scaleY(1);
                    background-color: <?php echo esc_attr($primary_color); ?>;
                }
                25% {
                    transform: perspective(120px) rotateX(-90deg) rotateY(0deg) scaleX(1) scaleY(1);
                    -webkit-transform: perspective(120px) rotateX(0deg) rotateY(0deg) scaleX(1) scaleY(1);
                    background-color: <?php echo esc_attr($primary_color); ?>;
                }
                25.1% {
                    transform: perspective(120px) rotateX(-90deg) rotateY(0deg) scaleX(1) scaleY(-1);
                    -webkit-transform: perspective(120px) rotateX(-90deg) rotateY(0deg) scaleX(1) scaleY(-1);
                    background-color: #333333;
                }
                50% {
                    transform: perspective(120px) rotateX(-180.1deg) rotateY(0deg) scaleX(1) scaleY(-1);
                    -webkit-transform: perspective(120px) rotateX(-180.1deg) rotateY(0deg) scaleX(1) scaleY(-1);
                    background-color: #333333;
                }
                75% {
                    transform: perspective(120px) rotateX(-180.1deg) rotateY(-90deg) scaleX(1) scaleY(-1);
                    -webkit-transform: perspective(120px) rotateX(-180.1deg) rotateY(-90deg) scaleX(1) scaleY(-1);
                    background-color: #333333;
                }
                75.1% {
                    transform: perspective(120px) rotateX(-180.1deg) rotateY(-90deg) scaleX(-1) scaleY(-1);
                    -webkit-transform: perspective(120px) rotateX(-180.1deg) rotateY(-90deg) scaleX(-1) scaleY(-1);
                    background-color: <?php echo esc_attr($primary_color); ?>;
                }
                100% {
                    transform: perspective(120px) rotateX(-180deg) rotateY(-179.9deg) scaleX(-1) scaleY(-1);
                    -webkit-transform: perspective(120px) rotateX(-180deg) rotateY(-179.9deg) scaleX(-1) scaleY(-1);
                    background-color: <?php echo esc_attr($primary_color); ?>;
                }
            }
            .content_out .review-score, ::-webkit-scrollbar-thumb, ::-webkit-scrollbar-thumb:window-inactive 
            {background-color: <?php echo gloria_hex2rgba (esc_attr($primary_color), 0.9); ?>;}
            
            .footer .cm-flex .flex-control-paging li a
            {background-color: <?php echo gloria_hex2rgba (esc_attr($primary_color), 0.3); ?>;}
            

        <?php }?>
        <?php if ( ($secondary_color) != null) {?> 
                .widget_most_commented .comments:after 
                {border-right-color: <?php echo esc_attr($secondary_color); ?>;}
                #main-mobile-menu, .bk-dropdown-menu, .bk-sub-sub-menu, .sub-menu, .bk-mega-menu, .bk-mega-column-menu ,
                .ajax-form input, .module-title .main-title, .sidebar-wrap .widget-title, .widget_most_commented .comments,
                .related-box h3 a, .widget-tab-titles, .bk-tabs-wrapper, .widget-feedburner .feedburner-email, .widget-feedburner .feedburner-subscribe button
                {background-color: <?php echo esc_attr($secondary_color); ?>;}
            
        <?php }?>
        <?php
        if ( $bg_switch !== 'boxed') {?>
            #page-wrap { width: auto; }
        <?php }else{?>
            body { background-position: left; background-repeat: repeat; background-attachment: fixed;}
        <?php };  
        if ( $sb_responsive_sw == 0) {?>
            @media (max-width: 991px){
                .sidebar {display: none !important}
            }
        <?php };
        if ( $main_nav_layout == 'center') {?>
            .main-nav{
                text-align: center !important;
            }
        <?php };?>
        <?php if ($custom_css != '') echo ($custom_css);?>
    </style>
    <?php endif;?>
    <?php }?>
<?php }?>