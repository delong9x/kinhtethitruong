<?php
/**
 * Registering meta boxes
 *
 * All the definitions of meta boxes are listed below with comments.
 * Please read them CAREFULLY.
 *
 * You also should read the changelog to know what has been changed before updating.
 *
 * For more information, please visit:
 * @link http://metabox.io/docs/registering-meta-boxes/
 */


add_filter( 'rwmb_meta_boxes', 'bk_register_meta_boxes' );

/**
 * Register meta boxes
 *
 * Remember to change "your_prefix" to actual prefix in your project
 *
 * @param array $meta_boxes List of meta boxes
 *
 * @return array
 */
function bk_register_meta_boxes( $meta_boxes )
{
    $prefix = 'bk_';
    $bk_sidebar = array();
    foreach ( $GLOBALS['wp_registered_sidebars'] as $value => $label ) {
        $bk_sidebar[$value] = ucwords( $label['name'] );
	}
    // Post Layout Options
    $meta_boxes[] = array(
        'id' => "{$prefix}post_ops",
        'title' => esc_html__( 'BK Post Option', 'gloria' ),
        'pages' => array( 'post' ),
        'context' => 'normal',
        'priority' => 'low',
    
        'fields' => array(
            // Enable Review
            array(
    			'id' => "{$prefix}title_pos",
                'name' => esc_html__( 'Post Title Setup', 'gloria' ),
    			'desc' => esc_html__('Wide Title option is suitable for the long title', 'gloria'),
                'type' => 'select', 
    			'options'  => array(
                                'in-content' => esc_html__( 'Title in Article Content section', 'gloria' ),
                                'wide-title' => esc_html__( 'Wide Title', 'gloria' ),
        				    ),
    			// Select multiple values, optional. Default is false.
    			'multiple'    => false,
    			'std'         => 'in-content',
    		),
            // Enable Review
            array(
    			'id' => "{$prefix}post_layout",
                'name' => esc_html__( 'Post Layout Option', 'gloria' ),
    			'desc' => esc_html__('Setup Post Layout', 'gloria'),
                'type' => 'select', 
    			'options'  => array(
                                'standard' => esc_html__( 'Standard', 'gloria' ),
                                'standard2' => esc_html__( 'Standard Style 2', 'gloria' ),
                                'fw-feat-img' => esc_html__('Full Width Feature Image', 'gloria'), 
        				    ),
    			// Select multiple values, optional. Default is false.
    			'multiple'    => false,
    			'std'         => 'standard',
    		),
            // Enable Parallax
            array(
                'name' => esc_html__( 'Parallax', 'gloria' ),
                'id' => "{$prefix}parallax_single",
                'type' => 'checkbox',
                'desc' => esc_html__( 'Enable Parallax Feature Image', 'gloria' ),
                'std'  => 0,
            ),
            //  FW Style 2
            array(
                'name' => esc_html__( 'Style', 'gloria' ),
                'id' => "{$prefix}fwstyle",
                'desc' => esc_html__('Choose between Style 1 / Style 2 for FW Feature Image Style', 'gloria'),
                'type' => 'select', 
                'options'  => array(
                                'fw-style-1' => esc_html__( 'Style 1', 'gloria' ),
                                'fw-style-2' => esc_html__( 'Style 2', 'gloria' ),
                            ),
                'std'  => 'fw-style-1',
            ),
            // Feature Image Config
            array(
                'name' => esc_html__( 'Feature Image Config', 'gloria' ),
                'id' => "{$prefix}feat_img_status",
                'desc' => esc_html__( 'On / Off the Feature Image on this post (does not effect on FULL WIDTH Layout)', 'gloria' ),
                'type' => 'select', 
    			'options'  => array(
                                'on' => esc_html__( 'On', 'gloria' ),
                                'off' => esc_html__( 'Off', 'gloria' ),
        				    ),
    			// Select multiple values, optional. Default is false.
    			'multiple'    => false,
    			'std'         => 'on',
            ),
                        // Sidebar Remove?
            array(
                'name' => esc_html__( 'Remove the Sidebar', 'gloria' ),
                'id' => "{$prefix}remove_sidebar",
                'type' => 'checkbox',
                'desc' => esc_html__( 'Check to remove the sidebar on this post', 'gloria' ),
                'std'  => 0,
            ),
            
            // Sidebar Select
            array(
                'name' => esc_html__( 'Choose a sidebar for this post', 'gloria' ),
                'id' => "{$prefix}post_sb_select",
                'type' => 'select',
                'options'  => $bk_sidebar,
                'desc' => esc_html__( 'Sidebar Select', 'gloria' ),
                'std'  => 'single_sidebar',
            ),
            
            
            // Sidebar Position
            array(
                'name' => esc_html__( 'Sidebar Position', 'gloria' ),
                'id' => "{$prefix}sidebar_pos",
                'desc' => esc_html__( 'Select between left position or right position for sidebar', 'gloria' ),
                'type' => 'select', 
    			'options'  => array(
                                'right' => esc_html__( 'right', 'gloria' ),
                                'left' => esc_html__( 'left', 'gloria' ),
        				    ),
    			// Select multiple values, optional. Default is false.
    			'multiple'    => false,
    			'std'         => 'right',
            ),
            
            // Feature Image Config
            array(
                'name' => esc_html__( 'Sticky Share Box', 'gloria' ),
                'id' => "{$prefix}sticky_share",
                'desc' => esc_html__( 'On / Off the sticky share box', 'gloria' ),
                'type' => 'select', 
    			'options'  => array(
                                'off' => esc_html__( 'Off', 'gloria' ),
                                'on' => esc_html__( 'On', 'gloria' ),
        				    ),
    			// Select multiple values, optional. Default is false.
    			'multiple'    => false,
    			'std'         => 'off',
            ),
        )
    );
    // 2nd meta box
    $meta_boxes[] = array(
        'id' => "{$prefix}format_options",
        'title' => esc_html__( 'BK Post Format Options', 'gloria' ),
        'pages' => array( 'post' ),
        'context' => 'normal',
        'priority' => 'high',
    	'fields' => array(        
            //Video
            array(
                'name' => esc_html__( 'Format Options: Video, Audio', 'gloria' ),
                'desc' => esc_html__('Support Youtube, Vimeo, SoundCloud, DailyMotion Link', 'gloria'),
                'id' => "{$prefix}media_embed_code_post",
                'type' => 'textarea',
                'placeholder' => esc_html__('Link ...', 'gloria'),
                'std' => ''
            ),
    		// PLUPLOAD IMAGE UPLOAD (WP 3.3+)
    		array(
    			'name'             => esc_html__( 'Format Options: Image', 'gloria' ),
                'desc'             => esc_html__('Image Upload', 'gloria'),
    			'id'               => "{$prefix}image_upload",
    			'type'             => 'plupload_image',
    			'max_file_uploads' => 1,
    		),
            //Gallery
            array(
                'name' => esc_html__( 'Format Options: Gallery', 'gloria' ),
                'desc' => esc_html__('Gallery Images', 'gloria'),
                'id' => "{$prefix}gallery_content",
                'type' => 'image_advanced',
                'std' => ''
            ),
        )
    );
    
    // Post Review Options
    $meta_boxes[] = array(
        'id' => "{$prefix}review",
        'title' => esc_html__( 'BK Review System', 'gloria' ),
        'pages' => array( 'post' ),
        'context' => 'normal',
        'priority' => 'high',
    
        'fields' => array(
            // Enable Review
            array(
                'name' => esc_html__( 'Include Review Box', 'gloria' ),
                'id' => "{$prefix}review_checkbox",
                'type' => 'checkbox',
                'desc' => esc_html__( 'Enable Review On This Post', 'gloria' ),
                'std'  => 0,
            ),
            array(
				'id'     => 'reviewcriterias',
				'type'   => 'group',

				'fields' => array(
					// Criteria 1 Text & Score
                    array(
                        'name'  => esc_html__( 'Criteria Title', 'gloria' ),
                        'id'    => "{$prefix}ct",
                        'type'  => 'text',
                    ),
                    array(
                        'name' => esc_html__( 'Criteria Score', 'gloria' ),
                        'id' => "{$prefix}cs",
                        'type' => 'slider',
                        'js_options' => array(
                            'min'   => 0,
                            'max'   => 10.05,
                            'step'  => .1,
                        ),
                    ),
				),

				// Clone whole group?
				'clone'  => true,
			),
            
            // Final average
            array(
                'name'  => esc_html__('Final Average Score','gloria'),
                'id'    => "{$prefix}final_score",
                'type'  => 'text',
            ),
            
            // Summary
            array(
                'name' => esc_html__( 'Summary', 'gloria' ),
                'id'   => "{$prefix}summary",
                'type' => 'textarea',
                'cols' => 20,
                'rows' => 4,
            ),
            
            array(
    			'id' => "{$prefix}pros_cons_enable",
                'name' => esc_html__( 'Positive / Negative Author review (Pros and Cons List)', 'gloria' ),
    			'desc' => esc_html__('', 'gloria'),
                'type' => 'select', 
    			'options'  => array(
                                'disable' => esc_html__( 'Disable', 'gloria' ),
                                'enable' => esc_html__( 'Enable', 'gloria' ),
        				    ),
    			// Select multiple values, optional. Default is false.
    			'multiple'    => false,
    			'std'         => 'disable',
    		),
            // Positive Title
            array(
                'name'  => esc_html__( 'Positive Title', 'gloria' ),
                'id'    => "{$prefix}pro_title",
                'type'  => 'text',
            ),
            // Positive
            array(
                'name' => esc_html__( 'Positive', 'gloria' ),
                'id'   => "{$prefix}pros",
                'type' => 'textarea',
                'cols' => 20,
                'rows' => 2,
                'clone' => true,                        
            ),
            // Negative Title
            array(
                'name'  => esc_html__( 'Negative Title', 'gloria' ),
                'id'    => "{$prefix}con_title",
                'type'  => 'text',
            ),
            // Negative
            array(
                'name' => esc_html__( 'Negative', 'gloria' ),
                'id'   => "{$prefix}cons",
                'type' => 'textarea',
                'cols' => 20,
                'rows' => 2,
                'clone' => true,            
            ),
            array(
                'name' => esc_html__( 'User Rating', 'gloria' ),
                'id' => "{$prefix}user_rating",
                'type' => 'checkbox',
                'desc' => esc_html__( 'Enable User Rating On This Post', 'gloria' ),
                'std'  => 1,
            ),        
            array(
    			'id' => "{$prefix}review_box_position",
                'name' => esc_html__( 'Review Box Position', 'gloria' ),
    			'desc' => esc_html__('Setup review box position', 'gloria'),
                'type' => 'select', 
    			'options'  => array(
                                'above' => esc_html__( 'Position 1 (Above the content)', 'gloria' ),
                                'below' => esc_html__( 'Position 2 (Below the content)', 'gloria' ),
        				    ),
    			// Select multiple values, optional. Default is false.
    			'multiple'    => false,
    			'std'         => 'below',
    		),
    
        )
    );
	return $meta_boxes;
}


