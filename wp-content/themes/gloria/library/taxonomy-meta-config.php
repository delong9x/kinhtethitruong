<?php
/**
 * Registering meta sections for taxonomies
 *
 * All the definitions of meta sections are listed below with comments, please read them carefully.
 * Note that each validation method of the Validation Class MUST return value.
 *
 * You also should read the changelog to know what has been changed
 *
 */

// Hook to 'admin_init' to make sure the class is loaded before
// (in case using the class in another plugin)
add_action( 'admin_init', 'bk_register_taxonomy_meta_boxes' );

/**
 * Register meta boxes
 *
 * @return void
 */
function bk_register_taxonomy_meta_boxes()
{
	// Make sure there's no errors when the plugin is deactivated or during upgrade
    	if ( !class_exists( 'RW_Taxonomy_Meta' ) )
    		return;
        $gloria_option = gloria_core::bk_get_global_var('gloria_option');
            if ( isset($gloria_option) && isset($gloria_option['bk-primary-color'])) {
                $primary_color = $gloria_option['bk-primary-color'];
            }else {
                $primary_color = '';
            }
    	$meta_sections = array();
    
    	// First meta section
    	$meta_sections[] = array(
    		'title'      => esc_html__('BK Category Options','gloria'),             // section title
    		'taxonomies' => array('category', 'post_tag'), // list of taxonomies. Default is array('category', 'post_tag'). Optional
    		'id'         => 'bk_cat_opt',                 // ID of each section, will be the option name
    
    		'fields' => array(                             // List of meta fields
    			// SELECT
    			array(
    				'name'    => esc_html__('Category / Tag Page layout','gloria'),
    				'id'      => 'cat_layout',
    				'type'    => 'select',
    				'options' => array(
                                    'global' => esc_html__('Global Setting', 'gloria'),
                                    'classic-blog'=>esc_html__('Classic Blog', 'gloria'), 
                                    'large-blog'=>esc_html__('Large Blog with Sidebar', 'gloria'),
                                    'masonry'=>esc_html__('Masonry with Sidebar', 'gloria'),
                                    'masonry-nosb'=>esc_html__('Masonry no Sidebar', 'gloria'),
                                    'square-grid-3'=>esc_html__('Square Grid no Sidebar', 'gloria'),
                                    'square-grid-2'=>esc_html__('Square Grid with Sidebar', 'gloria'),
                                ),
                    'std' => 'global',
                    'desc' => esc_html__('Global setting option is set in Theme Option panel','gloria')
    			),
                // SELECT SIDEBAR
    			array(
    				'name'      => esc_html__('Select a sidebar for this category page','gloria'),
    				'id'        => 'sb_category',
    				'type'      => 'sidebarselect',
                    'std'       => 'home_sidebar',
                    'desc'      => esc_html__('Global setting option is set in Theme Option panel','gloria')
    			),
                // Feature Posts layout
    			array(
    				'name'      => esc_html__('Layout for Feature Posts Section','gloria'),
                    'desc'      => esc_html__('Choose a layout to display the feature posts OR disable this function','gloria'),
    				'id'        => 'cat_feat',
    				'type'      => 'select',
                    'std'       => 'disable',
                    'options' => array(
                                    'disable' => esc_html__('Disable', 'gloria'),
                                    'layout1'=>esc_html__('Layout 1 (Full Width Slider Module - Require at least 2 featured posts )', 'gloria'),
                                    'layout2'=>esc_html__('Layout 2 (Feature 1 Module - Require at least 5 featured posts)', 'gloria'),
                                    'layout3'=>esc_html__('Layout 3 (Feature 2 Module - Require at least 3 featured posts)', 'gloria'),
                                    'layout4'=>esc_html__('Layout 4 (Grid module - Require at least 5 featured posts)', 'gloria'),
                                ),
    			),
    		),
    	);
                        
    	foreach ( $meta_sections as $meta_section ){
    	   
    		new RW_Taxonomy_Meta( $meta_section );
            
    	}
}