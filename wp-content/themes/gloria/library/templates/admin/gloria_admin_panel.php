<?php
function gloria_admin_panel() {
?>
<br />
<div class="page-wrap" style="margin: 20px 30px 0 2px;">
    <div class="postbox bkpostbox">
    	<div class="hndle" style="padding: 15px 30px;">
            <h1><?php esc_html_e('Welcome to Gloria', 'gloria'); ?></h1>
            <p class="bk-admin-notice">
    			Thank you for using our theme, Gloria is now installed and ready to use. Please install and activate BK admin tabs plugin to use "one click demo import" function and manage your plugins or view your system status. 
    		</p>
        </div>
    	<div class="inside" style="margin: 30px -15px 30px -15px;">
    		<div class="main gloria-welcome-main">
                <div class="gloria-activate-admin-panel-wrapper" style="width: 100% ; height: 350px;">
                    <div class="gloria-bg-intro"></div>
                    <div class="gloria-activate-admin-panel-button">
                        <?php
                            $nonce_url = wp_nonce_url(
                                add_query_arg(
                                    array(
                                        'plugin'           => 'gloria-admin-panel',
                                        'tgmpa-install' => 'install-plugin',
                                    ),
                                    admin_url('admin.php?page=install-required-plugins')
                                ),
                                'tgmpa-install',
                                'tgmpa-nonce'
                            );
                            $nonce_activate_url = wp_nonce_url(
                                add_query_arg(
                                    array(
                                        'plugin'           => 'gloria-admin-panel',
                                        'tgmpa-activate' => 'activate-plugin',
                                    ),
                                    admin_url('admin.php?page=install-required-plugins')
                                ),
                                'tgmpa-activate',
                                'tgmpa-nonce'
                            );
                            $theme_plugins = TGM_Plugin_Activation::$instance->plugins;
                            $bk_plugin_list = get_plugins();
                            foreach ($theme_plugins as $theme_plugin) {
                                if($theme_plugin['title'] == 'Gloria Admin Panel') {
                                    $plugin_status = 'not-installed';
                        
                                    if (is_plugin_active( $theme_plugin['file_path'])) {
                                        $plugin_status = 'plugin-active';
                                        $required_label = 'plugin-active';
                                    }elseif (isset($bk_plugin_list[$theme_plugin['file_path']])) {
                                        $plugin_status = 'plugin-inactive';
                                    }
                                }
                            }
                            if($plugin_status == 'not-installed') {
                                echo ('<a href="'.esc_url($nonce_url).'" class="gloria-admin-activate">Install and Activate BK Admin Tabs Plugin</a>');
                                echo ('<a href="'.esc_url(admin_url()).'" class="gloria-admin-activate-ignore">Ignore</a>');
                            }else if($plugin_status == 'plugin-inactive'){
                                echo ('<a href="'.esc_url($nonce_activate_url).'" class="gloria-admin-activate">Activate BK Admin Tabs Plugin</a>');
                                echo ('<a href="'.esc_url(admin_url()).'" class="gloria-admin-activate-ignore">Ignore</a>');
                            }else {
                                echo ('<a href="'.admin_url('admin.php?page=bk-theme-welcome').'" class="gloria-admin-activate">Go to BK Admin Tabs Panel</a>');
                            }
                        ?>
                    </div>
                </div>
                <div class="gloria-welcome-footer clearfix">
                    <div class="bk-footer-1">
                        <h2>Support Forum</h2>
                        <p>
                            Technical Support is free for all customers purchasing Gloria. For any questions or any bugs related to the setup of Gloria that are not covered by the theme documentation, please register and submit a ticket at our forum. Our support team is always ready to help you.
                        </p>
                        <div class="bk-button"><a target="_blank" href="http://forum.bk-ninja.com">Open Forum</a></div>
                    </div>
                    <div class="bk-footer-2">
                        <h2>Documentation and Video Tutorials</h2>
                        <p>
                            Our online documentation with Video Tutorials is an incredible resource for learning the ins and outs of using Gloria. Here you will find key points to manuals, tutorials and references that will come in handy easily. Watching a video is one of the best ways to understand well about Gloria. 
                        </p>
                        <div class="bk-button"><a target="_blank" href="http://forum.bk-ninja.com/documentation/">Open Documentation</a></div>
                    </div>
                </div>
    		</div>
    	</div>
    </div>
	
	
	<br class="clear"/>

</div>

<?php
}