<?php
    $gloria_option = gloria_core::bk_get_global_var('gloria_option');
    $logo = array();
    if ($gloria_option != 'null'){
        if ((isset($gloria_option['bk-logo'])) && (($gloria_option['bk-logo']) != NULL)){ 
            $logo = $gloria_option['bk-logo'];
        };
        if(isset($gloria_option['bk-header-banner-switch'])){$header_banner = $gloria_option['bk-header-banner-switch'];}else {$header_banner = 1;};
        
        if(isset($gloria_option['bk-banner-script'])){$ga_script = $gloria_option['bk-banner-script'];}else {$ga_script = '';};
        
        if(isset($gloria_option['bk-backtop-switch'])){$backtop = $gloria_option['bk-backtop-switch'];}else {$backtop = 1;};
    
        if(isset($gloria_option['bk-site-layout'])){$bkSiteLayout = $gloria_option['bk-site-layout'];}else {$bkSiteLayout = 'wide';};
        if ($header_banner){ 
            $imgurl = $gloria_option['bk-header-banner']['imgurl'];
            $linkurl = $gloria_option['bk-header-banner']['linkurl'];
        }; 
    }
    $schema_org = '';
    if (is_single()) {
    	$schema_org .= '';
    } else {
    	$schema_org .= ' itemscope itemtype="http://schema.org/WebPage"';
    }
    ?>
    <head>
    	<meta charset="<?php bloginfo('charset'); ?>" />
        
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        
        <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
    	
    	<?php if (is_search()) { ?>
    	   <meta name="robots" content="noindex, nofollow" /> 
    	<?php } ?>
    	
    	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    
    	<?php wp_head(); ?>
        <link rel="stylesheet" type="text/css" href="<?=get_theme_root_uri()?>/gloria/css/style-dev.css">
        <link rel="stylesheet" type="text/css" href="<?=get_theme_root_uri()?>/gloria/css/responsive-dev.css">
    </head>
    
    <body <?php body_class(); echo (esc_attr($schema_org)); ?>>
        <div id="page-wrap" class= '<?php if($bkSiteLayout !== 'boxed') echo "wide";?>'>
        <div id="main-mobile-menu">
            <div class="block">
                <div id="mobile-inner-header">
                    <h3 class="menu-title">
                        <?php echo esc_attr(bloginfo( 'name' ));?>
                    </h3>
                    <a class="mobile-menu-close" href="#" title="Close"><i class="fa fa-long-arrow-left"></i></a>
                </div>
                <?php if ( isset($gloria_option ['bk-header-top-switch']) && ($gloria_option ['bk-header-top-switch'] == 1) ){ ?>
                     <div class="top-menu">
                        <h3 class="menu-location-title">
                            <?php esc_html_e('Top Menu', 'gloria');?>
                        </h3>
                    <?php
                    wp_nav_menu( array( 
                        'theme_location' => 'menu-top',
                        'depth' => '3',
                        'container_id' => 'mobile-top-menu' ) );
                    ?>
                    </div>
                <?php }?>
                <div class="main-menu">
                    <h3 class="menu-location-title">
                        <?php esc_html_e('Main Menu', 'gloria');?>
                    </h3>
                    <?php
                    wp_nav_menu( array( 
                        'theme_location' => 'main-menu',
                        'depth' => '3',
                        'container_id' => 'mobile-menu' ) );
                    ?>
                </div>
            </div>
        </div>
        <div id="page-inner-wrap">
            <div class="page-cover mobile-menu-close"></div>
            <div class="bk-page-header">
                <div class="header-wrap header-1">
                    <div class="top-bar" style="display: ;">
                        <div class="bkwrapper container">
                            <?php if ( isset($gloria_option ['bk-header-top-switch']) && ($gloria_option ['bk-header-top-switch'] == 1) ){ ?>
                                <div class="top-nav clearfix">
                                    <?php if ( isset($gloria_option ['bk-ajaxlogin-switch']) && ($gloria_option ['bk-ajaxlogin-switch'] == 1) ){ ?>
                                        <?php 
                                            if ( function_exists('login_with_ajax') ) {  
                                                $bk_home_url = esc_url(get_home_url('/'));
                                                $ajaxArgs = array(
                                                    'profile_link' => true,
                                                    'template' => 'modal',
                                                    'registration' => true,
                                                    'remember' => true,
                                                    'redirect'  => $bk_home_url
                                                );
                                                login_with_ajax($ajaxArgs);  
                                        }?>
                                    <?php }?>
                                    <?php if ( has_nav_menu('menu-top') ) {?> 
                                        <?php wp_nav_menu(array('theme_location' => 'menu-top','container_id' => 'top-menu'));?> 
                                    <?php }?>
                                    <?php if ( isset($gloria_option ['bk-cart-icon-switch']) && ($gloria_option ['bk-cart-icon-switch'] == 1) ){ ?>
                                    <?php if (class_exists('Woocommerce') && (!is_cart())) {?>
                                        <div class="bk_small_cart">
                                            <?php global $woocommerce; ?>
                                            <a class="cart-contents" href="<?php echo esc_url(WC()->cart->get_cart_url()); ?>" title="<?php esc_html_e( 'View your shopping cart', 'gloria' ); ?>"><i class="fa fa-shopping-cart"></i><span><?php echo sprintf (_n( '%d', '%d', WC()->cart->cart_contents_count, 'gloria' ), WC()->cart->cart_contents_count ); ?></span></a>
                                            <div id="bk_small_cart_widget">
                                                <?php dynamic_sidebar('cart-1') ?>
                                            </div>
                                        </div>
                                    <?php }?>
                                <?php }?>
                                    <?php if ( isset($gloria_option ['bk-social-header-switch']) && ($gloria_option ['bk-social-header-switch'] == 1) ){ ?>
                    				<div class="header-social">
                    					<ul class="clearfix">
                    						<?php if ($gloria_option['bk-social-header']['fb']){ ?>
                    							<li class="social-icon fb"><a class="bk-tipper-bottom" data-title="Facebook" href="<?php echo esc_url($gloria_option['bk-social-header']['fb']); ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    						<?php } ?>
                    						
                    						<?php if ($gloria_option['bk-social-header']['twitter']){ ?>
                    							<li class="social-icon twitter"><a class="bk-tipper-bottom" data-title="Twitter" href="<?php echo esc_url($gloria_option['bk-social-header']['twitter']); ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    						<?php } ?>
                    						
                    						<?php if ($gloria_option['bk-social-header']['gplus']){ ?>
                    							<li class="social-icon gplus"><a class="bk-tipper-bottom" data-title="Google Plus" href="<?php echo esc_url($gloria_option['bk-social-header']['gplus']); ?>" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                    						<?php } ?>
                    						
                    						<?php if ($gloria_option['bk-social-header']['linkedin']){ ?>
                    							<li class="social-icon linkedin"><a class="bk-tipper-bottom" data-title="Linkedin" href="<?php echo esc_url($gloria_option['bk-social-header']['linkedin']); ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                    						<?php } ?>
                    						
                    						<?php if ($gloria_option['bk-social-header']['pinterest']){ ?>
                    							<li class="social-icon pinterest"><a class="bk-tipper-bottom" data-title="Pinterest" href="<?php echo esc_url($gloria_option['bk-social-header']['pinterest']); ?>" target="_blank"><i class="fa fa-pinterest"></i></a></li>
                    						<?php } ?>
                    						
                    						<?php if ($gloria_option['bk-social-header']['instagram']){ ?>
                    							<li class="social-icon instagram"><a class="bk-tipper-bottom" data-title="Instagram" href="<?php echo esc_url($gloria_option['bk-social-header']['instagram']); ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
                    						<?php } ?>
                    						
                    						<?php if ($gloria_option['bk-social-header']['dribbble']){ ?>
                    							<li class="social-icon dribbble"><a class="bk-tipper-bottom" data-title="Dribbble" href="<?php echo esc_url($gloria_option['bk-social-header']['dribbble']); ?>" target="_blank"><i class="fa fa-dribbble"></i></a></li>
                    						<?php } ?>
                    						
                    						<?php if ($gloria_option['bk-social-header']['youtube']){ ?>
                    							<li class="social-icon youtube"><a class="bk-tipper-bottom" data-title="Youtube" href="<?php echo esc_url($gloria_option['bk-social-header']['youtube']); ?>" target="_blank"><i class="fa fa-youtube"></i></a></li>
                    						<?php } ?>      							
                    						                                    
                                            <?php if ($gloria_option['bk-social-header']['vimeo']){ ?>
                    							<li class="social-icon vimeo"><a class="bk-tipper-bottom" data-title="Vimeo" href="<?php echo esc_url($gloria_option['bk-social-header']['vimeo']); ?>" target="_blank"><i class="fa fa-vimeo-square"></i></a></li>
                    						<?php } ?>
                                            
                                            <?php if ($gloria_option['bk-social-header']['vk']){ ?>
                    							<li class="social-icon vk"><a class="bk-tipper-bottom" data-title="VKontakte" href="<?php echo esc_url($gloria_option['bk-social-header']['vk']); ?>" target="_blank"><i class="fa fa-vk"></i></a></li>
                    						<?php } ?>
                                            
                                            <?php if ($gloria_option['bk-social-header']['rss']){ ?>
                    							<li class="social-icon rss"><a class="bk-tipper-bottom" data-title="Rss" href="<?php echo esc_url($gloria_option['bk-social-header']['rss']); ?>" target="_blank"><i class="fa fa-rss"></i></a></li>
                    						<?php } ?>                    						
                    					</ul>
                    				</div>
                    
                                    <?php }?>  
                                </div><!--top-nav-->
                            <?php }?>
                        </div>
                    </div><!--top-bar-->
        
                    <div class="header container">
                        <div class="row">
                            <div class="col-md-12">
                    			<div class="header-inner <?php if($gloria_option['bk-header-logo-position'] == 'center') {echo 'header-center';}?>">
     			                   <!-- logo open -->
                                        <?php if (($logo != null) && (array_key_exists('url',$logo))) {
                                                if ($logo['url'] != '') {
                                            ?>
                            			<div class="logo">
                                            <h1>
                                                <a href="<?php echo esc_url(get_home_url('/'));?>">
                                                    <img src="<?php echo esc_url($logo['url']);?>" alt="logo"/>
                                                </a>
                                            </h1>
                            			</div>
                            			<!-- logo close -->
                                        <?php } else {?> 
                                        <div class="logo logo-text">
                                            <h1>
                                                <a href="<?php echo esc_url(get_home_url('/'));?>">
                                                    <?php echo esc_attr(bloginfo( 'name' ));?>
                                                </a>
                                            </h1>
                            			</div>
                                        <?php }
                                        } else {?> 
                                        <div class="logo logo-text">
                                            <h1>
                                                <a href="<?php echo esc_url(get_home_url('/'));?>">
                                                    <?php echo esc_attr(bloginfo( 'name' ));?>
                                                </a>
                                            </h1>
                            			</div>
                                        <?php } ?>
                                        <?php if ( $header_banner ) : ?>
                                            <!-- header-banner open -->                             
                                			<div class="header-banner">
                                            <?php
                                                if (($ga_script != '') && ($ga_script != 'Put your google adsense code here')){
                                                    echo ($ga_script);
                                                } else { ?>
                                                    <a class="ads-banner-link" target="_blank" href="<?php echo esc_attr( $linkurl ); ?>">
                                    				    <img class="ads-banner" src="<?php echo esc_attr( $imgurl ); ?>" alt="Header Banner"/>
                                                    </a>
                                                <?php }
                                            ?> 
                                			</div>                            
                                			<!-- header-banner close -->
                                        <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- nav open -->
            		<nav class="main-nav">
                        <div class="main-nav-inner bkwrapper container">
                            <div class="main-nav-container clearfix">
                                    <div class="main-nav-wrap">
                                        <div class="mobile-menu-wrap">
                                            <h3 class="menu-title">
                                                <?php echo esc_attr(bloginfo( 'name' ));?>
                                            </h3>
                                            <a class="mobile-nav-btn" id="nav-open-btn"><i class="fa fa-bars"></i></a>  
                                        </div>
                                        
                                        <?php if ( has_nav_menu( 'main-menu' ) ) { 
                                            wp_nav_menu( array( 
                                                'theme_location' => 'main-menu',
                                                'container_id' => 'main-menu',
                                                'walker' => new BK_Walker,
                                                'depth' => '5' ) );}?>
                                    </div>
                                    <?php 
                                        echo gloria_ajax_form_search();
                                    ?> 
                            </div>    
                        </div><!-- main-nav-inner -->       
            		</nav>
                    <!-- nav close --> 
        		</div>                
                <!-- ticker open -->
                <?php        
                    if (isset($gloria_option)){
                        if ($gloria_option['bk-header-ticker'] == 1) {?>
                            <div class="bk-ticker-module">
                            <?php gloria_core::bk_get_ticker('header');?>
                            </div><!--end ticker-module-->
                        <?php }
                    }
                ?>
                <!-- ticker close -->
            </div>                
            
            <!-- backtop open -->
    		<?php if ($backtop) { ?>
                <div id="back-top"><i class="fa fa-long-arrow-up"></i></div>
            <?php } ?>
    		<!-- backtop close -->