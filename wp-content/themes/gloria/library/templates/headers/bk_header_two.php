<?php
    $gloria_option = gloria_core::bk_get_global_var('gloria_option');
    $logo = array();
    if ($gloria_option != 'null'){ 
        if ((isset($gloria_option['bk-logo'])) && (($gloria_option['bk-logo']) != NULL)){ 
            $logo = $gloria_option['bk-logo'];
        };
        
        if(isset($gloria_option['bk-backtop-switch'])){$backtop = $gloria_option['bk-backtop-switch'];}else {$backtop = 1;};
    
        if(isset($gloria_option['bk-site-layout'])){$bkSiteLayout = $gloria_option['bk-site-layout'];}else {$bkSiteLayout = 'wide';};

    }
    $schema_org = '';
    if (is_single()) {
    	$schema_org .= '';
    } else {
    	$schema_org .= ' itemscope itemtype="http://schema.org/WebPage"';
    }
    $hs_feat = array();
    $hs_cat = array();
    
    $hs_num = $gloria_option['bk-headerslider-number'];
        if ( $hs_num == '' ) { $hs_num = 5;}                
        
        $featured_enable = $gloria_option['bk-headerslider-featured'];
        if ($featured_enable == 1) {
            $feat_ops =  $gloria_option['bk-headerslider-featured-option'];
            if ($feat_ops == 'Sticky') {
                $args = array(
        			'post__in'  => get_option( 'sticky_posts' ),
        			'post_status' => 'publish',
        			'ignore_sticky_posts' => 1,
        			'posts_per_page' => $hs_num,
                );
            }else {
                $feat_tag = $gloria_option['headerslider-featured-tags'];
                 if (sizeof($feat_tag)) { 
                    $args = array(
            			'tag__in' => $feat_tag,
            			'post_status' => 'publish',
            			'ignore_sticky_posts' => 1,
            			'posts_per_page' => $hs_num,
                        );   
                }
            }
        }else {
            if(isset($gloria_option['headerslider-category'])) {
                $hs_cat = $gloria_option['headerslider-category'];
            }else {
                $hs_cat = '';
            }
                if (sizeof($hs_cat)) {
                $args = array(
                    'category__in'  => $hs_cat,
                    'post_status' => 'publish',
                    'ignore_sticky_posts' => 1,
                    'posts_per_page' => $hs_num,
                );
            }
        }
        $bk_contentin4 = new gloria_contentin4;
        $hs_query = new WP_Query( $args );
        $uid = uniqid('', true);
        $header_meta_ar = array('author', 'date');
        $post_args = array (
            'thumbnail_size'    => 'full',
            'meta_ar'           => false,
            'cat_meta'            => false,
            'except_length'     => 30,
            'rm_btn'            => true,
        );         
        $hs_str = '';
        $hs_str .= '<div class="bk-header-slider">';
        $hs_str .= '<div class="flexslider">';
        $hs_str .= '<ul class="col-md-12 slides">';
        while ( $hs_query->have_posts() ): $hs_query->the_post();
            $hs_str .= '<li class="item content_in">';
            $hs_str .= $bk_contentin4->render($post_args);
            $hs_str .= '</li>';
        endwhile;
        $hs_str .= '</ul></div> <!-- Close render modules -->';
        $hs_str .= '<div class="bk-scrolldown-button">';
        $hs_str .= '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="95px" height="80px" viewBox="21 0 49 77" xml:space="preserve"><polyline fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" points="89.375,0.375 45.63,51.087 1.675,0"></polyline></svg>';
        $hs_str .= '</div>';
        $hs_str .= '</div>';
        wp_reset_postdata();
    ?>
    <head>
    	<meta charset="<?php bloginfo('charset'); ?>" />
        
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        
        <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
    	
    	<?php if (is_search()) { ?>
    	   <meta name="robots" content="noindex, nofollow" /> 
    	<?php } ?>
    	
    	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    
    	<?php wp_head(); ?>
        <link rel="stylesheet" type="text/css" href="<?=get_theme_root_uri()?>/gloria/css/style-dev.css">
        <link rel="stylesheet" type="text/css" href="<?=get_theme_root_uri()?>/gloria/css/responsive-dev.css">
    </head>
    
    <body <?php body_class(); echo (esc_attr($schema_org)); ?>>
        <?php if((is_front_page())) {?>
            <div class="bk-header-loader loading"><div class="bk-header-preload"></div></div>
        <?php }?>
        <div id="page-wrap" class= '<?php if($bkSiteLayout !== 'boxed') echo "wide";?>'>
        <div id="main-mobile-menu">
            <div class="block">
                <div id="mobile-inner-header">
                    <h3 class="menu-title">
                        <?php echo esc_attr(bloginfo( 'name' ));?>
                    </h3>
                    <a class="mobile-menu-close" href="#" title="Close"><i class="fa fa-long-arrow-left"></i></a>
                </div>
                <div class="main-menu">
                    <h3 class="menu-location-title">
                        <?php esc_html_e('Main Menu', 'gloria');?>
                    </h3>
                    <?php
                    wp_nav_menu( array( 
                        'theme_location' => 'main-menu',
                        'depth' => '3',
                        'container_id' => 'mobile-menu' ) );
                    ?>
                </div>
            </div>
        </div>
        <div id="page-inner-wrap">
            <div class="page-cover mobile-menu-close"></div>
            <div class="bk-page-header">
                <div class="header-wrap header bk-header-90 bk-header-90-sport">
                    <!-- nav open -->
                    <?php if((is_front_page())) {echo ($hs_str);}?>
                    <div class="bk-destination-point"></div>
            		<nav class="main-nav">
                        <div class="main-nav-inner bkwrapper container">
                            <div class="main-nav-container clearfix">
                                <div class="main-nav-wrap">
                                    <div class="mobile-menu-wrap">
                                        <a class="mobile-nav-btn" id="nav-open-btn"><i class="fa fa-bars"></i></a>  
                                    </div>
                                    <!-- logo open -->
                                    <?php if (($logo != null) && (array_key_exists('url',$logo))) {
                                            if ($logo['url'] != '') {
                                        ?>
                        			<div class="logo">
                                        <h1>
                                            <a href="<?php echo esc_url(get_home_url('/'));?>">
                                                <img src="<?php echo esc_url($logo['url']);?>" alt="logo"/>
                                            </a>
                                        </h1>
                        			</div>
                        			<!-- logo close -->
                                    <?php } else {?> 
                                    <div class="logo logo-text">
                                        <h1>
                                            <a href="<?php echo esc_url(get_home_url('/'));?>">
                                                <?php echo esc_attr(bloginfo( 'name' ));?>
                                            </a>
                                        </h1>
                        			</div>
                                    <?php }
                                    } else {?> 
                                    <div class="logo logo-text">
                                        <h1>
                                            <a href="<?php echo esc_url(get_home_url('/'));?>">
                                                <?php echo esc_attr(bloginfo( 'name' ));?>
                                            </a>
                                        </h1>
                        			</div>
                                    <?php } ?>
                                    
                                    <?php if ( has_nav_menu( 'main-menu' ) ) { 
                                        wp_nav_menu( array( 
                                            'theme_location' => 'main-menu',
                                            'container_id' => 'main-menu',
                                            'walker' => new BK_Walker,
                                            'depth' => '5' ) );}?>
                                    <?php 
                                        echo gloria_ajax_form_search();
                                    ?> 
                                    <?php if ( isset($gloria_option ['bk-ajaxlogin-switch']) && ($gloria_option ['bk-ajaxlogin-switch'] == 1) ){ ?>
                                        <?php 
                                            if ( function_exists('login_with_ajax') ) {  
                                                $bk_home_url = esc_url(get_home_url('/'));
                                                $ajaxArgs = array(
                                                    'profile_link' => true,
                                                    'template' => 'modal',
                                                    'registration' => true,
                                                    'remember' => true,
                                                    'redirect'  => $bk_home_url
                                                );
                                                login_with_ajax($ajaxArgs);  
                                        }?>
                                    <?php }?>
                                </div>
                            </div>    
                        </div><!-- main-nav-inner -->       
            		</nav>
                    <!-- nav close --> 
        		</div>                
                <!-- ticker open -->
                <?php        
                    if (isset($gloria_option)){
                        if ($gloria_option['bk-header-ticker'] == 1) {?>
                            <div class="bk-ticker-module bk-white-bg">
                            <?php gloria_core::bk_get_ticker('header');?>
                            </div><!--end ticker-module-->
                        <?php }
                    }
                ?>
                <!-- ticker close -->
            </div>                
            
            <!-- backtop open -->
    		<?php if ($backtop) { ?>
                <div id="back-top"><i class="fa fa-long-arrow-up"></i></div>
            <?php } ?>
    		<!-- backtop close -->