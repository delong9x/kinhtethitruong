<?php

/**
	ReduxFramework Config File
	For full documentation, please visit: https://github.com/ReduxFramework/ReduxFramework/wiki
**/

if ( !class_exists( "ReduxFramework" ) ) {
	return;
} 

if ( !class_exists( "Redux_Framework_config" ) ) {
	class Redux_Framework_config {

		public $args = array();
		public $sections = array();
		public $theme;
		public $ReduxFramework;

		public function __construct( ) {

			// Just for demo purposes. Not needed per say.
			$this->theme = wp_get_theme();

			// Set the default arguments
			$this->setArguments();
			
			// Set a few help tabs so you can see how it's done
			$this->setHelpTabs();

			// Create the sections and fields
			$this->setSections();
			
			if ( !isset( $this->args['opt_name'] ) ) { // No errors please
				return;
			}
			
			$this->ReduxFramework = new ReduxFramework($this->sections, $this->args);
			

			// If Redux is running as a plugin, this will remove the demo notice and links
			//add_action( 'redux/plugin/hooks', array( $this, 'remove_demo' ) );
			
			// Function to test the compiler hook and demo CSS output.
			//add_filter('redux/options/'.$this->args['opt_name'].'/compiler', array( $this, 'compiler_action' ), 10, 2); 
			// Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.

			// Change the arguments after they've been declared, but before the panel is created
			//add_filter('redux/options/'.$this->args['opt_name'].'/args', array( $this, 'change_arguments' ) );
			
			// Change the default value of a field after it's been set, but before it's been used
			//add_filter('redux/options/'.$this->args['opt_name'].'/defaults', array( $this,'change_defaults' ) );

			// Dynamically add a section. Can be also used to modify sections/fields
			add_filter('redux/options/'.$this->args['opt_name'].'/sections', array( $this, 'dynamic_section' ) );

		}


		/**

			This is a test function that will let you see when the compiler hook occurs. 
			It only runs if a field	set with compiler=>true is changed.

		**/

		function compiler_action($options, $css) {
		}



		/**
		 
		 	Custom function for filtering the sections array. Good for child themes to override or add to the sections.
		 	Simply include this function in the child themes functions.php file.
		 
		 	NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
		 	so you must use get_template_directory_uri() if you want to use any of the built in icons
		 
		 **/

		function dynamic_section($sections){
		    /*//$sections = array();
		    $sections[] = array(
		        'title' => esc_html__('Section via hook', 'redux-framework-demo'),
		        'desc' => esc_html__('<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'redux-framework-demo'),
				'icon' => 'el-icon-paper-clip',
				    // Leave this as a blank section, no options just some intro text set above.
		        'fields' => array()
		    );*/

		    return $sections;
		}
		
		
		/**

			Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.

		**/
		
		function change_arguments($args){
		    //$args['dev_mode'] = true;
		    
		    return $args;
		}
			
		
		/**

			Filter hook for filtering the default value of any given field. Very useful in development mode.

		**/

		function change_defaults($defaults){
		    $defaults['str_replace'] = "Testing filter hook!";
		    
		    return $defaults;
		}


		// Remove the demo link and the notice of integrated demo from the redux-framework plugin
		function remove_demo() {
			
			// Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
			if ( class_exists('ReduxFrameworkPlugin') ) {
				remove_filter( 'plugin_row_meta', array( ReduxFrameworkPlugin::get_instance(), 'plugin_meta_demo_mode_link'), null, 2 );
			}

			// Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
			remove_action('admin_notices', array( ReduxFrameworkPlugin::get_instance(), 'admin_notices' ) );	

		}


		public function setSections() {

			/**
			 	Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
			 **/


			// Background Patterns Reader
			$sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
			$sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
			$sample_patterns      = array();

			if ( is_dir( $sample_patterns_path ) ) :
				
			  if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) :
			  	$sample_patterns = array();

			    while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

			      if( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
			      	$name = explode(".", $sample_patterns_file);
			      	$name = str_replace('.'.end($name), '', $sample_patterns_file);
			      	$sample_patterns[] = array( 'alt'=>$name,'img' => $sample_patterns_url . $sample_patterns_file );
			      }
			    }
			  endif;
			endif;

			ob_start();

			$ct = wp_get_theme();
			$this->theme = $ct;
			$item_name = $this->theme->get('Name'); 
			$tags = $this->theme->Tags;
			$screenshot = $this->theme->get_screenshot();
			$class = $screenshot ? 'has-screenshot' : '';

			$customize_title = sprintf( esc_html__( 'Customize &#8220;%s&#8221;','gloria' ), $this->theme->display('Name') );

			?>
			<div id="current-theme" class="<?php echo esc_attr( $class ); ?>">
				<?php if ( $screenshot ) : ?>
					<?php if ( current_user_can( 'edit_theme_options' ) ) : ?>
					<a href="<?php echo esc_url(wp_customize_url()); ?>" class="load-customize hide-if-no-customize" title="<?php echo esc_attr( $customize_title ); ?>">
						<img src="<?php echo esc_url(esc_url( $screenshot )); ?>" alt="<?php esc_attr_e( 'Current theme preview', 'gloria' ); ?>" />
					</a>
					<?php endif; ?>
					<img class="hide-if-customize" src="<?php echo esc_url( $screenshot ); ?>" alt="<?php esc_attr_e( 'Current theme preview', 'gloria' ); ?>" />
				<?php endif; ?>

				<h4>
					<?php echo ($this->theme->display('Name')); ?>
				</h4>

				<div>
					<ul class="theme-info">
						<li><?php printf( esc_html__('By %s','gloria'), $this->theme->display('Author') ); ?></li>
						<li><?php printf( esc_html__('Version %s','gloria'), $this->theme->display('Version') ); ?></li>
						<li><?php echo '<strong>'.esc_html__('Tags', 'gloria').':</strong> '; ?><?php printf( $this->theme->display('Tags') ); ?></li>
					</ul>
					<p class="theme-description"><?php echo ($this->theme->display('Description')); ?></p>
					<?php if ( $this->theme->parent() ) {
						printf( ' <p class="howto">' . esc_html__( 'This <a href="%1$s">child theme</a> requires its parent theme, %2$s.', 'gloria' ) . '</p>',
							esc_html__( 'http://codex.wordpress.org/Child_Themes','gloria' ),
							$this->theme->parent()->display( 'Name' ) );
					} ?>
					
				</div>

			</div>

			<?php
			$item_info = ob_get_contents();
			    
			ob_end_clean();

			$sampleHTML = '';
			
			// ACTUAL DECLARATION OF SECTIONS
            
                $this->sections[] = array(
    				'icon' => 'el-icon-wrench',
    				'title' => esc_html__('General Settings', 'gloria'),
    				'fields' => array(
    					array(
    						'id'=>'bk-primary-color',
    						'type' => 'color',
    						'title' => esc_html__('Primary color', 'gloria'), 
    						'subtitle' => esc_html__('Pick a primary color for the theme.', 'gloria'),
    						'default' => '#d13030',
    						'validate' => 'color',
						),
                        array(
    						'id'=>'bk-secondary-color',
    						'type' => 'color',
    						'title' => esc_html__('Secondary color', 'gloria'), 
    						'subtitle' => esc_html__('Pick a Secondary color for the theme.', 'gloria'),
    						'default' => '#333946',
    						'validate' => 'color',
						),
                        array(
    						'id'=>'bk-retina-display',
    						'type' => 'switch', 
    						'title' => esc_html__('Retina Display', 'gloria'),
    						'subtitle' => esc_html__('Choose to enable/disable Retina Display for your page', 'gloria'),
                            'default' => 1,
    						'on' => esc_html__('Enabled', 'gloria'),
    						'off' => esc_html__('Disabled', 'gloria'),
						),
                        array(
    						'id'=>'bk-smooth-scroll',
    						'type' => 'switch', 
    						'title' => esc_html__('Smooth Scroll Enable', ' gloria'),
    						'subtitle' => esc_html__('Choose to enable/disable Smooth Scroll Script', ' gloria'),
                            'default' => 0,
    						'on' => esc_html__('Enabled', ' gloria'),
    						'off' => esc_html__('Disabled', ' gloria'),
						), 
    				)
    			);
                $this->sections[] = array(
    				'icon' => 'el-icon-tasks',
    				'title' => esc_html__('Site Layout', 'gloria'),
    				'fields' => array(
                        array(
    						'id'=>'bk-site-layout',
    						'type' => 'button_set',
    						'title' => esc_html__('Site layout', 'gloria'),
    						'subtitle'=> esc_html__('Choose between wide and boxed layout', 'gloria'),
    						'options' => array('wide' => esc_html__('Wide', 'gloria'),'boxed' => esc_html__('Boxed', 'gloria')),
    						'default' => 'wide'
						),
                        array(
    						'id'=>'bk-body-bg',
    						'type' => 'background',
                            'required' => array('bk-site-layout','=','boxed'),
    						'output' => array('body'),
    						'title' => esc_html__('Site background', 'gloria'), 
    						'subtitle' => esc_html__('Choose background image or background color for boxed layout', 'gloria'),
						),
                        array(
    						'id'=>'bk-rtl-sw',
    						'type' => 'switch', 
    						'title' => esc_html__('RTL Layout', 'gloria'),
    						'subtitle' => esc_html__('Choose to enable/disable RTL Layout for your page', 'gloria'),
                            'default' => 0,
    						'on' => esc_html__('Enabled', 'gloria'),
    						'off' => esc_html__('Disabled', 'gloria'),
						),
                        array(
    						'id'=>'bk-sb-location-sw',
    						'type' => 'select', 
    						'title' => esc_html__('Sidebar Location', 'gloria'),
    						'subtitle' => esc_html__('Choose to display sidebar in the left or right the content section', 'gloria'),
                            'options' => array('left' => esc_html__('Left', 'gloria'),'right'=>esc_html__('Right', 'gloria')),
    						"default" => 'right',
						),
                        array(
    						'id'=>'bk-sb-responsive-sw',
    						'type' => 'switch', 
    						'title' => esc_html__('Enable sidebar in responsive layout (For small device width < 991px)', 'gloria'),
    						'subtitle' => esc_html__('Choose to display or hide sidebar in responsive layout', 'gloria'),
    						"default" => 1,
    						'on' => esc_html__('Enabled', 'gloria'),
    						'off' => esc_html__('Disabled', 'gloria'),
						),
                        array(
            				'id'=>'bk-main-nav-layout',
            				'type' => 'select',
                            'title' => esc_html__('Main nav alignment', 'gloria'), 
                            'subtitle' => esc_html__('Choose between left and center aligned for the main navigation', 'gloria'),
    						'options' => array('left' => esc_html__('Left', 'gloria'),'center'=>esc_html__('Center', 'gloria')),
    						'default' => 'left',
        				),
    				)
    			);
                $this->sections[] = array(
    				'icon' => 'el-icon-credit-card',
    				'title' => esc_html__('Header Settings', 'gloria'),
    				'fields' => array(
                        array(
    						'id'=>'bk-logo',
    						'type' => 'media', 
    						'url'=> true,
    						'title' => esc_html__('Site logo', 'gloria'),
    						'subtitle' => esc_html__('Upload logo of your site that is displayed in header', 'gloria'),
                            'placeholder' => esc_html__('No media selected','gloria')
						),
                        
                        array(
    						'id'=>'bk-header-type',
    						'type' => 'image_select', 
    						'title' => esc_html__('Header Type', 'gloria'),
                            'options' => array(
                                                'header-1' => array(
                                                    'alt' => 'Header 1',
                                                    'img' => get_template_directory_uri() . '/images/headers/Header-1.png',
                                                    'title' => esc_html__('Default Header', 'gloria')
                                                ),
                                                'header-2' => array(
                                                    'alt' => 'Header 2',
                                                    'img' => get_template_directory_uri() . '/images/headers/Header-2.png',
                                                    'title' => esc_html__('Sport - Header 1', 'gloria')
                                                ),
                                                'header-3' => array(
                                                    'alt' => 'Header 3',
                                                    'img' => get_template_directory_uri() . '/images/headers/Header-3.png',
                                                    'title' => esc_html__('Sport - Header 2', 'gloria')
                                                ),
                                                'header-4' => array(
                                                    'alt' => 'Header 4',
                                                    'img' => get_template_directory_uri() . '/images/headers/Header-4.png',
                                                    'title' => esc_html__('Art', 'gloria')
                                                ),
                                                'header-5' => array(
                                                    'alt' => 'Header 5',
                                                    'img' => get_template_directory_uri() . '/images/headers/Header-5.png',
                                                    'title' => esc_html__('Technology - Header 1', 'gloria')
                                                ),
                                                'header-6' => array(
                                                    'alt' => 'Header 6',
                                                    'img' => get_template_directory_uri() . '/images/headers/Header-6.png',
                                                    'title' => esc_html__('Technology - Header 2', 'gloria')
                                                ),
                                        ),
                            'default' => 'header-1',
						),            
                        array(
    						'id'=>'bk-ajaxlogin-switch',
    						'type' => 'switch',                          
    						'title' => esc_html__('Enable header Ajax login', 'gloria'),
    						'subtitle' => esc_html__('You must install Login With Ajax plugin to have this function', 'gloria'),
    						'default' => 1,
    						'on' => esc_html__('Enabled', 'gloria'),
    						'off' => esc_html__('Disabled', 'gloria'),
						),                    
                        array(
                            'id' => 'section-headerslider-start',
                            'title' => esc_html__('Header Slider Setting', 'gloria'),
                            'subtitle' => '',
                            'required' => array('bk-header-type','=',array( 'header-3', 'header-4', 'header-2' )),
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),  
                        array(
    						'id'=>'bk-headerslider-number',
    						'type' => 'text',
    						'title' => esc_html__('Post Number', 'gloria'),
    						'subtitle' => esc_html__('Set up number of posts to be shown up on the Header Slider', 'gloria'),
    						'default' => '5',
          
						),
                        array(
    						'id'=>'bk-headerslider-featured',
    						'type' => 'switch', 
    						'title' => esc_html__('Featured Posts', 'gloria'),
    						'subtitle' => esc_html__('Display feature posts', 'gloria'),
    						"default" => 0,
    						'on' => esc_html__('Enabled', 'gloria'),
    						'off' => esc_html__('Disabled', 'gloria'),
						),
                        array(
    						'id'=>'bk-headerslider-featured-option',
    						'type' => 'select', 
                            'required' => array('bk-headerslider-featured','=','1'),
    						'title' => esc_html__('Featured Options', 'gloria'),
    						'subtitle' => esc_html__('Display featured posts as sticky post or base on Tag name', 'gloria'),
    						'options' => array('Sticky' => esc_html__('Sticky', 'gloria'), 'Tags'=>esc_html__('Tag name', 'gloria')), 
                            'default' => 'Sticky',
						),
                        array(
                            'id' => 'headerslider-featured-tags',  
                            'type' => 'select',
                            'required' => array('bk-headerslider-featured-option','=','Tags'),
                            'data' => 'tags',
                            'multi' => true,
                            'title' => esc_html__('Featured Tags', 'gloria'),
                            'subtitle' => esc_html__('Set up ticker display featured base on tag name', 'gloria'),
                            'desc' => esc_html__('', 'gloria'),
                        ),
                        array(
                            'id' => 'headerslider-category',  
                            'type' => 'select',
                            'data' => 'categories',
                            'multi' => true,
                            'title' => esc_html__('Categories', 'gloria'),
                            'subtitle' => esc_html__('Set up category', 'gloria'),
                            'desc' => esc_html__('', 'gloria'),
                        ),
                        array(
                            'id' => 'section-headerslider-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ),    
                        array(
    						'id'=>'bk-header-bg',
    						'type' => 'background',
                            'required' => array('bk-header-type','=','header-1'),
    						'output' => array('.header-wrap'),
    						'title' => esc_html__('Header background', 'gloria'), 
    						'subtitle' => esc_html__('Choose background image or background color for site header', 'gloria'),
						),                       
                        array(
                            'id' => 'section-topbar-start',
                            'title' => esc_html__('Top bar Setting', 'gloria'),
                            'subtitle' => '',
                            'required' => array('bk-header-type','=',array( 'header-1', 'header-2', 'header-5' )),
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ), 
                        array(
    						'id'=>'bk-header-top-switch',
    						'type' => 'switch', 
    						'title' => esc_html__('Enable header top bar', 'gloria'),
    						'subtitle' => esc_html__('', 'gloria'),
						    'default' => 1,
    						'on' => esc_html__('Enabled', 'gloria'),
    						'off' => esc_html__('Disabled', 'gloria'),
						),
                        array(
    						'id'=>'bk-cart-icon-switch',
    						'type' => 'switch', 
                            'required' => array('bk-header-top-switch','=','1'),
    						'title' => esc_html__('Enable header cart icon', 'gloria'),
    						'subtitle' => esc_html__('You must install Woocommerce plugin to have this function', 'gloria'),
    						'default' => 1,
    						'on' => esc_html__('Enabled', 'gloria'),
    						'off' => esc_html__('Disabled', 'gloria'),
						),
                        array(
    						'id'=>'bk-social-header-switch',
    						'type' => 'switch', 
                            'required' => array('bk-header-top-switch','=','1'),
    						'title' => esc_html__('Enable social header ', 'gloria'),
    						'subtitle' => esc_html__('Enable social header by display icons', 'gloria'),
    						'default' => 0,
    						'on' => esc_html__('Enabled', 'gloria'),
    						'off' => esc_html__('Disabled', 'gloria'),
						),
                        array(
    						'id'=>'bk-social-header',
    						'type' => 'text',
                            'required' => array('bk-social-header-switch','=','1'),
    						'title' => esc_html__('Social media', 'gloria'),
    						'subtitle' => esc_html__('Set up social links for site', 'gloria'),
    						'options' => array('fb'=>'Facebook Url', 'twitter'=>'Twitter Url', 'gplus'=>'GPlus Url', 'linkedin'=>'Linkedin Url',
                                               'pinterest'=>'Pinterest Url', 'instagram'=>'Instagram Url', 'dribbble'=>'Dribbble Url', 
                                               'youtube'=>'Youtube Url', 'vimeo'=>'Vimeo Url', 'vk'=>'VK Url', 'rss'=>'RSS Url'),
    						'default' => array('fb'=>'', 'twitter'=>'', 'gplus'=>'', 'linkedin'=>'', 'pinterest'=>'', 'instagram'=>'', 'dribbble'=>'', 
                                                'youtube'=>'', 'vimeo'=>'', 'vk'=>'', 'rss'=>'')
						),
                        array(
                            'id' => 'section-topbar-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ), 
                        array(
    						'id'=>'bk-header-logo-position',
    						'type' => 'select',
                            'required' => array('bk-header-type','=','header-1'), 
    						'url'=> true,
    						'title' => esc_html__('Logo position', 'gloria'),
    						'subtitle' => esc_html__('Choose logo position between left or center', 'gloria'),
                            'options' => array('left'=>'Left', 'center'=>'Center'),
                            'default' => 'left',
						),
                        array(
    						'id'=>'bk-header-banner-switch',
    						'type' => 'switch', 
                            'required' => array('bk-header-type','=','header-1'),
    						'title' => esc_html__('Enable header banner', 'gloria'),
    						'subtitle' => esc_html__('Enable banner in header', 'gloria'),
    						"default" => 0,
    						'on' => esc_html__('Enabled', 'gloria'),
    						'off' => esc_html__('Disabled', 'gloria'),
						),
                        array(
    						'id'=>'bk-header-banner',
    						'type' => 'text',
                            'required' => array('bk-header-banner-switch','=','1'),
    						'title' => esc_html__('Header banner', 'gloria'),
    						'subtitle' => esc_html__('Set up banner displays in header', 'gloria'),
    						'options' => array('imgurl'=> esc_html__('Image URL', 'gloria'), 'linkurl'=> esc_html__('Link URL', 'gloria')),
    						'default' => array('imgurl'=>'http://', 'linkurl'=>'http://')
						),
                        array(
                            'id'=>'bk-banner-script',
                            'type' => 'textarea',
                            'title' => esc_html__('Google Adsense Code', 'gloria'),
                            'required' => array('bk-header-banner-switch','=','1'),
                            'default' => '',
                        ),
                        array(
    						'id'=>'bk-fixed-nav-switch',
    						'type' => 'button_set', 
    						'title' => esc_html__('Enable fixed header menu', 'gloria'),
    						'subtitle'=> esc_html__('Choose between fixed and static header navigation', 'gloria'),
                            'options' => array('1' => esc_html__('Static', 'gloria'),'2' => esc_html__('Fixed', 'gloria')),
    						'default' => '1',
						),          
                        array(
                            'id' => 'section-ticker-header-start',
                            'title' => esc_html__('Ticker Header Setting', 'gloria'),
                            'subtitle' => '',
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),                       
                        array(
    						'id'=>'bk-header-ticker',
    						'type' => 'switch', 
    						'title' => esc_html__('Enable ticker', 'gloria'),
    						'subtitle' => esc_html__('Enable ticker in header', 'gloria'),
    						"default" => 0,
    						'on' => esc_html__('Enabled', 'gloria'),
    						'off' => esc_html__('Disabled', 'gloria'),
						),
                        array(
    						'id'=>'bk-ticker-title',
    						'type' => 'text',
                            'required' => array('bk-header-ticker','=','1'),
    						'title' => esc_html__('Ticker title', 'gloria'),
    						'subtitle' => '',
    						'default' => 'Breaking News',
          
						),
                        array(
    						'id'=>'bk-ticker-number',
    						'type' => 'text',
                            'required' => array('bk-header-ticker','=','1'),
    						'title' => esc_html__('Ticker post number', 'gloria'),
    						'subtitle' => esc_html__('Set up number of posts to be shown up', 'gloria'),
    						'default' => '5',
          
						),
                        array(
    						'id'=>'bk-ticker-featured',
    						'type' => 'switch', 
                            'required' => array('bk-header-ticker','=','1'),
    						'title' => esc_html__('Enable featured ticker', 'gloria'),
    						'subtitle' => esc_html__('Display featured post', 'gloria'),
    						"default" => 0,
    						'on' => esc_html__('Enabled', 'gloria'),
    						'off' => esc_html__('Disabled', 'gloria'),
						),
                        array(
    						'id'=>'bk-ticker-featured-option',
    						'type' => 'select', 
                            'required' => array('bk-ticker-featured','=','1'),
    						'title' => esc_html__('Featured Options', 'gloria'),
    						'subtitle' => esc_html__('Display featured post as sticky post or base on tags name', 'gloria'),
    						'options' => array('Sticky' => esc_html__('Sticky', 'gloria'), 'Tags'=>esc_html__('Tags name', 'gloria')), 
                            'default' => 'Sticky',
						),
                        array(
                            'id' => 'ticker-featured-tags',  
                            'type' => 'select',
                            'required' => array('bk-ticker-featured-option','=','Tags'),
                            'data' => 'tags',
                            'multi' => true,
                            'title' => esc_html__('Featured tags', 'gloria'),
                            'subtitle' => esc_html__('Set up ticker display featured base on tags name', 'gloria'),
                            'desc' => esc_html__('', 'gloria'),
                        ),
                        array(
                            'id' => 'ticker-category',  
                            'type' => 'select',
                            'required' => array('bk-ticker-featured','=','0'),
                            'data' => 'categories',
                            'multi' => true,
                            'title' => esc_html__('Categories', 'gloria'),
                            'subtitle' => esc_html__('Set up category', 'gloria'),
                            'desc' => esc_html__('', 'gloria'),
                        ),
                         array(
                            'id' => 'section-ticker-header-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        )          
    				)
    			);
                $this->sections[] = array(
    				'icon' => 'el-icon-credit-card',
    				'title' => esc_html__('Footer Settings', 'gloria'),
    				'fields' => array(    
                        array(
    						'id'=>'bk-footer-instagram',
    						'type' => 'switch',
    						'title' => esc_html__('Footer Instagram', 'gloria'),
    						'default' 		=> 0,
    						'on' => esc_html__('Enabled', 'gloria'),
    						'off' => esc_html__('Disabled', 'gloria'),
						),
                        array(
                            'id' => 'section-instagram-header-start',
                            'title' => esc_html__('Footer Instagram Setting', 'gloria'),
                            'subtitle' => '',
                            'required' => array('bk-footer-instagram','=','1'),
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),
                        array(
    						'id'=>'bk-footer-instagram-title',
    						'title' => esc_html__('Instagram Section Title', 'gloria'),
                            'type' => 'text',                            
						),
                        array(
    						'id'=>'bk-footer-instagram-username',
    						'title' => esc_html__('Instagram Username', 'gloria'),
                            'type' => 'text',                            
						),
                        array(
                            'id' => 'section-instagram-header-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ),
                        array(
    						'id'=>'bk-backtop-switch',
    						'type' => 'switch', 
    						'title' => esc_html__('Scroll top button', 'gloria'),
    						'subtitle'=> esc_html__('Show scroll to top button in right lower corner of window', 'gloria'),
    						'default' 		=> 1,
    						'on' => esc_html__('Enabled', 'gloria'),
    						'off' => esc_html__('Disabled', 'gloria'),
						),
                        array(
    						'id'=>'cr-text',
    						'type' => 'textarea',
    						'title' => esc_html__('Copyright text - HTML Validated', 'gloria'), 
    						'subtitle' => esc_html__('HTML Allowed (wp_kses)', 'gloria'),
    						'validate' => 'html', //see http://codex.wordpress.org/Function_Reference/wp_kses_post
    						'default' => '',
						),
    				)
    			);
                $this->sections[] = array(
            		'icon'    => ' el-icon-font',
            		'title'   => esc_html__('Typography', 'gloria'),
            		'fields'  => array(
                        array(
            				'id'=>'bk-top-menu-font',
            				'type' => 'typography', 
                            'output' => array('#top-menu>ul>li, #top-menu>ul>li .sub-menu li, .bk_u_login, .bk_u_logout, .bk-links-modal'),
            				'title' => esc_html__('Top-menu font', 'gloria'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>true, // Disable google fonts. Won't work if you haven't defined your google api key
            				//'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
            				'font-size'=>false,
            				'line-height'=>false,
            				//'word-spacing'=>true, // Defaults to false
            				//'letter-spacing'=>true, // Defaults to false
            				'color'=>false,
            				//'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => false,
            				'subtitle'=> esc_html__('Font options for top menu', 'gloria'),
            				'default'=> array( 
            					'font-weight'=>'600', 
            					'font-family'=>'Open Sans', 
            					'google' => true,
            				    ),
                        ),
                        array(
            				'id'=>'bk-main-menu-font',
            				'type' => 'typography', 
                            'output' => array('.header .logo.logo-text h1 a, .module-title h2, .page-title h2, .sidebar-wrap .widget-title h3, .widget-tab-titles h3, .main-nav #main-menu .menu > li, .main-nav #main-menu .menu > li > a, .mega-title h3, .header .logo.logo-text h1, .bk-sub-posts .post-title,
                            .comment-box .comment-author-name, .today-date, .related-box h3, .comment-box .comments-area-title h3, .comment-respond h3, .comments-area .comments-area-title h3, 
                            .bk-author-box .author-info h3, .footer .widget-title h3, .recommend-box h3, .bk-login-title, #footer-menu a, .bk-copyright, 
                            .woocommerce-page div.product .product_title, .woocommerce div.product .woocommerce-tabs ul.tabs li a, .module-title .bk-tabs,
                            .related.products > h2 span, .woocommerce-page #reviews h3 span, .upsells.products > h2 span, .cross-sells > h2 span, 
                            .woocommerce-page .cart-collaterals .cart_totals h2 span, .woocommerce-page div.product .summary .product_title span'),
            				'title' => esc_html__('Main-menu font', 'gloria'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>true, // Disable google fonts. Won't work if you haven't defined your google api key
            				//'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
            				'font-size'=>false,
            				'line-height'=>false,
            				//'word-spacing'=>true, // Defaults to false
            				//'letter-spacing'=>true, // Defaults to false
            				'color'=>false,
            				//'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => false,
            				'subtitle'=> esc_html__('Font options for main menu', 'gloria'),
            				'default'=> array( 
            					'font-weight'=>'400', 
            					'font-family'=>'Open Sans', 
            					'google' => true,
            				    ),
                        ),
                        array(
            				'id'=>'bk-review-font',
            				'type' => 'typography', 
                            'output' => array('.review-score, .bk-criteria-wrap > span, .rating-wrap span, .pros-cons-title'
                            ),
            				'title' => esc_html__('Review score font', 'gloria'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>true, // Disable google fonts. Won't work if you haven't defined your google api key
            				//'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
            				'font-size'=>false,
            				'line-height'=>false,
            				//'word-spacing'=>true, // Defaults to false
            				//'letter-spacing'=>true, // Defaults to false
            				'color'=>false,
            				//'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => false,
            				'subtitle'=> esc_html__('Font options for review score', 'gloria'),
            				'default'=> array( 
            					'font-weight'=>'700', 
            					'font-family'=>'Archivo Narrow', 
            					'google' => true,
            				    ),
                        ),
                        array(
            				'id'=>'bk-meta-font',
            				'type' => 'typography', 
                            'output' => array('.widget-tabs .cm-header, .widget-review-tabs ul li .bk-final-score, .widget-social-counter .counter, .widget-social-counter ul li .data .subscribe, .meta, .post-category, .widget_comment .cm-header div, .comment-box .comment-time, .share-box ul li .share-item__value',
                            '.share-box .bk-share .share-item__valuem, .share-total, .loadmore span.ajaxtext, .bk-search-content .nothing-respond, .share-sticky .total-share-wrap'
                            ),
            				'title' => esc_html__('Meta font', 'gloria'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>true, // Disable google fonts. Won't work if you haven't defined your google api key
            				//'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
            				'font-size'=>false,
            				'line-height'=>false,
            				//'word-spacing'=>true, // Defaults to false
            				//'letter-spacing'=>true, // Defaults to false
            				'color'=>false,
            				//'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => false,
            				'subtitle'=> esc_html__('Font options for meta', 'gloria'),
            				'default'=> array( 
            					'font-weight'=>'400', 
            					'font-family'=>'Archivo Narrow', 
            					'google' => true,
            				    ),
                        ),
                        array(
            				'id'=>'bk-title-font',
            				'type' => 'typography', 
                            'output' => array('h1, h2, h3, h4, h5, #mobile-top-menu > ul > li, #mobile-menu > ul > li, .widget_display_stats dt,
                            .widget_display_views ul li a, .widget_display_topics ul li a, .widget_display_replies ul li a, 
                            .widget_display_forums ul li a, .widget_loginwithajaxwidget .bk-user-data ,.bk-share-box-top > span'),
            				'title' => esc_html__('Title font', 'gloria'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>true, // Disable google fonts. Won't work if you haven't defined your google api key
            				//'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
            				'font-size'=>false,
            				'line-height'=>false,
            				//'word-spacing'=>true, // Defaults to false
            				//'letter-spacing'=>true, // Defaults to false
            				'color'=>false,
            				//'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => false,
            				'subtitle'=> esc_html__('Font options for title', 'gloria'),
            				'default'=> array( 
            					'font-weight'=>'700', 
            					'font-family'=>'Roboto Slab', 
            					'google' => true,
            				    ),
                        ),
                        array(
            				'id'=>'bk-body-font',
            				'type' => 'typography',
                            'output' => array('body, textarea, input, p, .ticker-wrapper h4,
                            .entry-excerpt, .comment-text, .comment-author, .article-content,
                            .comments-area, .tag-list, .bk-mega-menu .bk-sub-posts .feature-post .menu-post-item .post-date, .comments-area small'), 
            				'title' => esc_html__('Text font', 'gloria'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>true, // Disable google fonts. Won't work if you haven't defined your google api key
            				//'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
            				'font-size'=>false,
            				'line-height'=>false,
            				//'word-spacing'=>true, // Defaults to false
            				//'letter-spacing'=>true, // Defaults to false
            				'color'=>false,
            				//'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => false,
            				'subtitle'=> esc_html__('Font options for text body', 'gloria'),
            				'default'=> array(
            					'font-weight'=>'400', 
            					'font-family'=>'Open Sans', 
            					'google' => true,
                            ),
            			),
                    ),
                );
                $this->sections[] = array(
    				'icon' => 'el-icon-file-edit',
    				'title' => esc_html__('Post Settings', 'gloria'),
    				'fields' => array(
                        array(
                            'id'       => 'feat-tag',
                            'type' => 'select',
                            'data' => 'tags',
                            'multi' => true,
                            'title'    => esc_html__('Featured tag name', 'gloria'),
                            'subtitle' => esc_html__('Tag name to feature your post, if no posts match the tag, sticky post will be displayed instead.', 'gloria'),
                            'default'  => ''
                        ),
                        array(
                            'id' => 'section-postmeta-start',
                            'title' => esc_html__('Post meta', 'gloria'),
                            'subtitle' => esc_html__('Options for displaying post meta in modules and widgets','gloria'),
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),
                        array(
                            'id'=>'bk-meta-review-sw',
                            'type' => 'switch',
                            'title' => esc_html__('Enable post meta review score', 'gloria'),
                            "default" => 1,
    						'on' => esc_html__('Enabled', 'gloria'),
    						'off' => esc_html__('Disabled', 'gloria'),
                        ),
                        array(
                            'id'=>'bk-meta-author-sw',
                            'type' => 'switch',
                            'title' => esc_html__('Enable post meta author', 'gloria'),
                            "default" => 1,
    						'on' => esc_html__('Enabled', 'gloria'),
    						'off' => esc_html__('Disabled', 'gloria'),
                        ),
                        array(
                            'id'=>'bk-meta-date-sw',
                            'type' => 'switch',
                            'title' => esc_html__('Enable post meta date', 'gloria'),
                            "default" => 1,
    						'on' => esc_html__('Enabled', 'gloria'),
    						'off' => esc_html__('Disabled', 'gloria'),
                        ),
                        array(
                            'id'=>'bk-meta-comments-sw',
                            'type' => 'switch',
                            'title' => esc_html__('Enable post meta comments', 'gloria'),
                            "default" => 1,
    						'on' => esc_html__('Enabled', 'gloria'),
    						'off' => esc_html__('Disabled', 'gloria'),
                        ),
                        array(
                            'id' => 'section-postmeta-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ),
                        
    				)
    			);
                $this->sections[] = array(
            		'icon'    => 'el-icon-book',
            		'title'   => esc_html__('Pages Setting', 'gloria'),
            		'heading' => esc_html__('Pages Setting','gloria'),
            		'desc'    => '<p class="description">'.esc_html__("Setting layout for pages", "gloria").'</p>',
            		'fields'  => array(
                    /*** breadcrumbs ***/
                    array(
                            'id' => 'bk-breadcrumbs',  
                            'type' => 'select',
                            'multi' => false,
                            'title' => esc_html__('BreadCrumb Setting', 'gloria'),
                            'subtitle' => esc_html__('Show/Hide Breadcrumbs on pages', 'gloria'),
                            'desc' => esc_html__('', 'gloria'),
                            'options' => array('enable'=>esc_html__('Show', 'gloria'), 
                                                'disable'=>esc_html__('Hide', 'gloria')
                                                ),
                            'default' => 'enable',
                        ),
                    /*** Default Template ***/
                        array(
                            'id' => 'section-default-template-layout-start',
                            'title' => esc_html__('Default Template Setting', 'gloria'),
                            'subtitle' => '',
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),   
                        array(
            				'id'=>'default-template-layout',
            				'type' => 'select',
                            'title' => esc_html__('Default template page layout', 'gloria'), 
    						'options' => array('has-sb'=>esc_html__('Has sidebar', 'gloria'), 
                                                'full-width'=>esc_html__('Full Width (No Sidebar)', 'gloria'), 
                                                ),
    						'default' => 'has-sb',
            			),
                        array(
                            'id' => 'default-template-sidebar-sticky',  
                            'type' => 'select',
                            'multi' => false,
                            'title' => esc_html__('Stick Sidebar', 'gloria'),
                            'subtitle' => esc_html__('Enable Stick Sidebar / Disable Stick Sidebar', 'gloria'),
                            'desc' => esc_html__('', 'gloria'),
                            'options' => array('enable'=>esc_html__('Enable', 'gloria'), 
                                                'disable'=>esc_html__('Disable', 'gloria')
                                                ),
                            'default' => 'disable',
                        ),
                        array(
                            'id' => 'default-template-sidebar',  
                            'type' => 'select',
                            'data' => 'sidebars',
                            'multi' => false,
                            'title' => esc_html__('Sidebar', 'gloria'),
                            'subtitle' => esc_html__('Choose sidebar for blog page', 'gloria'),
                            'desc' => esc_html__('', 'gloria'),
                        ),
                        array(
                            'id' => 'section-default-template-layout-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ), 
                    /*** Pagebuilder ***/
                        array(
                            'id' => 'section-pagebuilder-layout-start',
                            'title' => esc_html__('Pagebuilder Setting', 'gloria'),
                            'subtitle' => '',
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),   
                        array(
                            'id' => 'pagebuilder-sidebar',  
                            'type' => 'select',
                            'multi' => false,
                            'title' => esc_html__('Stick Sidebar', 'gloria'),
                            'subtitle' => esc_html__('Enable Stick Sidebar / Disable Stick Sidebar', 'gloria'),
                            'desc' => esc_html__('', 'gloria'),
                            'options' => array('enable'=>esc_html__('Enable', 'gloria'), 
                                                'disable'=>esc_html__('Disable', 'gloria')
                                                ),
                            'default' => 'disable',
                        ),
                        array(
                            'id' => 'section-pagebuilder-layout-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ), 
                    /*** Blog Layout ***/
                        array(
                            'id' => 'section-blog-layout-start',
                            'title' => esc_html__('Blog Page Setting', 'gloria'),
                            'subtitle' => '',
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),   
                        array(
            				'id'=>'bk-blog-layout',
            				'type' => 'select',
                            'title' => esc_html__('Blog page layout', 'gloria'), 
    						'options' => array('classic-blog'=>esc_html__('Classic Blog', 'gloria'), 
                                                'large-blog'=>esc_html__('Large Blog with Sidebar', 'gloria'), 
                                                'masonry'=>esc_html__('Masonry with Sidebar', 'gloria'),
                                                'masonry-nosb'=>esc_html__('Masonry no Sidebar', 'gloria'),
                                                'square-grid-3'=>esc_html__('Square Grid no Sidebar', 'gloria'),
                                                'square-grid-2'=>esc_html__('Square Grid with Sidebar', 'gloria'),
                                                ),
    						'default' => 'classic-blog',
            			),
                        array(
                            'id' => 'blog-page-sidebar',  
                            'type' => 'select',
                            'data' => 'sidebars',
                            'multi' => false,
                            'title' => esc_html__('Sidebar', 'gloria'),
                            'subtitle' => esc_html__('Choose sidebar for blog page', 'gloria'),
                            'desc' => esc_html__('', 'gloria'),
                        ),
                        array(
                            'id' => 'blog-stick-sidebar',  
                            'type' => 'select',
                            'multi' => false,
                            'title' => esc_html__('Stick Sidebar', 'gloria'),
                            'subtitle' => esc_html__('Enable Stick Sidebar / Disable Stick Sidebar', 'gloria'),
                            'desc' => esc_html__('', 'gloria'),
                            'options' => array('enable'=>esc_html__('Enable', 'gloria'), 
                                                'disable'=>esc_html__('Disable', 'gloria')
                                                ),
                            'default' => 'disable',
                        ),
                        array(
                            'id' => 'section-blog-layout-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ), 
                    /*** Author Layout ***/
                        array(
                        'id' => 'section-author-layout-start',
                            'title' => esc_html__('Author Page Setting', 'gloria'),
                            'subtitle' => '',
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),   
                        array(
            				'id'=>'bk-author-layout',
            				'type' => 'select',
                            'title' => esc_html__('Author page layout', 'gloria'), 
    						'options' => array('classic-blog'=>esc_html__('Classic Blog', 'gloria'), 
                                                'large-blog'=>esc_html__('Large Blog with Sidebar', 'gloria'), 
                                                'masonry'=>esc_html__('Masonry with Sidebar', 'gloria'),
                                                'masonry-nosb'=>esc_html__('Masonry no Sidebar', 'gloria'),
                                                'square-grid-3'=>esc_html__('Square Grid no Sidebar', 'gloria'),
                                                'square-grid-2'=>esc_html__('Square Grid with Sidebar', 'gloria'),
                                                ),
    						'default' => 'classic-blog',
            			),
                        array(
                            'id' => 'author-page-sidebar',  
                            'type' => 'select',
                            'data' => 'sidebars',
                            'multi' => false,
                            'title' => esc_html__('Sidebar', 'gloria'),
                            'subtitle' => esc_html__('Choose sidebar for Author page', 'gloria'),
                            'desc' => esc_html__('', 'gloria'),
                        ),
                        array(
                            'id' => 'author-stick-sidebar',  
                            'type' => 'select',
                            'multi' => false,
                            'title' => esc_html__('Stick Sidebar', 'gloria'),
                            'subtitle' => esc_html__('Enable Stick Sidebar / Disable Stick Sidebar', 'gloria'),
                            'desc' => esc_html__('', 'gloria'),
                            'options' => array('enable'=>esc_html__('Enable', 'gloria'), 
                                                'disable'=>esc_html__('Disable', 'gloria')
                                                ),
                            'default' => 'disable',
                        ),
                        array(
    						'id'=>'bk-page-authorbox-sw',
    						'type' => 'switch', 
    						'title' => esc_html__('Enable author box', 'gloria'),
    						'subtitle' => esc_html__('Enable author information below single post', 'gloria'),
    						'default' => 0,
    						'on' => esc_html__('Enabled', 'gloria'),
    						'off' => esc_html__('Disabled', 'gloria'),
						),
                        array(
                            'id' => 'section-author-layout-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ), 
                    /*** Category Layout ***/
                        array(
                        'id' => 'section-category-layout-start',
                            'title' => esc_html__('Category Page Setting', 'gloria'),
                            'subtitle' => '',
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),
                        array(
            				'id'=>'bk-category-layout',
            				'type' => 'select',
                            'title' => esc_html__('Category page layout', 'gloria'),
                            'subtitle' => esc_html__('Global setting for layout of category archive page, will be overridden by layout option in category edit page.', 'gloria'), 
    						'options' => array('classic-blog'=>esc_html__('Classic Blog', 'gloria'), 
                                                'large-blog'=>esc_html__('Large Blog with Sidebar', 'gloria'), 
                                                'masonry'=>esc_html__('Masonry with Sidebar', 'gloria'),
                                                'masonry-nosb'=>esc_html__('Masonry no Sidebar', 'gloria'),
                                                'square-grid-3'=>esc_html__('Square Grid no Sidebar', 'gloria'),
                                                'square-grid-2'=>esc_html__('Square Grid with Sidebar', 'gloria'),
                                                ),
    						'default' => 'masonry',
            			),
                        array(
                            'id' => 'category-page-sidebar',  
                            'type' => 'select',
                            'data' => 'sidebars',
                            'multi' => false,
                            'title' => esc_html__('Sidebar', 'gloria'),
                            'subtitle' => esc_html__('Choose sidebar for Category page', 'gloria'),
                            'desc' => esc_html__('', 'gloria'),
                        ),
                        array(
                            'id' => 'category-stick-sidebar',  
                            'type' => 'select',
                            'multi' => false,
                            'title' => esc_html__('Stick Sidebar', 'gloria'),
                            'subtitle' => esc_html__('Enable Stick Sidebar / Disable Stick Sidebar', 'gloria'),
                            'desc' => esc_html__('', 'gloria'),
                            'options' => array('enable'=>esc_html__('Enable', 'gloria'), 
                                                'disable'=>esc_html__('Disable', 'gloria')
                                                ),
                            'default' => 'disable',
                        ),
                        array(
                            'id' => 'section-category-layout-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ), 
                    /*** Archive Layout ***/
                        array(
                            'id' => 'section-archive-layout-start',
                            'title' => esc_html__('Archive Page Setting', 'gloria'),
                            'subtitle' => '',
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),
                        array(
            				'id'=>'bk-archive-layout',
            				'type' => 'select',
                            'title' => esc_html__('Archive page layout', 'gloria'), 
                            'subtitle' => esc_html__('Layout for Archive page and Tag archive.', 'gloria'),
    						'options' => array('classic-blog'=>esc_html__('Classic Blog', 'gloria'), 
                                                'large-blog'=>esc_html__('Large Blog with Sidebar', 'gloria'), 
                                                'masonry'=>esc_html__('Masonry with Sidebar', 'gloria'),
                                                'masonry-nosb'=>esc_html__('Masonry no Sidebar', 'gloria'),
                                                'square-grid-3'=>esc_html__('Square Grid no Sidebar', 'gloria'),
                                                'square-grid-2'=>esc_html__('Square Grid with Sidebar', 'gloria'),
                                                ),
    						'default' => 'classic-blog',
            			),
                        array(
                            'id' => 'archive-page-sidebar',  
                            'type' => 'select',
                            'data' => 'sidebars',
                            'multi' => false,
                            'title' => esc_html__('Sidebar', 'gloria'),
                            'subtitle' => esc_html__('Choose sidebar for Archive page', 'gloria'),
                            'desc' => esc_html__('', 'gloria'),
                        ),
                        array(
                            'id' => 'archive-stick-sidebar',  
                            'type' => 'select',
                            'multi' => false,
                            'title' => esc_html__('Stick Sidebar', 'gloria'),
                            'subtitle' => esc_html__('Enable Stick Sidebar / Disable Stick Sidebar', 'gloria'),
                            'desc' => esc_html__('', 'gloria'),
                            'options' => array('enable'=>esc_html__('Enable', 'gloria'), 
                                                'disable'=>esc_html__('Disable', 'gloria')
                                                ),
                            'default' => 'disable',
                        ),
                        array(
                            'id' => 'section-archive-layout-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ), 
                    /*** Search Layout ***/
                        array(
                            'id' => 'section-search-layout-start',
                            'title' => esc_html__('Search Page Setting', 'gloria'),
                            'subtitle' => '',
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),
                        array(
            				'id'=>'bk-search-layout',
            				'type' => 'select',
                            'title' => esc_html__('Search page layout', 'gloria'), 
    						'options' => array('classic-blog'=>esc_html__('Classic Blog', 'gloria'), 
                                                'large-blog'=>esc_html__('Large Blog with Sidebar', 'gloria'), 
                                                'masonry'=>esc_html__('Masonry with Sidebar', 'gloria'),
                                                'masonry-nosb'=>esc_html__('Masonry no Sidebar', 'gloria'),
                                                'square-grid-3'=>esc_html__('Square Grid no Sidebar', 'gloria'),
                                                'square-grid-2'=>esc_html__('Square Grid with Sidebar', 'gloria'),
                                                ),
    						'default' => 'classic-blog',
            			),
                        array(
                            'id' => 'search-page-sidebar',  
                            'type' => 'select',
                            'data' => 'sidebars',
                            'multi' => false,
                            'title' => esc_html__('Sidebar', 'gloria'),
                            'subtitle' => esc_html__('Choose sidebar for Search page', 'gloria'),
                            'desc' => esc_html__('', 'gloria'),
                        ),
                        array(
                            'id' => 'search-stick-sidebar',  
                            'type' => 'select',
                            'multi' => false,
                            'title' => esc_html__('Stick Sidebar', 'gloria'),
                            'subtitle' => esc_html__('Enable Stick Sidebar / Disable Stick Sidebar', 'gloria'),
                            'desc' => esc_html__('', 'gloria'),
                            'options' => array('enable'=>esc_html__('Enable', 'gloria'), 
                                                'disable'=>esc_html__('Disable', 'gloria')
                                                ),
                            'default' => 'disable',
                        ),
                        array(
            				'id'=>'bk-search-result',
            				'type' => 'select',
                            'title' => esc_html__('Remove Pages in Search Result', 'gloria'),
    						'options' => array('yes' => esc_html__('yes', 'gloria'), 'no' => esc_html__('no', 'gloria')),
    						'default' => 'yes',
            			),
                        array(
                            'id' => 'section-search-layout-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ),
                        /*** Shop Layout ***/
                        array(
                            'id' => 'section-shop-layout-start',
                            'title' => esc_html__('Shop Page Setting', 'gloria'),
                            'subtitle' => '',
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),
                        array(
    						'id'=>'bk-shop-sidebar',
    						'type' => 'select',
    						'title' => esc_html__('Woocommerce layout', 'gloria'),
    						'subtitle' => esc_html__('Enable/Disable sidebar', 'gloria'),
                            'options' => array('on'=>esc_html__('Display with sidebar', 'gloria'), 
                                                'off'=>esc_html__('Display fullwidth (without sidebar)', 'gloria'),
                                         ), 
                            'default' => 'off',
						),
                        array(
                            'id' => 'shop-page-sidebar',  
                            'required' => array('bk-shop-sidebar','=','on'),
                            'type' => 'select',
                            'data' => 'sidebars',
                            'multi' => false,
                            'title' => esc_html__('Shop Page Sidebar', 'gloria'),
                            'subtitle' => esc_html__('Choose sidebar for Shop page', 'gloria'),
                            'desc' => esc_html__('', 'gloria'),
                        ),
                        array(
                            'id' => 'product-page-sidebar',  
                            'required' => array('bk-shop-sidebar','=','on'),
                            'type' => 'select',
                            'data' => 'sidebars',
                            'multi' => false,
                            'title' => esc_html__('Product Page Sidebar', 'gloria'),
                            'subtitle' => esc_html__('Choose sidebar for product page', 'gloria'),
                            'desc' => esc_html__('', 'gloria'),
                        ),
                        array(
                            'id' => 'shop-stick-sidebar',  
                            'type' => 'select',
                            'required' => array('bk-shop-sidebar','=','on'),
                            'multi' => false,
                            'title' => esc_html__('Stick Sidebar', 'gloria'),
                            'subtitle' => esc_html__('Enable Stick Sidebar / Disable Stick Sidebar', 'gloria'),
                            'desc' => esc_html__('', 'gloria'),
                            'options' => array('enable'=>esc_html__('Enable', 'gloria'), 
                                                'disable'=>esc_html__('Disable', 'gloria')
                                                ),
                            'default' => 'disable',
                        ),
                        array(
                            'id' => 'section-shop-layout-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ),
                        
                    /*** Forum Layout ***/
                        array(
                            'id' => 'section-forum-layout-start',
                            'title' => esc_html__('Forum Page Setting', 'gloria'),
                            'subtitle' => '',
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),
                        array(
    						'id'=>'bk-forum-sidebar',
    						'type' => 'select',
    						'title' => esc_html__('BBpress layout', 'gloria'),
    						'subtitle' => esc_html__('Enable/Disable sidebar', 'gloria'),
                            'options' => array('on'=>esc_html__('Display with sidebar', 'gloria'), 
                                                'off'=>esc_html__('Display fullwidth (without sidebar)', 'gloria'),
                                         ), 
                            'default' => 'off',
						),
                        array(
                            'id' => 'forum-page-sidebar',  
                            'required' => array('bk-forum-sidebar','=','on'),
                            'type' => 'select',
                            'data' => 'sidebars',
                            'multi' => false,
                            'title' => esc_html__('Forum Page Sidebar', 'gloria'),
                            'subtitle' => esc_html__('Choose sidebar for forum page', 'gloria'),
                            'desc' => esc_html__('', 'gloria'),
                        ),
                        array(
                            'id' => 'forum-stick-sidebar',  
                            'type' => 'select',
                            'required' => array('bk-forum-sidebar','=','on'),
                            'multi' => false,
                            'title' => esc_html__('Stick Sidebar', 'gloria'),
                            'subtitle' => esc_html__('Enable Stick Sidebar / Disable Stick Sidebar', 'gloria'),
                            'desc' => esc_html__('', 'gloria'),
                            'options' => array('enable'=>esc_html__('Enable', 'gloria'), 
                                                'disable'=>esc_html__('Disable', 'gloria')
                                                ),
                            'default' => 'disable',
                        ),
                        array(
                            'id' => 'section-forum-layout-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ),  
                    ),
                );
                $this->sections[] = array(
    				'icon' => 'el-icon-list-alt',
    				'title' => esc_html__('Single Settings', 'gloria'),
    				'fields' => array(
                        array(
                            'id' => 'single-page-sidebar',  
                            'type' => 'select',
                            'data' => 'sidebars', 
                            'multi' => false,
                            'title' => esc_html__('Single Page Sidebar', 'gloria'),
                            'subtitle' => esc_html__('Choose sidebar for single page', 'gloria'),
                            'desc' => esc_html__('', 'gloria'),
                        ),
                        array(
                            'id' => 'single-stick-sidebar',  
                            'type' => 'select',
                            'multi' => false,
                            'title' => esc_html__('Stick Sidebar', 'gloria'),
                            'subtitle' => esc_html__('Enable Stick Sidebar / Disable Stick Sidebar', 'gloria'),
                            'desc' => esc_html__('', 'gloria'),
                            'options' => array('enable'=>esc_html__('Enable', 'gloria'), 
                                                'disable'=>esc_html__('Disable', 'gloria')
                                                ),
                            'default' => 'disable',
                        ),
                        array(
    						'id'=>'bk-og-tag',
    						'type' => 'switch', 
    						'title' => esc_html__('Insert Open Graph Meta Tags to Header', 'gloria'),
    						"default" => 0,
    						'on' => esc_html__('Enabled', 'gloria'),
    						'off' => esc_html__('Disabled', 'gloria'),
                            'indent' => true
						),
                        array(
    						'id'=>'bk-sharebox-sw',
    						'type' => 'switch', 
    						'title' => esc_html__('Enable share box', 'gloria'),
    						'subtitle' => esc_html__('Enable share links below single post', 'gloria'),
    						"default" => 1,
    						'on' => esc_html__('Enabled', 'gloria'),
    						'off' => esc_html__('Disabled', 'gloria'),
                            'indent' => true
						),
                        array(
                            'id'=>'section-sharebox-start',
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),
                        array(
                            'id'=>'bk-fb-sw',
                            'type' => 'switch',
                            'required' => array('bk-sharebox-sw','=','1'),
                            'title' => esc_html__('Enable Facebook share link', 'gloria'),
                            "default" => 1,
    						'on' => esc_html__('Enabled', 'gloria'),
    						'off' => esc_html__('Disabled', 'gloria'),
                        ),
                        array(
                            'id'=>'bk-tw-sw',
                            'type' => 'switch',
                            'required' => array('bk-sharebox-sw','=','1'),
                            'title' => esc_html__('Enable Twitter share link', 'gloria'),
                            "default" => 1,
    						'on' => esc_html__('Enabled', 'gloria'),
    						'off' => esc_html__('Disabled', 'gloria'),
                        ),
                        array(
                            'id'=>'bk-gp-sw',
                            'type' => 'switch',
                            'required' => array('bk-sharebox-sw','=','1'),
                            'title' => esc_html__('Enable Google+ share link', 'gloria'),
                            "default" => 1,
    						'on' => esc_html__('Enabled', 'gloria'),
    						'off' => esc_html__('Disabled', 'gloria'),
                        ),
                        array(
                            'id'=>'bk-pi-sw',
                            'type' => 'switch',
                            'required' => array('bk-sharebox-sw','=','1'),
                            'title' => esc_html__('Enable Pinterest share link', 'gloria'),
                            "default" => 1,
    						'on' => esc_html__('Enabled', 'gloria'),
    						'off' => esc_html__('Disabled', 'gloria'),
                        ),
                        array(
                            'id'=>'bk-stu-sw',
                            'type' => 'switch',
                            'required' => array('bk-sharebox-sw','=','1'),
                            'title' => esc_html__('Enable Stumbleupon share link', 'gloria'),
                            "default" => 1,
    						'on' => esc_html__('Enabled', 'gloria'),
    						'off' => esc_html__('Disabled', 'gloria'),
                        ),
                        array(
                            'id'=>'bk-li-sw',
                            'type' => 'switch',
                            'required' => array('bk-sharebox-sw','=','1'),
                            'title' => esc_html__('Enable Linkedin share link', 'gloria'),
                            "default" => 1,
    						'on' => esc_html__('Enabled', 'gloria'),
    						'off' => esc_html__('Disabled', 'gloria'),
                        ),
                        array(
                            'id'=>'section-sharebox-end',
                            'type' => 'section', 
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ), 
                        array(
    						'id'=>'bk-authorbox-sw',
    						'type' => 'switch', 
    						'title' => esc_html__('Enable author box', 'gloria'),
    						'subtitle' => esc_html__('Enable author information below single post', 'gloria'),
    						"default" => 0,
    						'on' => esc_html__('Enabled', 'gloria'),
    						'off' => esc_html__('Disabled', 'gloria'),
						),
                        array(
    						'id'=>'bk-postnav-sw',
    						'type' => 'switch', 
    						'title' => esc_html__('Enable post navigation', 'gloria'),
    						'subtitle' => esc_html__('Enable post navigation below single post', 'gloria'),
    						"default" => 1,
    						'on' => esc_html__('Enabled', 'gloria'),
    						'off' => esc_html__('Disabled', 'gloria'),
						),
                        array(
    						'id'=>'bk-related-sw',
    						'type' => 'switch', 
    						'title' => esc_html__('Enable related posts', 'gloria'),
    						'subtitle' => esc_html__('Enable related posts below single post', 'gloria'),
    						"default" => 1,
    						'on' => esc_html__('Enabled', 'gloria'),
    						'off' => esc_html__('Disabled', 'gloria'),
						),
                        array(
    						'id'=>'bk-comment-sw',
    						'type' => 'switch', 
    						'title' => esc_html__('Enable comment section', 'gloria'),
    						'subtitle' => esc_html__('Enable comment section below single post', 'gloria'),
    						"default" => 1,
    						'on' => esc_html__('Enabled', 'gloria'),
    						'off' => esc_html__('Disabled', 'gloria'),
						),
                        array(
                            'id' => 'section-recommend-start',
                            'title' => esc_html__('Recommend Box Setting', 'gloria'),
                            'subtitle' => '',
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),     
                        array(
    						'id'=>'bk-recommend-box',
    						'type' => 'switch', 
    						'title' => esc_html__('Enable Recommend Box', 'gloria'),
    						'subtitle' => esc_html__('A random post appear on single page', 'gloria'),
    						"default" => 1,
    						'on' => esc_html__('Enabled', 'gloria'),
    						'off' => esc_html__('Disabled', 'gloria'),
						),     
                        array(
                            'id'       => 'recommend-box-title',
                            'type'     => 'text',
                            'title'    => esc_html__('Recommend Box title', 'gloria'),
                            'default'  => ''
                        ),
                        array(
                            'id' => 'recommend-categories',
                            'type' => 'select',
                            'data' => 'categories',
                            'multi' => true,
                            'title' => esc_html__('Categories', 'gloria')
                        ),
                        array(
                            'id'       => 'recommend-number',
                            'type'     => 'text',
                            'title'    => esc_html__('Number of posts', 'gloria'),
                            'subtitle' => esc_html__('Type number of posts will be displayed', 'gloria'),
                            'default'  => ''
                        ),
                        array(
                            'id' => 'section-recommend-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ) 
    				)
    			);
                $this->sections[] = array(
    				'icon' => 'el-icon-css',
    				'title' => esc_html__('Custom CSS', 'gloria'),
    				'fields' => array(
                        array(
    						'id'=>'bk-css-code',
    						'type' => 'ace_editor',
    						'title' => esc_html__('CSS Code', 'gloria'), 
    						'subtitle' => esc_html__('Paste your CSS code here.', 'gloria'),
    						'mode' => 'css',
    			            'theme' => 'chrome',
    			            'default' => "",
    					),                                              	
    				)
    			);				
					

			$theme_info = '<div class="redux-framework-section-desc">';
			$theme_info .= '<p class="redux-framework-theme-data description theme-uri">'.esc_html__('<strong>Theme URL:</strong> ', 'gloria').'<a href="'.$this->theme->get('ThemeURI').'" target="_blank">'.$this->theme->get('ThemeURI').'</a></p>';
			$theme_info .= '<p class="redux-framework-theme-data description theme-author">'.esc_html__('<strong>Author:</strong> ', 'gloria').$this->theme->get('Author').'</p>';
			$theme_info .= '<p class="redux-framework-theme-data description theme-version">'.esc_html__('<strong>Version:</strong> ', 'gloria').$this->theme->get('Version').'</p>';
			$theme_info .= '<p class="redux-framework-theme-data description theme-description">'.$this->theme->get('Description').'</p>';
			$tabs = $this->theme->get('Tags');
			if ( !empty( $tabs ) ) {
				$theme_info .= '<p class="redux-framework-theme-data description theme-tags">'.esc_html__('<strong>Tags:</strong> ', 'gloria').implode(', ', $tabs ).'</p>';	
			}
			$theme_info .= '</div>';

			$this->sections[] = array(
				'type' => 'divide',
			);

		}	

		public function setHelpTabs() {

		}


		/**
			
			All the possible arguments for Redux.
			For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments

		 **/
		public function setArguments() {
			
			$theme = wp_get_theme(); // For use with some settings. Not necessary.

			$this->args = array(
	            
	            // TYPICAL -> Change these values as you need/desire
				'opt_name'          	=> 'gloria_option', // This is where your data is stored in the database and also becomes your global variable name.
				'display_name'			=> $theme->get('Name'), // Name that appears at the top of your panel
				'display_version'		=> $theme->get('Version'), // Version that appears at the top of your panel
				'menu_type'          	=> 'menu', //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
				'allow_sub_menu'     	=> true, // Show the sections below the admin menu item or not
				'menu_title'			=> esc_html__( 'Theme Options', 'gloria' ),
	            'page'		 	 		=> esc_html__( 'Theme Options', 'gloria' ),
	            'google_api_key'   	 	=> 'AIzaSyBdxbxgVuwQcnN5xCZhFDSpouweO-yJtxw', // Must be defined to add google fonts to the typography module
	            'global_variable'    	=> '', // Set a different name for your global variable other than the opt_name
	            'dev_mode'           	=> false, // Show the time the page took to load, etc
	            'customizer'         	=> true, // Enable basic customizer support

	            // OPTIONAL -> Give you extra features
	            'page_priority'      	=> null, // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
	            'page_parent'        	=> 'themes.php', // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
	            'page_permissions'   	=> 'manage_options', // Permissions needed to access the options panel.
	            'menu_icon'          	=> '', // Specify a custom URL to an icon
	            'last_tab'           	=> '', // Force your panel to always open to a specific tab (by id)
	            'page_icon'          	=> 'icon-themes', // Icon displayed in the admin panel next to your menu_title
	            'page_slug'          	=> '_options', // Page slug used to denote the panel
	            'save_defaults'      	=> true, // On load save the defaults to DB before user clicks save or not
	            'default_show'       	=> false, // If true, shows the default value next to each field that is not the default value.
	            'default_mark'       	=> '', // What to print by the field's title if the value shown is default. Suggested: *


	            // CAREFUL -> These options are for advanced use only
	            'transient_time' 	 	=> 60 * MINUTE_IN_SECONDS,
	            'output'            	=> true, // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
	            'output_tag'            	=> true, // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
	            //'domain'             	=> 'redux-framework', // Translation domain key. Don't change this unless you want to retranslate all of Redux.
	            //'footer_credit'      	=> '', // Disable the footer credit of Redux. Please leave if you can help it.
	            

	            // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
	            'database'           	=> '', // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
	            
	        
	            'show_import_export' 	=> true, // REMOVE
	            'system_info'        	=> false, // REMOVE
	            
	            'help_tabs'          	=> array(),
	            'help_sidebar'       	=> '', // esc_html__( '', $this->args['domain'] );            
				);


			// SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.		
			$this->args['share_icons'][] = array(
			    'url' => 'https://github.com/ReduxFramework/ReduxFramework',
			    'title' => 'Visit us on GitHub', 
			    'icon' => 'el-icon-github'
			    // 'img' => '', // You can use icon OR img. IMG needs to be a full URL.
			);		
			$this->args['share_icons'][] = array(
			    'url' => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
			    'title' => 'Like us on Facebook', 
			    'icon' => 'el-icon-facebook'
			);
			$this->args['share_icons'][] = array(
			    'url' => 'http://twitter.com/reduxframework',
			    'title' => 'Follow us on Twitter', 
			    'icon' => 'el-icon-twitter'
			);
			$this->args['share_icons'][] = array(
			    'url' => 'http://www.linkedin.com/company/redux-framework',
			    'title' => 'Find us on LinkedIn', 
			    'icon' => 'el-icon-linkedin'
			);

			
	 
			// Panel Intro text -> before the form
			if (!isset($this->args['global_variable']) || $this->args['global_variable'] !== false ) {
				if (!empty($this->args['global_variable'])) {
					$v = $this->args['global_variable'];
				} else {
					$v = str_replace("-", "_", $this->args['opt_name']);
				}
				$this->args['intro_text'] = '';
			} else {
				$this->args['intro_text'] = '';
			}

			// Add content after the form.
			$this->args['footer_text'] = '' ;

		}
	}
	new Redux_Framework_config();

}


/** 

	Custom function for the callback referenced above

 */
if ( !function_exists( 'redux_my_custom_field' ) ):
	function redux_my_custom_field($field, $value) {
	    print_r($field);
	    print_r($value);
	}
endif;

/**
 
	Custom function for the callback validation referenced above

**/
if ( !function_exists( 'redux_validate_callback_function' ) ):
	function redux_validate_callback_function($field, $value, $existing_value) {
	    $error = false;
	    $value =  'just testing';
	    /*
	    do your validation
	    
	    if(something) {
	        $value = $value;
	    } elseif(something else) {
	        $error = true;
	        $value = $existing_value;
	        $field['msg'] = 'your custom error message';
	    }
	    */
	    
	    $return['value'] = $value;
	    if($error == true) {
	        $return['error'] = $field;
	    }
	    return $return;
	}
endif;