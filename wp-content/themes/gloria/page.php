<?php get_header(); ?>
<?php
$gloria_option = gloria_core::bk_get_global_var('gloria_option');
$bk_layout = '';
$bk_sidebar = '';
$bk_sidebar_sticky = '';

if (isset($gloria_option) && ($gloria_option != '')): 
    if(isset($gloria_option['default-template-layout'])) {
        $bk_layout = $gloria_option['default-template-layout'];
    }
    if ($bk_layout === 'has-sb') {
        if(isset($gloria_option['default-template-sidebar'])) {
            $bk_sidebar = $gloria_option['default-template-sidebar'];
        }
        if (isset($gloria_option['default-template-sidebar-sticky'])) {
            $bk_sidebar_sticky = $gloria_option['default-template-sidebar-sticky'];
        }
    }
endif;
?>
<div id="body-wrapper" class="wp-page">
    <div class="bkwrapper container">		
        <div class="row bksection">	         
            <?php if (class_exists('Woocommerce') && ((is_cart() == true) || (is_checkout() == true) || (is_checkout_pay_page() == true))) {?>
                <div class="col-md-12 fullwidth">
                	<?php if (have_posts()) : ?>
                		<?php while ( have_posts() ) : the_post(); ?>
                            <?php if ( is_page() && ! is_front_page() || ( function_exists( 'bp_current_component' ) && bp_current_component() )  ) : ?>
                            <div class="page-title">
                                <h2 class="heading" itemprop="name"><span><?php the_title(); ?></span></h2>
                                <?php gloria_core::bk_breadcrumbs();?>
                            </div>
                			<?php endif; ?>		
                			<article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?>>
                				<div class="post-content"><?php the_content(); ?></div>
                			</article>                
                        <?php
                            if (function_exists("gloria_paginate")) {
                                gloria_paginate();
                            }
                        ?>  
                        <?php if(($gloria_option['bk-comment-sw']) && (comments_open())) {?>
                            <div class="comment-box clearfix">
                                <?php comments_template(); ?>
                            </div> <!-- End Comment Box -->
                        <?php }?>
                    <?php endwhile; ?>
            	<?php else : ?>
            
            		<h2><?php esc_html_e('Not Found', 'gloria') ?></h2>
            
            	<?php endif; ?>
             </div>
            <?php }else {?>
                <div class="<?php if ($bk_sidebar !== ''): echo 'col-md-8 has-sb'; else: echo 'col-md-12 fullwidth';  endif;?>">
                	<?php if (have_posts()) : ?>
                		<?php while ( have_posts() ) : the_post(); ?>
                            <?php if ( is_page() && ! is_front_page() || ( function_exists( 'bp_current_component' ) && bp_current_component() )  ) : ?>
                            <div class="page-title">
                                <h2 class="heading" itemprop="name"><span><?php the_title(); ?></span></h2>
                                <?php gloria_core::bk_breadcrumbs();?>
                            </div>
                			<?php endif; ?>		
                			<article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?>>
                				<div class="post-content"><?php the_content(); ?></div>
                			</article>
                
                            <?php
                                if (function_exists("gloria_paginate")) {
                                    gloria_paginate();
                                }
                            ?>  
                            <?php if(($gloria_option['bk-comment-sw']) && (comments_open())) {?>
                                <div class="comment-box clearfix">
                                    <?php comments_template(); ?>
                                </div> <!-- End Comment Box -->
                            <?php }?>
                        <?php endwhile; ?>
                	<?php else : ?>
                
                		<h2><?php esc_html_e('Not Found', 'gloria') ?></h2>
                
                	<?php endif; ?>
                 </div>
                 <?php if ($bk_sidebar !== '') {?>
                        <div class="sidebar col-md-4">
                            <aside class="sidebar-wrap <?php if($bk_sidebar_sticky == 'enable') echo 'stick';?>" id="bk-default-template-sidebar">
                                <?php dynamic_sidebar($bk_sidebar);; ?>
                            </aside>
                        </div>
                 <?php }?>
             <?php }?>
        </div>
    </div>
</div>
		
<?php get_footer(); ?>