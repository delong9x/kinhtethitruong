<!--<home sidebar widget>-->
<?php $gloria_option = gloria_core::bk_get_global_var('gloria_option');?>
    <?php
        if (is_front_page()) {
            dynamic_sidebar('home_sidebar');
        }else if(is_single()){
            if (( function_exists('bbpress') && ( is_bbpress() == true ))) {
                if(isset($gloria_option['forum-page-sidebar'])) {
                    $sidebar = $gloria_option['forum-page-sidebar'];
                }else {
                    $sidebar = '';
                }
            }else if (class_exists('Woocommerce') &&  ( is_woocommerce() == true ) && ((is_product() == true) || (is_product_taxonomy() == true) || (is_product_category() == true) || (is_product_tag() == true))) {
                if(isset($gloria_option['product-page-sidebar'])) {
                    $sidebar = $gloria_option['product-page-sidebar'];
                }else {
                    $sidebar = '';
                }
            }else if(isset($gloria_option['single-page-sidebar'])) {
                $sidebar = $gloria_option['single-page-sidebar'];
            }else {
                $sidebar = '';
            } 
            if (strlen($sidebar)) {
                dynamic_sidebar($sidebar);
            }else {
                dynamic_sidebar('home_sidebar');
            }
        }else if(is_category()) {
            if(isset($gloria_option['category-page-sidebar'])) {
                $sidebar = $gloria_option['category-page-sidebar'];
            }else {
                $sidebar = '';
            }
            if (strlen($sidebar)) {
                dynamic_sidebar($sidebar);
            }else {
                dynamic_sidebar('home_sidebar');
            }
        }else if (is_author()){
            if(isset($gloria_option['author-page-sidebar'])) {
                $sidebar = $gloria_option['author-page-sidebar'];
            }else {
                $sidebar = '';
            }
            if (strlen($sidebar)) {
                dynamic_sidebar($sidebar);
            }else {
                dynamic_sidebar('home_sidebar');
            }
        }else if (is_archive()) {
            if (class_exists('Woocommerce') &&  ( is_woocommerce() == true ) && ((is_shop() == true) || (is_product_taxonomy() == true) || (is_product_category() == true) || (is_product_tag() == true))) {
                if(isset($gloria_option['shop-page-sidebar'])) {
                    $sidebar = $gloria_option['shop-page-sidebar'];
                }else {
                    $sidebar = '';
                }
                if (strlen($sidebar)) {
                    dynamic_sidebar($sidebar);
                }else {
                    dynamic_sidebar('home_sidebar');
                }
            }else if (( function_exists('bbpress') && ( is_bbpress() == true ))) {
                if(isset($gloria_option['forum-page-sidebar'])) {
                    $sidebar = $gloria_option['forum-page-sidebar'];
                }else {
                    $sidebar = '';
                }
                if (strlen($sidebar)) {
                    dynamic_sidebar($sidebar);
                }else {
                    dynamic_sidebar('home_sidebar');
                }
            }else {
                if(isset($gloria_option['archive-page-sidebar'])) {
                    $sidebar = $gloria_option['archive-page-sidebar'];
                }else {
                    $sidebar = '';
                }
                if (strlen($sidebar)) {
                    dynamic_sidebar($sidebar);
                }else {
                    dynamic_sidebar('home_sidebar');
                }
            }
        }else if (is_search()) {
            if(isset($gloria_option['search-page-sidebar'])) {
                $sidebar = $gloria_option['search-page-sidebar'];
            }else {
                $sidebar = '';
            }
            if (strlen($sidebar)) {
                dynamic_sidebar($sidebar);
            }else {
                dynamic_sidebar('home_sidebar');
            }
        }else {
            wp_reset_postdata();
            if (class_exists('Woocommerce') &&  ( is_woocommerce() == true ) && ((is_product() == true) || (is_product_taxonomy() == true) || (is_product_category() == true) || (is_product_tag() == true))) {
                if(isset($gloria_option['product-page-sidebar'])) {
                    $sidebar = $gloria_option['product-page-sidebar'];
                }else {
                    $sidebar = '';
                }
                if (strlen($sidebar)) {
                    dynamic_sidebar($sidebar);
                }else {
                    dynamic_sidebar('home_sidebar');
                }
            }else if (( function_exists('bbpress') && ( is_bbpress() == true ))) {
                if(isset($gloria_option['forum-page-sidebar'])) {
                    $sidebar = $gloria_option['forum-page-sidebar'];
                }else {
                    $sidebar = '';
                }
                if (strlen($sidebar)) {
                    dynamic_sidebar($sidebar);
                }else {
                    dynamic_sidebar('home_sidebar');
                }
            }else if (is_page_template('blog.php')) {
                if(isset($gloria_option['blog-page-sidebar'])) {
                    $sidebar = $gloria_option['blog-page-sidebar'];
                }else {
                    $sidebar = '';
                }
                if (strlen($sidebar)) {
                    dynamic_sidebar($sidebar);
                }else {
                    dynamic_sidebar('home_sidebar');
                }
            }else {                     
                dynamic_sidebar('home_sidebar');
            }
        }
    ?>  	
<!--</home sidebar widget>-->