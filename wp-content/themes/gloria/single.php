<?php 
    $gloria_option = gloria_core::bk_get_global_var('gloria_option');
    //echo (preg_replace('/[^A-Za-z0-9]/', '', $_SERVER["REQUEST_URI"]));
    $bk_uri = explode('/', $_SERVER["REQUEST_URI"]);
    $bkcookied = 0;
    $cookietime = 30*60;
    if($bk_uri[count($bk_uri) - 1] !== '') {
        $cookie_name = preg_replace('/[^A-Za-z0-9]/', '', $bk_uri[count($bk_uri) - 1]);
    }else {
        $cookie_name = preg_replace('/[^A-Za-z0-9]/', '', $bk_uri[count($bk_uri) - 2]);
    }
    if(!isset($_COOKIE[$cookie_name])) {
        setcookie($cookie_name, '1', time() + $cookietime);  /* expire in 1 hour */
        $bkcookied = 1;
    }else {
        $bkcookied = 0;
    }
?>
<?php get_header(); ?>
<?php 
    $social_share = array();
    $share_box = $gloria_option['bk-sharebox-sw'];
    if ($share_box){
        $social_share['fb'] = $gloria_option['bk-fb-sw'];
        $social_share['tw'] = $gloria_option['bk-tw-sw'];
        $social_share['gp'] = $gloria_option['bk-gp-sw'];
        $social_share['pi'] = $gloria_option['bk-pi-sw'];
        $social_share['stu'] = $gloria_option['bk-stu-sw'];
        $social_share['li'] = $gloria_option['bk-li-sw'];
    }
    $authorbox_sw = $gloria_option['bk-authorbox-sw'];
    $postnav_sw = $gloria_option['bk-postnav-sw'];
    $related_sw = $gloria_option['bk-related-sw'];
    $comment_sw = $gloria_option['bk-comment-sw'];
?>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <?php 
                $bkPostID = get_the_ID();
                if ($bkcookied == 1) {
                    gloria_core::bk_setPostViews($bkPostID);
                }
                $bkReviewSW = get_post_meta($bkPostID,'bk_review_checkbox',true);
                $postFormat = gloria_core::bk_post_format_detect($bkPostID);
                $sidebar_pos = get_post_meta($bkPostID,'bk_sidebar_pos',true);
                $stickyShareBox = get_post_meta($bkPostID,'bk_sticky_share',true);
                $bkTitlePos = get_post_meta($bkPostID,'bk_title_pos',true);

                if(isset($sidebar_pos) && ($sidebar_pos !== 'left')){
                    $sidebar_pos = 'right';
                }
                // For Ajax Purpose                                
                bk_section_parent::$bk_ajax_c[$bkPostID]['s-related-article-tab']['content'] = '';
                bk_section_parent::$bk_ajax_c[$bkPostID]['s-more-from-author']['content'] = '';
                bk_section_parent::$bk_ajax_c['current_author'] = $post->post_author;
                //wp_localize_script( 'gloria-customjs', 'ajax_c', bk_section_parent::$bk_ajax_c );
                                
                if($bkReviewSW == '1') {
                    $reviewPos = get_post_meta($bkPostID,'bk_review_box_position',true);
                }
                $bkPostLayout = get_post_meta($bkPostID,'bk_post_layout',true);
                $bk_has_sidebar = get_post_meta($bkPostID,'bk_remove_sidebar',true);
                if(isset($bkPostLayout)){
                    if($bkPostLayout == 'fw-feat-img'){
                        $bk_post_layout = 'fw-feat-img';
                    }else if($bkPostLayout == 'standard2'){
                        $bk_post_layout = 'bk-normal2-feat';
                    }else {
                        $bk_post_layout = 'bk-normal-feat';
                    }
                }else {
                    $bk_post_layout = 'bk-normal-feat';
                }
                if ($bk_post_layout == 'fw-feat-img') {
                    $bkThumbId = get_post_thumbnail_id( $bkPostID );
                    $bksingleThumbUrl = wp_get_attachment_image_src( $bkThumbId, 'full' );
                    wp_localize_script( 'gloria-customjs', 'bksingleThumbUrl', $bksingleThumbUrl[0] );
                }
            ?>
        <?php if ( $bkReviewSW == '1' ) { $itemprop =  'itemprop="itemReviewed"'; } else { $itemprop = 'itemprop="headline"'; }?>
        <div class="single-page <?php echo 'sidebar-'.$sidebar_pos;?>">
        <?php if($postFormat['format'] === 'gallery') {?>
            <?php echo gloria_core::bk_post_format_display($bkPostID, $bk_post_layout);?>
            <div class="article-wrap bkwrapper container" <?php if ( $bkReviewSW != '1' ) { echo 'itemscope itemtype="http://schema.org/Article"'; } else { echo 'itemscope itemtype="http://schema.org/Review"'; } ?>>
                <?php if ($bkTitlePos == 'wide-title') {?>
                    <div class="singletop">
                        <?php echo gloria_core::bk_meta_cases('cat', $bkPostID);?>
                        <?php gloria_core::bk_breadcrumbs();?>
                    </div>
                    <div class="s_header_wraper">
                        <div class="s-post-header"><h1 <?php echo esc_attr($itemprop);?>> <?php echo get_the_title();?></h1>
                            <?php echo gloria_core::bk_get_post_meta(array('author', 'date', 'postview', 'postcomment'), $bkPostID);?>
                        </div>
                    </div><!-- end single header -->  
                <?php }?> 
        <?php }else if($bk_post_layout === 'fw-feat-img') {?>
                <?php
                    echo gloria_core::bk_get_feature_image_bg($bkPostID);
                ?>
                <div class="article-wrap bkwrapper container" <?php if ( $bkReviewSW != '1' ) { echo 'itemscope itemtype="http://schema.org/Article"'; } else { echo 'itemscope itemtype="http://schema.org/Review"'; } ?>>
                    <?php if ($bkTitlePos == 'wide-title') {?>
                        <div class="singletop">
                            <?php echo gloria_core::bk_meta_cases('cat', $bkPostID);?>
                            <?php gloria_core::bk_breadcrumbs();?>
                        </div>
                        <div class="s_header_wraper">
                            <div class="s-post-header"><h1 <?php echo esc_attr($itemprop);?>> <?php echo get_the_title();?></h1>
                                <?php echo gloria_core::bk_get_post_meta(array('author', 'date', 'postview', 'postcomment'), $bkPostID);?>
                            </div>
                        </div><!-- end single header -->  
                    <?php }?> 
        <?php }else {?>
            <div class="article-wrap bkwrapper container" <?php if ( $bkReviewSW != '1' ) { echo 'itemscope itemtype="http://schema.org/Article"'; } else { echo 'itemscope itemtype="http://schema.org/Review"'; } ?>>
                <?php if ($bkTitlePos == 'wide-title') {?>
                    <div class="singletop">
                        <?php echo gloria_core::bk_meta_cases('cat', $bkPostID);?>
                        <?php gloria_core::bk_breadcrumbs();?>
                    </div>
                    <div class="s_header_wraper">
                        <div class="s-post-header"><h1 <?php echo esc_attr($itemprop);?>> <?php echo get_the_title();?></h1>
                            <?php echo gloria_core::bk_get_post_meta(array('author', 'date', 'postview', 'postcomment'), $bkPostID);?>
                        </div>
                    </div><!-- end single header -->  
                <?php }?>  
        <?php }?>               
                <div class="article-content-wrap">
                    <div class="row bksection bk-in-single-page clearfix">
                        <div class="main <?php if($bk_has_sidebar != 1) {echo 'col-md-8';}else {echo 'col-md-12';}?>">
                            <?php if ($bkTitlePos != 'wide-title') {?>
                                <div class="singletop">
                                    <?php echo gloria_core::bk_meta_cases('cat', $bkPostID);?>
                                    <?php gloria_core::bk_breadcrumbs();?>
                                </div>
                                <div class="s_header_wraper">
                                    <div class="s-post-header"><h1 <?php echo esc_attr($itemprop);?>> <?php echo get_the_title();?></h1>
                                        <?php echo gloria_core::bk_get_post_meta(array('author', 'date', 'postview', 'postcomment'), $bkPostID);?>
                                    </div>
                                </div><!-- end single header -->  
                            <?php }?>  
                            <?php if ($share_box) {?>                           
                                <div class="bk-share-box-top"> 
                                    <span><?php esc_html_e('Share:', 'gloria');?></span>
                                    <?php echo gloria_core::bk_share_box_top($bkPostID, $social_share);?>
                                </div>                 
                            <?php }?>            
                            <?php
                                if($postFormat['format'] !== 'gallery') {
                                    echo gloria_core::bk_get_single_interface($bkPostID, $bk_post_layout);
                                }
                            ?>
                                                        
                            <div class="article-content <?php if ($bk_post_layout != 'bk-normal-feat') echo ('no-margin-top');?> clearfix" <?php if ( $bkReviewSW == '1' ) { echo 'itemprop="reviewBody"'; } else { echo 'itemprop="articleBody"'; } ?>>
    <!-- ARTICAL CONTENT -->
                                <?php if(isset($reviewPos) && ($reviewPos != 'below')) {?>
                                <?php echo gloria_core::bk_post_review_boxes($bkPostID, $reviewPos);?>
                                <?php }?>
                                <?php echo gloria_core::bk_single_content($bkPostID);?>
                                <?php if(isset($reviewPos) && ($reviewPos == 'below')) {?>
                                <?php echo gloria_core::bk_post_review_boxes($bkPostID, $reviewPos);?>
                                <?php }?>
                            </div><!-- end article content --> 
                        <?php wp_link_pages( array(
    							'before' => '<div class="post-page-links">',
    							'pagelink' => '<span>%</span>',
    							'after' => '</div>',
    						)
    					 ); 
                        ?>
    <!-- TAGS -->
                        <?php
                			$tags = get_the_tags();
                            if ($tags): 
                                echo gloria_core::bk_single_tags($tags);
                            endif; 
                        ?>
    <!-- NAV -->
                        <?php
                            if($postnav_sw) {   
                                $next_post = get_next_post();
                                $prev_post = get_previous_post();
                                if (!empty($prev_post) || !empty($next_post)): ?> 
                                    <?php echo gloria_core::bk_single_post_nav($next_post, $prev_post);?>
                                <?php endif; ?>
                            <?php }?>
    <!-- SHARE BOX -->
                        <?php if ($share_box) {?>                                                                    
                            <?php echo gloria_core::bk_share_box($bkPostID, $social_share);?>
                        <?php }?>
    <!-- AUTHOR BOX -->
                        <?php if ($authorbox_sw) {?>
                        <?php
                            $bk_author_id = $post->post_author;
                            echo gloria_core::bk_author_details($bk_author_id);
                        ?>
                        <?php }?>
                        <?php echo gloria_core::bk_get_article_info(get_the_ID());?>
    <!-- RELATED POST -->
                        <?php if ($related_sw){?>  
                            <div class="related-box">
                                <h3>
                                    <a id="s-related-article-tab" class="related-tab <?php echo ($bkPostID);?> active" href="#"><?php esc_html_e('Related articles', 'gloria');?></a>
                                    <a id="s-more-from-author" class="related-tab <?php echo ($bkPostID);?> " href="#"><?php esc_html_e('More from author', 'gloria');?></a>
                                </h3>
                                <?php $bk_related_num = 6; echo (gloria_core::bk_related_posts($bk_related_num));?>
                            </div>
                        <?php }?>
    <!-- COMMENT BOX -->
                        <?php if($comment_sw) {?>
                            <div class="comment-box clearfix">
                                <?php comments_template(); ?>
                            </div> <!-- End Comment Box -->
                        <?php }?>
                        <?php if ($stickyShareBox === 'on') {?>                                                                
                            <?php echo gloria_core::bk_sticky_share($bkPostID, $social_share);?>
                        <?php }?>
                        </div>
                        <?php if ($gloria_option['bk-recommend-box'] == 1) {?>
                            <?php get_template_part( 'library/bk_recommend_box');?>
                        <?php }?>
                        <!-- Sidebar -->
                         <?php if($bk_has_sidebar != 1) {?>
                            <div class="sidebar col-md-4">
                                <aside class="sidebar-wrap <?php if($gloria_option['single-stick-sidebar'] == 'enable') echo 'stick';?>" id="bk-single-sidebar">
                                    <?php
                                        $sidebar_option = '';
                                        $sidebar_option = get_post_meta($bkPostID,'bk_post_sb_select',true);
                                        
                                        if (($sidebar_option != '')&&($sidebar_option != 'global')) {
                                            $sidebar = $sidebar_option;
                                        }else if((isset($gloria_option['single-page-sidebar'])) && ($gloria_option['single-page-sidebar'] != '')){
                                            $sidebar = $gloria_option['single-page-sidebar'];
                                        }else {
                                            $sidebar = '';
                                        }
                                        
                                        if (strlen($sidebar)) {
                                            dynamic_sidebar($sidebar);
                                        }else {
                                            dynamic_sidebar('home_sidebar');
                                        }                                                   
                                    ?>
                                </aside>
                            </div>
                         <?php }?>
                    </div>
                </div>
            </div>
        </div>
    
	<?php endwhile; endif;?>
<?php get_footer(); ?>