<?php
/**
 * The main template file.
 */

get_header(); 
$gloria_option = gloria_core::bk_get_global_var('gloria_option');
?>

<div class="page-wrap bkwrapper container">
    <div class="row bksection">
        <section class="shop-page <?php if (isset($gloria_option['bk-shop-sidebar']) && ($gloria_option['bk-shop-sidebar'] == 'on')){ echo 'col-md-8 col-sm-12 three-cols';}else { echo 'col-sm-12 four-cols'; }?>">
        <?php $classes = array();?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php if ( ! is_singular( 'product' ) ) : ?>
                    <div class="page-title">
    				    <h2><span><?php woocommerce_page_title(); ?></span></h2>
                        <?php gloria_core::bk_breadcrumbs();?>
                    </div>
    			<?php endif; ?>
    
    			<?php woocommerce_content(); ?>    
            
        </article><!-- #post-<?php the_ID(); ?> -->
        
        </section>
        <?php
            if (isset($gloria_option['bk-shop-sidebar']) && ($gloria_option['bk-shop-sidebar'] == 'on')) {?>
                <div id="page-sidebar" class="sidebar col-md-4 col-sm-12">
        			<aside class="sidebar-wrap <?php if($gloria_option['shop-stick-sidebar'] == 'enable') echo 'stick';?>" id="bk-shop-sidebar">
                        <?php get_sidebar(); ?>
                    </aside>
        		</div>
        <?php }?>
    </div>
</div>
		
<?php get_footer(); ?>